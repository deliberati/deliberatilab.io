
## Jeffrey Conditioning

In Bayesian inference, new information is typically certain: when a Bayesian reasoner learns for example that 𝐵=1 they update their belief in 𝐴 using the simple Bayesian belief revision rule:

$$
    P_j(𝐴=1) = P_i(𝐴=1|𝐵=1)
$$

Where 𝑃ᵢ indicate prior beliefs and 𝑃ⱼ indicates posterior beliefs. Alternatively after learning 𝐵=0, the belief revision rule is:

$$
    P_j(𝐴=1) = P_i(𝐴=1|𝐵=0)
$$


However, in our current example, the new information is not that 𝐵 is false, but that 𝐵 is *probably* false. In this case, we need to use the more general  belief revision rule \eqref{eq:2} we derived above, which gives us a value somewhere between 𝑃ᵢ(𝐴=1\|𝐵=0) and 𝑃ᵢ(𝐴=1\|𝐵=1), depending on how certain we are of our evidence (𝑃ₕ(𝐵=1)).


##
The conditional independence assumption follows from the **fair deliberation assumption**, which says that there are no beliefs affecting the average juror's opinion that have not been argued during deliberation. Specifically in this case, it says that if the premise 𝐺 *was* directly relevant to 𝐴, it would also have been included in the argument graph in direct support for or opposition to 𝐴. We demonstrate this reasoning more rigorously in the appendix.




### Jeffrey Conditioning 

What we have just done is known as **Jeffrey Conditioning**. Jeffrey Conditioning is a Bayesian belief revision rule that is used in cases where the evidence is uncertain.

In Bayesian inference, new information is typically certain: when a Bayesian reasoner learns for example that 𝐵=1 they update their belief in 𝐴 using the familiar Bayesian belief revision rule:

$$
    P_j(𝐴=1) = P_i(𝐴=1|𝐵=1)
$$

Where 𝑃ᵢ indicate prior beliefs and 𝑃ⱼ indicates posterior beliefs. Likewise after learning 𝐵=0, the correct posterior belief is;

$$
    P_j(𝐴=1) = P_i(𝐴=1|𝐵=0)
$$


However, in our current example, the new information is not that 𝐵 is false, but that 𝐵 is *probably* false. In this case, we need to use the more general  belief revision rule \eqref{eq:1} we derived above, which gives us a value somewhere between 𝑃ᵢ(𝐴=1\|𝐵=0) and 𝑃ᵢ(𝐴=1\|𝐵=1), depending on how certain we are of our evidence (𝑃ᵢ(𝐵=1)).


### Conditional Independence Assumption


Jeffrey conditioning on 𝐵 is the [only rational way](https://plato.stanford.edu/entries/bayes-theorem/#4) to revise beliefs in 𝐴 response to a learning experience whose sole immediate effect is to alter the probability of 𝐵.

However, when the meta-reasoner participated in the second argument, they altered their belief not just about 𝐵, but also about 𝐺.

So to justify the use of Jeffrey Conditioning here, we need to assume that the fact of voting on 𝐺 has no immediate effect on acceptance of 𝐴. In other words, we need to assume that the meta-reasoner first forms their belief about 𝐵, and then use that to update their opinion about 𝐴, and the *reason* 𝐺 for accepting or rejecting 𝐵 does not further affect their judgment on guilt. 

This assumption can be expressed as the **Conditional Independence Assumption**:

$$
\begin{equation}
\begin{aligned}

    P_i(𝐴=1|𝐵=1, 𝐺≥0) = P_i(𝐴=1|𝐵=1)

\end{aligned}
\tag{3}\label{eq:3}
\end{equation}
$$

In the terminology of probability theory, this can be read as *acceptance of 𝐴 and votes on 𝐺 are conditionally independent, given acceptance of B*. 

\eqref{eq:3} means that we don't need to know what 𝑃ᵢ(𝐴=1\|𝐵=1,𝐺≥0) is in order to calculate 𝑃ₕ(𝐴). All we need is 𝑃ᵢ(𝐴=1\|𝐵=1) This is crucial because otherwise we would need jurors to vote jointly on 𝐴, 𝐵, and 𝐺, in order to obtain the joint probability distribution needed to compute 𝑃ₕ(𝐴). So the conditional independence assumption is critical to distributed reasoning. We will discuss this assumption and the justification behind it further in another article


### Warrant Arguments and Argumentation Theory

On the contrary, we cannot make the conditional independence assumption for the argument (𝐶) *the defendant retracted her confession*. This argument is made precisely because it is expected *not* to be considered independently of (𝐵=1) *the defendant signed a confession*. 

Although 𝐺 and 𝐶 are both counter-arguments to 𝐵, they are very different types of arguments. 𝐺 is a reason to reject the **premise** 𝐵, whereas 𝐶 is a reason to reject the conclusion 𝐴 despite 𝐵. Or in other words, it is a reason that accepting 𝐵 does not **warrant** acceptance of 𝐴. 

We call arguments such as 𝐺 **premise arguments**, and arguments such as 𝐶 **warrant arguments**. The idea of warrant and the distinction between these two types of arguments comes from the field of [argumentation theory](https://en.wikipedia.org/wiki/Argumentation_theory). For the rest of this paper we will make use of some concepts and terminology from this field. In a separate post on [The Argument Model](/argument-model), we define the following terms:

- [claim, premise, and warrant](/argument-model/#anatomy-of-an-argument)
- [argument types](/argument-model/#premise-arguments-and-warrant-arguments): supporting and opposing. Premise arguments and warrant arguments.
- [argument identifiers](/argument-model/#argument-identifiers)
- [argument threads](/argument-model/#argument-threads)


To see why the conditional independence assumption does not hold for warrant arguments, suppose 𝐵 is unanimously accepted, and is also convincing to jurors.

$$
    P(𝐴=1|𝐵=1) > P(𝐴=1)
$$

But suppose 𝐴<-𝐵<-𝐶 successfully opposes 𝐴<-𝐵, to the extent that jurors who accept both 𝐵 and 𝐶 **are no more or less likely to accept 𝐴**: 

$$
    P(𝐴=1|𝐵=1,𝐶) = P(𝐴=1)
$$

So 𝑃(𝐴=1\|𝐵=1,𝐶) ≠ 𝑃(𝐴=1\|𝐵=1), which means by the definition of conditional independence, 𝐴 is not conditionally independent of 𝐶 given 𝐵.

So while we can carve off subjuries to argue about the acceptance of 𝐵 and 𝐶, we need a main jury that has voted on all of 𝐴, 𝐵, 𝐶 in order to estimate 𝑃(𝐴=1\|𝐵=1,𝐶), which we need to calculate the justified opinion.

