---
layout: single
title:  "The Attention Game"
date:   2022-03-01 12:00:00 -0700
toc: true
toc_sticky: true
sidebar:
excerpt: "TODO excerpt "

---

## Walkthrough

So it looks like this. Suppose we have some Reddit-like forum. The algorithm can direct attention to a post by giving it more front-page imrpessions, or it can bury a post completely.

There are like and dislike buttons for each post. So each post effectively comes with a survey, where the question is "do you like this post?"

Our mechanism would give a payout to users when they vote. Assuming that the only input we have from users is this like/dislike response, and assuming users don't have "common priors", formulas such as the one described by [the multi-task peer prediction paper] would produce an equilibrium at truth-telling if users are rational agents trying to maximize their payout.

All the Bayesian truth-elicitation scoring formulas have been [shown to be] different ways of rewarding users for the information content of their responses. Random answers, always answering the same way, copying other accounts, and other non-informative answers will provide zero information and therefore zero or negative expected payout. On the other hand votes that correlate with the votes of other users is information that helps refine the system's prediction of how likely future users are to up-vote the same piece of content. Informative votes will have a positive expected payout. With some scoring formulas, the payout is actually an information-theoretical information measure denominated in bits.

We have said that attention -- measured as impressions -- is the currency of a social platform. More specifically, the payout should translate to ability to direct other users' attention, by causing their own posts, or posts that they like, to receive more **rank-normalized** impressions.

This can be done using a reputation system. Votes of users who have larger reputation have greater weight, giving users more power to determine which posts get more normalized impressions.

This can also be done using a virtual currency system. Users are paid in a currency that act as virtual advertising dollars. They can spend this currency to promote a post. This of course creates the same sorts of problems that ad dollars create now: people can pay to promote bad (false/misleading/etc) content. But they can only do this if they have *earned* currency -- or purchased it from someone else who has earned it. We can control the ratios here, ensure that the amount of **value** created in order to earn the currency exceeds by a large margin the value destroyed by sponsored content.














----



---





[TODO: shorten/cut, starting with an overview of how an equilibrium around objectively honest behavior is used to secure blockchains, touching on how auctions can be designed to induce people to reveal how much an object is really worth to them, and finally introducing how survey games such as the Bayesian Truth Serum can induce people to reveal arbitrary subjective beliefs and preferences.]

## Information Games

Getting people to reveal what they honestly think is true could go a long way towards dampening the spread of misinformation. But the problem is that people can be honest and wrong. So out algorithms also need to somehow promote more informed content, or content from experts, or something like that. Once again, the 



Online discussion can be seen as **arguments**, where comments are seen as "reasons" or "information" or "evidence" that people give to justify their upvotes or downvotes. An information-optimization algorithm 



## An Example

Suppose for example that in some social network, posts written in all caps are always shown at the top of everyone's feed. How will people behave?

Will all posts send up being written in all caps? Not necessarily. Some people might, out of principle, scroll past posts written in all caps. If enough people do this, all-caps posts will not be rewarded with extra attention, despite the algorithms promotion of these posts. If the platform also has upvotes or downvotes, the community might develop a norm of downvoting all-caps posts, for example.

Assuming anybody actually uses such a silly platform, there are many different "equilibria" the online community could settle into. Equilibria at 0% or 100% all-caps posts are both plausible. 

The point is not necessarily to create algorithms that **guarantee** an healthy equilibrium behavior. It is merely to create those where a healthy equilibrium is possible. Those who seek honest, intelligent, civilized discourse will flock to those communities.








sociotechnical engineering
