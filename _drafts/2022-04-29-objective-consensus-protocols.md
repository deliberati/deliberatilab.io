---
layout: single
title:  "Honesty Games: Subjective Consensus"
date:   2022-03-01 12:00:00 -0700
toc: true
toc_sticky: true
sidebar:
  - nav: "honesty-on-the-internet"
    title: "In This Series"
  - nav: "honesty-on-the-internet-related"
    title: "Related Articles"
excerpt: "TODO excerpt for subjective-consensus-protocols"

---

or

# How to Win a Keynesian Beauty Contest


This is the second post in my series on [Honesty on the Internet](/honesty-on-the-internet).

In 2014 I read a [blog post](https://blog.ethereum.org/2014/03/28/schellingcoin-a-minimal-trust-universal-data-feed/) by a young Russian-American named [Vitalik Buterin](https://en.wikipedia.org/wiki/Vitalik_Buterin) who proposed **a method for getting anonymous people on the internet to tell the truth.**

## A Lesson from Game Theory

This method does not involve psychological tricks or enhanced interrogation techniques. Rather it uses concepts from [game theory](https://en.wikipedia.org/wiki/Game_theory), a fascinating approach to modeling complex human behavior based on the assumption that we all behave **strategically**: we try to predict what other people are going to do, and adapt our own behavior accordingly. 


One of the most simple, basic results from game theory is that people may do something for no other reason than that other people are doing it, because sometimes you are better off doing what everyone else is doing. It is why for example why your watch is set to the same time as everyone around you, why you use the local currency and language, drive on the right correct side of the road etc.

The trick to getting you to tell the truth, then, is somehow to create a situation where everyone else is already telling the truth, and then reward you for saying the same thing as everyone else. And of course why is everyone else telling the truth in the first place? Because they are rewarded for saying the same thing as you...

## Blockchain Consensus Protocols


<figure>

<img src='/assets/images/honesty-on-the-internet/VitalikButerinProfile.png'
                 alt='Vitalik Buterin - regular Keynesian beauty contest winner'
                 style='max-height: 350px' />


  <figcaption>Vitalik Buterin</figcaption>
</figure>


<style>


figure {
    display: block; float: right;
    margin:  10px;
}

figure p {
    margin: 0px; padding:  0px;
}


</style>

It's not just a theory: this basic approach has been shown to work fantastically well. It is essentially how cryptocurrency works. A cryptocurrency is like a bank with nobody in charge: everybody gets to say how much cryptocurrency they have in their account. The trick is, they don't get the money unless everybody else agrees with them. As explained by Vitalik Buterin (co-creator of Ethereum, and one of the great thought leaders in the blockchain world):

> Everyone wants to provide the correct answer because everyone expects that everyone else will provide the correct answer and the protocol encourages everyone to provide what everyone else provides.
> 
> -- Vitalik Buterin, [SchellingCoin](https://blog.ethereum.org/2014/03/28/schellingcoin-a-minimal-trust-universal-data-feed/), 2014
{: .notice--info}


The set of rules for coming to an agreement about the balance of every account in the blockchain is called a [consensus protocol](https://decrypt.co/resources/consensus-protocols-what-are-they-guide-how-to-explainer). These protocols can be very complex, but they all are based on this same principle.

## Consensus Protocols for Factual Data

Buterin's blog post argued that these same protocols could be used for consensus on facts about the real world, and not just the balance of people's bank accounts.

Suppose for example we create an online poll that asks *What is the capital of France?* and promises participants some small but meaningful amount of money if their answer matches what the majority of other participants' answer.

What will people answer? Well, obviously *Paris*. Because...what else are they going to answer? Without any clue to indicate a different answer, the easiest thing is to provide the answer that you know everyone knows -- which in this case is the truth. 



If somebody wanted to pass off some other city as the capital of France, he would have to somehow get a majority of other people to coordinate on a different answer. This can be hard -- especially if participants are selected randomly from a large group of people who don't know each other. 

As Buterin put it:

> it’s easy to be consistent with many other people if you tell the truth but nearly impossible to coordinate on any specific lie.
> 
> -- Vitalik Buterin, [SchellingCoin](https://blog.ethereum.org/2014/03/28/schellingcoin-a-minimal-trust-universal-data-feed/), 2014
{: .notice--info}

<img src='/assets/images/honesty-on-the-internet/random-lie.png'
                 alt='Random Lie'
                 style='display: block; margin-left: auto; margin-right: auto; max-height: 300px' />


<aside class="custom-aside" markdown="1">

## Aside: The Curious Absence of Collusion on the Blockchain

Unfortunately, if somebody really is determined to promote a lie, it's easy to create a bunch of fake accounts all voting in the same way. This is called a [Sybil Attack](https://en.wikipedia.org/wiki/Sybil_attack). Protocols can defend against Sybil attacks various clever ways, but there is no way to absolutely prevent them.

This possibility used to keep blockchain people up at night. But over the years, serious attacks never materialized. There were lots of hacks, heists, scams, and fraud, but nobody messed with the consensus protocol. 

Eventually, the community came to recognize a phenomenon called [Social Consensus](https://twitter.com/vitalikbuterin/status/945126429473234944). They realized that if anyone controlled enough of the votes on a blockchain to force an incorrect answer and defraud other users of money, the other users would just make a copy ([hard fork](https://en.wikipedia.org/wiki/Fork_(blockchain)#Hard_fork)) of the blockchain, and kick the fraudsters out. Then there would be two chains, but only one would be seen by society as legitimate.

As Buterin put it:

> On the one hand, you have a fork where the larger share of the internal currency is controlled by truth-tellers. On the other hand, you have a fork where the larger share is controlled by liars. Well, guess which of the two currencies has a higher price on the market…
> 
> -- Vitalik Buterin, [The Subjectivity / Exploitability Tradeoff](https://blog.ethereum.org/2015/02/14/subjectivity-exploitability-tradeoff/), 2015
{: .notice--info}

<img src='/assets/images/honesty-on-the-internet/truth-lies-chart.png'
                 alt='Truth vs. Lies'
                 style='display: block; margin-left: auto; margin-right: auto; max-height: 200px' />


<!--
todo: ethereum classic fork
-->

In reality, the mere *possibility* of a hard fork is enough that a rational actor cannot expect to benefit from corrupting a Consensus protocol. 

</aside>



<style>
.custom-aside
{
  margin: auto;
  background-color: lightgrey;
  border: 1px solid black;
  max-width: 600px;
  padding-top: 1em;
  padding-bottom: 0px;
  padding-left: 1em;
  padding-right: 1em;
  margin-bottom:  1em;
}

aside h3 {
    margin-top: 0px;
}

</style>

## Weakly-Subjective Facts

But consensus protocols of course only work if there is actually a consensus on truth: if people know the truth...and agree on what it is...and expect other people to know the truth...and for that matter, even understand the question. Everybody knows the capital of France, but what about questions such as *Are badgers philosophical?* (ambiguous), *Did the president commit an impeachable offense?* (controversial), or *What is inside area 51?* (not common knowledge)?

Consensus protocols work in blockchain because they deal with uncontroversial, **objective** facts. Beauty is in the eye of the beholder, but an account balance in a blockchain is not a matter of opinion: it is determined by uncontroversial, unambiguous, verifiable rules. So when asked whether a certain account balance is correct, how do people expect others to answer? With the simple truth.

The same goes for **[weakly subjective](https://blog.ethereum.org/2014/11/25/proof-stake-learned-love-weak-subjectivity/)** facts such as stock prices, weather data, or the score of a football game. These are still subjective in the sense that you cannot algorithmically verify they are true: at some point, some human beings took the step to take some real-world data and put it on the blockchain but you didn't watch them do it. But these simple, publicly verifiable facts are *almost* totally objective in that there is practically no room for difference of opinion. The method works as long as people are pretty sure the majority of other people will vote in a certain way.

We might call these **weakly-subjective consensus** protocols. These protocols are behind blockchain [oracles](https://chain.link/education/blockchain-oracles): sources of facts used to power financial contracts. Other people have written about oracles and subjective consensus on the blockchain include [Jose Garay from Witnet](https://medium.com/witnet/on-oracles-and-schelling-points-2a1807c29b73), and [Michael B. Abramowicz from GWU](https://scholarship.law.gwu.edu/cgi/viewcontent.cgi?article=2673&context=faculty_publications), and [Paul Sztorc](https://www.truthcoin.info/papers/truthcoin-whitepaper.pdf).



## Highly-Subjective Beliefs

But once we go beyond weak subjectivity -- once "truth" becomes controversial, ambiguous, or personal in any way -- this method breaks down and can even become perverse. Truth gives way to conformity. Reason is pushed aside for popularity. It fuels echo chambers and cult dynamics (in another essay I attribute many of the ills of society to this phenomenon).

For this reason, the blockchain community still struggles with consensus protocols for highly subjective data (todo: token-Curated Registrie, Decentralized Courts (aragon, Kleros).. But I think this is because they have not picked up on the advances in the field of game theory.

INtroduce: Subjective Consensus Protocols


