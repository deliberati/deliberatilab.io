---
layout: single
title:  "The Honest and Informed Opinion"
date:   2022-04-25 12:00:00 -0700
toc: true
toc_sticky: true
sidebar:
  - nav: "honesty-on-the-internet"
    title: "In This Series"
  - nav: "honesty-on-the-internet-related"
    title: "Related Articles"
excerpt: "TODO excerpt for subjective-consensus-protocols"

---

or 

## How to Get People on the Internet to Tell the Truth


## Engagement-Driven Feedback Loops

A commonly accepted explanation for how engagement-driven social media algorithms promote misinformation is this: when somebody posts an outrageous lie in a social platform, it gets people commenting and arguing. The algorithms interprets this interaction as **engagement**, and since engagement is exactly what these algorithms are trying to produce, they **promote** that engaging content by putting it at the top of our feeds and our mentions. This drives further attention to the engaging lie, rewarding the person who produced it. That person in notices that outrageous lies are being rewarded by the algorithm, and learns to produces more. This dynamic extends to the entire media ecosystem, so that even respectable mainstream news outlets are trained to produce headlines that attract engagement by invoking fear, fear, and outrage, and conflict.

This means that there is an **unnatural** amplification of engaging content. This can be a good thing if the content is engaging for a good reason. But unfortunately it means that outrageous lies, and other toxic but engaging content, is also unnaturally amplified.

So if an unopinionated algorithm can (inadvertently) amplify misinformation, how can it amplify accurate information?

## The Honest and Informed Opinion

Our solution has two parts. One addresses the problem of **dishonesty**. The other address the problem of **ignorance**. The combination is an algorithm that amplifies content that people would **honestly endorse if they all shared the same information**.

### The Honest Opinion

The possibility of a feedback loop that induces **honesty** comes from the field of Game Theory, where researchers have discovered how to take advantage of the inevitable statistical correlations between people's private preferences and beliefs. They have designed **survey games** where participants are paid for their answers using a formula that makes truth-telling the optimal strategy, even if it is impossible to verify if respondents are telling the truth. In **honesty games** we explain how this works. [todo: intuitive, non-technical]

In [Attention Games], we defend the application of Game Theory to the design of social algorithms, arguing that classic measures of **attention** act as a currency in the attention economy, and that participants in a social platform will behave **as if** they are rational agents seeking to maximize attention. Thus by paying users in the currency of attention, according to the formulas derived by game theorists, we can induce a feedback loop where participants will only post or upvote content they honestly like, approve, or believe to be true.

### The Informed Opinion

[TODO: the most convincing content]


The idea of processes that invoke more **informed** opinions has a long history that has given us techniques such as Deliberative Polling and the Delphi Method. In [The Deliberative Poll] we describe a simple process for promoting productive deliberation and identifying the most informed opinion, that can be integrated into social platforms with minimal changes to the UI.

In [Distributed Bayesian Reasoning] we describe a way of evaluating large-scale internet discussions where participants have different information, to discover a hypothetical fully-informed opinion, in order to identify content that the average participant would (honestly) endorse if everyone shared the same information.


## The Marketplace of Ideas

> If we do not have the capacity to distinguish what’s true from what’s false, then by definition the marketplace of ideas doesn’t work. And by definition our democracy doesn’t work."
> 
> -- Barack Obama
{: .notice--info}

Today, the marketplace of ideas has largely moved online. But it is not working well. Democratic societies are in critical need of an online platform that functions as a healthy marketplace of ideas. These are some ideas about how that could work.




---- Or as Walter Lippman wrote a century early, "There can be no liberty for a community which lacks the means by which to detect lies."



------------------




There are many different types of feedback loops that an algorithm can induce, just by promoting content based on different metrics. An algorithm that only maximized likes instead of engagement, for example, produces less controversy, but reinforces bubbles and other types of toxicity.

But it is actually possible to produce a feedback loop that induces people to produce content that they **honestly think is true**. 

I expect many readers to think that this is clearly, fundamentally impossible. As we have mentioned, many people are just liars, sometimes engaged in large-scale deliberate manipulation. Yet in these articles I will show not only how this is possible, but explain where it has been successfully implemented with remarkable success.

It is also possible for the algorithms to identify content that carries the most **information content**, and then by promoting this content, inducing feedback loops that amplify content that is the most **informed** peopled believe.

Further, using a technique called [Distributed Bayesian Reasoning], it is possible to identify content that the most people **would believe if they all shared the same information**.

These techniques make an extremely powerful combination: a socail algorithm that amplifies truth by amplifying content that people would **honestly endorse if they all shared the same information**.


## Next in this Series

In this series of articles, I will explain these algorithms, and show examples of where they have been successfully implemented, though have not yet been implemented in a large scale social network.

Understanding these algorithms requires understanding a lot of theory. My goal with this articles is explain these theories to a non-technical audience -- to create the intuitive understanding necessary to convince the reader that these algorithms can work.

- In [Attention Games](/attention-game), I argue that social networks can be modeled as economic systems where **attention** plays the role of money. This means many economic theories can be applied to social networks, which means we can design social networks using some very powerful results from Game Theory. 

- In [Honesty Games](/subjective-consensus-protocols), I explain how Game Theory can be used to design, or at least explain, systems where people behave **honestly** even when there are rewards for cheating.

- In [Argument Games], I describe how social algorithms can turn argumentation into a game, and how to make the rules of the game favor the most informed content






consensus protocols that have been extremely successful in feeding truthful information onto the blockchain, how these protocols are limited to objective or weakly-subjective facts, and how advances mechanisms can be design to elicit truthful feedback for even highly subjective questions where the answers cannot be verified.

- In [Bayesian Truth Serums, Peer Prediction, and Other Mechanisms](/bayesian-truth-serum-and-other-mechanisms), I introduce more advanced mechanisms from game-theory for eliciting truthful feedback even when for subjective, personal questions, and even when participants believe their opinion is in the minority.

- In [The Argument Game]


In Part IV, I propose that the difference between the Keynesian Beauty Contest, which promotes conformity, and Peer-Prediction family of mechanisms, which promotes honesty, makes all the difference.


## Truth-Elicitation Mechanisms

The solution lies at somewhere around the intersection of psychology and mathematics. In this series of posts about Honesty on the Internet I will introduce a lot of theory, mostly game theory and Bayesian statistics. But since I want to reach more than just academics or engineers, I will try to interpret and synthesize the research and make the concepts as accessible, intuitive, and math-free as possible. 

## Theory

Many readers may doubt the usefulness of "theory", especially since many of these theories treat people as "rational agents," which any street-smart cynic will assure you is not what people are. But years ago there was a theory that you could fill a metal tube with explosives, light it, and ride it up to the moon. The Apollo program started by asking the same question that I ask here: if we are going to do this difficult but important thing, how, in theory, can it be done? 

And I am going to explain how eliciting honest behavior online is not only **theoretically** possible, but show places where some of this technology has actually been implemented with **fantastic success**. 

But these technologies not been applied at scale in the places where humankind needs them the most: our social networks and online forums where much of public discourse takes place.

## Honesty vs. Truth

What does it mean to "tell the truth?" Colloquially it means **to say what one believes to be true**: to be honest. But this is not the same as speaking truth; **people can be honest and wrong.** 

So **dishonesty** is only one part of the problem. The other part is **ignorance**. An algorithm that gives truth the advantage must therefore tend to amplify content that is **honest** and **informed**. 

Deliberati's proposed overall solution solves both problems, with an algorithm that amplifies content that **that the most informed members of an online community would honestly endorse if they they all shared the same information.**

Our series on [distributed bayesian reasoning] focuses on the problem of information. This series focuses on the  problem of honesty. 


## Next in this Series

- In [The Honesty Game](/subjective-consensus-protocols), I explain how consensus protocols that have been extremely successful in feeding truthful information onto the blockchain, how these protocols are limited to objective or weakly-subjective facts, and how advances mechanisms can be design to elicit truthful feedback for even highly subjective questions where the answers cannot be verified.

- In [Bayesian Truth Serums, Peer Prediction, and Other Mechanisms](/bayesian-truth-serum-and-other-mechanisms), I introduce more advanced mechanisms from game-theory for eliciting truthful feedback even when for subjective, personal questions, and even when participants believe their opinion is in the minority.

In Part IV, I propose that the difference between the Keynesian Beauty Contest, which promotes conformity, and Peer-Prediction family of mechanisms, which promotes honesty, makes all the difference.

Finally: Attention and Reputation


- In Other Parts:
    - Consensus Protocols and the Keyenesian Beauty Contest. These have had practical success...
    - Truth-Elicication Mechanisms Peer Prediction. ..even for subjective questions, such as your favorite flavor of ice-cream. Obviously, an algorithm can't read you mind, yet these clever mechanisms get you to reveal this nontheless.
    - What would Larry Do.
    - Subjective Truth and Echo Chambers
    - Information Theory
    - Game Theory...Common Knowledge...
