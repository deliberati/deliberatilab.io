---
layout: single
title:  "The Warrant"
toc: true
toc_sticky: true
sidebar:
  - title: "In This Series"
    nav: "bayesian-argumentation"
  - nav: "bayesian-argumentation-related"
    title: "Related Articles"

---

## In Search of the Warrant

In our [introduction to Bayesian Argumentation], we introduced the concept of the **warrant** as a a **claim** that justifies the inference from premise to conclusion. And we said that as long as the premise is relevant to the conclusion, a warrant must exist.

We also defined the idea of corelevant claim: some third claim where the premise is more relevant to the conclusion if the subject accepts that third claim than if they do not. 

Clearly the warrant is some corelevant claim. The question is, which one? There may many corelevant claims. For example, suppose the claim that (𝐷) the battery is working is corelevant with (𝐺) *the car has gas* to the conclusion (𝐶) *the car will*. So 𝐷 is clearly a candidate for the warrant justifying the relevance of 𝐺 to 𝐶.

But there may be many other corelevant premises: the belief that the car has a working engine, etc. So perhaps the warrant should be something that encompasses all of these -- something along the lines of **the car is in perfect mechanical working order**. But the subject must also believe that a car in perfect mechanical working order will start if it has gas. So the warrant really boils down to **the car will start iff it is not out of gas**. Or logically **𝐶 ⟺ 𝐺̅**. In other words, maybe the warrant is the second premise in a logical syllogism?

But this can't be true, because Bayesian reasoners don't think in logical syllogisms. Sometimes the premise is only a little relevant -- accepting it only increases the probability of accepting the conclusion a small amount. 

For example, if **Tom Cruise is in Top Gun II** supports the conclusion **Top Gun II is a good movie**, the warrant is probably something like **movies with Tom Cruise tend to be good**, and not **Top Gun II is good if and only if Tom Cruise is in it**.



Now recall that a **necessary** premise is a **relevant premise that the subject accepts to some degree**. These definitions are almost identical. Indeed we can say that the warrant is a  premise that it is **necessary** for the subject to accept for the premise to be relevant to the conclusion. And indeed we can quantify how necessary any corelevant premise is to the relevance of the premise by considering how much less relevant the premise would be if the subject did not accept the warrant.




[]
We said that a warrant **exists** if the probability of the conclusion is greater given the premise than not. We also said warrant is a claim (or in Bayesian terminology, a proposition). But what exactly is this claim?

Is it the claim that $$P(A \vert B) > P(A \vert notB)$$? No: this claim is not something the **subject** believes. It is **our** description of the subject's beliefs. So it can't be the warrant because it is not a claim that justifies, in the subject's mind, the inferential leap from premise to conclusion.

]

TODO: example warrant

[
    For example, the claim *any candidate with a pulse is good* would be very corelevant with *the candidate has a pulse*. But this cannot be the warrant, because the subject probably doesn't believe this. If the boss passes by and comment "We're desparate! Hire anybody with a pulse!", then the argument "well this guy has a pulse" suddenly becomes



]


### Definition of Necessity to Relevance

We can define the **necessity of C to the relevance of B to A as**

    Rd(A,B) - Rd(A,B|C)

### Definition of Warrant

If there are multiple claims that are necessary to the relevance of the premise, which one is the warrant? It seems reasonable that the claim that most justifies the inference from premise to conclusion would be the claim with the highest necessity to relevance.

So 


If you believed the car did not have a working battery, then you would also believe it would not be in mechanical working order, and that it would not start.



In all the possible outcomes where a working battery makes having gas relevant, working mechanical order also does so.



\bar{B} implies \bar{M}
P(C|G,B) < P(C|G,M)

Rd(C,G|B) < Rd(C,G|M)

P(C|G,B) - P(C|notG,B) < P(C|G,M) - P(C|notG,M)
because P(C|notG,*) = 0, and P(C|G,B) < P(C|G,M)

P(B) > P(M)

NR(C,G,B) < NR(C,G,M)
Crd(C,G,B)*P(B) < Crd(C,G,B)*P(M)





But this clearly is not the warrant. First, the subject probably doesn't believe this.


But this doesn't work:
    Tim liked the move...



Now, a warrant according to Toulmin is a **rule** of inference: if the subject accepts that rule, then they will also be more likely to accept the conclusion if they accept the premise.


Taking an example from Toulmin's *The Uses of Argument*, suppose (𝐵) *Petersen is a Swede* is argued in support of (𝐴) *Petersen is not a Roman Catholic*. The unexpressed warrant, according to Toulmin, is a rule that can roughly be expressed as (𝐶) *A Swede can be taken almost certainly not to be a Roman Catholic*. 

So if the subject accepts the claim 𝐶, then they should also be more likely to accepts 𝐴 if they accept 𝐵.










Let's represent this situation with some data. Suppose the beliefs of the subject are represented by the following probability distribution table. 

|  C |  A |  B |  Pr
| ---|----|----|-----
| 0  | 0  | 0  | 7.2%
| 0  | 0  | 1  | 64.8%
| 0  | 1  | 0  | 0.8%
| 0  | 1  | 1  | 7.2%
| 1  | 0  | 0  | 1.8%
| 1  | 0  | 1  | 3.6%
| 1  | 1  | 0  | 0.2%
| 1  | 1  | 1  | 14.4%

From this we can calculate the following conditional probabilities:

$$
\begin{aligned}
    &P(A|\bar{B})    = P(A|\bar{B},C) = P(A|B,\bar{C}) = P(A|\bar{B},\bar{C}) = 10\%\\
    &P(A|B,C)        = 80\%
\end{aligned}
$$

So acceptance of either 𝐶 or 𝐵 alone has no effect on the probability of accepting 𝐴. But acceptance of 𝐶 and 𝐵 together increases the probability from 10% to 80%.

This means that 𝐵 is relevant to A only if the subject accepts the rule of inference 𝐶. In other words, B and C are corelevant to A.

What's more, the relevance of B to A depends entirely on acceptance of C. 



## Conclusion

So we can look at argument as the **exchange of information among Bayesian reasoners**, except the information is merely the fact that one of them asserted some premise. But these **assertions can influence belief** under certain conditions.

First, the assertion must be **informative**. The assertion itself must effectively be new information that causes the subject to change their belief in the premise (even if the assertion induces them to change their belief by actively seeking new information, fixing their reasoning, etc.). 

Second, the premise must be **relevant** to the conclusion, which means the belief in the conclusion has a linear relationship with belief in the premise, as illustrated in Chart 1. When this is the case, the agent will necessarily revise their beliefs in the conclusion according to Jeffrey's Rule \eqref{eq:jeffreys}. The argument can then said to be **persuasive**.

However, if the argument was not informative, it is not necessarily a bad argument. The subject may already believe in the premise, and it may still be a **necessary** argument, because the premise forms the basis for the subject's belief in the conclusion in that the subject **would** change their belief in the conclusion if they were for any reason to reject the premise.

However, this account of argumentation still doesn't account for arguments such as *the car won't start because it is out of gas*, in the case that the battery is also dead. This is clearly a good argument, and yet by this account it is not relevant and therefore not persuasive, because changing belief in the premise would have little or not effect on belief in the conclusion. 

So clearly this account of argumentation is still limited. And yet it provides at least some intuition on what might be going on in the minds of people engaged in argument when they perceive arguments as relevant, persuasive, necessary, etc. This idea of a Bayesian Reasoner who changes their belief through argument is the basis for the idea of the [Meta Reasoner](/the-meta-reasoner) and the logic of [Distributed Bayesian Reasoning](/distributed-bayesian-reasoning-introduction).





## Summary of Identities

- Relevant + Accepted ⟺ Necessary: $$Rd(A,B) > 0 ∧ P(B) > 0 ⟺ P(A) > P(A \vert \bar{B})$$
- Relevance × Acceptance = Necessity: $$Rd(A,B)P(B) = P(A) - P(A \vert \bar{B})$$
- Relevant + Rejected ⟺ Sufficient: $$Rd(A,B) > 0 ∧ P(B) > 0 ⟺ P(A \vert B) > P(A)$$
- $$Nd(A,B) = Sd(\bar{A},\bar{B})$$
- Relevant + Informative ⟹ Persuasive: $$Rd(A,B) > 0 ∧ P_i(B) > P(B) ⟹ P_i(A) > P(A)$$
- Relevance × Informativeness = Persuasiveness: $$Rd(A,B)P(B) = P(A) - P(A \vert \bar{B})
- $$Rd(A,B) = -Rd(A,\bar{B}) = -Rd(\bar{A},B) = Rd(\bar{A},\bar{B})








