---
layout: tag
classes: wide
entries_layout: grid
taxonomy: community-moderation
title:  "Community Moderation"
date:   2020-11-08 13:40:43 -0700
header:
    teaser: /assets/images/wylly-suhendra-Swk4G_xi_uM-unsplash-wide-1920.jpg
    image: /assets/images/wylly-suhendra-Swk4G_xi_uM-unsplash-wide-1920.jpg
    caption: "photo credit: [**Unsplash**](https://unsplash.com)"
---
There are online communities out there whose members have agreed to high standards of civility and quality, and that have succeeded in upholding these standards with the help of community moderation tools.

But in social networks today, there are no moderation tools, because there are no community standards to uphold. Because there is no community.

We propose a new paradigm that defines a social network not as a network of interconnected individuals, but as a community or network of communities, whose members have agreed to basic standards of civility, decency, and tolerance for a diversity of opinion. This community can use the same basic methods that have been proven to work in online forums to defend itself against abuse, allowing users of the community members to anonymously flag and help moderate content -- using a fair, transparent, democratic process, not censorship.

