
## Honest Equilibria

And why would they expect other people to tell the truth? Because they expect everyone else to tell the truth! 

A situation where each person is acting rationally, given the way everyone else is acting, is called an **equilibrium** in game theory. 
TODO: we already mentioned coordination games.

Once a group arrives at an equilibrium, it tends to be very stable, because to move to another equilibrium a large enough number of participants must coordinate the move together. This is often a very difficult collective action problem.

But you might ask, how did the group ever arrive at an "honest" equilibrium in the first place?

Actually, there are many possible honest equilibrium. What we'd like is an equilibrium on subjective honesty: honestly reporting your own beliefs and preferences. But there is also an equilibrium on honestly reporting the opposite of what you believe. Or what you think the majority believes. Or always giving the same answer. And of course equilibrium around honestly reporting the common-knowledge **correct** answer (like a blockchain equilibrium).

Which equilibrium a group will arrive at is hard to predict: it depends among other things on initial mutual expectations.

So how can we guarantee that people will land on an equilibrium of subjective honesty? 

We don't have to. Blockchain consensus protocols also have the problem of multiple possible equilibria, and yet, in all successful blockchain protocols, people end up coordinating on a honest equilibrium. Why? Because projects that failed to do so were not successful.

We can only assume, or at least hope, that online community known to be honest would be more successful than one known to be dishonest.
