---
layout: single
title:  "What would Larry DDo"
date:   2022-03-01 13:40:43 -0700
toc: true
toc_sticky: true
sidebar:
  - nav: "deliberative-poll-related"
    title: "Related Articles"

---

TODO: Egregore

### The Problem with Keyensian Beauty Contests

If the reader will allow me to digress a moment, I believe this explains many of the ills of society. Keyensian Beauty Contests are omnipresent. They not only govern a great deal of economic activity; they determine the evolution and localization of language, religious beliefs and taboos, cultural norms, and political beliefs. 

Most importantly, **professed belief** in social contexts is often a Keyensian beauty contest where professing beliefs that conforms to some perceived majority view is often critical for obtaining the approval of and acceptance of society and avoiding [social isolation](/the spiral of silence). This is especially case for hot-button political, moral, or religious issues, especially in situations of ideological oppression where expressing or even hinting the wrong opinion can be ruinous and even fatal.

TODO: Schelling MOdels.

But since in a Keyensian Beauty Contest, people can only coordinate on X when X is common knowledge, people's professed beliefs become limited to a rigid, known set of common knowledge rules for correct thought: dogmas, memes, etc. For any given question, people seek the "closest" rule as a Schelling point to coordinate on. 

For example, during oppressive times, the rule is often of the form "those who are accused of Y are guilty". This is not a rational rule, nor necessarily one that anyone honestly believes, but it is an unambiguous and powerful Schelling point. If expectations have converged around this Schelling point, then it doesn't matter if most people believe the accused is innocent; because they cannot coordinate on expressing this belief! You don't know if other people will also think the accused is innocent, and even if they did, you don't expect they will say so. You would need some other Schelling point to coordinate on that is stronger than "those who are accused of Y are guilty", such as for example "party leaders are always innocent." 

In healthy societies, there are rational memes such as "people are innocent unless there is evidence of their guilt." But in unhealthy societies, people **fail to coordinate on rational memes**. Once there is enough precedent around condemning people who are accused of Y, regardless of evidence, then society is stuck in an unhealthy equilibrium. The fact that the accused seems innocent to me and other rational people are likely to think the same is simply not a strong enough Schelling point if there is already a long history of precedent around "those who are accused of Y are guilty" regardless of evidence. And the cost of expressing an unpopular opinion can be too high -- especially if society has also coordinated around the belief that "those who defend people who are accused of Y are guilty of Y themselves". 



## On the DNA of a community


Let's say we have a group of trusted core users. Using the peer-prediction method, the last voter's reference voter (the one that determines their score) is a member of the trusted core. This means that the last voter, and thus all previous voters, want to vote the same as members of the trusted core.

### What Would Larry Do?

Let's say initially the trusted core is just one member, Larry, who votes on every item (he either likes/approves/upvotes it or not).

So to maximize their reputation, users needed to be good at predicting how Larry would vote. Over time, after observing countless votes by Larry, their predictions have become better and better. There understanding of Larry has become more and more sophisticated and nuanced. They knew Larry tries to keep conversations limited to the topic of the forum, but tolerates  off-topic jokes or funny videos if they really make him laugh. They know that Larry tolerates foul and offensive language, but never malice or personal attacks. They know Larry like cats. In short, the community members know Larry inside and out. Their votes agree with Larry's votes most of the time. The cross-entropy between the average voter and Larry is low.

But then Larry starts to reduce his involvement in the community. For some randomly selected items, the last vote in the chain is not always Larry's. But there's always a chance that it will be. This small possibility is sufficient -- it's easy to show that people still maximize their expected score by voting the way they expect Larry to vote. There is still an equilibrium around trying to vote the same way as Larry. TODO: key subtlety here...equilibrium around voting for what you **honesetly think Larry would vote for**.

Gradually, Larry's involvement dwindles to zero. One day, people learn that Larry is dead.

How will the community vote going forward?

This is a classic coordination problem. The community is in equilibrium at universally voting their honest answer to the question "what would Larry do"? They believe that everybody else in the community knows Larry too. The answer to "what would Larry do?" is common knowledge, with little ambiguity.

So it will be very hard for a group of people to coordinate on some new equilibrium. If you don't vote the way Larry would have voted, how do you vote? What is your basis for deciding to vote any other way?

A single member that tries to unilaterally rebel against the ghost of Larry will only cause damage their own reputation. To succeed, they would need to get a majority of other users to join the rebellion, and coordinate on a different standard on which to base their votes. Coordinating on a new equilibrium is hard to do (but not impossible, as I will discuss below). Until they choose to do this, the community will remain in Larry-equilibrium.

Larry is dead, but essence of Larry -- his DNA, his spirit -- continues to exist and have effect on the real world, living on in the minds of the community members, in the form of a mental "model" of Larry. This model estimates how Larry would react to any bit of content. I don't think there is really any theoretical limit as to how accurate this model can become, after being trained over the years on an arbitrarily large number of diverse bits of content. It could be that the average model of Larry-the-moderator converges, so to speak, such that the probability that the decisions of the community differ from the decision of Larry is arbitrarily small. Whether Larry is modeled by artificial or real Neural networks, a complex, aggregate, distributed model of Larry continues to exist. Larry is dead, but a part of him is immortal.

I think, as an aside, that when people speak of some influential personage as "living on" after they have passed, it may sometimes be basically this same phenomenon. That person's way of seeing the world, and the shared understanding people in that person's sphere of influence have formed about their way of seeing the world, continues to act as a Schelling Point around which people coordinate. The saying "What would Jesus do" may be a real example of this. If Christians do as they think Jesus would do, they are most likely to enjoy the approbation of their community. Whoever the historical Jesus was, even if he was in fact a mere mortal, a part of him lives on in the Christian culture to this day.


TODO: Larry does not need to be "fully specified". There does not have to be a precedent. can have a nuanced, complex view of Larry. 

## On Conformity and Extremism

THe method we have described here encourages people to **Honestly report whether they believe the core will approve.** Does this simply reinforce conformity? Does it reinforce filter bubbles and Echo Chambers? It would seem so...it certainly rewards conformity to your understanding of the thinking of average member of the core -- or whatever norms and standards the core is trying to uphold. 

But this is not the same as the Keyensian beauty contest, which encourages a herd movement towards extremes. KBC encourages you to honestly report what you believe the **majority of other users will report**, not what you believe. So the vote tally will not reflect average belief. In situations where the average belief is common-knowledge -- for example everyone knows that 75% of people actually agree with something -- 100% of users will upvote that thing. This creates a distortion. If we conflate a vote in a Keynsian Beauty Contest with an opinion, we will think there is more agreement than there us.

This is one reason I think Keynsian Beauty Contests tends towards extremism. 

Developing this line of thinking further. What happens when belief is a Keyensian Beauty Contest is that people coordinate on simplified, dummed down mental models of "the core." The core, of course, is the people whose judgment matters to you. It may be friends, family, or co-workers. It may be a powerful but vocal minority, or an ossified reactionary majority. They may be liberal or conservative, depending on the circles you move in. They might be different people in different contexts, causing you to express different types of beliefs depending on who you are worth.

The core is "they". It is "people". They say that's not true. People will talk.

The mental model you form about the core is critical. But unfortunately, this virtual "they" is not very intelligent. It is irrational, inflexible, dogmatic, ignorant. It is "the masses." It is the least common denominator of intelligent thought.

It is the nature of the Keyensian beauty contest that people end up coordinating on a set of simplistic, dogmatic rules instead of sophisticated reasoning. 

"always approve/disapprove of Y". Why? The core might have a more nuanced view. Suppose generally members of the core may or may not agree with Y, depending on Z. But let's say in 10 instances, Z was always the case, so the core always agreed with Y. What is the mental model that was developed of the core? If we are using a Keynsian Beauty Contest, it will be that the core always agrees with Y. Some people might understand that the core believes Y for a reason. They may have some some mental model of members of the core as intelligent human beings with nuanced understanding and complex reasoning processes. But they don't know that other people have the same mental model. There is no common knowledge of any particular way of reasoning other than "always agree with Y". Lacking any basis for coordinating on any particular more sophisticated model of the thinking of core members, people end up with one Schelling point: "always agree with Y". 

There may successful coordination around a certain type of "reasonableness". The core has been pretty reasonable in the past, so they are likely to be reasonable in the future. And i think that "upvote Y if and only if Z" is pretty reasonable. I believe that in general, if I think its reasonable, other people might too. But "people have always voted Y" is a very clear Schelling point. "Y is reasonable only if Z" is not such a clear Schelling point. The safer thing is to expect other people to expect other people to expect Y. After enough instances of near universal agreement on Y, the community reaches a state of "convergence of expectations". Y is no longer just a Schelling point: there is significant *precedent* established around Y. We can say that an equilibrium has been established around upvoting Y.

In all cases where clear precedence exists, the results of a KBC will tend to be a binary outcome, with 100% voting yes or no.

I think this may be part of why online communities to drift towards dogma. It is much easier to coordinate around simplistic but clear rules than nuanced, sophisticated thinking. Even communities that do coordinate around more sophisticated thoughts -- such as the scientific communities coordination around the scientific method -- do end up coordinating on certain points dogmas as well.
[TOOD: introduce the idea that belief expression in offline communities is a coordination game]

But I think (peer prediction, BTS) solve this problem. It unlocks the possibility of judgments based on nuance. It can make it safe to honestly vote for what you think is reasonable, for no other reason than that you think it is reasonable, and your opinion about what is reasonable is likely to be statistically correlated with others. It makes it so that instead of applying a simplistic, inflexible set of rules with clear precedent, you can apply all the resources of your experience and intelligence, all the nuances of your judgment, confident that other intelligent people's nuanced judgment will tend to correlate with yours.


With peer prediction, in this particular scenario, there may not be the necessary "common prior." The center, being an automated system, does not understand what Y and Z mean. So it would start with only a weak prior. But the users who understand the content would know that Y is a strong Schelling point. So they will believe the center under-estimates the percentage of people that will agree, which means everybody's best move is to agree. However, as I will discuss below, I think peer prediction will work without a common prior as long as users believe the center has, or is likely to have, a "stronger" prior than me based on votes collected so far (that I don't have), and is therefore more capable of making a better prediction than I am. Or using something like BTS, I can say "I think the majority will agree but I disagree". 

[TODO: move this. If I think 70% of people will honestly agree, and the center has collected 1000 honest votes, then I will expect that whatever percentage of those are agreement, it is a better estimate than my 70% estimate. In this case, the logic of peer prediction holds and I am better off just giving them my honest opinion, because this is actual information that is more likely than not to improve the accuracy of the center's prediction]. 

What we'd hope for is an equilibrium around people reporting what they honestly think members of the core think is reasonable, instead of people reporting what they honestly think everybody else honestly think is consistent with dogma.

Peer prediction also allows dissenting opinion to be visible. Instead of a binary outcome, with 100% voting yes or no, we see that sometimes the decision is not so clear cut. Know this gives users information that allows them to start to build a more nuanced mental model of the core.

I think that the Keyensian Beauty Contest around belief reflects what happens in society better than peer prediction, at least in certain unhealthy situations. There are times when a wave of conformity overpowers a society. When some ideology, some dogma, becomes so powerful, that people can no longer reason with each other, or say what they think. They must conform or be punished. The witch hunts. The inquisitions. The gulags. The reeducation camps. The denunciations. Today the cancellations and twitter mobs. Those who have studied history know what I am talking about. I wonder if this happens when the "loss function" becomes linear and not logarithmic. When the social/reputational punishment for disagreement grows in direct proportion to the number of people who agree, instead of the log probability. People then can only coordinate around dogma instead of reason. I should perhaps write a separate essay here: a mathematical model of dogma

TODO: so it is confirmity, but allows theoretical intelligence whose opinion you are conforming to to be a real intelligence, with complex, nuanced thought, instead of a set of rules -- however complex. Because it doesn't require the (being) to be fully-specified. In situations where there is no precedent, where you really don't have much to base your opinion on what the core would think, you can fall back on your own intelligence, experience, and nuanced thought. You can say this is what I believe, and the center will conclude, simply because you are an intelligent human being, that a member of the core might believe the same.




## Preference Falsification and Pluralistic Ignorance

This method also offers a way out of the trap of pluralistic ignorance. If you know that the 


## Coordinating on Growth: How Can Larry Improve?

Now, what if there is common knowledge that people disliked one of Larry's policies? It might be common knowledge, for example, that Larry was a little **too** tolerant of offensive language. 



- IDEA: "I believe this, but I predict that". One is used to score other voters, the other yourself.
IDEA: If I am a member of the core, my disagree vote will be used to score *other* people's votes. But it is my agree vote that will be scored.






