---
layout: single
title:  "Argument Games"
toc: true
toc_sticky: true
sidebar:
  - title: "In This Series"
    nav: "give-truth-the-advantage"
  - nav: "give-truth-the-advantage-related"
    title: "Related Articles"
# header:
#     teaser: /assets/images/decentralized-truth-wide.png

---

[
	Todo:
	what people **honestly believe is reasonable**. Let's review how this is even possible.

	The modern (clever) cynic will tell you with absolute certainty that this is a fantasy. People are dishonest. They are irrational. Many are just plain bad. There are entire organizations full of bad people acting maliciously.

	But let's not forget that there also exist things in this world such as the rule of law. (These are far from perfect, but why should we expect a that was designed so haphazardly, that is such an accident of history, to be perfect?) But despite their imperfection, they are example of systems where judges are *generally* motivated, for selfish reasons, to intepret the law in the way that they honestly find reasonable, or at least that they think their peers will find reasonable.

	Let's review step by stop how we came to the conclusion that people will vote based on what they honestly believe is reasonable. First we conclude that in a prediction market, people will honestly 

	Blockchains have shown that it is possible to design systems where people will reveal what they honestly believe to be the true state of the blockchain -- or in other words, the correct interpretation of the rules. The literature on IEWV has shown that it is possible to design systems where people honestly reveal their **subjective** beliefs, or at least what they think others' subjective beliefs are. 

	What people **honestly think other people will believe is true**.


	TODO:
	each **reason** has a market. And the start price is price of the (parent) without that reason.

	TODO:
	your opinion is the only Schelling point?

	TODO:
	las of attention. If it is full of bots, you are getting attention from bots. This is sustainable only as long as people believe the bots are real people. 




]


First, every post or comment has a mini prediction market attached to it. The market price is the item's score, which is calculated using a formula we'll discuss in a moment. But in general, the score increases with the upvote rate (the number of upvotes divided by the amount of attention the item has received).

Now, the score of a **new** item is roughly the historical average score of all items, which should be very close to zero. So when you upvote a relatively new item, you are betting the item will end up with a higher-than-average score, essentially taking a "long" position. And The earlier you upvote, the more you can profit. But you can also lose points if you upvote something that turns out to be a flop. 

The score influences how much attention each item receives. So your upvote is in a sense a recommendation: you are telling the system that it should show this item to more people, and you are staking a small amount of your reputation on this recommendation. By upvoting a successful item early and thus helping that item be discovered, you can profit. But you lose points when you help to spread bad content.

Now the quality of the content that this system encourages will depend entirely on how the final score is calculated.  But if the score is nothing more than the upvote rate, then the result is a simple Keyensian Beauty Contest: people will upvote based entirely on their expectations of how other people will upvote, and not their own opinion, and aggregate expectations can become completely disconnected from people's actual opinions.

The first trick for grounding the score in something fundamental is to add **arguments**. People can downvote a post and add a comment explaining why they downvoted it. If people who see this comment are less likely to subsequently upvote the post, the score will fall. So the argument process works kind of like an appeal in a trial. You are saying "yes this item had a high score, but it shouldn't, because of this". 

For example, suppose there is some post circulating with a rumor that such-and-such politician had an affair or something, and this post has a high upvote rate. And assume for the moment that people are upvoting honestly: they believe the rumor is true. Now suppose somebody comments with a link to an article showing that the rumor is clearly false. If people are less likely to vote on the post after seeing the comment, the score will fall. The argument itself will therefore have a high score and will consequently receive attention -- in particular those who upvoted on the post before seeing the comment will be be shown the comment and asked to vote again, and if many people change their votes the score will fall further. Those who downvoted the post early -- taking a short position -- will gain points.

People can respond to this comment with a counter-argument, and the score may as a result bounce up and down for a while, until it gets to the point where nobody is adding anything new an convincing to the conversation and the upvote rate stabilizes.

This gives people incentive to consider whether their posts and comments will **stand up to scrutiny**. Before upvoting a post, you now need to stop and consider its weaknesses, and anticipate whether it is likely that somebody will respond with a comment ripping it to shreds. If you share some chart showing "X is increasing", you might want to double check the source of the data, and also make sure the chart can't be shown to be misleading. During a heated discussion, you should consider whether the post you are upvoting violates some explicit rule of the forum that someone might point out.   

Whereas in a traditional social network, people can count on knee-jerk reactions and superficial popularity, in this one you need to try to predict the votes of a subset of people **who have seen all the most convincing arguments on both sides**. 

If you recognize that a post shouldn't be upvoted because it is misleading, or violates the code of conduct of the site, you can make a profit by leaving a comment that convinces people of this, or by recognizing a convincing comment and being one of the first to change your vote in response. Just as users are rewarded for helping to discover and spread quality content by being among the first to recommend it, users are rewarded for helping to identify and de-amplify low-quality content by being among the first to downvote it and to upvote arguments against it.

### Honesty

Now the second trick is dealing with dishonest behavior and, in the worst case, large scale attempts at manipulation. This is done by calculating the score in such a way that people can maximize their expected profit by voting their honest personal opinion, and not based on their expectations of other people's opinions. This is done by using machine learning techniques to **anticipate people's expectations** of other people's votes, and discounting these from the user's profit. Scoring methods that incentive truthtelling are an active area of research in Game Theory and Machine Learning and are discussed in more depth in [Truthtelling Games]. 

### Attention and Reputation

The user's score is their reputation, and the higher their reputation, the more weight their recommendations have in the system. So the score of each piece of content, and therefore the amount of attention it receives, will be determined by users who have reputation.

As argued in the [the law of attention], users of a social platform will behave as if they are trying to maximize attention. If people's posting and upvoting behavior goes unnoticed by others -- if it has no effect on what content other people see -- then they will eventually stop posting and upvoting. But those who gain a large reputation, even if they don't author original content, will be able to command a large amount of attention. Their upvotes will be interpreted by the system as strong evidence that subsequent users with reputation will upvote the same item, and each upvote will result in a large number of additional users seeing that item. 

Gaining reputation requires upvoting content that existing high-reputation users will upvote **after** considering any convincing arguments that have been made against it. This means that if there is a core of **reasonable** users with high epistemic standards, new users will have to meet those standards to gain reputation. So new users will be motivated to only upvote content they honestly believe will stand up to scrutiny, and to defend their opinions with arguments that existing users find reasonable and convincing.


### TODO: Clustering



[
But the basic idea is to **anticipate people's expectations** of other people's votes, and discounting these from the user's profit. The result is that users only profit if future upvotes are [surprisingly common] -- if after their vote, future users are even **more** likely to upvote the content than the system expected. If the system is smart enough, this will occur only if you know something that the system doesn't know, and in most cases, the only thing users will that the system can't anticipate is their own actual opinion[^1]. We provide a deeper explanation of techniques for eliciting truthful information in [Truthtelling Games].

The problem of **Information Elicitation without Verification** -- extracting truthful information from agents (human or otherwise) who may or may not be honest is an active and exciting area of research and game theory and machine learning. 


[^1]: The only exception are cases will be when a user believes that their post-argument opinion will be uncommon even among users with a similar voting history. In such cases, if a user can guess how they have been clustered they can maximize their expected score by reporting the opinion they believe to be popular in their cluster, but in these cases their upvote will be **helpful**: it will result in a score that is closer to the true honest upvote rate [TODO: have to understand clustering to understand this]. 

Combining these techniques with a reputation system protects from massive manipulation using duplicate accounts. If I create 1000 accounts that all vote the same way, then even if my vote is based on my honest opinion, only the **first** vote will have information value. The system will soon learn to **expect** these 1000 accounts to vote in the same way, and the information content of the 1000 votes will be equal to the information content of the first vote. 
]

### Pinning People Down

By posting an argument, you change the playing field. You cause the system to consider the opinion of only those people who have seen your argument, strategically eliminating the uninformed opinion of those who have not. This can be an extremely powerful move if you predict that the people who seen your argument will be statistically more or less likely to upvote the item in question; because your argument provides information many people didn't have before, points something out people didn't notice before, or makes people think in a different way.

Of course, the other side of that is your argument itself should stand up to scrutiny. If you successfully reduce the score of some post by citing some statistics, and somebody responds by pointing out your statistics are wrong, then not only do you lose the overall argument, you your comment could end up with a high downvote rate and a low score.

TODO: when you make an argument, your position changes. It is as if you exit the overall argument at the current price, and enter in a position of "downvote overall argument and upvote this". 


## Conclusion

So the result is an argument game, and the biggest winners are those who can make and recognize convincing arguments.



TODO: unusual opinion

the belief that other people will like that content as well.

and if you will gain points if future users


-  You are staking your reputation

But the key is it estimates this probability post-argument: if a post is initially very popular, 

- why is honesty the best strategy

- predicting that this content is going to be popular.

cluster
informed
tru;thful

When you upvote a piece of content, it is like "going long" -- 






---



Structured Debate
Twitter is an argument game




----
The algorithms that promote honesty do so based on the correlation between your opinions and others. (even if minority)


In the argument game, users win points by promoting content that 

If a social



So the best way to win the argument game is to make posts that will stand up to critical scrutiny, and to make comments that others **honestly find convincing**. 



A reason-driven algorithm demotes a popular that has been "ratioed": if the average participant is statistically more likely to upvote or share a negative comment on that post than they are to upvote or share the original post. A critical comment itself can be ratioed, turning the discussion into a kind of argument game, that is won by the side that has statistically more support among people who have participated in a chain of arguments and counter-arguments with the highest ratios -- that is, the opinion of a subset of people who are **informed** of the strongest arguments that have been on both sides. So the best way to win the argument game is to make posts that will stand up to critical scrutiny, and to make comments that others **honestly find convincing**. 


On Twitter, a ratio often jut reflects the relative popularity of the posters. But adjusting for such confounding influences, a ratio can be used to demote **uninformed** content -- content that people who have read the comments are less likely to endorse.


...stand up to scrutiny, or defending your opinion against attacks



An algorithm that simply amplifies honest opinions instead of controversial content is an improvement over an engagement-driven algorithm. But ultimately this algorithm will tend to promote popular but **uninformed** content: amplifying shallow takes and knee-jerk reactions. 


A reason-driven algorithm demotes a popular that has been "ratioed": if the average participant is statistically more likely to upvote or share a negative comment on that post than they are to upvote or share the original post. On Twitter, a ratio often jut reflects the relative popularity of the posters. But adjusting for such confounding influences, a ratio can be used to demote **uninformed** content -- content that people who have read the comments are less likely to endorse.

A critical comment itself can be ratioed, turning the discussion into a kind of argument game, that is won by the side that has statistically more support among people who have participated in a chain of arguments and counter-arguments with the highest ratios -- that is, the opinion of a subset of people who are **informed** of the strongest arguments that have been on both sides. So the best way to win the argument game is to make posts that will stand up to critical scrutiny, and to make comments that others **honestly find convincing**. 


Many social platforms already gamify arguments, but arguments on the internet are toxic and unproductive, except in the case of platforms specifically engineered to promote productive discussion (e.g. Discourse.org, Hacker News). But healthy online argument is key to (unlock) the collective knowledge of a group and to give an advantage to informed opinion instead of a knee-jerk reaction.

