
All we need to do is estimate the probability that the reference rater will accept given user i has accepted.



We have prior information: how many users have accepted. From this we can estimate the probability that a **random** user will accept given another random user accepts (thus incrementing the params of the beta distribution).

But if we have specific information about how user i and j are correlate, this update will be a different story. We now need to estimate how much specifically user is vote increases the expectations for user j.

Obviously, if user i and j have totally uncorrelated votes (or at least, no more corelleated than would be expected by the beta distribution), then we have the maximum of additional information.

	That is, if Pr(S1=H|S2=h) = PR(S1=H)

On the other hand, if i and j have totally correlated votes, such that

	Pr(S1=H|S2=H) = 1 or PR(S1=H|S2=H) = 0

Then the additional information is zero.


	Pr(S2=H|S1=H) is either 
		(α+1)/(κ+1) if uncorrelated
	or
		α/κ	if correlated

Pr(S2=H|S1=H) = PR(S2=H,S1=H)/PR(S1=H)	


Pr(S2=H|S1=H)/Pr(S2=H) = PR(S2=H,S1=H)/PR(S1=H)/PR(S2=H)



Ahah. Independence of S2 and S1 says that Pr(S2|S1) = Pr(S2). There is some notation confusion here. Pr(S2|S1) is not the same as g(S2|S1). When we observe S2s signal, g(S2|S1) changes to reflect the new information from the signal, even if there is no correlation...


P1(S2=H|S1=H)


What we are trying to is measure the amount of **information** that S1 gives us. And this we measure by our decrease in uncertainty. And this we measure by the change in probability. The log significance ratio gives us the information we receive in a Bayesian inference scenario. But we still haven't framed the question right.

We have multiple components we are dealing with. There is the estimate of the probability of S2 given S1. But what we want is the **delta** of that over S0. How much does it change? 

if P(S2|S0,S1) = P(S2|S0), then S1 has provided absolutely no information

if P(S2|S0,S1) = (α+1)/(κ+1), then we have provide the **expected** amount of information. That is, the information expected based on the priors the center has about the correlations between votes. Those priors are that votes are independently drawn from a beta distribution. However, if the center has additional information: specific information about how the votes of S1 correlate with otehr votes, then this disribution should be different.


We may be talking about conditional mutual information. That is the information S1 gives us about S2 given we know S0. 


	I(X, Y) = H(X) - H(X | Y) = ∑_{x,y} p(x,y) log p(x,y)/(p(x)p(y)) = ∑_{x,y} p(x,y) log S(x,y)


	# Totally Uncorrelated
	S0 S1 p
    0  0  1/4
    0  1  1/4
    1  0  1/4
    1  1  1/4
    p(s0) = 1/2
    p(s1) = 1/2

	I(S0,S1) 
		= ∑_{s0, s1} p(s0,s1) log p(s0,s1) / ( p(s0) * p(s1) ) 
		= ∑_{s0, s1} p(s0)*p(s1|s0) log p(s1|s0) / p(s1) 
		= 
			4 * //since all combinations have the same probability
				1/4 log 1/4 / (1/2 * 1/2)
				= 4 * 1/4 log 1/4 / 1/4
				= log 1
				= 0

	# Totally Correlated
	S0 S1 p
    0  0  1/2
    0  1  0
    1  0  0
    1  1  1/2
    p(s0) = 1/2
    p(s1) = 1/2

	I(S0,S1) 
		= ∑_{s0, s1} p(s0,s1) log p(s0,s1) / ( p(s0) * p(s1) ) 
		= 1/2 log 1/2 / (1/2 * 1/2)
		+ 0 
		+ 0
		+ 1/2 log 1/2 / (1/2 * 1/2)
		= 2 * 1/2 log 2
		= log 2 
		= 1

	# Kind of correlated
	S0 S1 p
    0  0  1/3
    0  1  1/6
    1  0  1/6
    1  1  1/3
    p(s0) = 1/2
    p(s1) = 1/2

	I(S0,S1) 
		= ∑_{s0, s1} p(s0,s1) log p(s0,s1) / ( p(s0) * p(s1) ) 
		= 2 * // symmetry
			1/3 log 1/3 / 1/4
			+ 1/6 log 1/6 / 1/4
		= 2 * 
			1/3 log 4/3
			+ 1/6 log 2/3

		2/3 * math.log(4/3,2) + 1/3 * math.log(2/3,2)
		= 0.08170416594551039


	# Correlated on upvotes
	S0 S1 p
    0  0  1/4
    0  1  1/4
    1  0  0
    1  1  1/2
    p(s0) = 1/2
    p(s1) = 3/4

    p(s0)*p(s1) = 3/8

	I(S0,S1) 
		= ∑_{s0, s1} p(s0,s1) log p(s0,s1) / ( p(s0) * p(s1) ) 
		1/4 log 1/4 / (1/2 * 1/4)
		+ 1/4 log 1/4 / (1/2 * 3/4)
		+ 0
		+ 1/2 log 1/2 / (1/2 * 3/4)

		= 1/4*math.log(2) + 1/4*math.log(2/3) + 1/2*math.log(4/3) 
		= 2.157



So if we already have S0, we use that to update our beta dist



Okay, so say S1 has strong correlation to S2 -- it is highly predictive. Center decides significance is high, and provides a high score to U1 for truthful report. But let's say S1 also correlates to S0. S1 has little predictive power given S0.

Significance is now P(S2|S0,S1) / P(S2|S0). 

So we can't simply incrementally update the beta distribution when we get S1. We need a calculation that takes into consideration the mutual information between S1 and S2, and the background probability based on previous votes (S0).

How do we model this as a hierarchical model. S1 is drawn from a beta distribution with paramters determined by previous votes (S0). S2 is then drawn from what? A different distribution, that is formed based on the evidence of past votes of S1 and S2. 

For example, we start with the prior belief that S1 and S2 are not correlated any more than they naturally are by being drawn from the same general population.
Each pair of votes is new evidence. SO our prior is actual a uniform distribution over the tuple {S1,S2}, where each tuple {S1=1,S2=1}, {S1=0,S2=1}, etc. is equally probable (1/4). Then based on the evidence we update this disribution.  But this doesn't allow us to have a strong prior belief that S1 and S2 are or are not highly correlated: for example if we get evidence {1,1} twice, we now strongly believe the distribution is skewed. What we really need are two beta distributions. P(S2=1|S1=1), and P(S2=1|S1=0). Our prior average ω could be the population average, and the concentration κ represents our prior belief in how likely it is or to what extent S2 is likely correlate with S1, or something like that. In fact we can generate this prior with a though experiment, a very large number of voters, each of which have voted on M items, but in fact totally uncorrelated. Randomly pair together, and for each pair we have the percentage of items that are the same. We now have a distribution that says, for example, there is 10% chance that they have more than 80% of items in common.






----


















