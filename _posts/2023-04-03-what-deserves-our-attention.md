---
layout: single
title:  "What Deserves Our Attention?"
toc: false 
# toc_sticky: true
sidebar:
  - title: "In This Series"
    nav: "social-protocols"
  - nav: "social-protocols-related"
    title: "Related Articles"

---

Every online community has rules that determine how the attention of the community is directed. For example in an online forum, the most up-voted posts may be shown on at the top of the page. This rule concentrates attention on popular content. 

But this is a terrible rule. It creates perverse incentives for people to share content with generic appeal based on uninformed first impressions. It encourages shallow conversation on lowbrow topics. The engagement-based ranking used in many social networks is even worse, concentrating attention on controversy, outrage, and out-group animosity. Three decades of Internet toxicity and countless academic studies unequivocally condemn these algorithms. <!--(TODO: references)-->

So how should content be ranked? What content deserves our collective attention, and how should this be determined? 

This is an important question. This is perhaps the most important question for society to answer today. Attention translates to influence. What people pay attention to influences what they believe. What they buy. How they vote. What they fight for. That's why autocrats try to control the media. It's why companies pay billions in advertising.

But our collective attention should not just be a commodity to be exploited. It should be used for the common good. The collective attention of a society turned towards some common object is what allows the formation of common knowledge, a shared basis of facts, a shared reality. Our collective participation in public discourse produces the shared values and narrative that enables collective action toward common goals.

Social networks and online forums, as the public square where public discourse takes place, are public goods that should be independent of the whims of politics. They should be governed by transparent, democratic protocols that direct society's collective attention towards production of common knowledge, narratives, values, and culture. They should direct our attention towards constructive, civilized public discourse and a collective, cooperative pursuit of truth. Towards the realization of our collective will, and manifestation of our collective intelligence.

Designing such rules is the goal of *Social Protocols*.

## Next Article

- What Is a Social Protocol

