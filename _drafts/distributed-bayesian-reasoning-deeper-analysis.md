

# Part III: Deeper Analysis




## Popular vs. Justified Opinion

In very small arguments it might be possible for everyone to participate in every jury. In these cases, popular opinion and justified opinion will be same.

Justified opinion will differ from popular opinion in cases where some people are not aware of some of the arguments that have been made. When one jury has access to evidence that has not been seen by jurors at higher levels, this information will propagate up through the argument graph, enabling a kind of distributed information processing even when not every participant has the same information.



---

, the potential improvements to online discourse, and how social platforms could benefiit

This information could be used by social platforms to promote more intelligent conversation and mitigate the spread of information, by avoiding algorithmic amplification of content that is engaging but that individuals are not likely to share if they knew more. We also think there is tremendous additional potential to use this method to improve the quality of online conversations and help people analyze their own beliefs and discover truth. We discuss these further in [another article].

<img src="/assets/images/distributed-bayesian-reasoning/viral-meme.svg"
     alt="Viral Meme and Critical Comment"
     style="display: block; margin-left: auto; margin-right: auto; height: 500px" />



## Analyzing Controversy

This model also provides a mathematical basis for analyzing controversy. It can identify the main reasons people disagree, the arguments that change minds, and the counter-arguments that change minds back. It can identify situations where popular opinion is based on ignorance, because the data shows most people would believe differently if they knew more. And it can find areas to focus conversation that are most likely to result in understanding and even resolution of disagreement.


The model also makes it possible to search for the **crux** of an argument: a premise, possibly deep in the argument graph, that might change the justified opinion if it was accepted or rejected. For example, the subjury's acceptance of the DNA evidence may "hinge" on their acceptance of the claim that the lab technician was drunk, which may hinge on the reliability of a witness, which many hinge on in incident involving iguanas. With this model, we can propagate probabilities up through the argument graph and show, for example, that the hypothetical fully-informed juror would probably vote guilty if they believed the story about the iguanas.

<img src="/assets/images/distributed-bayesian-reasoning/crux-of-argument.svg"
     alt="A Distributed Brain"
     style="display: block; margin-left: auto; margin-right: auto; width: 700px" />




## Better Conversation

A small number of individuals can have a disproportionate influence on the justified verdict by focusing on crux arguments. 

On the one hand, this means that it's important to defend against invalid results due to sampling error and deliberative manipulation. 

On the other hand, this creates tremendous potential to give voice to individuals who would not otherwise have it, because they can influence results not just by making popular posts and comments, but by supplying useful information and making influential arguments. 

And this opens up the potential for new types of incentive systems that award reputation based on the ability to defend content that stands up to scrutiny, and make and help to defend crux arguments. 

In [another article], we further explore the potential benefits of this method, as well as some of the potential challenges that may arise in implementing it.

[todo: contradiciton, circular, etc. including the problem of "fair deliberation", argument structure and identification of claims, "emotional" arguments and unstated premise, and when we can make assumptions about causality.]


## Giving Truth the Advantage

Yet this may be enough to improve the quality of online conversations.

Today's online social platforms tend to encourage and amplify polarization and misinformation (todo: link). We think that a platform could mitigate this problem by using the justified opinion to de-amplify potentially viral misinformation that an individual would likely **not** share if they knew more. In [another article] we argue how this kind of [deliberative filtering] would create a dynamic that tended to **give truth the advantage** on social platforms. 

<img src="/assets/images/distributed-bayesian-reasoning/viral-meme.svg"
     alt="A Distributed Brain"
     style="display: block; margin-left: auto; margin-right: auto; width: 750px" />



## Superhuman Collective Intelligence


The famous [Condorcet Jury Theorem](https://en.wikipedia.org/wiki/Condorcet%27s_jury_theorem), says that a large enough jury is guaranteed to produce the correct verdict under majority voting, as long as the jurors are independent, and the average juror is **competent**.

A competent juror (per the theorem) is one that is more likely than not to vote correctly. This assumption is of course doubtful. The average juror may be biased, ignorant, and misinformed. So by the same argument, we can easily prove a  corollary to the Condorcet Jury Theorem: **a large enough jury of incompetent jurors is guaranteed to be wrong**.

We've often heard stories of the "wisdom of crowds" and their uncanny ability to estimate the number of jellybeans in a jar or the weight of an ox. But this just tells us that the average person happens to be competent at estimating these things.


[We've all heard stories of the "wisdom of crowds" and their uncanny ability to estimate the number of jellybeans in a jar or the weight of an ox. But this just tells us that the average participant happens to be competent at estimating these things. Whether judging guilt or counting jellybeans, asking a large number of individuals their opinion is just a poll: asking a large numbers of people just cancels out individual errors.]
 


The wisdom of crowds in many cases is really just the competence of the average individual plus the law of large numbers. If the average person is **not** competent, then the average opinion of a large enough sample is guaranteed to be wrong

Collective intelligence is not automatic. A random citizen, who knows nothing but what he has seen on the news, is probably not a competent juror: the jury trial was invented to **form** competent jurors.

But even stuffed full of knowledge and law, there is a maximum competence the average juror can achieve with respect to a given case. 

Other group decision-making processes attempt to increase average competence by identifying the most competent people. But this at best limits the competent of the group to that of the most competent people in the group.

The real potential of collective intelligence is discovering not the opinion of the average person in the group, nor the opinion of the most competent people in the group, nor that of the most competent people in the group after deliberation has made them more competent...but an opinion that is beyond the abilities of any one person: the opinion that the most competent person in the group would form if they could surpass the limits of time and memory and comprehend all the relevant knowledge shared by all the other members of the group.

This super-human collective intelligence can only be achieved through a process of distributed reasoning.

[Part II: Math](/distributed-bayesian-reasoning-math)


## Jury Selection



The results of a trial of ideas will be meaningless to people who don't perceive the jurors to be reasonable, knowledgeable, and trustworthy. Unfortunately, most online communities will often be viewed with distrust by some portion of society. 

This is partly due to the vast differences in core values and belief systems throughout society, and partly due the particular challenges of healthy discourse in online social platforms, which are susceptible to [massive manipulation](https://yalereview.yale.edu/computational-propaganda), censorship, and perverse feedback loops that encourage misinformation an abuse.

Distributed Bayesian Reasoning is particularly vulnerable to manipulation because the formulas in [Part II](/distributed-bayesian-reasoning-math) reveal exactly where an attacker can concentrate their efforts to greatest effect. These challenges should not be taken lightly. 


## Decentralized Juries

A possible solution to the problem of trustworthy jury selection is a decentralized trust model that estimates, for each individual, the **justified opinion of the people in that person's social network**.


GREAT POINT: discover what arguments *do* convince people.


We also explore a decentralized credibility model that estimates the justified opinion of a **group of people with similar judgment**, or similar priors, which is a way of estimating **the opinion that the individual would have**, if they were aware of all the arguments. We believe this could create healthy feedback loops that reward people for posting content that stands up to the scrutiny of their peers.

We will explore the challenges of jury selection, including decentralized trust models, in depth in another essay.

## Descriptive vs Normative Claims

This method is not limited to claims about what is true or false (*the defendant is guilty*). It also works for value judgments such as *the Beatles are the greatest rock band ever*; for it estimates the probability not that this is in fact the case, but that a fully-informed participant in this particular discussion would think so.

The method also works for group decisions, for example a family's decision whether or not to go to the beach. They might initially vote to go until somebody points out that it is raining. Though it still doesn't make sense to estimate the probability that the family **should** go to the beach at this point, it makes sense to estimate the probability that the average family-member, after hearing all the arguments, would want to.

Claims that imply a value judgment, such is what is good or what should be done, are often called *normative* claims, as opposed to **descriptive** claims that attempt to objectively describe reality. The idea of **acceptance** embraces both types of claims: the justified opinion on a claim is an estimate of the probability that a fully-informed person in some group would **accept** the claim as true, just, probable, desirable, etc.

## Good and Bad Reasons

Often, the reasons people use to justify their decisions are not good reasons, or even their actual reasons. In fact, [Dan Sperber] argues that reasons are *always* essentially justifications for beliefs we form without reason.

But distributed Bayesian reasoning works the same regardless of the type of reasons people offer to justify their opinions. The method does not consider the logical content of the argument. What matters is that people state their reasons; that these reasons are subjected to critical scrutiny; that the underlying claims can be questioned; and that people can respond with contrary appeals arguments. This is what gives the system the information necessary to estimate a justified opinion.

Can the opinion of a jury said to be just if an argument that might change minds was withheld from them? 

## Unstated Premises and Agreeing to Disagree

On the other hand, people hold countless reasons for their beliefs that don't need to be stated, because although they may be technically relevant, they are not **convincing**. For example, the fact that the defendant is physically capable of holding the murder weapon may be relevant, but it may not change minds -- unless someone is asserting that this is *not* the case.

This means that we can consider a deliberation to have been fair not when there are no more arguments being made, but when **arguments are no longer changing minds**. Sometimes, people simply have to agree to disagree. 

## Popular vs Justified Opinion

As the example of the jury trial demonstrated, justified opinion may differ significantly from popular opinion. In fact it is possible for nearly 100% to agree with some claim, and yet for the system to determine that nearly 0% would agree if they were fully informed. For example, imagine the jury trial example where 11/12 jurors voted guilty and accepted the DNA evidence, and only one voted guilty and rejected it.

So this method allows a minority to effectively override a majority. This is potentially problematic; the minority could be a small, unrepresentative sample of the average juror, especially if motivated individuals try to manipulate the result.

There are two solutions to this problem. The first is [jury selection](/distributed-bayesian-reasoning-introduction/#jury-selection) as discussed above. The second is something called Bayesian Averaging. Just as this method applies the rules of Bayesian inference to simulate the reasoning of the hypothetical average juror, it uses Bayesian inference to *to estimate the prior beliefs of the average juror*. This means that there must be **sufficient evidence** to support the hypothesis that the average juror *would* vote differently if they had participated in the subjury. If there is only one dissenting juror, this evidence is scant, and the justified opinion will therefore be closer to popular opinion. The details of Bayesian Averaging are discussed in the section on Bayesian Hierarchical Models in [Part II].

If democracy requires fairness, then simple majority-rule decisions based on initial-impressions are not necessarily very democratic. This method allows a minority to override a majority; but only if there is sufficient evidence that the majority *would* believe differently if they were fully informed. 

## Fact Checking and Misinformation 

> "Falsehood flies, and truth comes limping after it, so that when men come to be undeceived, it is too late"
>
> -- Jonathan Swift
{: .notice--info}


The algorithms that social platforms use to filter, sort, and amplify content are generally based on engagement: they reward and amplify content that gets people to click, comment, and share.

Rumors spread easily in engagement-based social platforms because, first off, the people who think the rumor is true help spread it, while people who think it is false can usually only comment. But this engagement only signals the algorithms to further amplify the rumor. These corrective comments are rarely shown prominently, and they are often too late to prevent a rumor to go viral.

But a social ranking algorithm based on justified opinion would allow people to provide a reason for disliking or disapproving of a post by commenting. If people who saw the comment were less likely to share or upvote the post, then the estimated justified opinion of the post would be low, even though the comment was popular and engaging, and a responsible platform would de-amplify this content.

While misinformation often **starts** with a few people with malicious intent, it **spreads** when large numbers of people innocently believe it to be true. Even inside social media echo chambers, the spread of misinformation would be dampened if information could be amplified only if the average member of the echo chamber *would* believe it if they had all the information.

Fighting misinformation online is challenging, because fact checking ultimately depends on people, who are fallible and can be biased. The solution to online misinformation is not fact checking. The solution is social algorithms that **give truth the advantage.** 

Distributed Bayesian Reasoning cannot determine whether or not claim is true. It can only estimate the justified opinion of some group. But if an online community includes a large and diverse group of reasonable, informed, and honest people, and succeeds in creating a group dynamic conducive to honest and reasonable deliberation, then the justified belief of this group is perhaps the best estimate that can be made of the probability that something is true.


## Fair Deliberation

This last part -- chaining these inferences together -- is clearly the point of this model most vulnerable to criticism. We are going beyond standard statistical methods for making inferences about a population based on a sample, and entering the world of hypothetical predictions based on complex models of some part of reality. In doing so, we need to be very careful about our assumptions about causality.

In particular, we cannot necessarily assume that, if a juror who originally voted guilty, but was subsequently convinced that the DNA evidence was invalid, would change their vote to innocent. In the introduction we asserted that we **could** make this assumption because no other reasons were given during the trial or jury deliberation, and so can proceed that none exist. We call this the **fair deliberation** assumption, and we discuss this assumption and its implications in more detail in [another part.]

## Fair Deliberation

The quality of group judgments also depends on the quality and thoroughness of the deliberation process that produces the argument graph and the data on people's beliefs. A fair trial requires a fair process. So does a fair trial of ideas.

A fair process requires competent advocates for both sides of any question, and a system that encourages healthy discourse with deep argument threads.  The [Deliberative Poll](/the-deliberative-poll) describes a starting point for a fair deliberation process in a social platform. 

Online platforms can further foster healthy discourse using effective [community moderation](/moderation-as-consensus) systems, and sculpting of online communities to avoid echo chambers and encourage mutual trust and constructive disagreement despite diversity of opinion. These and other possible techniques are beyond the scope of this paper.

## Argument Structure

Although a [Deliberative Poll](/the-deliberative-poll) can be implemented in many social platforms with virtually no change to the basic conversation model, the full potentially of distributed reasoning additionally requires some method for creating sub-juries, or groups that independently argue over the acceptance of claims. This raises the difficult question about what exactly is a *claim*, and how to identify what claims are being made in arguments, and to ensure that the meaning of the claim is sufficiently unambiguous to all participants.

The nascent field of [argument mining](https://direct.mit.edu/coli/article/45/4/765/93362/Argument-Mining-A-Survey) is exploring techniques for automatically extracting the structure of an argument from online conversations. Another group that is working on solving the problem of unambiguous identification of claims in online arguments is the [Canonical Debate Lab](https://canonicaldebatelab.com/). 

We think that some fairly minimal changes to the interface of a typical online social platform would be sufficient to collect the minimum information necessary to identify arguments and the claims these arguments are based on. We'll discuss this in more depth in a future essay.

## Argument Analysis

Many interesting possibilities for analysis of disagreement arise from this model. Here is a summary of some of them:

- identifying differences between popular and justified belief: 
    - when the popular opinions is not justified (e.g. common myths)
    - when a fringe opinion is justified (mythbusting)
- identifying the major sources of disagreement
- identifying the most effective arguments 
- simulating what justified opinion *would* be if certain claims were assumed true or false
- identifying claims on which to focus attention that are most likely to result in:
    - consensus
    - large changes in the justified opinion
- identifying presence of biases as latent confounders
    - clustering participants by bias
    - identifying "schools of thought"
- measuring an individual's ability to:
    - defend their beliefs
    - make and identify influential arguments

Many of these calculations are just marginal probabilities on the joint probability distribution derived from the Bayesian network representation of the argument graph.

We will discuss these in more detail in other papers.




## Convergence of Opinion

[Aumann's Agreement Theorem](https://en.wikipedia.org/wiki/Aumann%27s_agreement_theorem) famously suggests that, if people were perfectly honest and rational Bayesian reasoners, they would attribute differences in beliefs not to their own superior power to discern truth, but rather to differences in information and experience (or difference in priors, [which amounts practically to the same thing](http://mason.gmu.edu/~rhanson/deceive.pdf).) 

So if we just could exchange enough information, we'd all agree on everything!

It doesn't often work this way, because real human beings are not perfectly reasonable, honest, humble, disinterested truth-seeking agents.

But sometimes, part of the problem that there is simply too much information to exchange. We get stuck in endless arguments over the smallest details. Often, reality is so complex, and the difference in our experience is so vast, that unwinding the reasons for our differences is just not worth our time.

Distributed Bayesian Reasoning process can help us transcend these limitations. Even if no two participants actually exchange the information that would be necessarily to approach an Aumann agreement, this process can still estimate what that agreement would be if they did.

## The Bayesian Robot

Imagine a Bayesian robot. This robot is intelligent but not human (though it yearns to be). It lacks the prior knowledge that humans gain through a lifetime of human experience: it does not understand pain, affection, jealousy, love, justice. It has no direct intuitions about space and time. It lacks even a child's common sense about everyday things: what sound a dog makes, how scary bees are, how water feels.

And so it has to borrow priors from human beings. It can't read our minds, so it has to ask people what they believe, and from the answers form a joint prior probability distribution of the possible true states of the world as might be believed by a human.

This robot could depend on a single person for input: its loner-genius maker perhaps. But it believes that its best chance of being truly human is to aggregate the prior knowledge of as many humans as it can.

And so it goes to the Internet and talks to a lot of people. It needs to create a joint probability distribution as required for Bayesian reasoning, so it asks people questions about beliefs in combinations. It asks if one can laugh and cry at the same time; if the grass can be wet if it is hasn't been raining, if one can be innocent despite conclusive DNA evidence. 

And when different people have different opinions on these things, it asks them, why? What information does the one have that the other doesn't? Would they agree if they had the same information? From this it begins to infer their prior probability distributions.

Only after acquiring a massive amount of common-sense prior knowledge can it begin to reason like a human. It can reason that if the grass is not wet it probably hasn't been raining. If the DNA evidence was contaminated it is probably not reliable, etc.

[CUT: 
The robot does not need one joint giant prior probability distribution over all combinations of all things. That is inefficient, because many probabilities are independent. Instead it must break its knowledge down into independent modules, and feed the inferences made using one module into the input of others. It can form its opinion on the DNA evidence in one module, holding a separate jury sub-trial in its mind, and use that belief to inform it's opinion on the defendant's guilt.]

How do the beliefs of the Bayesian robot differ from the beliefs of our hypothetical, fully-informed average participant in a group deliberation?

If the hypothetical prior probabilities are constructed by asking the same questions of the same people on the Internet, then their priors are identical. Which means their beliefs will be identical.

The fair deliberation assumption says that the the average participant has no beliefs beyond the those that were given to justify their opinions during deliberation. In the case of the Bayesian robot, we make essentially the same assumption: the robot has no way of forming priors beyond what it has learned by averaging the beliefs of humans observed by asking them the reasons for their beliefs.

So our hypothetical average juror reasons identically to the Bayesian robot.

## The Ubermind

Suppose, as has been argued by *so and so*, that human beings come into the world as Bayesian reasoners with common priors and their beliefs about what the world are entirely a product of different information.

Now imagine some ubermind, a perfect Bayesian reasoner which has acquired the some of all human knowledge.


# Part ?: Dynamics




## Discounting Unjustified Votes

In the example elaborated in Part I, the opinions of the jurors that accept the DNA evidence as conclusive effectively have no weight in the final probability calculation.

Why? Because the rules of Bayesian inference say that, after learning that the DNA evidence is not conclusive, a Bayesian reasoner should then only consider the probability that the defendant is guilty *given the DNA evidence is not conclusive*. This conditional probability is calculated by looking at the votes of people who think the DNA evidence is not conclusive, and computing the probability that they also find the defendant guilty. Votes of jurors whose beliefs are not consistent with the decision of the subjury are effectively ignored.

Ignoring people's votes doesn't seem very democratic. But if democracy requires fairness, then simple majority-rule decisions are not necessarily very democratic. A fair verdict *requires* that unjustified opinions be discounted. Since the DNA evidence has been completely rejected by the more informed sub-jury, a guilty vote is not justified.

Now, if the sub-jury does not unanimously reject the DNA evidence, the votes of those who accept the DNA evidence will have weight in the final verdict. The exact formula in this case is given in [Part II].

Also, if a juror **concedes** that the DNA evidence is not conclusive but sticks with a guilty vote, their vote will also have weight in the final probability calculation, because the probability that the average juror votes guilty given they reject the DNA evidence will now be greater than 0%. 

If they also provide a *reason* that the defendant should be convicted even though the DNA evidence is not conclusive (e.g. *the defendant signed a confession*), and a sub-jury concludes that this is in fact true, they will have justified their vote. ANd now the tables will have turned! The jurors on the other side must now *concede this point* for their votes to be counted! Why? Because now there are two items of learned evidence from the two sub-juries, and the hypothetical Bayesian reasoner must now consider the probability of guilt given these two items are true, which requires considering only the justified votes: the votes of jurors who believe (concede) that the DNA evidence is not conclusive but the defendant signed a confession.


## "Pinning Down" Beliefs

Everyone is familiar with the experience of wanting to "pin down" somebody on some point during an argument. *Did you say you would or didn't you?* *But do you admit the policy does some good?* *But do you deny being the owner of the gun?* 

And we all know that sometimes people try to avoid being pinned down. But why?

Sometimes people are a little dishonest, and know that while they can't deny the point, they don't know how to justify themselves once they concede. But sometimes they honestly fear they will be unfairly misunderstood, especially when faced with an aggressive opponent who might not give them a chance to justify themselves (*yes it is my gun but...*).

On the flip side, people tend to avoid the unpleasant aggressiveness required to insist on pinning somebody down on an opinion. But in a group decision-making process, truth and fairness often require it. For either the person being pinned down cannot provide acceptable reasons, which means their opinion is not justified, or they can, in which those reasons need to be revealed.

In the Distributed Bayesian Reasoning process, we can avoid the unpleasantness of overtly pinning people down. Nobody is obliged to actually justify their opinion, and there is no need for others to insist; unjustified votes simply are not counted or are given less weight, as a logical consequence of the rules of Bayesian belief revision.

## Refining Individual Reasoning

It can help not only identify the best reasons that have been given by others for having a different opinion -- it can identify when the reasons for one's opinion are not considered to be good reasons even by people who agree on the conclusion.

It can encourage individuals to be more careful about the reasons they provide for their beliefs.


TODO: counter-arguments.

## Forcing Consistency

A Distributed Bayesian Reasoning system can also help to identify inconsistencies in people's beliefs. For example one may be tempted to take a different standpoint on the claim *a politician who has lied to the public should not be reelected* depending on which politician is being discussed. The system can encourage re-use of the same claim in different contexts -- as long as some important problems of ambiguity in context are worked out -- so that individuals are forced to take a consistent position.

## Painting Into Corners

## Reuse of Argument

## Analyzing Controversy


## Promoting Critical Thinking


- challenge people's assumptions
- help people seek truth


## Healthy Feedback Loops

In an online platform, this model suggest the possibility of new types of reputation systems based not on popularity or conformity, but based on users' ability to justify their opinions and influence others with reasons.

- For example, when the system identifies a claim as the "crux" of a disagreement (e.g the iguana thing), users that can make a unique argument that convinces the small sub-sub jury to accept or reject that claim, they might be able to swing the verdict one way or another. So these crux arguments can give a great deal of leverage to participants with unique knowledge, allowing them to quickly acquire reputation.

- In general, when a user thinks a claim with popular support will ultimately not stand up to scrutiny, they can make large reputation gains from rejecting that claim from the start, then successfully defending their position with good arguments in strategic places.

- Users could even bet reputation points on their ability to swing the result. 

- The system could power a hybrid prediction market, where every claim has a corresponding contract whose price is equal to the current justified opinion (though this would obviously require some mechanism for defending against large-scale manipulation).

- On the other hand, users would lose reputation if they failed to justify their opinions. In our murder trial example, jurors that voted guilty because they accepted the DNA evidence would lose reputation after the sub-jury rejected the DNA evidence, unless they either:

1. concede the DNA evidence and change their verdict to innocent 
2. concede the DNA evidence and convince submit a counter-argument that swings the verdict to guilty 
3. submit an argument that convinces the sub-jury to change their opinion on the DNA evidence (e.g. iguanas again) 

- All votes could be weighted by reputation. This would amplify the feedback loop. New users would not have influence in the system until they had a record of defending their opinions, in the judgment of existing users. Gaining reputation would not necessarily even require actively submitting arguments: simply voting on claims that ultimately stand up to scrutiny would award reputation.


Overall this could create a dynamic that rewards careful reasoning and discourages submission of comments that cannot stand up to scrutiny.

## Unstated Premises

The verdict of a sub-jury will have no effect if that verdict is something that all the jurors already knew.

If all jurors believe that (some obvious thing), then bringing that up in argument cannot change people's beliefs (a Bayesian reasoner does not revise beliefs based on information)


But uncovering arguments that influence people's opinions -- whatever they are -- and allowing others to respond, is necessary to produce a group judgment that is based on knowledge instead of ignorance. 


Yet this process process works just as well for argument that appeal to emotion as to logic, for even the appeals to emotion can be subject to critical scrutiny. If I exclaim *you are a liar!* or *they don't respect our rights!*, both those claims and their implications can be questioned.


-----


Most of the reasons people have to believe something do not need to be explicitly stated in an argument. When arguing that we should not go to the beach because it's raining, I don't need to explicitly argue that a rainy beach is not fun, or that having fun is the point. 


On the other hand, many of the reasons that actually determine people's opinions do not need to be included in our model of the mind of the hypothetical fully-informed average person. 



But even appeals to emotion can be valid reasons for accepting or rejecting something, for often our core values and finer instincts only reveal themselves through emotion. And even dishonest rationalizations 



These arguments are left unstated because nobody believes that they will change other people's minds. 

Fair deliberation simply requires giving everyone a chance to make their case.

----








But people are not completely unreasonable: they will often change their minds when given good reasons. We believe in jury trials because, despite people's biases, we believe ultimately in the reasonableness of the average juror.




TODO: there is some subtlety here I am having trouble articulating. The unstaetd premises don't need to be stated because people don't think they will have influence. On the other hand, the relatinoship is *causal*. That a rainy beach is not fun *is* a reason not to go the beach. If people thought otherwise, it would change the justified opinion. But we don't need to include it in our model, because it won't chagne the outcome. "it is raining" is sufficient for people not to want to go to the beach "it is raining and a rainy beach is not fun" is no more convincing. It would be convincing if some people thought a rainy beach was fun. But nobody disagreees with the unexpressed premise, and those that see the argument and agree are no more likely to go to the beach than those who didn't see.



Certainly if a family member argued that going to the beach *would* be fun, they might change their mind





## Chess Chess Example







-----------------------------------------



## Justified Belief

This process measures *justified belief*. That is 

effectively discounts the opinions of people whose beliefs are not justified. If the second juror rejects the DNA evidence, then those who believe that the defendant is 

 In our jury trial example, once the DNA evidence is rejected by the second jury, the beliefs of those members of the first jury who believe that justify their vote of guilty based on the DNA evidence *no longer have justified beliefs*. Consequently, their votes are effectively ignored. The probability that the average fully-informed juror would guilty given they believe the DNA evidence is no longer of consequence, because the probabilty that the average fully-informed juror believes in the DNA evidence is zero. 

## Assumptions


 it does require is a convergence of justified belief in the critical thread. 



- discounts beliefs that are not justified

## Accuracy of the Model

The academic world has concluded that honest truth-seeking agents with common priors should not knowingly disagree [Are Disagreements Honest http://mason.gmu.edu/~rhanson/deceive.pdf]. This means that if people are honest and rational (e.g. perfect Bayesian reasoners), they would attribute differences in beliefs not to their own superior god-given power to discern truth, but rather to differences in information and experience.

These reasonable, honest, and humble truth-seeking agents do not sound like real human beings, yet this simplifying assumption is useful. We know that real people resemble the ideal to at least some extent, else they would not be able to reason with each other: people change their beliefs given information provided by others all the time (especially if if they aren't arguing politics or religion). So our idealized model of the hypothetical average juror let's as assume that to the extent the jury selection process can produce juries that approach this ideal, their beliefs approach on those predicted by the model.

If an ideal jury is able to effectively exchange relevant information through healthy deliberation, they will eventually converge on a unanimous vote. If the vote is not unanimous, it means that they have more information to exchange, or deliberation has become ineffective due to communication failure, or quite possibly it means that they fall short of the ideal of honest and rational truth-seeking agents.


Our model further assumes a "fair trail": that these honest truth-seeking agents have exchanged information through effective deliberation and have either converged on a unanimous vote, or narrowed down the reasons for their differences. The resulting juror is more informed than the pre-trial opinion.

But importantly, the result is a *justified* opinion. 

- to examine the assumptions that influence each juries vote
- allowed others to challenge those assumptions
- or to justify a different vote even if these assumptions are true

It allows us to see which jurors votes are *not* justified, and to assume that 


. And we can show that, to the extent that the process has been fair, the *justified* opinion approximates the opinion that the fully-informed average reasoner would actually have.





----

## Fair Deliberation and Causality



The justified opinion estimate is based on an assumption of causality: that a randomly-selected juror *would* believe a conclusion with a certain probability, given they accepted certain premises. This assumption of causality is justified to the extent that the actual reasons for jurors beliefs have been included in the argument graph. (TODO: this takes time. Fundamental disagreement. Augmann agreement.) 

Fleshing out these reasons requires argument. It requires people to give reasons for voting guilty despite the DNA evidence, and it requires these reasons to be subject to critical scrutiny and, if accepted, it requires the other side to provide a counter argument.

--------


This process calculates a justified opinion after fair deliberation. The assumption of fair deliberation is necessary not only because it results in an *informed* opinion, but also because it reveals the causal relationships necessary to simulate the hypothetical, fully-informed average group member.

In the jury trial example, we made the simplifying assumption that the DNA evidence was the only relevant issue in the trial, in order to justify our assumptions about how jurors *would* vote if they accepted or rejected the DNA evidence.

Let's relax that assumption. Suppose the trial has not yet ended, and the judge instructs the first jury to accept the DNA evidence as conclusive. 

The jurors then take another vote:

- Two juror accepts the instruction and change their vote to innocent.

- Three jurors won't be told what to think: they insist the evidence is inconclusive, and maintain their guilty vote. 

- Three jurors concede that the evidence is inconclusive (though perhaps secretly doubting it), but maintain their guilty vote anyway, arguing that the defendant "looks guilty". They maintain their position after significant argument. Eventually the jurors "agree to disagree" and deliberation ends.

This messy result can still be considered a reasonably fair deliberation, because each side has had a chance to make the arguments they think might convince others, and respond to those arguments. And the results are useful because they tell us *why* the jurors voted as they did. [TODO: voted or votes? Earlier in esssay we talk about the probabiliity that they vote]


Before the judge's instruction, we had a non-causal association between acceptance of the DNA evidence and a guilty verdict: we knew the probability that a juror voted guilty given they accepted the DNA evidence was high. But this could be due to a common cause, such as sympathy for the defendant. 

To infer a causal relationship, we needed to present the jurors who accepted the DNA evidence with a counter-factual: how *would* they have voted given they conceded that the DNA evidence was not conclusive? Knowing this allows us to estimate the probability that a juror *would* vote guilty given they *did* accept the DNA evidence.

Considering the counter-factual required further deliberation, because it required these jurors to reveal the reasons, or at least come up with justifications, for their vote. This gave other jurors the opportunity to provide counter-arguments.

And though the counter-arguments didn't change minds, in the end this process revealed what we need to know: the probability that a juror *would* vote guilty given acceptance or rejection of the DNA evidence, after fair deliberation. And this is enough to calculate a justified verdict: even though the final vote is tied is 6:6, the average informed juror would reject the DNA evidence, and would also vote innocent given they had rejected the DNA evidence, and so the overall probability that a fully-informed juror would vote guilty can be calculated to be exactly X%. 




These instead reasons do 

Certainly if anyone disagreed with the

...carvees the 

And however irrational the reasons that influence people may be, the hypothetical fully-informed Bayesian reasoner applies them dispassionately and rationally.


## Unexpressed Resaons


## Implementation Issues




