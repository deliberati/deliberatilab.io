

## Everything is about Attention

There's that saying "everything is about sex, except sex, which is about power." Well on the Internet, everything is about attention, except attention...

Every participant in a social platform acts as a rational agent seeking attention. This is not just cynicism: it is the consequence of the nature of online communication. Unlike a face-to-face conversation, where you already have the other's attention before you say something, your online posts may not be seen by anyone at all. If you don't figure out how to play the game, and get the algorithm to actually show your posts to other people, then you are wasting your time. You will eventually stop posting, and if you don't, it makes no difference. So while there may be people out there that don't care about attention, those people are not posting in social platforms. 

## Attention is about Influence

And what is attention about? It is about influence. Whether you are promoting your product, your political platform, your ideas, or yourself, if you have people's attention, you can influence their beliefs and behaviors.

That is why attention has enormous value. Advertisers pay billions of dollars to buy it. Autocratic states use force to monopolize it. Media seek to earn it with with scandal, terrorists win violence, politicians with fear. Social media is a massively multiplayer game with countless players interacting through the complex social media algorithms, constantly adapting strategies to win attention and influence for their various ends.

## Social Platforms Sell Attention

And what is the goal of the social media algorithms themselves? While there are certainly political agendas, the dominant motivation is just money. They are not trying to influence us so much as acquire our acquire our attention to sell for a profit. 

This is why social platforms direct our attention towards popular content. Because it engages us, not because it is good for us, and because as long as they have our attention, they can monetize it.
’
## Using Collective Intelligence to Direct Collective Attention

But what if a social platform was designed based on other principles. What **should** our attention be directed to if not popular content, or paid content?

The idea of X is to use the collective intelligence of the community direct its collective attention towards content that the community finds valuable. Towards content that is useful, entertaining, uplifting, trustworthy, and informative. And to promote intelligent conversation about this content.

This is not actually hard to do once we step away from superficial metrics such as upvotes and downvotes, which do not demand reflection, discussion, credibility, or trust. There is a big difference between a post that gets a lot of knee-jerk updates today, and one that "ages well": that people will still stand by after the truth has come out and the arguments have been made. 

A social algorithm should be based on measures not of popularity, but of informed opinion. It should measure not what people like, but what they think is true, civil, and relevant. It should direct people's attention not towards heated arguments but to constructive conversations.

.... link to reason-based ranking


## Summary

This is how our attention should be directed. 


# Discard



At the end of this conversation thread there will remain a small subset of **highly informed users**: those who have seed and responded to all the critical information that has been shared and arguments that have been made. And it is the deliberative judgment of these informed users -- the proportion who still endorse the rumor -- and not raw superficial popularity that determines whether or not the rumor deserves the attention of the community.

## Manipulation

Such a system is of course highly subject to manipulation. 

## The Details



---


. Promoting deep conversations requires nothing more than concentrating attention on depth. Promoting quality content requires concentrating the community's attention on judging quality.

This requires more than a simple button that allows users to say "this is deep! This is quality!" This would ultimately be just another upvote button, which is just a crude way of measuring popularity. Popularity metrics are superficial. Upvotes and downvotes do not demand reflection, discussion, credibility, or trust. There is a big difference between a post that gets a lot of knee-jerk updates today, and one that "ages well": that people will still stand by after the truth has come out and the arguments have played out. 

What we want to know is not how likely people are to upvote something after seeing it for a brief moment on their screen, but rather how willing they are to endorse it after reading the content, seeing the comments, and participating in the discussion. This requires concentrating the community's attention on deep conversation threads of informative comments, and using these conversations to determine which posts are most informed, in the judgment of the community.