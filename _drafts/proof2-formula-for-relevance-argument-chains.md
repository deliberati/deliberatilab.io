


## Bayesian Support

The terminology of Bayesian probability overlaps not coincidentally with the terminology of argumentation theory. Bayesian inference also involves evidence (premises) and hypothesis (conclusions), and quantifies exactly how strongly the evidence supports or opposes the conclusion (relevance).

Bayes theorem is:

    𝑃(𝐴|𝐵) = 𝑃(𝐴) * 𝑃(𝐵|𝐴) / 𝑃(𝐵)

But we can express the theorem simply as:

    𝑃(𝐴|𝐵) = 𝑃(𝐴) * 𝑆(𝐴,𝐵)   

where

    𝑆(𝐴,𝐵) = 𝑃(𝐴|𝐵) / 𝑃(𝐴)

This formula highlights the simple linear relationships between the probability of 𝐴 (`𝑃(𝐴)`) and the probability of A given B (`𝑃(𝐴|𝐵)`).

The coefficient `𝑆(𝐴,𝐵)` tells us exactly how much more or less likely some hypothesis `𝐴` is if some evidence `𝐵` is "given". Or in our example, it tells us how more or less likely a juror is to accept 𝐴 if they accept 𝐵.

### Support is Correlation

If we plug back the definition of `𝑃(𝐴|𝐵)` into the above formula for `𝑆(𝐴,𝐵)` we'll find that


    𝑆(𝐴,𝐵) =  𝑃(𝐴,𝐵)
             ---------
             𝑃(𝐴)*𝑃(𝐵)

Which makes it clear that:

    𝑆(𝐴,𝐵) = 𝑆(𝐵,𝐴)

So `𝑆(𝐴,𝐵)` can be thought of as a measure of the correlation between A and B.

This correlation is symmetrical, which means for purposes of inference, observing evidence 𝐴 provides the same support for hypothesis 𝐵, as observing evidence 𝐵 provides for hypothesis 𝐴.

Support is equal to 1 if 𝐴 and 𝐵 are totally independent, greater than 1 if the probability of them occurring together is greater than what they would b if they were totally independent, and less than 1 if is the probability is less than you they would be. So S(𝐴,𝐵) > 1 means B support A (and A supports 𝐵), and S(𝐴,𝐵) < 1 means B opposes A (and A opposes 𝐵).

## Informed Support

As we did when deriving formula (2), we want a function that gives us the informed probability of A -- the probability that a user will accept A given they have voted on all the arguments in the dependent argument chain A -> B -> C. 

But we want this formula in a form to be written in terms of the probabilities of B, !B, and C, and !𝐶 so that we can do Jeffrey Conditioning and plug in the probabilities from the sub-juries

Let's define `𝐽`  as the event that the user voted on both B and C (all the arguments in the dependent argument chain). 

    `𝐽 ⟺ (𝐵 ∨ !𝐵) ∧ (𝐶 ∨ !𝐶)`  
    

And let's define `𝑃ⱼ` as our new informed probability given 𝐽.

We can define `𝑃ⱼ(𝐴)` using our new concept of support.

    𝑃ⱼ(𝐴)
        =   𝑃(𝐴|𝐽) 
        =                  
            𝑃(𝐴,𝐵|𝐽)         // by law of total probability, since B and !B partition J
            + 𝑃(𝐴,!𝐵|𝐽)        
        =
            𝑃(𝐴,𝐵) * 𝑆(𝐴𝐵,𝐽)
            + 𝑃(𝐴,!𝐵) * 𝑆(𝐴!𝐵,𝐽)

        = (2.3)               
            𝑃(𝐴)   * 𝑆(𝐴,𝐵)  * 𝑃(𝐵)  * 𝑆(𝐴𝐵,𝐽)
            + 𝑃(𝐴) * 𝑆(𝐴,!𝐵) * 𝑃(!𝐵) * 𝑆(𝐴!𝐵,𝐽)

Note `𝑆(𝐴𝐵,𝐽)` is the degree to which voting on J supports acceptance of A and B together. 


We can see that this is just another form of the equation

    𝑃ⱼ(𝐴) = 𝑃(𝐴|𝐽) = 𝑃(𝐴) * 𝑆(𝐴,𝐽)

Meaning that:

    𝑆(𝐴,𝐽) =              (2.5)
        𝑆(𝐴,𝐵) * 𝑃(𝐵) * 𝑆(𝐴𝐵,𝐽)
        𝑆(𝐴,!𝐵) * 𝑃(!𝐵) * 𝑆(𝐴,!𝐵) * 𝑆(𝐴!𝐵,𝐽)

So we now can calculate 𝑃ⱼ(𝐴) in terms of 𝑃(𝐵) and 𝑃(!𝐵), allowing Jeffrey conditioning on B.

And it turns out we can define `𝑆(𝐴𝐵,𝐽)` in terms of `𝑃ⱼ(C)` and `𝑃ⱼ(!𝐶)`. Once again, we'll start with the law of total probability.

    𝑃ⱼ(𝐴,𝐵) 
        = 𝑃(𝐴,𝐵|J)                                            // by definition of 𝑃ⱼ
        = 𝑃(𝐴,𝐵|C,J)*𝑃ⱼ(C) + 𝑃(𝐴,𝐵|!𝐶,J)*𝑃ⱼ(!𝐶)               // law of total probability, since C and !𝐶 partition J
        = 𝑃(𝐴,𝐵|C)*𝑃ⱼ(C) + 𝑃(𝐴,𝐵|!𝐶)*𝑃ⱼ(!𝐶)                   // since by definition of J, C ⟹ J, and !𝐶 ⟹ J
        = 𝑃(𝐴,𝐵)*𝑆(𝐴𝐵,C)*𝑃ⱼ(C) + 𝑃(𝐴,𝐵)*𝑆(𝐴𝐵,!𝐶)*𝑃ⱼ(!𝐶)      // by definition of 𝑆
        = 𝑃(𝐴,𝐵) *
             𝑆(𝐴𝐵,C)*𝑃ⱼ(C) + 𝑆(𝐴𝐵,!𝐶)*𝑃ⱼ(!𝐶)

And so:

    𝑆(𝐴𝐵, 𝐽) = 𝑃ⱼ(𝐴,𝐵) / 𝑃(𝐴,𝐵) =
        𝑆(𝐴𝐵, 𝐶)*𝑃ⱼ(𝐶)
        + 𝑆(𝐴𝐵,!𝐶)*𝑃ⱼ(!𝐶)

I summary, we can calculate 𝑃ⱼ(𝐴) using these three formulas:

    𝑃ⱼ(𝐴) = 𝑃(𝐴|𝐽) = 𝑃(𝐴) * 𝑆(𝐴,𝐽)

    𝑆(𝐴,𝐽) =             
        𝑆(𝐴,𝐵) * 𝑃(𝐵) * 𝑆(𝐴𝐵,𝐽)
        𝑆(𝐴,!𝐵) * 𝑃(!𝐵) * 𝑆(𝐴,!𝐵) * 𝑆(𝐴!𝐵,𝐽)
 
    𝑆(𝐴𝐵, 𝐽) = 𝑃ⱼ(𝐴,𝐵) / 𝑃(𝐴,𝐵) =
        𝑆(𝐴𝐵, 𝐶)*𝑃ⱼ(𝐶)
        + 𝑆(𝐴𝐵,!𝐶)*𝑃ⱼ(!𝐶)

Which combined give us:

    𝑃ⱼ(𝐴) = 
        𝑃(𝐴) * [𝑆(𝐴,𝐽)]
            𝑆(𝐴,𝐵) * 
                𝑃(𝐵) * [𝑆(𝐴𝐵,𝐽)]
                    𝑆(𝐴𝐵,𝐶)*𝑃ⱼ(𝐶)        
                    + 𝑆(𝐴𝐵,!𝐶)*𝑃ⱼ(!𝐶)
            + 𝑆(𝐴,!𝐵) * 
                𝑃(!𝐵) * [𝑆(𝐴!𝐵,𝐽)]
                    𝑆(𝐴!𝐵, 𝐶)*𝑃ⱼ(𝐶)
                    + 𝑆(𝐴!𝐵,!𝐶)*𝑃ⱼ(!𝐶)



## Generalized Algorithm assuming Single Relevance chain

We can now generalize the informed support function 𝑆ⱼ(𝑋) for arbitrarily long dependent argument chains.

Suppose the argument 𝑋 is a conjunction of premises with possible negations such as (`𝐴`, `𝐴∧𝐵∧C`, `𝐴∧!𝐵∧!𝐶` etc). 

Suppose `𝐸(𝑋)` provides the next premise supporting or opposing argument in the chain, if there is one. So for example:

    𝐸(𝐴) = 𝐵
    𝐸(𝐴∧!𝐵) = C
    𝐸(𝐴∧𝐵∧C) = ∅

Then, given 𝐸 and our underlying probability measure 𝑃:

    𝑆ⱼ(𝑋) = 
        if 𝐸(𝑋) = ∅
            return 1/𝑃(J)
        else 
            let 𝑌 = 𝐸(𝑋)
            return
                𝑃(𝑌) * 𝑆(𝑋,𝑌) * 𝑆ⱼ(𝑋∧𝑌)
                𝑃(!𝑌) + 𝑆(𝑋,!𝑌) * 𝑆ⱼ(𝑋∧!𝑌)



It should be clear that this function works correctly for arbitrarily long dependent argument chains.

Now, using the same logic for deriving (2), we can use the fair deliberation assumption to calculate the hypothetical probability 𝑃ₕ that the average fully-informed juror would accept the conclusion, by simply plugging the values from the sub-trials into the formula for the informed probability. 

Now, the claims being argued in the sub-trial may themselves has arguments



Defining 𝐸(𝑋) as above, and defining 𝑃(𝑋) = c(X)/cnt(*), we have:

    𝑃ₕ(𝑋) = 𝑃(𝑋) * 𝑆ₕ(𝑋)

    𝑆ₕ(𝑋) = 
        if 𝐸(𝑋) = ∅
            return 1/𝑃(J|X)
        else 
            let Y = 𝐸(𝑋)
            return
                𝑃ₕ(𝑌) * 𝑆(𝑋,𝑌) * 𝑆ₕ(𝑋∧𝑌)
                𝑃ₕ(!𝑌) + 𝑆(𝑋,!𝑌) * 𝑆ₕ(𝑋∧!𝑌)


So for example, if 𝑋=𝐵:

    𝐸(𝐵) = ∅
    𝑆ₕ(𝐵) = 1/𝑃(J)
    𝑃ₕ(𝐵) = 𝑃(𝐵) * 1/P(J) = Pj(𝐵)

Likewise if X=ABC 
    
    𝐸(ABC) = ∅
    𝑆ₕ(ABC) = 1/𝑃(J)
    𝑃ₕ(ABC) = P(ABC) *  1 / P(J) = 𝑃j(A,B,C)

if 𝑋=AB:

    𝐸(ABC) = ∅
    𝑆ₕ(A𝐵) = 
        Y = C
        return
            𝑃ₕ(C) * 𝑆(A∧B,C) * 1/𝑃(J)
            𝑃ₕ(!𝐶) + 𝑆(A∧B,!𝐶) * 1/𝑃(J)



