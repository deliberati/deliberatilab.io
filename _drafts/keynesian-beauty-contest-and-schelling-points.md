
## The Keynesian Beauty Contest

A situation like the one  we've described, where everybody is trying to act with the majority, is sometimes called a [Keynesian Beauty Contest](https://en.wikipedia.org/wiki/Keynesian_beauty_contest). This is a hypothetical beauty contest first described by John Maynard Keynes, where the winners are not the contests, but the judges who agree with the majority of other judges. What this means is that if judges really want to win, their answers are determined entirely by their **expectations** of other judges. These expectations can become completely disconnected from their actual opinions about the "beauty" of the contestants. Keynes argued that many phenomenon in economics could be compared to Keynesian Beauty Contests: for example the price of an asset during a bubble.

The consensus protocols we've been discussing here, whether used to come to an agreement on the amount of cryptocurrency in a wallet or the capital of a country, are examples of Keynesian Beauty Contests. But the key to making consensus protocols work is that people's expectations are not completely disconnected from reality. Rather, **people expect each other to tell the truth.**

But why?

## Schelling Points

todo: we already said that the truth is easiest to coordinate on.

Buterin argued that the truth is what is called a [Schelling Point](https://nav.al/schelling-point), or [*focal point*](https://books.google.es/books?redir_esc=y&hl=es&id=7RkL4Z8Yg5AC&q=%22some+clue+for+coordinating+behavior%22#v=snippet&q=%22some%20clue%20for%20coordinating%20behavior%22&f=false).



<!--attribution

    https://unsplash.com/@hughstevensonn?utm_medium=referral&utm_campaign=photographer-credit&utm_content=creditBadge

    Photo by Hugh Stevenson on Unsplash
-->


The economist Thomas Schelling argued that, in situations where people need to somehow coordinate their actions without communication, they seek:

> ...some clue for coordinating behavior, some focal point for each person's expectation of what the other expects him to expect to be expected to do.
> 
> -- Thomas Schelling, the Strategy of Conflict, 1960
{: .notice--info}

<img src="/assets/images/honesty-on-the-internet/grand-central.png" alt="Schelling points" style="max-width: 350px" align="right"/>

For example, in an experiment where students in separate rooms were asked "Tomorrow you have to meet a stranger in NYC. Where and when do you meet them?”, again without being allowed to coordinate their answers, the most common choice was “Noon at (the information booth at) Grand Central Terminal”.

Schelling points tend to be things that people expect each other to see as **unique, logical, important, funny, or salient in any way**.

Vitalik Buterin argued that in many cases:

> ...the truth is arguably the most powerful Schelling point out there. 
> 
> -- Vitalik Buterin, [SchellingCoin](https://blog.ethereum.org/2014/03/28/schellingcoin-a-minimal-trust-universal-data-feed/), 2014
