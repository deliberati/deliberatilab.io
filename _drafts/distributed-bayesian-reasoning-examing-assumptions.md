
# Examining Assumptions

## Causality and Fair Deliberation


## Causality, Fair Deliberation, and Justified Opinion

The third difference is very significant.

In reality people will often have reasons for their beliefs that are not explicitly stated. For example, if the jurors who voted guilty were to concede that the DNA evidence was not conclusive, they wouldn't necessarily change their minds about the verdict. Perhaps they think the defendant **looks** suspicious, and must be lying about something.

But when modeling the meta-reasoner, we assume that no unstated reasons exist, and as a result the meta-reasoner may come to a different conclusion than the average juror would come to if they were fully-informed. 

However, this delta only exists in cases where the argument is incomplete. If jurors were asked **if they would still vote guilty** if they conceded that the DNA evidence was not conclusive, then that provides the information necessary to assume causality (this is of course assuming the jurors are being honest, but this is a separate issue).

This means that the **process** used to generate the argument graph and collect votes is critical. If a sub-jury rejects some premise, jurors who justified their vote with that argument must be given a chance to either concede and change their vote, or stick to their vote and provide a reason.

The **fair deliberation** assumption is the assumption that the data used to generate the argument graph and jurors votes is the result of this basic process, where everybody has had a chance to give reasons for their opinions and respond to the reasons of others.


## Fair Deliberation



The validity of chaining these inferences together is clearly the point of this model most vulnerable to criticism. 

Our justification for making **causal** inferences about what people **would** believe is based on what we call the **fair deliberation assumption**. This assumption says that if participants in the discussion have been given a chance to give reasons for their beliefs and respond to the reasons given by others, we can assume that there are no hidden (latent) reasons for their beliefs and thus assume a causal relationship reasons given and belief.

We discuss the **fair deliberation** assumption and the conditions under which causal inferences about belief are justified in [another part].

---

Suppose after the subjury rejects the DNA evidence, the jurors who voted guilty were asked if they want to change their votes. We may be surprised to find that even if the jurors say they accept the decision of the subjury, they still stand by their guilty vote. The problem of course is that we can't assume any sort of causal relationship between votes. The group that voted guilty may have other unstated reasons for their guilty vote, and the strong correlation between acceptance of the DNA evidence and final verdicts reflected a common underlying cause, such as personal feelings for the defendant.

In the introduction we dispensed with this difficulty by asserting that no other reasons were advanced and therefore none existed. We call this assumption the **fair deliberation** assumption.


We have dispensed with this problem until now by asserting that no other reasons were brought up during trial, and thus they could be assumed not to exist. If we relax this assumption, then in order to know what how jurors *would* vote if the DNA evidence was rejected, we need to ask them. And if they say they would still vote guilty, we need to ask them, why? 

Jurors who think the defendant is guilty might respond for example that the defendant signed a confession. Advocates for innocence can then challenge this premise or provide some other justification innocence. As the deliberation uncovers more and more of the reasons that people justify believing this or that even if they accept these other things, it accumulates data about how **combinations** of reasons effect belief. 


And here is the key assumption behind this methodology: The **fair deliberation assumption** says that as the argument  continues and approaches a point where arguments are no longer changing minds, **the model becomes predictive**.

We will describe the justifications for, and implications of this assumption in an upcoming essay on fair deliberation.



---

When estimating the opinion of the hypothetical fully-informed juror, the system assumes that all the reasons that jurors have for their beliefs have been given. We call this assumption the **fair deliberation assumption**.

This assumption is very powerful because it implies a causal relationship between beliefs about evidence and beliefs about the conclusion (because we have dispensed with all latent confounding variables by assuming they do not exist!). 

In our example, if a juror who voted guilty were to concede that the DNA evidence was not conclusive, they might maintain their guilty vote because because they figure the guy is lying about something.

But if they don't actually justify their guilty vote with a reason, the system will assume no such reason exists. And if *no other reasons exist* for voting guilty other than the DNA evidence, then there exists nothing that would cause people to vote guilty if they reject the DNA evidence. So the DNA evidence is thus assumed to be the unique *cause* of people's votes.

This assumption of causality lets us build a [Bayesian Network](https://www.cs.ubc.ca/~murphyk/Bayes/bnintro.html) that estimates what the hypothetical fully-informed average participant in a deliberation would believe **if the fair deliberation assumption holds**. 

TODO: Further, an argument can reach a state of **convergence**, where we can assume fair deliberation holds.


## Fair Process

But when does this assumption hold?

A fair trial requires a fair process, where each side has a chance to make their arguments and respond to the arguments 
of the other side.

This process can take place in a real-world venue such as a courtroom, but large-scale distributed reasoning is probably only feasible in an online context. The [Deliberative Poll](/the-deliberative-poll) describes a starting point for a fair deliberation process in a social platform.

Distributed reasoning additional requires some method for creating sub-juries, or groups that independently argue over the acceptance of claims. This raises the difficult question about what exactly is a *claim*, and how to identify what claims are being made in an arguments, and to ensure that the meaning of the claim is sufficiently unambiguous to all participants. This may be one of the more difficult design challenges. One group that is working on solving this problem is the [Canonical Debate Lab](https://canonicaldebatelab.com/).

TODO: Argument Mining


[In less formal contexts, such as discussion in online social platforms, a fair "trail of ideas" requires the participation of a group of sufficiently reasonable people that can competently advocate for different points of view, and a process that the best arguments on both sides are seen and responded to. The [Deliverative Poll] describes a starting point for a fair deliberation process.]



## Reasonable Juries

A fair trail also requires a reasonable jury.  No matter how fair the deliberation process is, the results still depend very much on the group involved in the discussion: their shared values and epistemic culture, the diversity of knowledge and ideas, as well as individual honesty and intelligence.

And the group decision to will only be **perceived** as fair if the participants are perceived as reasonable and unbiased.

This poses tremendous challenges for any sort of **open**, public deliberation platform.

First, because of the vast differences in core values and belief systems throughout society, any reported result will inevitably be biased. This method does not reveal objective, universal truths: it reveals the justified opinion of whatever group uses it. It may end up being the justified opinion of religious conservatives, or social justice liberals, or hockey fans. The results will be better than mere opinion polls, but no amount of information will bring the average conservative and average liberal to converge on the same opinions on certain fundamental issues, no matter how fully informed.

And as we've seen in countless online platforms, perception of bias will tend to be self-fulfilling prophecy. This method cannot, on its own, prevent echo chambers.

Second, Distributed Bayesian Reasoning is especially vulnerable to coordinated manipulation attempts and Sybil Attacks (fake accounts), because the mathematical formulas provided in this paper point to the exact points in the argument graph where focused attacks can be most effective in changing the justified opinion. 

Groups with membership restrictions such as businesses, community organizations, and clubs can form closed online communities where the participants share some degree of trust. 

In open communities, trust and reputation systems can address problems of large-scale deliberate manipulation, and healthy feedback loops can be nurtured that rewards civil discussion, credible claims , and convincing argument. But these systems cannot prevent bias as long as participation is open.

The only way to reap the advantages of large scale, open distributed discussion is to *decentralize* justified opinion calculations. That means there is not one reported result for the entire globe. Instead, the system could report the justified opinion **among people in your social network**.

Going further, the system can look at past discussions to estimate the probability of any two users coming to the same justified conclusions after deliberation, and induce a network of trust based on this.

This induced social network of people *you* have found rational and they have found rational in the past

discover the opinion justified among this virtual social network. 
Indeed this result would be an individualized estimate...


Justified opinions could be calculated making each individual their own nexus of trust, with individualized estimates of the probability that they would believe something **if they were informed of all the arguments that had been made by everyone in the entire argument graph**, weighted by trust.


## Reason Checking Falsehood

Falsehood Flies

## Feedback Loops

## Positive Feedback Loops

The algorithms that social platforms use to filter, sort, and promote content are generally based on popularity: they feed people more of the content it thinks they will like. Or worse, they are based on engagement: they feed people what they don't like but engage with anyway because it makes them angry: outrage, misinformation, divisive and shallow content.

And users learn to produce that kind of content in order to win attention and followers.

An algorithm that was based on justified opinion, instead of popularity, would create a healthier feedback loops. Well supported, intelligent posts would be rewarded over content that cannot stand up to scrutiny.




These algorithms create unhealthy feedback loops that reward peole

Ranking algorithms can contribute to perverse feedback loops that amplify misinformation and abusive, divisive, and irrational behavior.




Credibility instead of popularity and initial impression. Stand up to scrutiny.

Understood that unhealthy feedback loops created. Replace these with feedback loops that reward people for posting content that stands up to scrutiny.

Simple misinformation example


Identifying these differences...promote more healthy conversations.


Abuse: should not be taken lightly
	inauthentic campaigns
	coordinated manipulation


[In addition to the kind of reputation and incentive systems discussed above, online platforms can foster reasonable juries and healthy discourse using moderation and meta-moderation, trust networks, deliberate avoidance of echo chambers, and sculpting of online communities to encourage formation of groups with mutual trust despite diversity of opinion. These and other possible techniques are beyond the scope of this paper.]

And it may require a great deal of time.

In reality, then, a fair deliberation is an ideal that can only be approached. The justified opinion of the group is a just opinion to the extent that the deliberation has been fair.


TODO: decentralized trust.



Truth the advantage
People respond to incentives, and activity in online social platforms is motivated above all by one incentive: attention. Whether the ultimate goal is personal gratification, social status, political influence, or monetizeable social media traffic, attention is the scarce resource for which people compete. If our social media activity did not garner attention, we would be shouting into the void. 

Social media platforms use algorithms to decide which content to promote and amplify in our feeds and notifications, and which content to censor or bury at the bottom. 

The combination of human motivation social algorithms results in very unhealthy feedback loops. The platform essentially trains users to produce attention-generating content and punishes content that does not elicit engagement based on initial reactions.

The result is trolling, abuse, outrage, misinformation and suppression of information, echo chambers, polarization and extremism


Social 
Attention-oriented social 


The developers of online social platforms are also motivated by attention: for ultimately their business model is to monetize it, by showing us ads or gathering data that can be monetized in other ways.

These platforms reward users who are able to generate attention with greater influence and reach. 


It is the currency of the online economy. 

So the platforms reward attention-grabbing content, by boosting it to the top of feeds and notifications. Attention-seekers learn what kind of content is popular and engaging, and adapt to generating even more of it.

Add what kind of content generates attention? Whatever grabs our attention *on first impression*.



And what 
As a result, attention-grabbing content is amplified even more than it would be naturally.

: trolling and abuse, outrage and controversy, and rumors that are scandalous and just believable enough to go viral. 


what grabs attentino?


These unnatural feedback loops reward 

And users are 
 where the sorts of things that have engaged our attention in the past become the only things we see. It rewards 
It rewards influencers that can find their niche and build an echo-chamber. It rewards polarization and extremism.


, and where consumers of content are put into a rut that feeds them the sort of things they like to see.

An algorithm based on distributed Bayesian reasoning would result in a very different type of feedback loops, where ultimately users are rewarded with attention for posting content that stands up to the scrutiny of their peers in the social network, and for making arguments that influence others.

This method also opens the possibility for reputation systems based on credibility instead of popularity.


## Decentralized Trust

One obvious solution to the problem of trust is to estimate, for each individual, the **justified opinion of the people in that person's social network.** In a sense, this is using the social graph to approximate the networks of trust and reputation that naturally modulate the flow of beliefs through society. If a person's social network is very narrow and homogeneous, this justified opinion may not be very informed, but it will still be better than a raw opinion poll.

Collaborative filtering is another possible solution. Collaborative filtering is the method behind recommendations of the form "people who bought X also bought Y", though it has an alternative, lesser-known variant that can be summarized as "if Bob likes X, these other people will like X". 

This latter is a kind of hidden reputation system that is commonly employed in social platforms (though few users realize it). 

A similar method could base reputation not on similar preferences (liking the same things), but on similar **judgment**. Two people form the same judgment if they accept the same conclusion **given the same premises** in the argument graph. 

Using similarity of judgment to (select) the members of the jury allows an individualized estimate of the justified opinion: it estimates what people with similar judgment would believe if they were aware of all the arguments in a fair deliberation. 


If two people tend to form the same opinions based on the same evidence, it indicates that they have similar priors. Recall that Distributed Bayesian Reasoning forms the priors of the hypothetical, fully-informed Bayesian reasoner by averaging the votes of the jurors. If the jurors are selected because they have similar priors to you, this can be seen as an estimate of *your* priors.  

Therefore the estimate of the justified opinion of a group of people with similar judgment can be seen as an estimate of the opinion **you would have**, given you were aware of all the arguments in a fair deliberation. 


If such estimates are accurate, and individuals also trust the **system** that produces these estimates, the problem of trust is solved. For it is no longer a question of trusting any particular group of people

not just feeding you back what you want to hear

[For example, I may originally like a friend's post (an indication of agreement), but then after seeing a comment from a second friend explaining the post is an unfounded rumor, I might change my vote. This would strengthen my second friend's credibility with me, and weaken the first friend's (unless perhaps they retracted their post).

my redibility would be harmed with these friends (and indirecdtly, others in my social network)

Weaken my credibility with those friends. If I think posted a reply arguing that the rumor was very true, and those friends did not reply, their credibility with **me** would be harmed...unless they changed their vote.
]


## Credibility Networks

Reputation based on similarity of judgment can be seen as **credibility**. The concept of a trust network, or web of trust, can be (recast) as a credibility network  

Collaborative Filtering can be seen as a way of automatically connecting people . The resulting social graph 

This social graph can be used to predict similarity between people who haven't liked, purchased, or watched any of the same things.

A great overlooked potential
