
Equivalences:
    sets of coordinate arguments
        10 pro voters: all 10 accept one same pro = 1 person each accepts 10 pros = all accept all 10 pros
        10 pro voters, 10 con voters: 10 accept one pro, 10 reject same pro = 10 accept one pro, 10 accept one con

    counter-arguments
        10 accept one pro, 10 reject same pro, 10 add counter-argument....


questions:
    - does 10 accept/10 reject on one pro cancel out? Or are reject votes required to justify with reason?


concepts:
    - idea of a *partially justified* ballot: votes on complete thread



5:5   <- 5:0 = 5:5   <- 10:0 = bavg(5:5  , 10:0)
      <- 5:0 


5:5   <- 5:2 <- 0:2 = bavg(5:5  , 0:2) <- 0:2 = bavg(5:5  , 0:2, 0:2)


# two pros, one counter
    κ=10
    5:5   <- 5:2 <- 0:2 = 5:5   <- .48 * 7 = 3.36:3.64 = 7.49:6.51  = .535
             5:2                <- .59 * 7 = 4.13:2.87

    This scenario, should be higher than previous because second pro is not contested.
    5:5   <- 10:4 <- 0:2 = 5:5   <- .5 * 14 = 7:7

    Below a very strong 3rd-level challenge reverses the votes on the first pro from 5:2 to .35:6.65. This means the 
    more people vote in support of that pro, the more the final result moves in the opposite direction. This doesn't seem right does it? Rather than reversing, the effect of the pro should just be nullified. The result should approach the result for the
    second pro (.59). Oh, the problem here is it doesn't make sense for there to be **more** votes on deeper challenges, such participants always vote on the *whole chain*, so at each level it is a subset of users from the higher level.

    5:5   <- 5:2 <- 0:100 = .05 * 7 = .35:6.65   = 4.48:9.52 = 0.32
             5:2          + .59 * 7 = + 4.13:2.87

    Let's take defeasible logic approach. Structure is
    A     <- B   <- C
             B2
    Propositions are
        d(A)
        d(A<+B)
        d(A<+B<-C)
        d(A<+B2)

    v(A) = 5:5   =~ .5
    v(A<+B) = 5:2 =~ .59


    if we say A<+B overrides A. In a way over-riding is like defeating. They both subsume the other argument...

    Pr(A) = Pr(A<+B or A<+B2) = 5 + 5 / 7 + 7 = bavg(10:14) = .63
                              = .59*7 + .59*7 / 7+7 = .59

    if we have counter to B

    Pr(A<+B) = Pr(!A<+B<-C) = .05

    Pr(A) = Pr(A<+B or A<+B2) = .05*7 + .59*7 / 7+7 = 4.48 / 14 = 0.32

    this is the same result as above. 


# pro and con. These examples show that taking the weighted sum of two bayesian averages gives similar, but not identical result, as bayesian average of sum 

    5:5   <- 5:5 =~ 1/2

    vs

    5:5   <- 4:1 =~ .6 = ( (.6, .4) ∙ (5, 5)) / ( (1,1) ∙ (5, 5) ) = .5
             1:4    .4

    vs


    5:5   <- 5:0 =~ .67 = .67*5 + .33*5 / 10 = 1/2 
          <- 0:5    .33


    vs

    5:5   <- 5:2 =~ .588
    vs
    5:5   <- 5:0 =~ .667 = ( .667*5 + .417*2 ) / 7 = 0.596 (geometric: 0.583)
             0:2    .417
    close not exact

    vs

    5:5   <- 50:10 =~ .786 = ... = 0.631 (geometric: 0.534)
              0:20    .167
    vs
    5:5   <- 50:30 =~ .631
    hmm, exact


    5:5   <- 50:10 =~ .786 = ... = 0.672 (geometeric: 0.656)
             20:20    .5
    vs
    5:5   <- 70:30 =~ 0.682
    close not exact

    T = bavg(50:10,5:5) = p1+a / (p1+n1+a+b) = 50+5 / 70 = .786
    U = bavg(20:20,5:5) = p2+a / (p2+n2+a+b) = 20+5 / 50 = .5
    V = bavg(70:30,5:5) = (p1+p2+a) / (p1+p2+n1+n2+a+b) = 75 / 110 = .682

    can V be written as a weighted average of T and U?


    T = p1+a / D1
    U = p2+a / D2


    M = T*D1 = p1+a
    N = U*D2 = p2+a

    V = ( M + N - a ) / ( D1 + D2 - a - b)





okay here is an important question. Can we do these calculations where we do the weighted average of all the L2 arguments first, then use those as priors to do Bayesian averaging of the L3 arguments, then weighted average of those?

    5:5   <- 5:2 <- 0:20 = .16 * 7 = .265
             6:1 <- 1:1    .37 * 7 


    vs:

    5:5   <- 11:3 <- 1:21 = 0.19


    big difference, because the strong counter applies to both B0 and B1, whereas in the first case it is in effect "wasted" on just B0.


    vs alternative calculation, where we pool the L1 calculations, then apply L2 args against the whole pool (instead of just what they argued against). 

       5:5   <- 5:2 <- 0:20 = 5:5   <- 11:3 ) <- 0:20 = 0.667 <- 0:20 = 0.17 * 2 = 0.270
                6:1 <- 1:1                       1:1             1:1    0.37 * 2


    This is just a tiny bit higher then .265, because the strong counter is countering a very slightly weaker prior after pooling. This example might not be very illustrative.

Okay, what about if we have overlap in votes in set of coordinate premise arguments. For example below is the same as above, but we have people who voted for both B and B2 (4:0). So we subtract these from the votes for just B and B2.

    5:5   <- 3:2 <- 0:20 = .15   * 5 = 0.348
             4:0         + .643  * 4
             4:1 <- 1:1  + .36   * 5

Okay this is NOT good because it means the middle group has more weight, but they should have less because they have not responded to counter-arguents....



Okay we have this dilemma where somebody supporting A but voting against a bad-argument on his side can harm his side. This should not be the case. But one solution is that each vote is counted **only once in the tree**. The person who casts a vote deeper in the tree (taking a more justified position) has more weight.


So in this example, everybody else votes against the bad argument, but the good argument holds pretty well.

    NOT CORRECT:
    5:5     <- 5:15
               5:2  

Okay, but is there overlap? There are only 20 voters. So let's say two voters voted against both (TODO: is vote against weak argument wasted?). So level two votes of the two overlap voted are applied half and half to the two arguments. 

    NOT CORRECT:
    5:5     <- 5:14
               5:1

Except now we are double counting the 5 voters who voted for the good argument and against the bad argument. Distributing those half-half we get


    5:5     <- 5.0:11.5 = 0.377 * 16.5 = 8.1665:11.8335 = 0.408325
               2.5:1      0.556 * 3.5

Okay this is not good because those benevolent voters shoot themselves in the foot for voting against the bad argument.

What we really need is for the arguments of those people to have extra weight for being justified to the second level, but for that weight to apply to the pro side. And because the con side did not strongly counter the second argument, the con side should be out-voted.

Okay key insight here is that the 5 benevolents are *more informed*. They voted on B0 and B1. So the way to set it up is this! Note the sum of votes in the the last two terms is 20. People don't get to vote at each level in the hierarchy.

    5:5   <- 5:10 <- 5:0 = 0.61


Okay this is very interesting. We treat B1, even though it was not a counter argument to B0, as if it was. But that's not really it -- it is just that in the hierarchical model B0&B1 falls under B0. Or rather, under B0|B1. So really we should do this?

    5:5   <- 5:9            = 14 * 0.417 =        
             0:1 ) <- 5:0   + 1  * 0.455 ) <- 5:0 = 0.420 <- 5:0 = 0.624

This is saying that we first do the computation for the B0|B1 group, and then the expectation for that group determines the prior of the B0&B1 group. 

Now, what if the benevolents weren't so? 

Well then, their votes are counted in the B0 group, not the B0&B1 group, so: 

    5:5   <- 5:9 = 0.417 * 14 = 0.4794
             5:1   0.625 * 6

What if they voted FOR both B0 and B1? Well one way of looking at this is that it is exactly the same as if the benevolent case (0.624). We still start with estimate of theta for those who voted for the 15 who voted for B0|B1, and then from this estimate the vote for those who voted for B0&B1. 

This doesn't seem right though. It rewards just upvoting everything, even weak arguments. When scoring JUST B0, it should result in a lower score for A<-B0, but the net result for the A is the same whether they upvote or downvote B0. This is desirable, but we would like benevolent votes to actually be better for A then cheap votes. 

One issue is we are using the same κappa for these groups, treating voting on a conjunction the same as justifying with reason.

So maybe, grasping a little bit here, we treat these situations differently. In the benevolent case, we let the vote count twice: once as a vote against the pro, but then to maek up for that sacrifice they get treated as a separate group.

So benevolent case is:

    5:5   <- 5:14            = 19 * 0.350 =        
             0:1 ) <- 5:0    + 1  * 0.455 ) <- 5:0 = 0.35525 <- 5:0 = 0.589

And the double-vote for B0/B1 group is the 0.4794 case above.

This isn't much different from the 0.61 case. In fact if we just lowered kappa we'd get the same result. Would this be so in all cases? Dunno, but raises the possibility of dealing with these cases by just using a different kappa.


And what if cons don't vote against weak argument and thereby increase their vote against strong argument? Well if we do the calculation that led to 0.61, the results are identical. 

So I think what we want is to do calculation as in 0.61. We assign everyone one vote, and then count the level 2 votes (votes on A<-B0 and A<-B1), but carve out a group that has voted against an argument on their side -- or for an argument on the other side -- and treat them as if they had provided a counter-argument. Let's say in fact that some users on the con side had conceded B1 as a good argument but did not change their vote. Then the math would be


    5:5   <- 5:8 <- 5:2 = 0.56

Upvoting a good argument on the other side moves them into the "informed" group.

So this is interesting, it means that when evaluating a group of coordinate arguments it is more the "vote structure", not argument structure, that matters? 5 people on the pro side voted for something and against something else. 2 users on the con side voted against something and for something else.


If there is a pro and an argument supporting the pro, and a few people on the pro side pile on to that argument, it shouldn't benefit the pro side. Or should it? Well why not? Obviously we don't want people manipulating this ability. So if somebody piles on to a poorly-performing argument with a lame additional argument, then the data should reflect that. The ratio for this argument won't be significantly higher than the argument is supporting, and if it it is, it means that people who accept that argument are more likely to accept the root claim, which is exactly what we want to know.


Okay an aside: accepting an argument vs accepting a premise. We might say that if a user accepts premise B and accepts A then they accept A <- B. But they might not have contemplated that argument. The might have accepted reusable claim B in some other context. What we want to know is the probability that they believe A given they think B is a reason to accept A. 

Or more specifically, given they think that B is a reason to accept or reject A, or is relevant to A, or they have considered the relevance of B to A.

So what do we do with the information, or the fact, that a user accepts vs rejects? It's an additional bit of information. Rejection of A<-B allows us to deduce that they think B is NOT sufficient reason for A. Do they think it is necessary? Rejection of A<-!B would mean they don't think it is necessary. Note these statements apply whether B or !B supports or opposes A.

Let's say rejection A<-B means B is not significant to A (which implies it is also not necessary). Let's say rejection of A<-!B means !B is not significant to A (which implies B is not sufficient). Let's say rejection of both A<-B and A<-!B mean B is not relevant. 

Acceptance of arguments allows us to give credit to specific arguments. But it doesn't actually affect justified opinion of root claim.

Wouldn't that create paradoxical situations where users reject A<-B but seem more likely to accept A given rejection of B? Hmm, it could be that B is interpreted as an opposing argument but actually functions as a supporting argument (I need to get my notation straight. At the moment I am using A<-B to indicate argument, without differentiating supporting/opposing). So they reject it as an opposing argument but weren't actually aware of B and it actually convinces them.


Okay so let's go back to two premise arguments, where one has a counter.

    5:5   <- 50:30  <- 0:10 = 0.33 * 80  = .64
             100:10           0.88 * 110



Okay now let's say there is overlap B0&B1 of 20:0. But no B0&B1&C0.

    5:5   <- 30:30 <- 0:10
             20:0
             80:10      

We could rewrite so that B0&B1 as a separate group under the B0 group

    5:5   <- 50:30 <- 0:10
                      20:0
             80:10      

And then merge

    5:5   <- 50:30 <- 20:10 = 0.64 * 80 = 0.75
             80:10            0.85 * 90


This is higher because the overlapping votes are 2:1 for, and these now have more weight because they are treated as second level arguments.

Now I could have calculated that differently, putting B0&B1 under B1.

    5:5   <- 30:30  <- 0:10  = 0.28 * 60  =  0.70
             100:10 <- 20:0   0.93 * 110


This is very different. Oh maybe we need to put the overlap under both.


    5:5   <- 50:30 <- 20:10   = .64 * 80    = 0.81
             100:10 <- 20:0     .93 * 110 

    weighted average exclusive only

        .64 * 60
        .93 * 90 = 0.814

Or finally, I can make B0 *with or without* B1 the prior for B0&C0, and still putting B0&B1 under B1

    5:5   <- 50:30  <- 0:10  = 0.28 * 80  =  0.67
             100:10 <- 20:0    0.92 * 110





### repeat, different numbers

Okay now let's say there is overlap B0&B1 of 20:10. But no B0&B1&C0.

    5:5   <- 30:20 <- 0:10
             20:10
             80:0      

We could rewrite so that B0&B1 as a separate group under the B0 group

    5:5   <- 50:30 <- 0:10
                      20:10
             80:0      

And then merge

    5:5   <- 50:30 <- 20:20 = 0.53 * 80 = 0.74
             80:0             0.95 * 90


This is higher because the overlapping votes are 2:1 for, and these now have more weight because they are treated as second level arguments.

Now I could have calculated that differently, putting B0&B1 under B1.

    5:5   <- 30:20  <- 0:10  = 0.33 * 50  =  0.59
             100:10 <- 20:10   0.71 * 110


This is very different. Oh maybe we need to put the overlap under both.


    5:5   <- 50:30 <- 20:20   = .53 * 80    = 0.64
             100:10 <- 20:10    .71 * 110 


Or finally, I can make B0 *with or without* B1 the prior for B0&C0, and still putting B0&B1 under B1. This is important, otherwise weight for B1<-B0 thread counts B0&B1 overlap, but thread for B0<-C0 does not.

    5:5   <- 50:30  <- 0:10  =  0.33 * 80  =  0.55
             100:10 <- 20:10    0.71 * 110


Overlap for both is good symmetry. It says, first of all, among the 80 percent of people who saw B0, there is a 53% chance of acceptance -- after considering counter to B0 (including B1 and C0). And of the 140 people who saw B1, there is a 67% chance of acceptance, after considering counters to B1 (including B0). Both statements stand in their own right. It is intuitive to just weight each at this point...

But it results in the B0&B1 group having a lot of weight and bringing up the average a lot.

So let's look at B0|B1|C0, exclusive. That would be B0=30:10 + B1=80:30 + C0=0:10 Then B0&C0=0:10 + B0&B1=20:10.

Ok, I am thinking that we should actually put B0&B1 and B1. So we have B0&C0, then B1&B0, as separate branches, weighted as B0 and B1 respectively. Reasoning? B0 could have been submitted as a separate but duplicate argument B0' as a counter to B1. In that case, we have two separate clean threads B0&C0, B1&B0', for which we can do a weighted average. We estimate B0&B1&C0 =~ B0&(B1 | C0). That means we want B0&C0, B1&B0. But why weigh the branches B0 and B1 respectively? 

Because we are assuming that there is a confounding variable that affects the probability that somebody *starts* with the B0 or B1 thread. We need to therefore control for the starting point. Which means calculating our probability given someone participated in one or the other thread, and then doing a weighted average. I think that this is similar to Pearl's backdoor correction....


Now on the other hand, B1 could have been submitted as a separate but duplicate argument B1' as a counter to B0. But if we do that, B0 by itself has very large weight, un-countered (or amplified) by B1. And then we are averaging B1 by itself with B0&(C0|B1). If we *only* had B1 under B0, then this would make sense. But we also have B0&B1. So we need the calculation to include our distinct groups B0&B1, and B0&C0. We can do this using threads B1<-B0 and B0<-C0.

What if we also have C1? So B0&C0, B1&C1, B0&B1. Then we have B0&B1&C1 =~ B0&(B1|C0) or B0&B1&C1 =~ B1&(B0|C1)

So it means we need all the paths B0<-C0, B0<-B1, B1<-C1


avg(B0&B1, B0&C0) or avg(B0&b1, B1&C1). Could that simply be B0&B1&C1 =~ B0&B1|B0&C0|B1&C1?

Which can be factored 

Factorization 1: B0&(B1|C0) | B1&C1

Factorization 2: B1&(B0|C1) | B0&C0

Could it be the results are the same?

Example

    5:5   <- 30:30 <- 0:10
             20:0
             80:10 <- 0:10

Using Factorization 1


    50:30 <- 20:10
    80:10 <- 0:10


Using Factorization 2

    30:30 <- 0:10
    100:10 <- 20:10








      

5:5   <- 5:5 <- 0:10
      <- 5:5 <- 0:10
vs
5:5   <- 10:10 <- 0:10
vs
10:10 <- 5:5 <- 0:10
      <- 5:5

without bayesian averaging...
2:1 <- 1:0 <- 0:1 = 2:1 <- 0:1 = 2:1 <- 1:1 = 2:1 
    <- 1:0              <- 1:0

