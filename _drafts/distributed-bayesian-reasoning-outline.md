# Distributed Reasoning Outline


TODO: Example of strong/weak warrant argument

# Hierarchical Models




## Integrating with Justified Opinion Formula

Putting it all together:

$$
    P_v(𝐴=1|B≠b) = \frac{ω(κ - 2) + 1 + c(A=1,B≠b)}{κ + c(B≠b)}\\
    P_v(𝐴=1|𝐵=b) = \frac{𝑃_v(𝐴=1|B≠b)(κ - 2) + 1 + c(A=1,B=b)}{κ + c(B=b)}\\
    P_h(𝐴=1) = \sum_{b=0}^{1} P_v(𝐴=1|𝐵=b)P_h(𝐵=b)
$$



### Multiple Argument Chains
    - In Part II, we described the math for a single argument thread.
    - For single thread easy. For large coordinate trees probably not (todo: intro coordinate tree)

    - What if there are multiple argument chains
    - For example pro-con arguments
    - As with the premises in an argument thread, we cannot assume multiple premise arguments are independent
        - could be duplicates
        - or could individually add weight
            - linked argument in argumentation theory
        - or could be somewhere in between.
        - somewhere on continuum of complete duplicate, or completely linked

    - In general, for coordinate argument tree, we cannot make independence assumption.
    - Seems like it should be dead end
    - Insight: but what about this situation...




### Inferring Priors from Sparse Data
    - If we have lots of data for joint probabilities, we know...
    - But for very large argument graphs, we won't have data on all combinations
    - Even for single thread...might have few votes
    - Chart with complex tree
        - Complex argument tree, may vote on arbitrary on combinations
    - The Problem is Inferring Prior Probability Space from Sparse Data



## Hierarchical Models
        - solution is hierarchical model
        - Here we are going to use the principle Bayesian inference at two levels: one to infer priors of the average reasoner, then use those priors to infer posteriors of average reasoner.

        - have to start with prior assumption about the the average reasoners priors
        - for example, assume that that B is close to A
        - then calculate a posterior probability given evidence...
        - for single thread: will skip the details...just provide formula...K is part of priors, prior assumption of variance...this can come from historical data.
        - called Bayesian Averaging
        - for more complex models, interaction parameter...
        - details not included in this paper...





## Bayesian Averaging
    ### Problem of Limited Data
        Example: have 100 votes, then 2 botes
        - Expect A|B to "shrink" towards A
        - Same for multiple levels
        - Then expect C to shrink towards B.
    - Hierarchical model...
        solution is hierarchical model
        have to start with prior assumption...in this case that B is close to A
        then calculate a posterior probability given evidence...
        will skip the details...just provide formula...K is part of priors, prior assumption of variance...this can come from historical data.
        new formula, with shrinkage calculation at each level...




## Complex Argument Graphs

### The Coordinate Subtree


To calculate the justified o𝑃ᵢnion of the root claim, we need to estimate the probability that the meta-reasoner will accept/reject the root claim given acceptance/rejection of the combination of the premises in this tree. But as shown in (another section), the conditional independence condition does not hold for the premises in a warrant argument thread. These arguments are all made in context of the root claim. And because this tree is composed entirely of warrant argument threads, all sharing the same root, **the conditional independence condition does not hold between any of the premises in this tree**.




### The Dilemma of Multiple Argument Threads

For an argument with a single thread, we can use the joint informed probability distribution as the priors of the meta-reasoner, because we are guaranteed to have votes on all arguments in the thread. But we may not be able to do this for large coordinate argument trees where there are multiple argument threads, and where not every participant has followed all the threads.

For example, if claim A has premise arguments B0 and B1, but not participant has voted on all three of these claims, how can we estimate the probability that a participant will accept A given they accept B0 and B1?

This seems at first to be an impossible problem, because we do not now how B0 and B1 are related. They might be exact duplicates, or have considerable semantic overlaop (e.g. there is a signed confession / the defendant signed a confession). Or their support for A might be statistically independent. Or they might be linked, such that each adds little support/opposition for A by themselves but a lot when considered together together. 

### Rejecting the Naive Assumption

Naive Bayes filters (the kind famously used for spam) let's us solve this problem by assuming conditional independence of B0 and B1 given A. Given this assumption, it can be easily shown that the following holds:

    P(A|B0,B1,...,Bn) = P(A) * P(B0|A)/P(B0) * P(B1|A)/P(B1) * ... * P(Bn-1|A)/P(Bn-1) 


The naive assumption is tempting, because it works surprisingly well in naive Bayes filters. But using the Naive assumption here could easily produce clearly undesirable outcomes, such as probabilities greater than 1 in the case of duplicate or semantically overlap𝑃ᵢng arguments. The problem here is that a single user can submit multiple arguments. If the same user who submits A also submits and upvotes enough supporting arguments B1, B2, ..., Bn, then it's easy to show that P(A|B1,B2,...,BN) monotonically approaches infinity as N approaches infinity, as long as 0 < P(A) < 1

Proof

    Each Bk gets one supporting vote from the same user, who also supports A. Therefore:

        (1) P(A|Bk) = 1

    Naive Bayes formula gives us

        (2) P(A|B1,B2,...,Bn) = P(A) * P(B1|A)/P(B1) * P(B2|A)/P(B2) * ... * P(Bn|A)/P(Bₙ) 

    NOw for each k between 1 and n, the corresponding term of the above equation is:

        P(Bk|A)/P(Bk) 
            = P(Bk,A)/(P(Bk)/P(A))          by definition of conditional probability
            = P(A|Bk)*P(Bk)/(P(Bk)/P(A))    by definition of conditional probability
            = P(A|Bk)/P(A)                  by algebra
            = 1/P(A)                        by (1)

    Therefore all the terms P(Bk|A)/P(Bk) in (2) are equal to 1/P(A), so

        P(A|B0,B1,...,BN)  = P(A) * 1/P(A)^(n)

    Since 0 < P(A) < 1, the above approaches infinity with n.

Infinite probability makes no sense, nor does the the ability for a single user to dominate the results just by being prolific.

Obviously our model should not be so sensitive to n. Our task is to estimate the probability that the meta-reasoner would accept A given they had seen all arguments, but clearly the **number** of arguments should not be a major factor.

### The Voter-Oriented Model

The key is to recognize that the fundamental unit of data we have is the *vote*, not the argument. To illustrate this, consider these situations:

1. there are 10 users that support A, and 2 supporting arguments, each upvoted by 5 users
2. there are 10 users that support A, but only 1 supporting argument, upvoted by all 10 users

In which case is A better supported? Do we have more evidence that the meta-reasoner supports A in one situation, than in the other?

There are certainly arguments to be made for various interpretations here. But ultimately, if we are going to model the meta-reasoner, we need a model. This model requires assumptions about the relationship between the beliefs of the hypothetical average juror and the arguments made and voted on in the argument graph, so we can decide what data supports what beliefs about the priors of the meta-reasoner.

In our model, the two cases above provide **identical support for A**. The number of different arguments submitted by the 10 users gives no substantive information, for it only reflects the fairly arbitrary decisions users make about how to author their arguments (e.g. one argument for each point they have to make vs. one argument containing each point).

On the other hand, our model does give more weight to **justified** votes -- that is, votes backed by **any** reason. Here we **do** consider a vote combined with a reason to convey more information about the meta-reasoner than a mere vote. 

This means that in the above case, our estimate of the probability of acceptance of A given acceptance of **all of the arguments** is equal to the same estimate given  acceptance of **any of the arguments**. In other words, our best estimate of P(A\|B0 AND B1) is P(A,B0 OR B1).

This is a critical result. It seems to defy the rules of probability, yet it is the key that allows us analyze arguments with multiple argument threads.

The problem is that these conditional probabilities focus on the wrong thing: arguments instead of votes supported by arguments. Once we recognize that our unit of data is the vote, and not the argument, and that the arguments themselves are just a structure on which to hang our votes, our intuition about the above conditional probabilities takes on less importance.

## Hierarchical Model for Multiple Argument Threads

If we construct a Bayesian hierarchical model as we did in the case of a single argument thread, then we could start by estimating two parameters: θ₀ = P(A|B₀) and θ₁ = P(A|B₁). The obvious prior distribution for these is beta, just as in the case of a single argument thread:

    θ₀ ~ beta(α₀, β₀)
    θ₁ ~ beta(α₁, β₁)

Our task is now to estimate θᵢ = P(A|B₀,B₁). 

So what is our prior for θᵢ? It seems logical that they would be related to θ₀ and θ₁, but we have no reason to believe that θᵢ would be closer to one or the another. We don't know if these are supporting or opposing arguments, whether they are duplicate, linked or independent. We have the same number of voters for each. The estimates of θ₀ and θ₁ already incorporate what prior beliefs we have about site users' tendency to agree or disagree with arguments in general. We don't have anything else on which to base our priors.

So our best estimate for θᵢ would have to be the **average** of θ₀ and θ₁. Call this estimate ωᵢ.

    ωᵢ = (θ₀ + θ₁) / 2

As in the case of a single thread, we can estimate the parameters of the beta distribution given an estimate of the mode and concentration:

    αᵢ = ωᵢ(κ - 2) + 1
    βᵢ = (1 - ωᵢ)(κ - 2) + 1

So 

    θᵢ ~ beta( ωᵢ(κ - 2) + 1, (1 - ωᵢ)(κ - 2) + 1  )

Let's use (nₖ, Zₖ) to indicate the number of users that voted on Bₖ, and the number of users that 
accepted Bₖ, respectively.

We now have this full hierarchical model:

    θᵢ ~ beta( ωᵢ(κ - 2) + 1, (1 - ωᵢ)(κ - 2) + 1  )
    ωᵢ = (θ₀ + θ₁) / 2

    (n₀, Z₀) ~ bern(θ₀)
    θ₀ ~ beta(α₀, β₀)

    (n₁, Z₁) ~ bern(θ₁)
    θ₁ ~ beta(α₁, β₁)

It's easy to see by pure symmetry that the MAP estimate for θᵢ will fall between θ₀ and θ₁.

In our example, there is an equal number of votes on B0 and B1. If this is no the case, we need to use the weighted average of θ₀ + θ₁. Otherwise, we again fall into a situation where our results depend on the number of arguments, not votes. So let's update our model so that;

    ωᵢ = (θ₀*n₀ + θ₁*n₁) / (n₀ + n₁)

Also, note we only have data for users that voted on **only B0** (γ₀) or **only B1** (γ₁). What if we **also** have at least some data on users that voted on the combination of B0 and B1?

We can of course easily add to the model

    (nᵢ, Zᵢ) ~ bern(θᵢ)

Our new model is:

    (nᵢ, Zᵢ) ~ bern(θᵢ)
    θᵢ ~ beta( ωᵢ(κ - 2) + 1, (1 - ωᵢ)(κ - 2) + 1  )
    ωᵢ = (θ₀*n₀ + θ₁*n₁) / (n₀ + n₁)

    (n₀, Z₀) ~ bern(θ₀)
    θ₀ ~ beta(α₀, β₀)

    (n₁, Z₁) ~ bern(θ₁)
    θ₁ ~ beta(α₁, β₁)


Now in our combined model, the posterior estimate of θᵢ will depend on how many votes we have. If there is a very large number of votes for γᵢ (users that voted on both B0 and B1), then these override the priors for θᵢ, and the posterior estimate of θᵢ will approach the vote ratio Zᵢ/nᵢ. On the other hand, if a lot of users voted on B0 but very few voted on B1 (or the combination), then the estimate will approach Z₀/n₀.

## More Complex Models

















## Sparse Data

For large argument graphs, there will probably not be any jurors who voted on all the premises in the coordinate set. 

Even when there are subsets of premises, such as the premises in a single thread, where there exists jurors who have voted on all the arguments in the set, there may be very few votes, making the justified o𝑃ᵢnion overly sensitive to sampling error. In the extreme case, there can be a dictatorship, where the one person who votes on a "crux" argument can cause the overall justified o𝑃ᵢnion to be 100% or 0%.

## Hierarchical Models

The problem of complex argument graphs and of sparse data can both be solved using Bayesian hierarchical models.


Bayesian inference, like all statistical inference, assumes that the data that is observed (the votes) is just a *sample* that gives us hints about the nature of the underlying population (the average beliefs of the set of all possible jurors). This sample lets us guess what the actual underlying beliefs are.



hierarchical model assume the data that is observed (the votes) are the result of some underlying actual beliefs, but distorted by randomness. For example, the ratio of accept/reject votes on A is not the actual underlying belief in A of a randomly selected juror, but instead is a sample  data that lets us infer what A might be.

