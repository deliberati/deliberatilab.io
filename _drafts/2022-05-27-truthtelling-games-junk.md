
---------------------------






## The Law of Attention

These mechanisms work on the assumption that people are **rational** agents trying to maximize their payout. In the [Law of Attention], I argue that social networks are games where people compete for attention, and that social networks will be dominated by people behaving **as if** they are rational agents trying to maximize attention, because users who don't behave this way will effectively stop participating. 

So if a social network used a Bayesian survey mechanism where the **payout** was attention, then those users who were actually active in the platform would tell the truth, and only post, share, and upvote content that they **honestly** thought was truthful.

This is true even for opinions on highly politicized or sensitive topics. Why would people tell the truth in an online platform if the truth would harm them personally? Of course, in many cases they would not. If winning the attention game in some online community comes at too much of a cost, you may choose not to play.





peopleSo do we need to pay people to participate in social networks?

No, for various reasons.

The key is to make the payout of these games





Online platforms such as Wikipedia and Stack Overflow are also coordination games, where people coordinate on **shared community standards**. The way to **win** the game is to provide posts or edits that other community members believe is up to standard. If you provide substandard content, or fail to revert or downvote substandard content, you lose trust and reputation..

Any online community with a system of reputation or karma, trust levels, or permission levels is a coordination game. Unlike 

So blockchain consensus protocols are coordination games, where people win by coordinating on objective truth. Bayesian Surveys are coordination games where people win by coordinating on subjective truth.





a coordination game, where people win by coordinating on upholding community standards.



With blockchain-like consensus protocols, people coordinate on telling the truth on objective facts. With Bayesian surveys, people coordinate on telling the truth about their subjective opinion.

With crowdsourcing platforms such as Wikipedia, people coordinate on upholding **shared epistemic standards**. 

coordinate on the sort of behaviors that are considered to be 



Everyone upholds the standards because everyone else is upholding the same standards, and 

Any online platform that has survived


[todo: lesswrong: https://www.lesswrong.com/posts/tscc3e5eujrsEeFN4/well-kept-gardens-die-by-pacifism]


The problem is...censorship. In a social platform such as 


TODO: explain how systems of reputation can enforce an equilibrium aroudn honesty.


## The Honest and Informed Opinion

What does it mean to "tell the truth?" Colloquially it means **to say what one believes to be true**: to be honest. But this is not the same as speaking truth. **People can be honest and wrong.** 

So **dishonesty** is only one part of the problem of false and misleading information on the internet. The other part is **ignorance**. 

In [Give Truth the Advantage](/give-truth-the-advantage), I argue that we can design social algorithms to address both **dishonesty** and **ignorance**, and create a feedback loop that tends to amplify the **honest and informed opinion**: what the average person in an online community **would honestly endorse if they they all shared the same information.**

This turns social platforms into games where the way to "win" is not to be engaging, or popular, or even to tell the truth, but to endorse and defend opinions that stand up to scrutiny of your peers. To **speak the truth**.



## The Law of Attention

These mechanisms work on the assumption that people are **rational** agents trying to maximize their payout. In the [Law of Attention], I argue that social networks are games where people compete for attention, and that social networks will be dominated by people behaving **as if** they are rational agents trying to maximize attention, because users who don't behave this way will effectively stop participating. 

So if a social network used a Bayesian survey mechanism where the **payout** was attention, then those users who were actually active in the platform would tell the truth, and only post, share, and upvote content that they **honestly** thought was truthful.

This is true even for opinions on highly politicized or sensitive topics. Why would people tell the truth in an online platform if the truth would harm them personally? Of course, in many cases they would not. If winning the attention game in some online community comes at too much of a cost, you may choose not to play.
