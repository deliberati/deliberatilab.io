


So I'll start with introducing my own notation here. The notation `{a,k}` indicates k observations out of which a are positive, or alternatively b units of attention out of which a upvotes are observed. Then if `p` is either a gamma or beta distribution, then `p+{a,k}` indicates the posterior after updating for those two observations. In the case of either a gamma or a beta distribution, the posterior is a new gamma or beta distribution where a and k are added to the parameters of the old. Note we use a and k instead of alpha and beta, where for a beta distribution k=alpha+beta and for a gamma distribution k=beta. For example, if `p(θ) = beta(θ,1,1)` (alpha = beta = 1, k = 2, which is a uniform prior) then `p+{2,5}` is `beta(θ|1+2,1+5-2)=beta(θ|3,4)` (the posterior after 2 positive and 3 negative bernouli trial). Or if `p(θ) = gamma(θ,1,1)` then  `p+{2,5}`is gamma `p(θ) = gamma(θ,3,6)` (the posterior after 5 draws from a bernoulli trial with an average count of 2/5 or a total count of 5). {a,k} used on its own is also a probability distribution -- whether it's a beta or gamma depends on the context.

<!-- NOTES: https://vioshyvo.github.io/Bayesian_inference/conjugate-distributions.html -->

We'll use `p` to indicate our hyperprior beliefs (e.g. the belief int he probability of an upvote for a beta dist, and a belief in the upvoteRate for a gamma).

We have users Un = U1, U2, U3 ... uk and tasks Tn=T1,T2,..., Tk. We will also used Tn to indicate the upvoteRate for task n. When a user votes on a task, we count for subsequent users the total upvotes and cumulative expected upvotes for that task. So for example if U1 upvotes item 1, and subsequent to this upvote the T1 receives 5 units of attention, and 2 upvote, then we have a data point V11={2,5}.

What is our new estimate of the upvote rate for this task? It is 

	P'(T1) = P(T1|V11) = p+{2,5}. 

Now suppose there is a new task T2, and U1 votes again after 4 units of attention have passed, giving us a data point V12={1,4}. We want to update our estimate the upvote rate for task 2 P(T2). The simple thing is to repeat what we just did:

	 P'(T2) = P(T2|V12)+{1,4}

But this time it's not so simple. We also have information on the past history for U1, which should be incorporated into our beliefs.

What is our prior belief in the upvote rate of T2 given U1 has voted, before obtaining the new data point V12={1,4}? Well this one is a little tricky. 

	




Our prior mean should be the same as the posterior for task 1 if the only data we looked at was the data after U1 voted: {1,5}. Or in other words, it is the upvote rate for task 1 **given** the data for U1 and T1, or P(T1|V11)=p+{1,5}. But then we make the prior assumption that that the upvote rate for T1, **given** then data for U1 on task 1, P(T2|V11), is equal to P(T1|V11). So the prior mean is p+{1,5}. And the data is {1,4}.

To get the posterior, we need to know how strongly to weight the prior belief. I think this weight will be global constant. No matter how much data we have for U1 for past tasks, each task is a new task. If we have a super high amount of data for U1 and have high confidence that upvote rates given U1 to be much lower than average, then our prior average for task 2 given the data for U1 will be low, but that shouldn't mean that it takes a huge amount of evidence showing a higher upvote rate for task 2 for user 1 to override the prior. 

So the posterior should be a weighted average, weighted by the amount of data (Bayesian Averaging). Given data V12={1, n_12} (1 upvotes out of n_12 units of attention for user 1 on task 2). Suppose our hyperprior is p={1,2}. Then our prior is p+V11 = {1 + 1,2+n_11}. Given a constant c representing the strength of the prior, our posterior average should be the weighted average:

	P(T2|U12) = { 1/n12*n12 + (1 + 1)/(2+n_11)*c , n_12 + c } = { 1 + 2c/(2+n_11) , n_12 + c }

We can go about this by creating a new distribution p1, the estimate of the upvote rate for the next task given the data we have. Given only the fact that user 1 has voted on task 2, which we represent as U12, we estimate:

	p1(Tn|U1n) = reset(p+V11) = { 2/(2+n_11)*c , c }

Then 

	p1(T2|V12) = p1(T2|U12)+V12 = reset(p+V11)+V12

Now suppose we had two users that upvoted on task 1. OUr posterior for task 1

	P(T1|V11,V21) = p+V11+V21

Then our prior for task n, given both users also voted it, is

	p1(Tn|U1n,U2n) = reset(p+V11+D21)

Then

	p1(T2|V12,V22) = p1(T2|U12,U22)+V12+V22 = reset(p+V11+D21)+V12+V22

But what if the votes of users 1 an 2 are correlated?





Okay 
	U1 and U2 have no record
	both upvote
	8 others don't

	P(V|U1) = P(V|U2) = p+{1,1}, which is the prior on the predictionRate
	P(V|U1,U2) = p+{2,2} 	// the prior on the prediction rate of the joint vote
	P(V|U1,U2,other8) = p+{2,2}+{0,8}, which is the prior on the joint prediction rate, plus the data for the 8 non-upvotes

	Then the outcomes is 1. So we now create new distribution P1 which incorporates history

	P1(V|U1) = p+{1,1}+{1,1}  (1 positive outcome for 1 times U1 upvoted)
	P1(V|U2) = p+{1,1}+{1,1}
	P1(V|U1,U2) = p+{2,2}+{1,1} (prior for joint upvote, plus 1 positive outcome for 1 time both upvoted)

	NEXT ITEM	

	So now we go to the next story. We start with P1 which has our knowledge of history of U1 and U2

	Suppose U1 is the only upvote. And U3 downvotes

		P1(V|U1,U3=0) = P1(V|U1)+{0,1} = p+{1,1}+{1,1}+{0,1} 
			= p+{1,1}+{1,2} = prior+{2,3}


	Suppose U1 and U3 upvote

		P1(V|U1,U3=1) = P1(V|U1)+{1,1} = p+{1,1}+{1,1}+{1,1} = prior+{3,3}

		So our prior before we have a second vote P1(V|U1). Then we get U3s vote and add this info to the prior.	

	Or suppose U1 and U2 upvote?

		Since we have history for combo, we have P1(V|U1,U2) = p+{2,2}+{1,1} = prior+{3,3}


	So in both cases it's prior+{3,3}. This kind of makes sense:
		ON the one hand, P1(V|U1,U2) should be higher because there is a successful history for the combo
		On the other hand, U1,U2 are effectively duplicates

	Now thoughts:
		Two items with no history that both upvote. Should be higher than two items with strong correlation.

	So suppose U3 has a history too
		P1(V|U3) = p+{1,1}+{1,1}

	What then is
		P1(V|U1,U3)?

	Well let's start with priors

		P1(V|U1) = p+{1,1}+{1,1} = {alpha,kappa}+{2,2}  
		P1(V|U3) = {alpha,kappa}+{2,2} 
		P(V|U1,U3) = p+{2,2}

	If we assume they are independent, can we use naive bayes?

	No they are definitely not independent. 

	Can we just add?

		P(V|U1,U3) = {alpha,kappa}+{2,2}+{2,2} = {alpha,kappa}+{4,4} 

	So since u1 and u3 don't vote together, we get higher prob than 
		P(V|U1,U2) = {alpha,kappa}+{3,3} 

	Let's change and say all through voted 3 times, with 2:3, 
	and say u1 and u2 always voted together

	Suppose prior={alpha,kappa}={1,4}

	P1(V|U1) = prior1+{2,3} = prior+{3,4} = 4/8 = 1/2
	P1(V|U2) = prior1+{2,3}
	P1(V|U3) = prior1+{2,3}
	P1(V|U1,U2) = prior2+{2,3} = prior+{4,5} = 5/9 = .555
	P(V|U1,U3) = prior1+{2,3}+{2,3} = prior+{5,7} = 6/11 = .545 
	Or alternatively
		Z is 2, n = 3, P1(V|U1) = 1/2, L=2
		P1(V|U1,U2) = (P1(V|U1) * L + Z) / (L + n) = (1/2 * 2 + 2/3*3) / (2 + 3) = (1 + 2) / (2 + 3) + 3/5 = .6

		If L = 8

		(P1(V|U1) * L + Z) / (L + n) = (1/2 * 8 + 2 / (8 + 3) = 6/11

		Which is the same as P(V|U1,U3)


	Maybe laternative formula
		Se start with prior P1(V|U1), and weigh the data (Z,N)|U1,U2 BY L

		P1(V|U1,U2)
		= 
		= (P1(V|U1) * L + Z/n * n) / (L + n)
		= (P1(V|U1) * L + Z) / (L + n)

		L should be higher the stronger the correlation between U1 and U2
		P(V|U1,U2) can't be greater than .545(if they are totally independent) and 1/2 (if they are totally correlated)


		If P(U1|U2) = P(U1), then L should be 8
			P(U1)/P(U1|U2)) = 1
		If P(U1|U2) = 1, then L should be high
			P(U1)/P(U1|U2) = P(U1)

		L could be 8*P(U1|U2)/P(U1)

Review of mutual information

	It is the expected value of log of significance right?

	E(log(P(X,Y)/P(X)/P(Y)) | X,Y) sum{x,y} P(X,Y) log P(X,Y) - sum{x,y} P(X,Y) P(X)*P(Y)

	I(X;Y) = D(P(X,Y) || P(X) * P(Y))

	= sum{X,Y} P(X,Y) log(P(X,Y) - sum{X,Y} log(P(X)) - sum{X,Y} log(P(X))

	= H(X,Y) - relativeH(X|X,Y) * relativeH(Y|X,Y)





	A priori,

		P1(V|U1,U2) = P1(V|U1) * X
		V1U1U2/U1U2 = VU1/U1 * X
		X = P1(V|U1,U2) / P1(V|U1) = S(V,U2|U1)

	If U1 and U2 are independent and without history, we can calculate this

		P1(V|U1) = {alpha,kappa}+{1,1} = 2/3
		P1(V|U1,U2) = {alpha,kappa}+{2,2} = 3/4

		S(V,U2|U1) = P1(V|U1,U2) / P1(V|U1) = 3/4 / 2/3 = 9/8



	naive bayes doesn't work
		I think to set it up correctly, we need
			P1(V|U1=1,U2=?)
			P1(V|U2=1,U1=?)	

		P1(V|U1) = P1(U1|V)*P1(V)/P1(U1)
		P1(V) = {alpha,kappa}

		S(V|U2) = P1(V|U2) / {alpha,kappa}

		P1(V|U1,U2) 
			= P1(V) * S(V|U1) * S(V|U2) 
			= P1(V|U1) * S(V|U2)
			= P1(V|U1) * P1(V|U2) / {alpha,kappa}
			
				= p+{1,1}+{1,1} * p+{1,1}+{1,1} / {alpha,kappa}

		If we just use point estimates

			(alpha+2)^2/(kappa+2)^2 * kappa/alpha

		suppose alpha=1, kappa=2

				{alpha,kappa} = 1/2
				p+{1,1}+{1,1} = 3/4

				3/4 * 3/4 / (1/2) = 9/16 * 2 = 27/32 = 1.125

		Which is invalid





	Suppose we didn't have history for combo

		Then we have P1(V|U1), and a prior belief P1(V|U1,U2)

		P1(V|U1,U2) = P1(V|U1)+{0,1}


	