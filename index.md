---
title: "Give Truth the Advantage"
layout: splash
excerpt: "Deliberati is building technologies to make conversations on the Internet more intelligent and less polarizing."
entries_layout: grid


header:
  overlay_image: /assets/images/wylly-suhendra-Swk4G_xi_uM-unsplash-wide-1920.jpg
  caption: "Photo credit: [**Unsplash**](https://unsplash.com)"
  show_overlay_excerpt: true

# intro: 
#   - excerpt: "Our vision is a social network that improves public discourse by promoting **civility, truth, mutual understanding, and the well-being of its users**. 

#     <br/><br/>
#     We believe this can be built based on the following principles:
  # "

intro:
  - excerpt: "Deliberati is building technologies to make conversations on the Internet more intelligent and less polarizing."

principles:

  - title: "The Problem"
    excerpt: "
Documentaries such as the [Social Dilemma](https://youtu.be/uaaC57tcci0) and books such as [Breaking the Social Media Prism](https://www.amazon.com/Breaking-Social-Media-Prism-Polarizing/dp/0691203423) have helped create general awareness of the way social media harms society: by creating perverse feedback loops that promote the spread of misinformation, political polarization, and psychological harm.
  "
  - title: "The Solution"
    excerpt: "We believe it is possible not only to understand these feedback loops, but to intentionally engineer healthy feedback loops that reward users for behavior that others find not just engaging, but also civil, honest and defensible with reasons."
  - title: "Our Work"
    excerpt: "In the articles below we describe some possible technologies for creating more healthy social media feedback loops. Whether these technologies are integrated into other social platforms, or we are build them into new social platforms of our own, our goal is to contribute to the next generation of healthy social platforms.
"

---


<style type="text/css">
.responsive-video-container {
  position: relative;
  margin-bottom: 1em;
  height: 0;
  overflow: hidden;
  max-width: 100%;
  margin-left: 15%;
  margin-right: 15%;
  padding-bottom: 39.375%;

  iframe,
  object,
  embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
}

</style>


{% include feature_row id="principles" %}


{% assign entries_layout = page.entries_layout | default: 'list' %}
<div class="entries-{{ entries_layout }}">

  {% assign filtered_posts = site.posts | where: 'id', '/give-truth-the-advantage' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}

  {% assign filtered_posts = site.posts | where: 'id', '/truthtelling-games' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}

  {% assign filtered_posts = site.posts | where: 'id', '/information-elicitation-mechanisms' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}

  {% assign filtered_posts = site.posts | where: 'id', '/the-law-of-attention' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}


  {% assign filtered_posts = site.posts | where: 'id', '/the-deliberative-poll' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}



  {% assign filtered_posts = site.posts | where: 'id', '/distributed-bayesian-reasoning-introduction' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}

  {% assign filtered_posts = site.posts | where: 'id', '/bayesian-argumentation' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}

  {% assign filtered_posts = site.posts | where: 'id', '/argument-model' %}
  {% assign post = filtered_posts[0] %}
  {% include archive-single.html type=entries_layout %}





</div>



