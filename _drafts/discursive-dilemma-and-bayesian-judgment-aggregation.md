# Bayesian Approach to Judgment Aggregation

## Research

This paper has great references on judgment aggregation in general: https://chicagounbound.uchicago.edu/cgi/viewcontent.cgi?article=8188&context=journal_articles


	48 For an excellent review of the evolution of the literature that has emerged
	from Kornhauser & Sager’s discovery of the doctrinal paradox, see List, supra note
	14. For examples of the formularization and increasingly generalized nature of
	the literature that has emerged, see generally, Franz Dietrich & Philippe Mongin,
	The Premiss-Based Approach to Judgment Aggregation, 145 J. ECON. THEORY 562
	(2010); Franz Dietrich & Christian List, Strategy-Proof Judgment Aggregation, 23
	ECON. & PHIL. 269 (2007); Christian List, The Discursive Dilemma and Public Reason,
	116 ETHICS 362 (2006); Christian List, The Probability of Inconsistencies in Complex
	Collective Decisions, 24 SOC. CHOICE & WELFARE 3 (2005); Christian List & Philip
	Pettit, On the Many as One: A Reply to Kornhauser and Sager, 33 PHIL. & PUB. AFF. 377


		7 See generally Christian List & Philip Pettit, Aggregating Sets of Judgments:
		Two Impossibility Results Compared, 140 SYNTHESE 207, 214 (2004) (describing an
		aggregation procedure “which takes as its input a profile of complete, consistent
		and deductively closed personal sets of judgments across the individuals . . . and
		which produces as its output a collective set of judgments . . . which is also
		complete, consistent and deductively closed”). 


		Paradox, 34 SOC. CHOICE & WELFARE 631, 631 (2010) (“A doctrinal paradox occurs
		when majority voting on a compound proposition (such as a conjunction or
		disjunction) yields a different result than majority voting on each of the elements
		of the proposition.”)

Petit's Papers

	(List & Petit 2002) Aggregating sets of judgments: An impossibility result: https://philpapers.org/rec/LISASO-3
		describes the first "impossibility result" and compares to arrow's theorem
		presents proof
		suggests escape route

	Aggregating Sets of Judgments: Two Impossibility Results Compared
		https://philpapers.org/rec/PETTDP
		http://eprints.lse.ac.uk/665/1/AGG2.pdf
		elaborates on List & Petit 2002
		more about whether arrow's theorem and the above impossibility result can be unified



	https://www.princeton.edu/~ppettit/papers/Deliberative_PhilosophicalIssues_2001.pdf
		Deliberative Democracy and the Discursive Dilemma.

Christian List

	The Discursive Dilemma and Public Reason.
		2006
		https://philpapers.org/rec/LISTDD
		What is the trade-off between the (minimal liberal) demand for reaching agreement on outcomes and the (comprehensive deliberative) demand for reason-giving? How large should the ‘sphere of public reason’ be? When do the decision procedures suggested by the two accounts agree and when not? How good are these procedures at truthtracking on factual matters?

	Dynamically rational judgment aggregation
		https://philpapers.org/rec/DIEDRJ

		 Formally, a judgment aggregation rule is dynamically rational with respect to a given revision operator if, whenever all individuals revise their judgments in light of some information (a learnt proposition), then the new aggregate judgments are the old ones revised in light of this information, i.e., aggregation and revision commute. We prove a general impossibility theorem: if the propositions on the agenda are sufficiently interconnected, no judgment aggregation rule with standard properties is dynamically rational with respect to any revision operator satisfying some mild conditions (familiar from belief revision theory)

	The theory of judgment aggregation: an introductory review 
		2011
		https://personal.lse.ac.uk/list/PDF-files/ReviewPaper.pdf
		http://eprints.lse.ac.uk/27596/1/The_theory_of_judgment_aggregation_%28LSERO%29.pdf

	Introduction to judgment aggregation
		list and polak
		https://www.sciencedirect.com/science/article/abs/pii/S0022053110000232


Dietrich
	Fully Bayesian Aggregation
	http://www.franzdietrich.net/Papers/Dietrich-FullyBayesianAggregation.pdf


Marija Slavkovik
	An introductory course to judgment aggregation
	2016

	introduce the basic frameworks that model judgment aggregation problems and give an overview of the judgment aggregation functions so far developed as well as their social theoretic and computational complexity properties. The focus of the tutorial are consensus reaching problems in multi agent systems that can be modelled as judgment aggregation problems. 

	...participants are expected to be able to read and understand judgment aggregation literature and have a grasp on the state-of-teh-art and open questions in judgment aggregation research of interest to multi agent systems.



Possible essay
	abstract
		judgment aggregation or belief merging aims to solve the problem of finding the *best* judgment of a group given individual judgments. What exactly is *best* can't be concretely defined but should be consistent with various attributes that people generally think a fair or reasonable judgment aggregation function should have. Various literature, following in the traditional of social choice theory and Arrow's impossibility theorem, have shown various impossibility results.
		For example, list and Petit have shown that, given certain assumptions, a set of logically consistent individual judgments cannot be aggregated into a set of logically inconsistent group judgments. As they put it "the propositionwise majority voting can generate an inconsistent collective set of judgments, even when each individual holds a perfectly consistent set of judgments"

		We show that these inconsistencies go away if the aggregate group judgments are seen as probabilities and not as binary judgments. So we need to get rid of the assumption that the "majority rule" group judgments must be logically consistent. 

		Also show that logical inconsistencies result from lack of independence which indicate confounding reasons. So judgment aggregation procedure should get rid of some sort of independence assumption?

		Also show that, if we assume that (everyone has opinion on everything -- completeness assumption), and the input is the logical propositions that have been proposed and agents binary beliefs on each proposition, the simple probability that an agent believes each proposition is the most logically consistent way to aggregate judgments...because this aggregation fully incorporates confounding reasons for beliefs.

		But these results are unsatisfactory, because they don't tell us how to produce better judgment aggregation. In this setup, we *cannot* produce better judgment aggregation than just by tallying up the votes. But where things get interesting is where we can identify not just differences in opinion (whose explanation we ultimately ascribe to different confounding reasons, for which we have no way of distinguishing which judges confounding beliefs are most valid -- we cannot detect logical inconsistency because a confounding reason always can be conjured to explain an opinion) but differences in actual knowledge...and point to which judges have knowledge the others don't. 

		Suggest that if agents are honest bayesian reasoners the only way to solve this problem is to identify differential knowledge. This can come from some reputation source. We won't address identification of experts here or reputations here.  But simply relaxing the completeness assumption allows us to identify differential knowledge, we can indeed identify opporunities to imrpove judgment aggregation.


		In another paper, show how deliberative Bayesian reasoning works.
		
		In another paper, show that this process is truthtracking (assumption of reasonable jury and fair deliberation)

		[REF: Paradox, 34 SOC. CHOICE & WELFARE 631, 631 (2010) (“A doctrinal paradox occurs
		when majority voting on a compound proposition (such as a conjunction or
		disjunction) yields a different result than majority voting on each of the elements
		of the proposition.”)]


### Some Probability Logic Ideas

Can represent the probability space of the doctrinal paradox using a probability logic.

For example, you can derive propositions that are definitely true in the data:

	A∧B ⟺ C
	¬A ⟹ B
	¬B ⟹ A
	A∨B

And then some that are only probably or possibly true

	P(A) = 2/3
	P(B) = 2/3
	P(C) = 1/3
	P(A&B) = 1/3
	etc.

Can you represent the entire probability space using a probability logic?

Obviously you can represent the aggregate knowledge as propositions using conditional probability:

	P(A) = 2/3
	P(B|A) = 1/2
	P(A∧B) = P(A) * P(B|A) = 1/3

But this is not a probability logic. A probability logic is a proposition plus a probability.

The key, I think, is representing the knowledge in the probability space using propositions whose probabilities are independent. Here is one combination **independent** probabilistic propositions from which you can reconstruct the probability space:

	P(A)=2/3, P(A&B)=1/3, A∧B ⟺ C, 

From this you can derive P(B|A) = 1/2. And then P(!B|A) = 1/2, thus P(A&!B)=1/3, etc. etc.

But there are actually other combinations that provide the same information. You need, in this case, at least *two* uncertain propositions in order to accurately represent the entire space. You could also start with for example

	P(C) = 1/3, P(A) = 2/3, A∨B, A∧B ⟺ C, ¬A ⟹ B

or 

	P(!A and B)=1/3, P(C)=1/3, A∧B ⟺ C, ¬B ⟹ A

	P(C)=1/3 => P(A&B)=1/3 => P(!A or !B) = 2/3 = P(!A and B)+P(!(!A and B))

	2/3 - P(!A and B) = P(!(!A and B)) = P(A or !B)
	so P(A or !B) = 2/3 - P(!A and B) = 2/3 - 1/3 = 1/3

or

	Z = 1/2
	A&B = 1/3
	Z implies B
	!Z implies A
    and of course A∨B, A∧B ⟺ C


    !(A&B) Z 	P                 A    B
    0      0    2/3 * 1/2 = 1/3   1    0
    0      1    2/3 * 1/2 = 1/3   0    1    
    1      0    1/3 * 1/2 = 1/6   1    1
    1      1    1/3 * 1/2 = 1/6   1    1    




And probably others. But if you try it becomes clear the number of uncertain propositions cannot be reduced beyond 2 without loss of information. So this probability space cannot be reduced beyond 2 dimensions. 

## Informed Opinion

So, let's say we go with:
	
    Z = 1/2
    A&B = 1/3
    Z implies B
    !Z implies A
    and of course A∨B, A∧B ⟺ C

These bits of uncertainty are sort of irreduceable. They are just disagreement among judges. But there is no correlation apparently among the disagreement. 

If the judges were to observe new evidence that proved Z. All the uncertainty that comes from Z disappears.

Bayesian inference requires reallocating probabilities. So We have

    !(A&B) Z    P                 A    B
    0      1    1/3 / 1/2 = 2/3   0    1    
    1      1    1/6 / 1/2 = 1/3   1    1    

Which we can represent as 

    Z
    P(A) = 1/2

From which we can derive
    B
    P(A&B) = 1/2
    P(C) = 1/2
    etc.

We now have reduced the number of uncertain dimensions to 1/2, which is kind of like projecting from 2 to 1 dimension.




## The Doctrinal Paradox

... introduce doctrinal paradox. 

valid contract, non-performance of contract, 

## Towards a Possible Solution

Now increase the number of judges to 300, but keep the proportions the same:

	same table.

We now have more data -- or from a Bayesian perspective, more evidence. And this evidence starts to suggest a pattern to the votes. It seems that the judges belief in A are not *independent* of their votes on B. While 2/3 of the judges accept B, only *one half* of the judges who accept A accept B. Accepting A makes it *less* likely that a judge will accept B.

Mathematically, this is expressed as:

	P(A) = 2/3
	P(A|B) = 1/2

The conditional probability P(A|B) is calculated using the formula

	P(A|B) = P(A,B)/P(B)

That is the, the probability that a judge accepts both A and B, divided by the probability that they accept B. Which in this case is:

	 100/300 / 200/300 = 1/200 = 1/2

Another interesting fact in the data is that:

	P(C|A,B) = 1 = P(A,B|C) = 1

In other words, every judge that accepts both A and B accept C, and every judge that accepts C accepts both A and B: the judges all respect the doctrine.

## Probability of Judgment

If there were not these patterns in the data, we might expect the probability of a judge accepting C to simply by the probability of them accepting A times the probability of them accepting B, which is:

	P(A)*P(B) 
		= 2/3 * 2/3 
		= 4/9

Or a little less than half. Yet the actual probability of a judge accepting B, which is 1/3.

But since A and B are not independent, we need to use the conditional probabilities. The probability that a judge accepts A times the probability a judge accepts B *given* they accept is the probability that the accept both A and B - which consistent with the doctrine is also the probability that they accept C.

	P(C) = 
		= P(A,B) 
		= P(A)*P(B|A) 
		= 2/3 * 1/2 
		= 1/3 



## Opinions as Probabilities

The Doctrinal Paradox is only a paradox if we take each of the decisions of the court to be binary. There is no paradox if we interpret the decisions of the judges as probabilities.

Instead of the court finding that A is true, it finds that A is probably true. Instead of finding that B is true, it finds that B is probably true, and that if A is true B is only fifty percent likely to be true, and so on.

Only in cases were the where judges opinions are unanimous are we justified in treating the courts findings as logical propositions. If the court unanimously ruled that both A and B were true, but did not rule that C were true, then the findings of the court as a whole would be inconsistent with the doctrine. 

But if the judges unanimously agreed on A and B, and each individual judge respected the doctrine, then each judge would also agree on C. 

So logical consistency of the judges implies logical consistency of the court as a whole, and vice versa.

Now although in our example table the court was not unanimous in their opinion on A and B, they *were* unanimous in their opinion on the doctrine `A∧B ⟺ C`.


Logical implication (`⟺`) is just a logical operator like `∧` and `∨`. `X ⟺ Y` is equivalent to `X∧Y ∨ ¬X∧¬Y` (Either they are both true or both false). So just as we can calculate P(A∧B) = P(A,B) by counting the number of judges for which `A∧B` is true, we can calculate by `P(A∧B ⟺ C)` by counting the number of judges for which `C ∨ ¬(A∧B)` is true. It's easy to see from the data that:

	P(A∧B ⟺ C) = 1

It's interesting to think that we can take this table of votes and extract the probability any logical proposition as a probable or improbable aggregate opinions of the court.

## Latent Reasons

Why aren't votes on A and B independent? Perhaps there is some additional reason D that a judge who accepts one is less likely to accept the other.

For example, suppose the plaintiff bases their allegation of non-performance on the claim that the other party didn't make a certain payment. 

But suppose that the defendant bases their claim of non-validity on the exact same claim: because there is a clause in the contract stating that if the payment wasn't made within some amount of time, the contract would be terminated without liability on either part. 

As a result, judges who accept non-validity also tended to reject non-performance, and vica versa. 

## A Paradox without Doctrine

This belief that non-validity implies non-performance, and vice versa, can be expressed as the proposition `A ⟺ ¬B`.

We can actually calculate the aggregate opinion on proposition D as:

	P(D) = P(A ⟺ ¬B) = P(A∧¬B ∨ ¬A∧B) = 2/3



This opinion D can be described as a confounding opinion. It is a hidden variable that does not exist in our table but is effecting the judges votes.  It should be clear that any dependence between votes on two propositions (as long as this is not just statistical noise) indicates the presence of a confounding opinion -- a reason for the opinions of the judges that they have not explicitly voted on. 

If the confounding opinion was explicitly voted on, and we were still stuck in a majority opinion regime, we'd find another paradox: the aggregate opinion of the court is simultaneously `A`, `¬B`, and `A ⟺ ¬B`. We didn't even need the exogonous doctrine `A∧B ⟺ C` to find a paradox in the data.

But if we look at aggregate opinions as probabilities, we can say that the court believes probably `A`, but probably `A ⟺ ¬B`, thus probably not B.

## How to Aggregate Probabilities

It's tempting to ask the question, how do we aggregate the court's judgment on the individual propositions, and does this produce more logically consistent judgments? 


### 


TODO: Address Petit's Argument
	if we don't consider these confounding reasons, we are ignoring information



and thereby calculate P(A∧B ⟺ C).










There are several different ways of looking at the type of logical inference our hypothetical judge engaged in here.


The first is plain old deductive logic. Our average judge believes that "not B" implies "not A". Our judge also believes "not B". They therefore logical consistency requires them to believe "not A"


	¬B -> ¬A
	¬B
	∴ ¬A

The second is non

	- A unless there is a good reason to believe otherwise
	- A -> B: B is a good reason to believe not A
	- ¬B -> A -> B: not B is a good reason not to believe B is a good reason to believe not A



The first is non-monotonic logic. This is a type of reasoning where....

So 



This example is trivial, but it is an example of Bayesian inference? 

	P(A,B)

It's hard to see how Bayesian inference

Non-monoto