## Interpretation, Context, Ambiguity, and Refinement

Here are some thoughts based on conversations with folks from CDL the week of 2021-Jun-28

## Context

- statements are made in context
- on the Internet, the context is often a page or part of the page on which the statement is posted.
- claims are often but not always identifiable by URIs
- a "standalone claim" is one that is *not posted in the context of any other discussion or clai,*. The context of a standalone claim is a top-level page of a web site (e.g. https://canonicaldebate.com/claims)
- standalone claims still have context (the web site or forum where they were posted), but that context is as "global" as possible.
- posters make assumptions about the common knowledge that they share with their audience based on context: what has been discussed previously, what kind of people are involved in the discussion, etc.

## Interpretation and Ambiguity

- posters make statements that they believe will convey the idea that they have in their mind *to the minds of people who share that context*
- an *interpretation* of a statement is an idea that *might* be conveyed by that statement in that context
- a statement with more than one reasonable interpretation is ambiguous
- theoretically *all* statements are ambiguous even in context: we can never be certain what exactly people will understand by the statements we make
- in practice "possible interpretations" can mean "interpretations that might be made by a reasonable person"

## Disambiguiation 

- information that narrows the range of possible interpretation of a statement (partially) disambiguates:
- since a statement can never be fully completely unambiguous, a statement is "disambiguated" if it is made less ambiguous  
	+ context can disambiguate a statement: e.g. "Paris is nice" + Fyoders travel forum = "Paris the city is nice"
	+ informal clarification can disambiguate a statement: e.g. "Paris Hilton is nice" 
	+ formal, explicit definition can disambiguate a statement: e.g. "Paris(https://en.wikipedia.org/wiki/Paris) is nice")

## Refinements (Clarifications/Reinterpretations)

- often the speaker and/or reader is unaware that their statement not only is ambiguous *has in fact been interpreted in different ways* by readers
- confusion and frustration often ensue
- when a participant in a conversation realizes that the ambiguity of a statement is hindering a conversation, they might propose a refinement (clarification/reinterpretation) of the statement that they *believe* will be less ambiguous and improve the conversation
	- I propose the word "refinement" to distinguish between other types of "interpretation"
- refinements can be proposed by the poster of a statement: e.g. "I want to claim this"
- refinements can be proposed by the poster of a response to the statement: e.g. "I was assuming that you were claiming this"
- refinements can be proposed by other participants in the conversation
- while proposing refined statements is productive, arguing about what a specific speaker meant is not


### Points of possible disagreement:

- refinement must involve formalization to be useful
- an ambiguous statement can be sufficiently unambiguous in context to allow for productive conversation
- over-refinement or premature refinement can be counter-productive

## Refinement Process: Jonathan's Ideas

- in an online system with conversation threads, somebody may respond to an argument with a refinement (formally or no). 
- For example, suppose two people engage in this conversation:
 
	"Paris is nice"
		"no too many tourists"
			clarification: "Paris, Texas is nice". 

- at this point, it is the natural prerogative of each participant to
	1. Decide if the refined claim represents the interpretation they had in mind. 
		- optionally, continue to engage
	2. It does not, to decide if they want to: 
		a. engage with the refined claim
		b. ignore the refined claim and continue discussing the unrefined statement
		c. propose an alternative refinement that your statement applied to

- these choices exist in any online forum, whether or not the mechanisms of statement refinement are formalized
- in a formalized process, submitting a refinement could create a alternative clone of the conversation. Here's the conversations that might be created  in the scenarios above.

- Case 1a: Clarification DOES represent interpretation all participants understood all along.


	https://cld.com/claims/paris-is-nice

	"Paris is nice" [this statement has clarifications]
		- For "Paris, Texas is nice", click here


	https://cld.com/claims/paris-texas-is-nice

	"Paris, Texas is nice"
		"no too many tourists"
			"not as many as Paris, France hah hah!"


No discussion exists under the more ambiguous claim "Paris is Nice" because all participants have engaged with the refined claim.


- Case 2A: Engage with Refined Claim


	https://cld.com/claims/paris-is-nice

	"Paris is nice" [this statement has clarifications]
		- For "Paris, Texas is nice", click here


	https://cld.com/claims/paris-texas-is-nice

	"Paris, Texas is nice"
		"no too many tourists"
			"not as many as Paris, France hah hah!"




	https://cld.com/claims/paris-is-nice

	"Paris is nice" 

	[this claim has other interpretations]
		- For "Paris, Texas is nice", click here

	[discussion]:
		"Paris is Nice"
			"no too many tourists"
				"not in the off-season"
				clarification: "Paris, Texas is nice" 

2B: Ignore Refined Claim

	"Paris is nice"
		- "no too many tourists"
			"not in the off-season"

## Curation Actions

In this last case, 




2C: Propose Alternative Refinement
	- "I don't think that (Paris, France is nice)"
		- "too many tourists"
			"not in the off-season"

2B

	- "Paris is nice"
		- "no too many tourists"
			- Cory: "just go in the off season"
			- Alice: "I mean 'Paris, Texas is nice'". 



		- it can be assumed they continue to endorse the statements they have made or supported regarding that claim
		- 



argument over "the interpretation that was assumed by this response"



- at this point, Bob (or any other Participant) can choose to:
	1. Engage with the refined statement:
	    - Accepting the claim e.g. "oh yeah I Texas is great"
		- Rejecting the claim e.g. "I don't like that Texas either"

	2. Disregard the refined statement.

- these choices exist in any online forum, whether or not the mechanisms of statement refinement are formalized
- in a formalized process, submitting a refinement could create a git-like branch of the conversation thread (damn, mixed metaphor but what can we do):
	- version 1: 
	 	* Paris Hilton is Nice
			* oh yes she's great
	+ version 2:
		* Paris is nice [this claim has other interpretations. Click here...]
			- no too many tourists
- curators could manually
- problem: refinement is made in context!!!!

- the version of the claim displayed by default could be determined by the proportion of people who adopt the refinement

- participants would be view/engage in the version 

- when a refined statement is proposed in a conversation thread, and more participants adopt than refuse the refinement, the version of the thread using the refined statement would become the

- 
refinement could "take over" the thread. If the thread is the "critical thread" (see the Deliberative Poll essay) the refined statement could supplant the original claim.

- in open online forums, where there may be many responses to every argument, social platforms will almost inevitably resort to some sort of collaborative filtering to push the "best" argument to the top, and the "best" response to that argument, and so on, focusing attention on a "best" argument thread. Other argument threads will be effectively "pruned" from disuse.
- if the platforms similarly allows anybody to post refinements to statement, it would be natural for the system to advance the "best" refinement, or the one that *people engage with*
- for example, if the above example was on an online travel forum, Alice's refinement might simply be ignored. Bob and others may continue with the unrefined claim "Paris is Nice" under the shared assumption that they are talking about Paris the city.


In the latter case


- a standalone


- refinement is necessary to the point where conversation is productive

 and that claim has previously been the subject of argument, the question arises: do the arguments made about the more ambiguous claim still apply to the refined claim?
- 




- people can in bad faith "pretend" that they thought the author was trying to convey another idea
- if anyone involved in the discussion believes that the statement has been interpreted in more than one way, thy can submit an "interpretation" for that statement. 

-  
- if the speaker recognizes that this statement might be interpreted in more than one way bh people int

