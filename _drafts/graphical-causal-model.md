

## Graphical Causal Model

As described in [The Meta-Reasoner], the distributed Bayesian reasoning process requires a **causal model** of the beliefs of the meta-reasoner: a set of assumptions about what beliefs **cause** what other beliefs.

The key assumption in our model is that **beliefs in premises cause beliefs in conclusions, and not vice-versa**. For 
example, the meta-reasoners degree or belief in premise G causes its belief in the conclusion B. Now, B is also the premise of the argument A:<B, so the meta-reasoner's belief in the premise B in turn causes its belief in conclusion A. Thus G also causes belief in A, but indirectly.

But these causes only go in one direction: if something else causes the meta-reasoner to change its belief in A, this will not cause it to change its belief in B, because there is no argument where A is the premise and B is the conclusion. This may or may not be a good model of the beliefs of any actual person, but these assumptions allow us to create a model of the meta-reasoner and produce a [**justified opinion** TODO: link].


Now, for warrant arguments, our causal assumptions are a little different. Belief in C does **not** cause the meta-reasoner to change its belief in B, despite the arrow pointing from A<-B:<C to A:<B in the argument graph. A<-B:<C is a warrant argument, so belief in C should cause the meta-reasoner to change its belief in the warrant A<-B. Now, it is very difficult to reason about what exactly this means, but effectively it means that belief in B causes the meta-reasoner's belief in A *given the meta-reasoner believes B*.

This means that the combination of belief in B and C causes belief in A.

We can represent all these assumptions in a **Graphical Causal Model**. Graphical Causal Models are simply directed graphs where arrows indicate direct causal relationships between measured variables. They represent **all** the assumptions we make about what causal relationships exist and do not exist between the beliefs of the meta-reasoner. Clearly defining the complete set of causal assumptions is useful primarily because it lets us justify  our calculations in [The Basic Math Math].



These causal assumptions can be represented with the following [causal graph](hhttps://www.stat.cmu.edu/~cshalizi/uADA/12/lectures/ch22.pdf). 

$$
\begin{aligned}
    G → &~B\\
        &↓\\
    C → &~A\\
\end{aligned}

$$

This causal graph differs from the argument graph in two ways:

- it includes only premises, not arguments
- the arrow from $$C$$ points to $$A$$, not to $$B$$, because although $$C$$ is used in a counter-argument to $$B$$, belief in $$C$$ effects  belief in $$A$$, not $$B$$. 


... A graphical causal model uses arrows to indicate a direct causal relationship between two things. 


It should go without saying that the above is true if the premises are relevant.


. But it should be fairly clear this causes (or could cause) the meta-reasoner to change its belief in A. That is, if 




But 



. It calculate how belief in one premise *causes* 


 .
These causal assumptions can be represented with the following [causal graph](hhttps://www.stat.cmu.edu/~cshalizi/uADA/12/lectures/ch22.pdf). 

$$
\begin{aligned}
    G → &~B\\
        &↓\\
    C → &~A\\
\end{aligned}

$$

This causal graph differs from the argument graph in two ways:

- it includes only premises, not arguments
- the arrow from $$C$$ points to $$A$$, not to $$B$$, because although $$C$$ is used in a counter-argument to $$B$$, belief in $$C$$ effects  belief in $$A$$, not $$B$$. 


