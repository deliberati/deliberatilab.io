---
layout: single
title:  "The Honest Internet"
date:   2022-03-01 12:00:00 -0700
toc: true
toc_sticky: true
sidebar:
  - nav: "honesty-on-the-internet"
    title: "In This Series"
  - nav: "honesty-on-the-internet-related"
    title: "Related Articles"
excerpt: "TODO excerpt for subjective-consensus-protocols"

---

or 

## How to Get People on the Internet to Tell the Truth

In this series of essays I am going to argue that it is possible to use **technology** to get people in social networks to tell the truth.

The harm caused by false and misleading information on the Internet to society as a whole is abundantly clear, and the reason falsehoods tend to be amplified in social platforms is fairly well understood. (todo: computational propaganda, social dilemma). 

[cut: And the problem is not just about misinformation. It is also the **intellectual** dishonesty that prevails in public discourse: the cherry-picking and mis-representation of facts that lead to beliefs that are incongruent with reality.]

But the idea of a technical solution to this problem will probably sound suspicious to many readers. Surely, the problem is simply that some people are dishonest: if they want to lie they will lie. Technology can only filter, or in other words censor, objectionable content.

But we cannot on the one hand blame social platforms for the spread of misinformation in social platforms, and at the same time blame it all on human nature. We know that the [algorithms are doing something]...they are manipulating the fabric of human communication...somehow perverting public discourse.

Instead of fixing the algorithms, the large social networks try to stem the bleeding by hiring armies of moderators. But human moderators only remove the most egregious content, in an imperfect and biased way, which creates new problems. 

Using AI to detect and remove bad content doesn't solve the problem either, because an AI can only learn to approximate the judgment of a human. It is merely a way to automate censorship. The solution is not to **identify misinformation**. The algorithms should not have an opinion about what is true or false. 

Instead, they should simply do the reverse of what they do now: they should create an environment that **gives truth the advantage**, that makes it so accurate information spreads more easily than lies.

But how can technology promote truth without having an opinion about what is true? Well, in the same way that today's social networks amplify falsehood without having an opinion about what is false: by creating a feedback loop.

hen somebody posts an outrageous lie in a social platform, it gets people commenting and arguing. The algorithms interpret this interaction as **engagement**, and since they are trying to maximize engagement -- time and attention spent on the site -- they **promote** that engaging content, putting it at the top of our feeds and our mentions. This drives further attention to that content, and rewards produces with influence, status, and often money. They in turn learn what kind of content is being rewarded by the algorithms and how to produce more of it. 

This means that there is an **unnatural** amplification of engaging content. This can be a good thing if the content is engaging for a good reason. But unfortunately it means that misinformation, which is engaging, is unnaturally amplified.

So an algorithm, having no opinion about whether content is true or false, can create an environment that unnaturally amplifies misinformation.



---



And has been written about by so many offers, the most engaging content is often the worst. 


It  helps to look at social platforms from a "cybernetic" point of view: as complex system arising from the interaction of humans and machines, where humans modify their behavior based on feedback from the machines, while the machines themselves learn and adapt to the behavior of people.

If a platform optimize for engagement -- time and attention spent on the site, then it will identify content that engages other people's attention, and promote this content, putting it at the top of our feeds and our notifications. This rewards people who produce that content with additional attention, status, influence, and often money. They in turn learn what kind of content is rewarded by the algorithms and how to produce more of it. This means that there is an **unnatural** amplification of  

And has been written about by so many offers, the most engaging content is often the worst. 




[To answer this, we need to understand how today's social networks amplify falsehood without having an opinion about what is false.]


## Cybernetics


The solution lies at somewhere around the intersection of psychology and mathematics. It requires looking at social platforms from a "cybernetic" point of view: as a complex system arising from the interaction of humans and machines, where humans modify their behavior based on feedback from the machines, while the machines themselves learn and adapt to the behavior of people.

Within these complex systems we can observe simple overall patterns: feedback loops and states of equilibrium that give rise to certain emergent properties, including echo chambers, civil or toxic conversations, and the tendency for accurate or inaccurate information to spread.

## Engagement-Driven Feedback Loops

The dominant feedback loops in online communities today are easy to analyze and [well-understood]. The platforms optimize for engagement -- time and attention spent on the site. People who produce and spread content that engage other people's attention are thus rewarded with attention, status, influence, and often loads of money. They in turn learn what kind of content is rewarded by the algorithms and how to produce more of it. 

Unfortunately the most engaging is often the worst. Scandalous lies induce people to comment in order to correct the lie. More generally, conflict produce engagement. The machines have learned to get us going at each other's throats. This toxic spiral pulls in the whole media ecosystem: mainstream newspapers have learned to produce headlines that invoke fear, outrage, and anger, because these will win them the most social media traffic.

Attention-seeking feedback loops existed before the internet. People constantly adjust their behavior in pursuit of attention, status, influence, etc., which causes others to adjust their behavior accordingly, in a big complex social feedback loop. But only in social platforms are the feedback loops algorithmically amplified by the intermediary of machines: people are not just responding to other people's behavior, they are responding to other people's behavior filtered or amplified by an algorithm. And the algorithm is itself responding and learning from people's past behavior. 

These distorted social feedback loops don't occur naturally in human society. It is worth questioning whether, like a bubble in the market for a complex financial derivatives, just how much the consequent behavior is still connected to anything fundamental or real, such as what people actually believe is true, or useful, or just.

## Honesty-Inducing Feedback Loops

If these systems can be analyzed and understood to an extent, can they also be engineered?

There are simple, obvious ways of dampening the conflict-driven-engagement feedback loop: for example, removing comment counts from the equation. Hacker News, one of the least toxic places for online public group discussions, once implemented a "Flame Ware Detector" for conversations with a high ratio of comments to upvotes. This same idea is reflected in the Twitterverse's concept of a tweet being "ratioed". The idea is that a high ratio should be a signal of content that should be muted, not amplified.

But we are trying to address the problem of **truth**. How can we design an algorithm that gives truth the advantage, without it forming an opinion about what the truth actually is? 

Our proposed solution is an algorithm that gives rise to a feedback loop that induces people to promote what they **honestly think is true**.

Once again, I expect many readers to think that this is clearly, fundamentally impossible, since many people are liars: plus troll farms, etc.

To explain why this is actually possible, we need to understand two concepts: the Law of Attention, and Bayesian truth-elicitation mechanisms from the field of Game Theory.

## The Law of Attention

All social platforms are games, in which people compete for attention. You are right to object that not everyone is motivated purely by attention. But if their social media posts and upvotes are not seen by anyone, and have no effect on what other people see, they are just shouting into the void. They have lost the game. They will probably eventually give up and stop shouting. And if they don't, we can safely ignore them.

I call this **The Law of Attention**. Like economists who model human beings as agents who seek to maximize money, or more generally utility, we can look at participants on social platforms as agents who seek no maximize attention. Economic agents who despise money eventually stop participating in the economy, because they run out of money to participate with. Likewise agents in social platforms who despise attention effectively stop participating in social platforms, because everybody stops paying attention to them.

In reality, many people are **motivated** by much more than attention: by helping other people, but standing for truth; some people are not willing to lie even if they can profit from it. Still, if nobody sees their posts, they must eventually give up. They will go to other communities, where their contributions are better received. Those that remain in the community will be those who know how to win the attention game in that particular community

So the Law of Attention says:

> online communities will filled with those behaviors that are rewarded with attention.

## Attention as Utility

The algorithms of a social platform are the rules of the game. They determine exactly what content comes to our attention by showing up in our feeds and notifications. And they determine how much attention each piece of content receives from the community as a whole. 

We can quantify attention received using the classic metrics from the ad industry such as rank-normalized impressions. These impressions have value, or utility. This utility metric lets us apply economics theories to model human behavior. This means that we can use one of the most powerful tools the the economists toobelt in the design of our social (algorith/protocol): Game Theory.


[TODO: CUT
  ## Reputation


  Attention can be rewarded to users in the form of digital assets. This is what reputation systems are. If you earn reputation points, you have the power to influence or direct the attention of others on the platform. [TODO: Reputation points can be identity-based (non-transferable), or they can be "spendable", like digital ad dollars that give you the power to promote certain content. TODO: a16z article on this idea].

  All social platforms have reputation systems, even if they don't actually show you follower counts, karma, or other explicit reputation statistics; somewhere in a database there are coeffecients used to estimate how likely other people are to engage with content you produce. People that win the the attention game take actions that increase their reputations, even if that's not their intent.

  Ultimately, the algorithms have complete control over what pieces of content come to our attention. Indeed we can quantify and control exactly how much attention each piece of content receives, using the classic metrics from the ad industry such as rank-normalized impressions. 


  exactly how the algorithm rewards attention


  If we conceive of attention as the 


  Quantifyable reputation allows us to employ the theories of economists to model human behavior in large groups, substituting attention for utility. This means that we can use one of the most powerful tools the the economists toolbelt in the design of our social (algorith/protocol): Game Theory.
]


## Game Theory

GAME THEORY is used to model human behavior as games where the players can take certain actions and receive payouts according to a certain formula, with takes into consideration each players actions AND the actions of other players. And we can mathematically analyze these games and prove things about them. For example, we can prove that if all other players behave according to a certain strategy, I can maximize my expected payout according to some other strategy. We can also prove that there are "equilibria", which are situations where everyone has adopted a strategy that is optimal for them, given the strategy everyone else has adopted. People tend to remain in this equilibrium unless they can somehow coordinate with others to move to another one (a collective action problem).

In my opinion, the most astonishing things to come out of game theory are the design Bayesian truth-elicitation mechanisms. These can be thought of survey games, where you answer completely subjective questions such as "do you like anchovies" -- where the answer is impossible to objectively verify -- and these games have an equilibrium where everyone's optimal strategy is to **tell the truth!** 

In [Honesty Games] I will explain how this miraculous-seeming mechanism works.


[TODO: shorten/cut, starting with an overview of how an equilibrium around objectively honest behavior is used to secure blockchains, touching on how auctions can be designed to induce people to reveal how much an object is really worth to them, and finally introducing how survey games such as the Bayesian Truth Serum can induce people to reveal arbitrary subjective beliefs and preferences.]

## Information Games

Getting people to reveal what they honestly think is true could go a long way towards dampening the spread of misinformation. But the problem is that people can be honest and wrong. So out algorithms also need to somehow promote more informed content, or content from experts, or something like that. Once again, the 



Online discussion can be seen as **arguments**, where comments are seen as "reasons" or "information" or "evidence" that people give to justify their upvotes or downvotes. An information-optimization algorithm 








If attention can be rewarded in the form of a payment, then we can apply 





game theorists have proven that it is possible to design games such that the only winning strategy is honesty. And this goes for even highly subjective truths.









sociotechnical engineering

## Truth-Elicitation Mechanisms

The solution lies at somewhere around the intersection of psychology and mathematics. In this series of posts about Honesty on the Internet I will introduce a lot of theory, mostly game theory and Bayesian statistics. But since I want to reach more than just academics or engineers, I will try to interpret and synthesize the research and make the concepts as accessible, intuitive, and math-free as possible. 

## Theory

Many readers may doubt the usefulness of "theory", especially since many of these theories treat people as "rational agents," which any street-smart cynic will assure you is not what people are. But years ago there was a theory that you could fill a metal tube with explosives, light it, and ride it up to the moon. The Apollo program started by asking the same question that I ask here: if we are going to do this difficult but important thing, how, in theory, can it be done? 

And I am going to explain how eliciting honest behavior online is not only **theoretically** possible, but show places where some of this technology has actually been implemented with **fantastic success**. 

But these technologies not been applied at scale in the places where humankind needs them the most: our social networks and online forums where much of public discourse takes place.

## Honesty vs. Truth

What does it mean to "tell the truth?" Colloquially it means **to say what one believes to be true**: to be honest. But this is not the same as speaking truth; **people can be honest and wrong.** 

So **dishonesty** is only one part of the problem. The other part is **ignorance**. An algorithm that gives truth the advantage must therefore tend to amplify content that is **honest** and **informed**. 

Deliberati's proposed overall solution solves both problems, with an algorithm that amplifies content that **that the most informed members of an online community would honestly endorse if they they all shared the same information.**

Our series on [distributed bayesian reasoning] focuses on the problem of information. This series focuses on the  problem of honesty. 


## Next in this Series

- In [The Honesty Game](/subjective-consensus-protocols), I explain how consensus protocols that have been extremely successful in feeding truthful information onto the blockchain, how these protocols are limited to objective or weakly-subjective facts, and how advances mechanisms can be design to elicit truthful feedback for even highly subjective questions where the answers cannot be verified.

- In [Bayesian Truth Serums, Peer Prediction, and Other Mechanisms](/bayesian-truth-serum-and-other-mechanisms), I introduce more advanced mechanisms from game-theory for eliciting truthful feedback even when for subjective, personal questions, and even when participants believe their opinion is in the minority.

In Part IV, I propose that the difference between the Keynesian Beauty Contest, which promotes conformity, and Peer-Prediction family of mechanisms, which promotes honesty, makes all the difference.

Finally: Attention and Reputation


- In Other Parts:
    - Consensus Protocols and the Keyenesian Beauty Contest. These have had practical success...
    - Truth-Elicication Mechanisms Peer Prediction. ..even for subjective questions, such as your favorite flavor of ice-cream. Obviously, an algorithm can't read you mind, yet these clever mechanisms get you to reveal this nontheless.
    - What would Larry Do.
    - Subjective Truth and Echo Chambers
    - Information Theory
    - Game Theory...Common Knowledge...
