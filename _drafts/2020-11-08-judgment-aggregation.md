---
layout: tag
classes: wide
entries_layout: grid
taxonomy: judgment-aggregation
title:  "Judgment Aggregation"
date:   2020-11-08 13:40:43 -0700
header:
    image: /assets/images/daniele-d-andreti-i2zKYlnFnCU-unsplash-wide-1920.jpg
    teaser: /assets/images/daniele-d-andreti-i2zKYlnFnCU-unsplash-wide-1920.jpg
    caption: "photo credit: [**Unsplash**](https://unsplash.com)"
---

In a sense, social media is just a systems that allows social connections to crowdsource our news feeds. Other people not only provide the content for our feeds; data about what other people like or engage with is used by the algorithms to amplify some posts, and suppress others.

But for many reasons, the content people like or engage with does not necessarily reflect what they **judge to be accurate or good**. Indeed often people comment on a post precisely **because** people they it to be harmful, misleading, or unfair.


The field of [Judgment Aggregation](/judgment-aggregation) addresses the question of how to use subjective input from a group of people -- for example what they like, agree with, or engage with -- to output not just a summary of their opinions, but a measure that somehow represents the *reasoned* judgment of the group.



Humans have long used formal processes in attempts to to make better group judgments: from trials and parliamentary procedure, to modern group forecasting techniques as the RAND institutes [Delphi Method](https://en.wikipedia.org/wiki/Delphi_method) and modern techniques of [deliberative opinion polling](https://en.wikipedia.org/wiki/Deliberative_opinion_poll#:~:text=The%20typical%20deliberative%20opinion%20poll,informed%20and%20reflective%20public%20opinion.).

Much has been written about the "wisdom of crowds" and the "madness of crowds.". Which is it? It depends largely on the tools and processes that are used, and the cultures of the people using them.

The essays below describe in more detail our ideas about judgment aggregation and social networks.
