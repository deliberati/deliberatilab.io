
## Kasparov vs the World


> "It is like playing against a collective Deep Blue.”
> 
> -- Gary Kasparov, [Reuters Interview](http://www.chesslab.com/0799/kasparov1.htm), 1999
{: .notice--info}



<img src="/assets/images/distributed-bayesian-reasoning/kasparov-vs-world.jpg"
     alt="Kasparov vs the World"
     style="display: block; float: left; margin-right: 20px; margin-bottom: 20px; height: 300px" />

In 1999, Gary Kasparov played a game of chess against the Internet. Kasparov would move, then any random person on the Internet could vote on the move of "Team World". 

Based on previous experiences with crowdsourced chess games, few people expected a very interesting game. And yet four months, sixty-two moves, and 50,000 players later, Kasparov declared that it had been [**"the greatest game in the history of chess."**](https://en.wikipedia.org/wiki/Kasparov_versus_the_World)

This game was a breathtaking example of **superhuman collective intelligence** -- the capacity for a group of individuals to act, in aggregate, more intelligently than even the most intelligent individuals in the group. Team World achieved these results because they self-organized into a massive distributed brain, where knowledge and ideas propagated from sub-group to subgroup. 

Collective intelligence emerges naturally in healthy communities: the scientific community may be the best example. But it rarely emerges in online communities, where the natural dynamics of healthy human communication are distorted and manipulated.

Distributed Bayesian Reasoning is meant to reproduce the natural processes that give rise to collective intelligence, but deliberately, and computationally.
