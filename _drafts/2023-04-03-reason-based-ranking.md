---
layout: single
title:  "Reason-Based Ranking"
toc: true
toc_sticky: true
sidebar:
  - title: "In This Series"
    nav: "give-truth-the-advantage"
  - nav: "give-truth-the-advantage-related"
    title: "Related Articles"
# header:
#     teaser: /assets/images/daniele-d-andreti-i2zKYlnFnCU-unsplash-wide-1920.jpg
#     caption: "photo credit: [**Unsplash**](https://unsplash.com)"

---

Reason-based ranking algorithms direct attention to social-media posts that people can defend with reasons.

Obviously, reasonableness is subjective. Nevertheless, reason-based ranking algorithms provide a simple, fair, transparent procedure for estimating how well a post stands up to the scrutiny of an online community.

## How it Works

Much of the false and misleading information that circulates on the internet is very easy to debunk. Sometimes all it takes is a link.

We can find lots of examples in Twitter's Community Notes. For example, in the tweet below, @JebraFaushay claimes that the photo was a celebration of Biden's election (possibly believing it to be true). Community Notes users added "Context" to the tweet, debunking the claim with a link to the source of the original photo. If you click on the link in the Tweet below, you see that this photo is indeed from a 2012 LA Times article, published four years before Biden's election.

https://twitter.com/JebraFaushay/status/1649260058461585408

other examples:
https://twitter.com/mirandadevine/status/1649759660851724289
Eating Eggs: https://twitter.com/nutrition_facts/status/1645124476055568384
Biden vacation time: https://twitter.com/RNCResearch/status/1649556541060665348
Fico Score: https://twitter.com/unusual_whales/status/1649730013493882880
McDonalds: https://twitter.com/WallStreetSilv/status/1649488567356227585

https://pbs.twimg.com/media/FglCM27WQAE1LVg?format=jpg&name=large



A Community Note attached to a post can be thought of an **argument** against the tweet -- a reason that people might not want to share it. The argument is "attached" to the post -- it is shown every time the post is shown -- which may discourage people from sharing the post. 

Community Notes does a good job of identifying misleading posts and adding useful context that a diverse group of people find helpful. It probably helps prevent misinformation from spreading. 

But a reason-based ranking algorithm goes further.

First of all, reason-based ranking adds counter-arguments. A majority of a diverse group of people might find the argument helpful and convincing, but they might be wrong. Reason-based ranking is recursively: a note can be attached to a note. 


 It explicitly reduces the attention received by posts that cannot

It not only reduces the amount of attention directed to misleading posts, it actually helps to undo the damage done by the post by bring the argument to the attention of the group of people who have seen, liked, shared shared the missing post. 



1. it explicitly stops directing attention misleading post
1. it directs more attention to the argument -- especially from people who have already seen the rumor, and most importantly among those who have reposted or upvoted it
1. it gives people the chance to respond to the argument by either:
  - retracting
  - providing a counter-argument

Below we'll show how reason-based ranking can transform the dynamics of online discussion, using reason and argument to help a group manifest its collective intelligence, discouraging misinformation and abuse, amplifying factually accurate and morally defensible content, and driving constructive, informed, intelligent conversation.



Every day on social media people are making **arguments**. They are using facts, anecdotes, and logic to defend their opinions or challenge others. Reason-based raking algorithms focus attention on constructive arguments and content that stands up to scrutiny. 



## Falsehood Flies

Thomas Swift said "falsehood flies, and the truth comes limping along after it". With a reason-based ranking algorithm, falsehood may still fly, but now the truth can fly just as fast, spreading along the very same social patheways by which the lie spread in the first place.

What's more, the system can identify which comments, if any, lead to the greatest change in behavior: either a retraction, or a reduction in the probability of sharing or upvoting in the first place. It can then channel attention to these comments. 

The anecdote to misinformation is corrective information.


## Retraction: Creating Accountability

A reason based ranking algorithm should also encourage **retraction**. Since many of the people who upvoted or shared the misleading post acted based on carelessness, not malice, if the context is informative and convincing, some of them may retract.

If a user retweets a post after seeing the attached note, people will know that they saw the argument and retweeted anyway. 

TODO: game-theoretical dynamic


Although people's reputation usually suffers little from believing something that everyone else believes, even if it is later shown to be false, a person's reputation will be more severely damaged by endorsing something when everyone knew that everyone knew it was false.

Further, if a user sees a convincing argument and decides they made a mistake in posting or retweeting, instead of simply deleting the tweet, a retract feature will (work like this)

Encouraging a culture of retraction rather than deletion fosters healthier public discourse. Retractions signal an honest admission of error, without erasing the history of the conversation or depriving users of the opportunity to learn from the exchange. 

A retraction feature turns what would have been viral piece of misinformation into a vaccine (the antibody of misinformation being argument). The retraction, and the argument that led to the retraction, should receive as much or more attention as the tweet itself. The antibody will propagate through the social network by the same pathways the original tweet propagated.




, first of all, downrank misleading tweets, giving them less attention. But 

. Now, a community note doesn't always mean a tweet is misleading, but 





Instead of driving attention towards controversial posts, the platform should drive attention to your comment: a subset of people who have already seen the rumor, especially those who have reposted or upvoted it, should be shown your comment and asked if they would like to **retract** their endorsement. Since many of those people acted based on carelessness, not malice, if your comment truly is informative and convincing, some of them may retract.





Every day on social media people are making **arguments**. They are using facts, anecdotes, and logic to defend their opinions or challenge others. Reason-based raking algorithms focus attention on constructive arguments and content that stands up to scrutiny. 





Reason-based ranking goes one step further, and ensures that debunked tweets receive less attention.

accountability
doesn't necessarily mean...
the criteria should be this: does it get less attention
informed
retraction 



Suppose some easily-debunkable rumor has been circulating. It spreads because people tend to be careless about repeating things they've heard; one rarely takes the time to verify that a rumor is true before upvoting or sharing it, especially if it comes from a credible source. In other words, the rumor spreads because people lack information.

But suppose you happen to possess information others don't. You know that it's just a rumor. You can flag this post as being false or misleading, and leave a comment explaining **why**.

Now in many social platforms, your engagement will only lead to more exposure for the rumor. Such engagement-based algorithms create perverse incentives for people to produce content that may not stand up to scrutiny.

Instead of driving attention towards controversial posts, the platform should drive attention to your comment: a subset of people who have already seen the rumor, especially those who have reposted or upvoted it, should be shown your comment and asked if they would like to **retract** their endorsement. Since many of those people acted based on carelessness, not malice, if your comment truly is informative and convincing, some of them may retract.



----

Twitter's Community Notes implements the first step in a reason-based ranking algorithm. Unfortunately, as of the time of this writing (April 2023), Twitter hasn't yet realized its potential. 






This is easiest to explain with an example. 



Every day on social media people are making **arguments**. They are using facts, anecdotes, and logic to defend their opinions or challenge others. Reason-based raking algorithms focus attention on constructive arguments and content that stands up to scrutiny. 


## How it Works

Suppose some easily-debunkable rumor has been circulating. It spreads because people tend to be careless about repeating things they've heard; one rarely takes the time to verify that a rumor is true before upvoting or sharing it, especially if it comes from a credible source. In other words, the rumor spreads because people lack information.

But suppose you happen to possess information others don't. You know that it's just a rumor. You can flag this post as being false or misleading, and leave a comment explaining **why**.

Now in many social platforms, your comment will be interpreted as an engagement signal and only lead to more exposure for the rumor, and not necessarily for your comment. Such engagement-based algorithms create perverse incentives and toxic outcomes. A platform willing to optimize for anything other than short-term engagement should instead drive attention to your comment: a subset of people who have already seen the rumor, especially those who have reposted or upvoted it, should be shown your comment and asked if they would like to **retract** their endorsement. Since many of those people acted based on carelessness, not malice, if your comment truly is informative and convincing, some of them may retract.

## Falsehood Flies

Thomas Swift said "falsehood flies, and the truth comes limping along after it". But with this algorithm, falsehood may still fly, but now the truth can fly just as fast, spreading along the very same social patheways by which the lie spread in the first place.

What's more, the system can identify which comments, if any, lead to the greatest change in behavior: either a retraction, or a reduction in the probability of sharing or upvoting in the first place. It can then channel attention to these comments. 

## Driving Informed Conversations

A comment will not change behavior unless it tells them something people didn't already know, or hadn't already considered. So this algorithm can be seen as simply concentrating attention on the comments with the highest information content.

Now of course, your informative comment may itself be a lie. Perhaps the rumor was true after all. That is why it is so important to drive **conversations**. People get to have their say, you get to say why you disagree, and *then they get to respond to you*. If somebody responds with a counter-argument rebutting your argument and explaining why the rumor was true after all, then that new information will spread along the same pathways by which your comment spread. Everyone who retracted their endorsement of the rumor based on your comment will be asked if they want to un-retract based on this new information. 

And so the conversation thread will continue as long as comments are providing information that influences belief as expressed by voting behavior. At some point, all the arguments will have been made, and all the information there is to share will have been shared, and further comments will only rehash previous points without changing minds. At that point there is no need for any more attention to be spent.





## Arguments can Change Minds

Understanding reason-based ranking requires accepting that arguments on the internet often change minds. A cynical will tell you this never happens. And yet cynical conventional wisdom also says that social media has tremendous influence on public opinion. These can't both be true. In fact, people are influenced by social media, and although they may not always be influenced by what seem to us as rational argument, the are influenced by something: ideas, information, anecdotes. We will generously classify any information that changes minds as an argument.

Reason-based algorithms work by identifying and directing attention to:
  - the most persuasive arguments
  - content that stqands up to scrutiny.

Now, a convincing argument may itself be factually inaccurate. So the algorithm evaluates counter-arguments and counter-counter-arguments in a recursive manner, highlighting the most compelling line of reasoning, and identifying content that stands up to critical scrutiny. 

## "Dangling" Tweets

 Sometimes, a tweet goes viral based on initial reactions, but it does not "age well". A new fact comes to light, a prediction proves to be false, public opinion changes. However the tweet may continue to spread even if the poster no longer endorses it.

The reason-based ranking algorithm addresses this issue by automatically de-amplifying content that users don't or can't defend.

## Antibodies

Misinformation often spreads on social media due to ignorance or laziness, rather than malice. A reason-based ranking algorithm ensures that users are exposed to well-founded arguments against a tweet, attaching them in the same way community notes are attached now.

  Todo: screenshot. X people found this convincing

## Retraction: INcentivizing Reason

If a user retweets a post after seeing the attached argument, people will know that they saw the argument and retweeted anyway. Although people's reputation usually suffers little from believing something that everyone else believes, even if it is later shown to be false, a person's reputation will be more severely damaged by endorsing something when everyone knew that everyone knew it was false.

Further, if a user sees a convincing argument and decides they made a mistake in posting or retweeting, instead of simply deleting the tweet, a retract feature will (work like this)

Encouraging a culture of retraction rather than deletion fosters healthier public discourse. Retractions signal an honest admission of error, without erasing the history of the conversation or depriving users of the opportunity to learn from the exchange. 

A retraction feature turns what would have been viral piece of misinformation into a vaccine (the antibody of misinformation being argument). The retraction, and the argument that led to the retraction, should receive as much or more attention as the tweet itself. The antibody will propagate through the social network by the same pathways the original tweet propagated.


Quote from Thomas Swift about "falsehood flies"


A reason-based algorithm could calculate reputation scores based on post-argument popularity, promoting users who can defend their content and demoting those with a history of unverified or misleading information.

TODO: Game theory

Building on the concept of community notes, the reason-based ranking algorithm fosters a collaborative effort to counter misinformation and drive intelligent conversations. 




----






So the algorithm applies the same principle recursively to informative counter-arguments, and counter-counter-arguments, and so on, directing attention to the thread of the most convincing arguments until minds are no longer being changed.




This allows the algorithm to identify tweets that can *stand up to scrutiny*. Sometimes, a tweet will receive a lot of likes and retweets based on people's first impressions and knee-jerk reactions (todo: example), but then after the comments or a community note make it clear that the tweet was misguided (todo: biden example), the poster will delete the tweet. However in other cases, users simply abandon a tweet they no longer feel they can stand behind, not defending it but not bothering to delete it either. This tweets may continue to get significant attention, continuing to spread misinformation long after it has been abandoned. A reason-based ranking algorithm solves this.

A reason-based ranking algorithm well automatically de-amplify tweets that people don't or can't defend. 

Although false and misleading information often has a malicious origin, it starts spread through social networks largely because of ignorance and laziness. For example, many people spread a rumor because it sounds plausible and here it from a trusted acquaintance, even though the rumor can be easily debunked by verifying an original quote or statistic. A reason-based ranking algorithm ensures that when someone in your network has responded with such quotes or statistics that other people have found convincing, that this argument also comes to your attention. The argument will be **attached** to the tweet, much as community notes today, so that it comes to your attention before you choose to upvote or retweet it.

  Todo: screenshot. X people found this convincing
  
If you retweet it anyway, your tweet will have the argument attached, and people will know that you saw the argument and retweeted anyway. Although people's reputation usually suffers little from believing something that everyone else believes, even if it is later shown to be false, a person's reputation will be more severely damaged by endorsing something when it can be shown they already knew it to be false. The result will be that people will take care that the information they share can stand up to scrutiny.
TODO: game theory

Further, if a user sees a convincing argument and decides they made a mistake in posting or retweeting, instead of simply deleting the tweet, they can retract it.

A retract feature is much healthier to public discourse than a delete feature. A retraction will generally be perceived as an honest admission of error, instead of a sneaky attempt to evade accountability. A retraction does not alter history, erasing what was said and the conversation around it, robbing users of the ability to learn from it. 

Instead it turns what would have been viral piece of misinformation into a vaccine (the antibody of misinformation being argument). The retraction, and the argument that led to the retraction, should receive as much or more attention as the tweet itself. The antibody will propagate through the social network by the same pathways the original tweet propagated.


Quote from Thomas Swift about "falsehood flies"

A reason-based algorithm could calculate reputation scores based on post-argument popularity. Users whose past tweets have not stood up well to scrutiny would be de-amplified, and users with a history of tweets that they or their followers can defend will receive more attention. 

This should result in second-order positive effects. Users will learn that success on Twitter requires fact-checking tweets before posting. 

Conclusion. Work in the following points.
- tweets that does not age well
- tweets that are ratioed
- gameify argument
- takes the idea of community notes to the next level
- countering misinformation and driving more intelligent conversations.






retracting

did not age well


---


In the [What Deserves our Attention]?, I discuss principles for designing the protocols that determine what content gets attention in a social platform? In [Give Truth the Advantage], I suggest some solutions
