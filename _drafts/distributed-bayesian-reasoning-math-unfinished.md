##


### Junk


JUNK


$$
\begin{align} \displaystyle 
    


    P_i(A)  &= \\
            &\begin{aligned}
                &= P_i(A|b≥0) ~~~~ &&\text{definition of P_i}\newline\\
                &= P_i(B=0)P_i(A|B=0) ~~~~&&\text{law of total prob}\\
                &~~~+ P_i(B=1)P_i(A|B=1)\newline\\
                &= P_i(B=0)P(A|B=0) ~~~~&&\text{definition of P_i}\\
                &~~~+ P_i(B=1)P(A|B=1 ∧ C≥0)\newline\\
                &=    P_i(B=0) P(A|B=0)~~~~&&\text{same as} ~ \eqref{eq:informed}
            \end{aligned}\\

            &~~~~\begin{aligned}
                    + P_i(B=1) &\sum_{c≥0} P(c|B=1) \\
                          &× P(A|B=1,c)
            \end{aligned}


    \end{align}
$$


and


        B
      ↗ ↓
    I   A
      ↘ ↑
        C

$$

\begin{aligned} \displaystyle

    P(a|&do(I)) \\
               = &∑_b ∑_c P(a|I,b,c)P(b,c) && \text{back-door adj.} \\
               = &\left. ∑_c P(a|I,b,c)P(b,c) \right|_{b=0} \\
                 &+ \left. ∑_c P(a|I,b,c)P(b,c) \right|_{b=1} && \text{I ⟹ b ≥ 0}   \\
               = &\left. ∑_c P(a|b,c)P(b,c) \right|_{b=0} && \text{b=0 ⟹ I} \\
                 &+ \left. ∑_{c≥0} P(a|b,c)P(b,c) \right|_{b=1} && \text{b=1,c≥0 ⟹ I}  \\
               = &~P(B=0)P(a|B=0) && \text{law of total prob} \\
                 &+ P(B=1)∑_{c≥0} \\
                 & ~~× P(c|B=1)P(a|B=1,c)


\end{aligned}

$$

    front-door formula 

$$

    p(y|do(x)) = ∑_z p(z|x) ∑_x' P(y|x',z)p(x')$$

    ∑_x' P(y|x',z)p(x') = ∑_x' P(x',y,z) / p(z|x') 
                        = p(y|do(z))

$$



## Multiple Argument Chains

TODO: Often when processing independent pieces of evidence, the independence assumption is made. Then you can just multiply...but this won't work here. However, in this case this is not a problem, because we have a joint probability formula for the entire chain.

-----

## Subjuries


---------




## Some Observations 

- even if C is not explicitly submitted as a counter-argument to B, we can still look at the data and see if C supports `𝐴 +> B` -- e.g. counting only people who voted for both C and B, are people more likely to accept 𝐴 if they accept both premises than if they just accept the first.

- this system does not require voting about warrant. People only vote on the claims, and it infers warrant based on those votes.

## TODO: Example with Counter-Argument

TODO: extend the above example where counter-argument C is submitted, 10/10 agree with C, and half of those who voted on C also Voted against 𝐴 even if they agreed agreeing with B. So `𝑃(𝐴|𝐵,𝐺) < 𝑃(𝐴|𝐵)`, meaning `𝑆(𝐴 +> B, C) < 1` (𝐺 opposes `𝐴 +> B`), resulting an a posterior support of `sup'(𝐴, 𝐵) =~ 1` -- even though the prior support was `𝑆(𝐴,𝐵) = 8/5`, resulting in a posterior P'(𝐴) that is approximately the same as the prior 𝑃(𝐴) because the arguments for A have low posterior warrant.


## Analyzing the Controversy

Formula 1 allows us to measure how much the warrant of B and acceptance of B separately contribute to the acceptance of A. And it highlights the easily-overlooked fact that *rejection* of B has a different effect on A than mere non-acceptance of B.



## Rule of Succession Priors

When dealing with sparse data (not a lot of votes) -- especially in the case of multiple premises -- we can use Laplace's "rule of succession" to create more reasonable prior estimates. This formula would define a prior:

     𝑃(𝐴) = (t(𝐴) + 1 ) / ( t(𝐴) + t(!𝐴) + 2). 

- So if only 1 person voted on A, and that person rejected A, we'd estimate `𝑃(𝐴)` not as `0/1 = 0%`, but as `(0 + 1) / (1 + 2) = 1/3`. 
- But if 100/100 people rejected A, `𝑃(𝐴)` would be `(0+1) / (100 + 2) = 1/102` (e.g. approaching zero).
- If the an equal number of people vote for/against, then the prior will be 1/2, regardless of the total number of votes.

# TODO: Conclusion


# TODO: Discuss

- Measuring Controversy: Probability of consensus of A will increase most by arriving at consensus on this point in the tree

- POssibility of explicit warrant votes. If I accepted B I'd accept A.

- Discuss interpretation of formula. It is not the probability that it's true. It's the probability that a voter will think it's true given the evidence.

# Additional Thoughts



## Log-Scale Relevance Score

The Baysian support `𝑆(𝐴, 𝐵)` is an obvious candidate for the "warrant score" `r(𝐴,𝐵)` that we have looking for.

But I wonder if representing this score using a log scale would better mesh with the idea of "warrant" of arguments. We could define:

    r(𝐴, 𝐵) = lg( 𝑆(𝐴, 𝐵) )

Or in reverse

    𝑆(𝐴, 𝐵) = 2^r(𝐴, 𝐵)

So:

- if `r(𝐴, 𝐵) = 1`, then `𝑆(𝐴, 𝐵) = 2`, meaning accepting B doubles our posterior estimate for A. 
- if `r(𝐴, 𝐵) = -1`, `𝑆(𝐴, 𝐵) = 1/2`, so accepting B halves the estimate
- if `r(𝐴,𝐵) = 0`, `𝑆(𝐴, 𝐵) = 1`, meaning B makes no difference. In other words, a score of 0 means "irrelevant", a positive score means support, and a negative score means opposition. 
 
If supporting claim B has negative warrant, it means it is effectively an opposing claim -- it supports the negation of A. And conveniently, it holds that:

    r(!A, 𝐵) = -r(𝐴, 𝐵)

The absolute value of the warrant score is the same whether a premise is viewed as opposing the conclusion or supporting the conclusion.

This warrant score can range from zero to infinity.

A sum of warrant scores can be meaningful, as it translates to a product of supports, which is used in Naive Bayes estimates.


# Notes

## Bentley and MAPs Scoring Discussion

𝑆(𝐴): set of supporting statements
O(𝐴): set of opposing statements

𝑆(𝐴, 𝐵): the warrant of A's support of B
o(𝐴, 𝐵): the warrant of A's opposition of B

## MAP's simple formula

At = 
    Max
        max(0, Xt-Xf) * 𝑆(X, 𝐴) 
        for X in 𝑆(𝐴)


Af = 
    Max
        max(0, Xt-Xf) * o(X, 𝐴) 
        for X in O(𝐴)

Okay what reasoning does this represent? Take all the reasons, use the highest NET truth score times warrant score.

