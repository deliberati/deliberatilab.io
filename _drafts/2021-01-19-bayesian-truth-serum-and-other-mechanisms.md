---
layout: single
title:  "Bayesian Truth Serums, Peer Prediction, and Other Mechanisms"
date:   2022-01-19 13:40:43 -0700
toc: true
toc_sticky: true
sidebar:
  - nav: "honesty-on-the-internet"
    title: "In This Series"
  - nav: "honesty-on-the-internet-related"
    title: "Related Articles"
excerpt: "TODO excerpt for subjective-consensus-protocols"

---


# Bayesian Truth Serums, Peer Prediction and Other Mechanisms...
...for getting people to tell the truth on the internet

## Introduction

This is the third post in my series on [Honesty on the Internet](/honesty-on-the-internet). It is an attempt to explain my own understanding of the family of game-theoretic mechanisms for getting people to tell the truth about their own personal opinions or preferences, even when there is no way to verify the answers. These mechanisms include [the Peer-Prediction method](http://presnick.people.si.umich.edu/papers/elicit/FinalPrePub.pdf) and the [Bayesian Truth Serum](https://economics.mit.edu/files/1966), and various other related mechanisms.

The tl;dr; of **my** intuition about these mechanisms work is that **your own opinion is information**. It correlates with other people's opinions, and can therefore be used to help predict them. These mechanisms reward honesty by rewarding information that helps improve predictions.

[TODO: If I am taking a survey and you answer honestly, it will give a better idea of what the next person's honest answer will be. If you answer randomly there will be no correlation. 

In [another essay](/truth-induction-and-information-theory), I explore an information-theoretic perspective on these mechanisms and propose a reputation system for social platforms based on cross-entropy.

### Your Opinion is Information

Here's a story to illustrate the key to designing mechanisms for inducing honest feedback.

There are 12 jurors. The first 10 have voted on the verdict, and their vote is divided 5:5. For whatever reason, the 11th juror wants to predict the vote of the 12th juror. But given the divided vote of the first 10 jurors, it would seem that the 11th juror has no way of guessing how the 12th juror will vote.

However, the 11th juror has other information besides the votes of the first 10 jurors: **his own opinion**. His opinion is a "sample of one". If he steps into a more objective frame, he will see that there are actually 11 data points to consider: the votes of the first 10 jurors, and his own opinion. If the 11th juror thinks the defendant is innocent, that means 6/11 jurors believe the defendant is innocent (or at least say so). All other things remaining equal, an objective observer would have reason to guess that the 12th juror is also slightly more likely to vote innocent. The 11th juror, reasoning objectively, should come to the same conclusion.

This is a critical insight. The juror should recognize that **their own opinion is information** that correlates with other people's opinions. It means that, the mere fact that he believes something makes it **more likely that other people believe it too**. 

This line of thinking suggests that people who hold a some opinion or preference should **correctly** predict that opinion or preference to be more common than the average person predicts it to be! For example, people who rate Picasso as their favorite painter should theoretically give higher estimates of the proportion of the population that shares this opinion. It turns out, empirical data suggests this is indeed the case [ref: 21. R. M. Dawes, J Exp Soc Psychol 25, 1 (1989)]. As Drazen Preloc wrote in a [paper on the Bayesian Truth Serum](https://economics.mit.edu/files/1966)


> It is one of the most robust findings in experimental psychology that subjects’ self-reported characteristics — behavioral intentions, preferences, opinions — are positively correlated with their estimates of the relative frequency of these characteristics (19). The psychological literature initially regarded this as an egocentric error of judgment (a ‘false consensus’) (20) and did not consider the Bayesian explanation, as was pointed out by Dawes (16, 21). There is still some dispute whether the relationship is entirely consistent with Bayesian updating (22).

The same holds no matter what the question, even if it is completely subjective. *Do you prefer Chocolate or Strawberry ice cream?* *Have you ever cheated on your tax returns?* As personal as the answers may be, they are not random. As a member of the human race everything about you correlates with other people, and therefore has value as information that can help predict facts about others.

### Eliciting Truth Without Verification

Suppose that, for some reason, the 11th juror is rewarded in some way if he votes the same way as the 12th juror. How should he vote to maximize the chance of a reward? 

Well, if he thinks the defendant is innocent, he expects the 12th juror to agree, and vice versa. So **his best strategy is to simply vote honestly**.

Game Theorists have published several papers since 2004 that build on this core idea to design mechanisms for eliciting honest information "without verification". In other words, for getting people to tell the truth without any way of checking if what they say is actually true: for finding out for example how common it is for people to cheat on their taxes without extensive audits or investigations, or to discover out personal details such as how many sexual partners people have had without putting cameras in everyone's bedrooms. Two seminal papers in this field include [the Peer-Prediction method](http://presnick.people.si.umich.edu/papers/elicit/FinalPrePub.pdf) (by Nolan Miller, Paul Resnick, and Richard Zeckhauser, from Harvard and the University of Michigan) and [the Bayesian Truth Serum](https://economics.mit.edu/files/1966) (Drazen Prelec from Harvard). And there have been [some studies](http://www.eecs.harvard.edu/cs286r/courses/fall10/papers/DW08.pdf) empirically validating that these mechanisms do result it more honest answers. And many other papers have since been published refining these mechanisms (TODO: bayesian Markets, multi-agent, coordinated agreement).

These mechanisms all work by **paying** respondents for their anonymous answers, and then performing a kind of Bayesian judo, making it so that in order to maximize your expected payment, you have to reveal the private information you have about your own opinions or preferences. It works because that information, if true, measurably correlates with other people's opinions.

In order to illustrate how this might work, our contrived example glosses over some important details: we assume the other  jurors are always honest, we ignore any prior beliefs the jurors my have about other jurors' opinions, and we of course make the standard game-theory assumptions that the jurors are rational agents and the payments actually motivate them. We'll talk about all of these later in this essay.


### The Problem with Prior Beliefs

A Bayesian reasoner cannot revise their beliefs based on data unless they have prior beliefs to revise. In our example, if somebody observes the 5:5 vote of the first 10 jurors and concludes that the next juror has a 50% chance of voting guilty, it implies that **prior** to observing these 10 votes they had no reason to lean one way or another, and the votes of the first 10 jurors didn't provide evidence for changing this.

But a juror might have other priors. He might for example think that, a priori, 90% of jurors generally vote guilty on any given trial, or something like that. In this case, after observing the split votes of the first 10 jurors, his would still think the next juror is more likely than not to vote guilty; his new belief would fall somewhere between his prior of 90% and the 50% implied by the new data.

Then, after factoring in his own "sample of 1" opinion, his posterior belief about the next juror's vote would likely still be above 50%, even if he personally believes the defendant is innocent. 

This means that to maximize his payout, he should predict that the 12th juror will vote guilty, no matter what he individually believes. So a mechanism that rewards the 11th juror for voting the same way as the 12th juror won't reward truth-telling. It would reward conformity.

### Peer Prediction

The [Peer-Prediction](http://presnick.people.si.umich.edu/papers/elicit/FinalPrePub.pdf) mechanism solves this problem by assuming that the jurors have common prior beliefs (about other jurors beliefs), and that this prior is known to the mechanism.

This assumption severely limits the places where this method can realistically be used. But it simplifies things so it is a good place to start; other more advanced methods build off peer prediction but drop the common prior assumption.

It works like this: there is some agent in charge of providing payments. We call this agent **the center**. The center shares the common prior with the participants. 

And of course, we assume that the center and all the agents are proper Bayesian reasoners. So after the center and the 11th juror both observe the votes of the first 10 participants, they will have the same posterior...

...almost. They have the same priors, and the same information about the other 10 votes, but the 11th participant will have one data point the center doesn't have: **his own opinion**.

The 11th juror then votes, and the center assumes his vote is honest and updates her (the center's) own beliefs given this new information. The juror and the center thus now share the same posterior -- the same prediction about the opinions of the 12th juror.

The center than pays the 11th juror based on **the accuracy of that prediction**. More specifically, the payment is calculated such that the juror's **expected** payment increases with the accuracy of the center's predictions after taking into consideration his vote.

### Proper Scoring Rules

What we need now need is a formula that measures the accuracy of a prediction. This function should look at the actual outcome (e.g. the 12th juror voted guilty) and the probability that was predicted for that outcome (there was 75% chance the 12th juror would vote guilty), and assign some sort of score. Clearly the score should be high if the outcome that occurred was predicted to be probable, and low if it was predicted to be improbable.

A seemingly obvious formula would be to pay 1 point if the outcome that occurred was predicted to be more than 50% likely. Or better, to pay in direct proportion to how likely actual outcome was predicted to be. But it turns out this doesn't work: the expected payment ends up being maximized just by predicting 100% or 0% (this is because the expected value is a linear function of the predicted probability).

Fortunately academics have invented many rules for scoring the accuracy of a prediction that have the desired properties. They are called [proper scoring rules](https://www.cis.upenn.edu/~aaroth/courses/slides/agt17/lect23.pdf). It turns out, we can make our scoring rule proper simply by taking the **log** of the predicted probability.

Using the log scoring rule, the 11th juror's expected score is maximized if his posterior beliefs matches the center's posterior beliefs. Since the center is a Bayesian reasoner with the same priors as the 11th juror, any difference in posteriors beliefs is the result of information asymmetry. By simply **telling** the center his opinion on the verdict, it erases the information asymmetry. Now the two agents have the same priors, and the same information (11 votes), thus the same posteriors.

The log of a probability will always be a negative number: so instead of maximizing a score, the juror is trying minimizing a penalty. In [another article](/truth-induction-and-information-theory) we'll see that we can convert this into a positive score representing a **reduction** in entropy, or the amount of Shannon information provided by the answer.


<aside class="custom-aside" markdown="1">

## Mathematical Details

This section delves into the mathematical details and works through the calculations for our jury trial example. This helps to develop the intuition for why this works. It assumes basic understanding of probability theory (probability density functions, expected values, and Bayes rule). But the reader can skip this section and I believe still understand the rest of this article.

### Modeling Beliefs about Beliefs

Beliefs about other people's beliefs are best represented not by a single probability, but by a range of possible probabilities.

For example, let's say θ is the 11th jurors belief in the probability that a subsequent juror will vote guilty. The 11th juror might start with a prior estimate of θ=50%, meaning basically they don't know how subsequent jurors will vote. But then suppose that after the first juror votes guilty, they revise that estimate up to θ=66.67%. 

Now this implies that the 11th juror thought that θ=66.67% was possible in the first place: for the rules of Bayesian belief revision do not allow a rational agent to believe something that he previously believed was impossible[^1]. This is an important and subtle point. The 11th juror's beliefs about other people's beliefs are not just a single number θ: it includes a whole range of possibilities for θ: 0%, 100%, and everything in between, and each of these values of θ has some prior probability.

[^1]: If $$P(B) = 0$$, then $$P(A \vert B) = P(A,B)/P(B) = P(A,B)/0$$. So if $$P(A \vert B)$$ is defined, $$P(B)$$ must be greater than 0.

So his beliefs can be better described with a probability density function, that assigns probability density to each possible value of θ. Since thinking about "probabilities of probabilities" is confusing, it is useful to think of θ as the **ratio** of future jurors that will vote guilty.

The 11th jurors might not have particularly strong beliefs, thinking that all possible values of θ from 0 to 1 are equally probable. In this case, his beliefs would be best represented by a uniform probability density function:

$$
    P(θ) = 1
$$

The average, or expected value of θ in this case is 50%. If asked to estimate the probability that subsequent jurors vote guilty, this would be his best estimate. Using $$G$$ to represent the event that the first juror votes guilty, we can say that:

$$
    P(G) = \mathbf{E}~θ = 0.5
$$

Now, after the first juror votes guilty, how does the 11th juror update his beliefs? 

We can apply Bayes Rule to come up with a new posterior probability density function:

$$
    P(θ|G) = \frac{P(G|θ)P(θ)}{P(G)}
$$

$$ P(G \vert θ) $$ is just by definition $$θ$$. And we just defined $$ P(θ) = 1 $$ $$P(G) = 1/2$$. So the posterior belief is a new probability density function:

$$
\begin{aligned}
    P(θ|G)  &= \frac{P(G|θ)P(θ)}{P(G)} \\
            &= \frac{θ}{1/2} = 2θ
\end{aligned}
$$

This is a "triangle" distribution, a line sloping upwards from 0 to 2. The expected value of this distribution is:

$$ 
\begin{aligned} \displaystyle
    \mathbf{E}~θ &= \int_0^1 θ p(θ) ~ dθ \\
    &= \int_0^1 2θ^2 ~ dθ  \\
    &= \left. 2/3θ^2 \right|_0^1 = 2/3 = 66.67\%
\end{aligned}
$$


So after the first juror votes guilty, a rational agent would expect there to be a 66.67% chance that the next juror votes guilty.

### The Beta-Bernoulli Model

After observing more votes, we could keep on using Bayes rule + calculus to update beliefs...but these calculations have already been done for us and generalized in the form of the [beta distribution](https://en.wikipedia.org/wiki/Beta_distribution) function. The probability density function $$beta(θ \vert α, β)$$, has parameters α and β, which start out at 1 for a uniform distribution. Then α is incremented by 1 for every subsequent guilty vote that is observed, and β is incremented by 1 for every innocent vote.

This distribution is super easy to work with, since now Bayesian belief revision reduces to simple addition. What's even better, the mean of the beta distribution is just $$\frac{α}{α+β}$$! 

So all that math we did above to come up with 66.67% as the expected value of θ after a single guilty vote? We could have reduced that to simple arithmetic. We start with α=1, β=1 (representing a uniform prior). We then observe 1 guilty vote so now we have α=2, β=1 (representing that triangle distribution we derived above). And the average of this distribution is just:

$$
    𝐸~θ = \frac{α}{α+β} = \frac{2}{2+1} = 2/3 = 66.67\%
$$

### The Beliefs of the 11th Juror

So back to the 11th juror. If he has observed 5 guilty and 5 innocent votes, the parameters are 

$$

    α=β=5+1=6

$$

Since α and β are still equal the mean is still 50%. 

Now suppose the 11th juror thinks the defendant is innocent. Then his new posterior, considering his own belief as a "sample of one", is now $$α=6, β=7$$, and the mean is $$6÷(6+7) ≈ 46.2\%$$. So the 11th juror believes there is a 46.2% chance that the 12th juror will vote guilty.

Let's use $$g(x \vert y)$$ to notate this posterior estimate of the probability that the 12th juror will vote 𝑥 given the the 11th juror votes 𝑦. So $$g(x \vert y)$$ is the mean of the beta distribution after adding the 11th juror's opinion to α (for guilty) or β (for innocent). So:

$$

\begin{aligned} \displaystyle

    g(G|G) &= \frac{α+1}{(α+1)+β} = \frac{7}{7+6} &≈ 53.8\%\\
    g(G|¬G) &= \frac{α}{α+(β+1)} = \frac{6}{6+7} &≈ 46.2\% \\

\end{aligned}

$$

and then of course

$$

\begin{aligned} \displaystyle


    g(¬G|G) &= 1-g(G|G) &≈ 46.2\%\\
    g(¬G|¬G) &= 1-g(G|¬G) &≈ 53.8\%

\end{aligned}

$$



### Expected Score

Now let's say $$q(x)$$ is the center's posterior belief in the probability that $$X=x$$ (after revising her beliefs based on the 11th jurors vote). The log scoring rule $$S(q,x)$$ will assign juror 11 a score of:

$$
    S(q,x) = \log q(x)
$$

Let's denote the 11th juror's own posterior belief by $$p$$, and the 11th jurors expected score by $$S(p;q)$$. So:

$$

\begin{aligned} \displaystyle

    S(p;q)  &= E ~ S(q,x) \\
            &= \sum_x p(x) S(q,x) \\
            &= \sum_x p(x) \log q(x) \\
            &= p(¬G) \log q(¬G) + p(G) \log(q(G))

\end{aligned}

$$

As this is a proper scoring rule, it can be shown that this value is maximized if $$p = q$$: that is, if the center and the 11th juror have the same posterior because the 11th juror voted honestly. The expected score becomes more negative as 𝑝 and 𝑞 get further apart.

### Honesty Maximizes the Expected Score

Since the 11th juror believes the defendant is innocent. His posterior $$ p(∙) = g(∙ \vert Y=1) $$.

If the 11th juror tells the center the truth, the center's posterior will be the same as his posterior, so $$q = p$$, and his expected payment is therefore:

$$
\begin{aligned} \displaystyle

        S(p;q)  &= S(p;p) \\
                &= p(¬G) \log p(¬G) + p(G) \log(p(G)) \\
                &= g(¬G|¬G) \log g(¬G|¬G) \\
                &~~+ g(G|¬G) \log(g(G|¬G)) \\
                &≈ 53.8\% \log 53.8\% + 46.2\% \log 46.2\%  \\
                &≈ -0.481 - -0.515\\
                &≈ -.996
\end{aligned}
$$


If he lies, the center's posterior q is:

$$
    q(X=1) = g(G|¬G) ≈ 46.2\% 
$$

And the expected payment is

$$

\begin{aligned} \displaystyle

    S(p;q)  &= p(¬G) \log q(¬G) + p(G) \log(q(G)) \\
            &≈ 46.2\% \log 53.8\% + 53.8\% \log 46.2\% \\
            &≈ -0.601 - 0.412 \\
            &≈ -1.013

\end{aligned}

$$

Note the payment (penalty) is negative in both cases, but it is less negative with a honest answer. In [another article](/truth-induction-and-information-theory) we'll see that we can convert this into a positive score representing a **reduction** in entropy, or the amount of Shannon information provided by the answer.

</aside>


<style>
.custom-aside
{
  margin: auto;
  background-color: lightgrey;
  border: 1px solid black;
  max-width: 600px;
  padding-top: 1em;
  padding-bottom: 0px;
  padding-left: 1em;
  padding-right: 1em;
  margin-bottom:  1em;
}

aside h3 {
    margin-top: 0px;
}

</style>



## Coordinating Honesty

<!--

Note: let's introduce Nash Equilibrium and Coordination Problem here. For the same reason we introduce priors/posterior etc. We want an audience that doesn't have deep knowledge of these concepts to still understand this article.

TODO: links to resources on bayesian inference

-->

In the [previous article](/subjective-consensus-protocols) in this series, we explained how blockchain consensus protocols work on the principle of a Keynesian Beauty Contest, where everyone will answer with the truth if they expect everyone else to answer with the truth.

Peer Prediction is based on a similar idea: everyone is better off answering honestly if everyone else answers honestly. But there is a difference: with a Keynesian Beauty Contest, everyone has to give the **same** answer. This means you need to know what other people's (honest) answer is going to be. But in Peer Prediction, answering honestly can be an equilibrium **even if you don't know how other people are going to answer**!!! All that it requires is that you **expect everyone else to answer honestly**.

But why do you expect other people to answer honestly? With consensus protocols, it's because truth is a Schelling Point. But with peer prediction, you simply answer honestly: you don't need a Schelling point to help you guess what other people's answers will be.

So the question remains, why would people expect each other to answer honestly? Sure it would be **great** if everybody did answer honestly. As with many things in life, it is easy to do the right thing if everyone else is. But how do we get to that state in the first place?

This is sort of a chicken-and-egg problem, but it also has a solution. In the next article in this series, [Coordinating Honesty], I will introduce the concept of Nash Equilibria and Coordination Games, and explain how an online community can "coordinate on honesty" and arrive at a state where everyone is honest because they expect everyone else to be honest.

## Truth without Common Priors: the Bayesian Truth Serum

The peer prediction method assumes that the participants and the center have common priors. In the example of the jury trial, this is most certainly not the case. The jurors have all just sat through a trial after all: they probably formed at least some opinions during the trial, including opinions about how other jurors are likely to vote.

This problem was addressed in the fascinating, seminal 2004 paper by Drazen Prelec, which described [A Bayesian Truth Serum for Subjective Data](https://economics.mit.edu/files/1966). The Bayesian Truth Serum works by asking users for two pieces of information: 1) a vote, and 2) a prediction of the average vote of **other jurors** (e.g. the percent of other jurors that vote guilty).

So jurors are asked in effect to reveal their prior beliefs (about other jurors' beliefs). This information can be used in the payout function to compensate for the lack of the common prior.

The payout function includes two parts. One part is called the **prediction score**, and it rewards users for how well they predict the actual votes of other users. This means that rational users will be honest with their predictions -- just as long as they believe other user are being honest with their votes!

But will users be honest with their votes? Well, the second part of the score, called the **information score**, is calculated such that users can expect the largest payout by being honest with their vote -- just as long as they believe other users are being honest with their predictions!

Now once again there is the chicken-and-egg problem here. How do people get into this state where everyone expects each other to be honest in the first place? I will discuss the solution in the next essay on [Coordinating Honesty].

But for now, the key to understanding BTS is understanding that the information score works on the same basic principle as the Peer Prediction scoring rule. It rewards honest votes, because those votes are **information** that can be used to help predict other user's votes.

But instead of starting with a prior, and rewarding users for votes that result in a more accurate posterior prediction, BTS starts with the **average estimate** of the other jurors, and rewards users for votes that improve on this estimate.

## Surprisingly Common Votes

Both the priors in Peer Prediction, and the average estimate in BTS, represent an estimate (e.g. of the probability that the 12th juror votes guilty). And that estimate does not yet take into consideration the information that only the 11th jurors has: his own opinion. 

If that estimate was revised to take his opinion into consideration, it would be **increased** if his opinion was guilty, and **decreased** if his opinion was not guilty. (A Bayesian reasoner would never observe a guilty vote and conclude that guilty votes are less common).

In other words, whatever his opinion, that opinion will be more common among subsequent jurors than previously estimated. The answer will be "surprisingly common."

Since BTS doesn't have priors to update, it can't do the proper scoring rule thing. Instead it just rewards user if the **actual** proportion of jurors voting guilty is higher than the estimated proportion. But the idea is the same.


## Other Methods

Correlated Agreement, Bayesian Markets

....




### TODO: Law of Large Numbers

Another way is looking at this. Suppose we ask for your weight. Random people. If everyone reports honestly, then with each answer, the estimate gets closer and closer to the actual weight. Or at least tends to. The average estimate will bounce around, getting closer or further to the true value (depending on whether each data point is above or below the current average) but over time will converge on the actual average. This is kind of what the law of large numbers says.

If you are reporting your weight, then you are part of the sample. If you report accurately, then the estimated weight after your report will be, all thing remaining equal, a better estimate of the actual weight.

When an honest statistician takes a sample from a population in order to estimate the population average, they don't alter data points that are particularly high or low, and would be upset to learn that the data-points were mis-representing themselves. They know that, all things being equal, each **true** data point improves the estimate.

This is even the case if you are, say, an unusually tall person. You might think that by reporting your true height, you would throw off the estimate. But the opposite is true. Your height is information. Removing that data point arbitrarily can be expected, on average, to worsen the estimate. TODO: what is this true?

On the other hand, 


### Common Priors and Aumann's Agreement Theorem

This recalls Aumann's Agreement Theorem, which says that two rational agents that have different beliefs (e.g. of the probability of some outcome) should revise their beliefs merely upon learning the other agent's beliefs. Assuming that the agents actually know each other's beliefs, and believe each other to be rational, they will each conclude that the other agent only has different beliefs because she has different information, and that if they had the same information, their beliefs would be more similar. So both agents revise their belief accordingly.


In peer prediction, the center did not observe the trial and has no opinion about how convincing the evidence was. So it bases its beliefs about the 12th juror's probable vote based mainly on the votes of the other jurors. On the other hand, the 11th juror has additional information from the jury trial itself that allows it to form a strong prior belief in the probability of other jurors voting guilty, even before observing the other jurors' votes.

So the center and the 11th juror don't actually have a common prior, as required by peer prediction. 

But the method doesn't actually require a common prior!

In fact, if the 11th juror believes the defendant is innocent, then all that is required is that the 11th juror believes that the center has not **underestimated** the probability of the 12th juror voting guilty. If the 11th juror believes that the center's estimate is likely to be as good or better than his own, he won't expect to benefit by lying.

We can illustrate the decision of the 11th juror the chart below. 

If the 11th juror believes the center underestimates the probability of a guilty vote -- so the center's prior estimate q is less than his own estimate p, then voting guilty will increase the center's posterior estimate from q to q₁. Since $$S(p;q)$$ is higher the closer p is to q, (dishonestly) voting guilty maximizes his score. But the center's prior estimate q is greater than his own estimate p, then (honestly) voting innocent will maximize his score.

    p = 75% = 11th jurors prediction (probability that 12th juror votes guilty)
    q = the center's prior prediction (before I vote)
    q₀ < q = center's posterior prediction if I vote innocent
    q₁ < q q-ε = center's posterior prediction if I vote guilty

    situation 1:            situation 2:
    p > q₁ > q > q₀         q₁ > q > q₀ > p
    S(p;q₁) > S(p;q₀)       S(p;q₀) > S(p;q₁)

If the 12th has seen compelling trial evidence, he might indeed believe that q is an underestimate. Especially if he believes the center bases q only on the 5:5 divided vote of the other 10 jurors. But what if the 11th juror doesn't know how the other 10 jurors voted? He would expect about 75% (7 or 8 out of 10) of the voters to vote guilty, and for the center to then use that information to revise her beliefs towards 75%. And what if the center also has other information he doesn't have, such as a sophisticated machine learning model, data on the 12th juror's past votes, etc.?

For lying to be profitable, the 12th juror would need to have some reason specifically to believe that the center will err on the low or the high side. Otherwise, his best estimate 𝑝 of the 12th jurors vote is q₁, or "whatever the center would believe after considering my vote". So he maximizes his score if $$p = q = q1$$, which requires voting honestly.



TODO: Link to Distributed Bayesian Reasoning.




## More ideas about scoring

Users can have separate "contribution scores" with the sum of the bits of information they have provided to a community, and a "prediction" score, which is the **expected** amount of information of one vote. Users who have a history of providing informative votes can have greater weight. For example, if historically when user A upvotes a piece of content, the percentage of subsequent users who upvote it triples, then this indicates that user A's votes should have more weight. They are more informative. 

The weight can be a moving average, so users can reasonably quickly lose their prediction-reputation if they start upvoting crap content, even if they have a long history of informative votes. Linear growth and exponential decay can make it hard spam the system making useful votes for a while to build up reputation, then squandering it to promote spam content. To build up the reputation you would actually have to provide informative votes, and because of exponential decay, the negative information you put into the platform before you trashed your reputation would be less than the information you had to contribute to get that reputation.

In this way, if there is a "trusted core" of users who determine payments, then this system is spam-proof.

## TODO: Spendable Contribution POints

You could also allow contribution points to be spendable. Your contribution score is directly proportional to the log of the amount of value you have created, if value is measured by people's exposure to quality content. 

If you spend reputation points in order to promote content

If you have contributed to the forum by providing information that helped direct people's attention to more quality content, then you have the right to 



## Information Cascades



## Breaking Out of Conformity Traps -- Thinking Out Loud

What is the holy grail here? It is honesty. If we just ask people for honest feedback about what other people believe, we will not be able to reveal situations of pluralistic ignorance. To take an extreme example, say there is something that 90% of people believe, but they believe they are a minority, and the average estimate of the popularity of the belief is 10%. If you have a mechanism that only induces honest beliefs about what other people believe you'll end up with an underestimate of only 10%.

This is related to the phenomenon of preference falsification, where people pretend to agree with some policy they disagree with, because the mechanism -- informal social status and reputation systems -- rewards for expressing popular opinions. 

What we need is a way to get out of this trap in the case of pluralistic ignorance.

Essentially the same problem is obtaining information about embarrassing things, like how many sexual partners have you had, have you ever stolen office supplies, etc.





## Thoughts on Dropping Common Prior Assumption

The center has information I don't have: other users votes so far. I have information the center doesn't have: private information that effects my belief in the probability others will believe something.

If I think that my belief is uncommon, I will still think it is more common than the average person will believe. So I would expect that the center may under-predict how common the thing is. But if I don't think the center has as much information as I do, I will expect that the the center over-estimates how common the thing is.

Only when I think the center has enough votes that it dominates my priors is honest voting safe.

Let's say I disagree (downvote), but think think most people will upvote. My posterior (after considering my own signal) is beta(alpha=8, beta=2). Concentration = 10, Mode = 80%. The center's estimate is 75%. 

Since the center under-estimates, then I should upvote to improve the accuracy of the center's estimate.

But first, the center's belief is also evidence. Let's say I learn the center's kappa is 8 (alpha=6, beta=2).

I should actually revise my beliefs. So now I believe beta(14, 4), mean is 77.77%. Center still under-estimates because it lacks my data...

But what if I don't know the center's probability. Say I know it's concentration, K=10. And it's prior before any data -- say a weak prior of beta(1,2) based on site stats. I will still expect the center to under-estimate because of the weak prior.

Okay I got to my own estimate of 80% only after the evidence dominated my own weak prior based on site stats.

So if the concentration of additional evidence the center has received is stronger than my private information, then I expect the center's estimate of other people's beliefs to be greater than mine.

The key is that **I might have knowledge that the center does not have. But the center has knowledge I don't have!!!** 

If i don't know what concentration is I can average over my prior beliefs in possible concentrations.






## Next

- equilibrium
    - doesn't have to be only
- log scoring
- link to information theory
- Bayesian Truth Serum
- converging on honesty
- weight of 
- additivity of information value
- trusted core



MI(X,y) = sum_x P(x,y) P(x,y)/(P(x)P(y))

reward(X,y) = D_kl(P(X|y), P(X))
    = sum_x P(x|y) log P(x|y)/P(x) 
    = sum_x P(x,y)/P(y) log P(x,y)/(P(x)P(y))
    = 1/P(y) MI(X,y)










if their posterior belief, after taking into consideration the votes of other jurors, is that other jurors are more likely than not to agree with them, they will tell the truth, otherwise they will  lie.




TODO: their is a correlation.



