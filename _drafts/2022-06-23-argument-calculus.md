# Argument Calculus

## Arguments are Claims

We can also look at the **argument** itself as a kind of claim with a specific logical structure: the logical conjunction of its premise and warrant. That is, an argument is the claim that the premise is true, and that it is also a good reason to accept the conclusion.

This will be the first axiom of our argument calculus.

### Axiom 1: Logical Structure or Argument

    A◂-B = B ∧ A◂B

## Negated Conclusions

An opposing argument, is just an argument that supports the **negation** of the conclusion. For example, if the premise "penguins have wings" supports the conclusion "penguins can fly", then it also opposes the conclusion "penguins can't fly."

The same is true for warrants, an opposing warrant is just a warrant that supports the negation of the conclusion. 

This will be the first axiom of our argument calculus.

### Axiom 2: Opposition is Support of Negated Conclusion

	X◃Y = (¬X)◂Y

It follows that:

	X◃-Y = (¬X)◂-Y

Because:

	X◃-Y = Y ∧ X◃Y
		 = Y ∧ (¬X)◂Y
		 = (¬X)◂-Y

[[[

### Definition: Opposing Warrant


An **opposing warrant** is the claim that the subject is **less** likely to accept conclusion 𝐴 if they accept premise 𝐵 than if they do not, or in other words that 𝐵 opposes 𝐴. An opposing warrant is expressed using the notation:

$$
A◃B
$$

So:

$$
    A◃B \iff P(A|B) < P(A|B̅)
$$

### Opposition is Support of Compliment

If 𝐵 opposes 𝐴, it supports 𝐴̅:

$$
    A◃B ⟺ P(A|B) < P(A|B̅) ⟺ P(A̅|B) > P(A̅|B̅) ⟺ A̅◂B
$$

Although not strictly necessary, it is useful to have terminology and notation for both supporting and opposing warrants (just like it is useful to have notation both for $$>$$ and $$<$$).
]]]





### Demorgan's Law for Warrants

Consider the expression:

	(X ∧ Y)◃Z

This says that Z, if true, is a good reason to reject that $$(X ∧ Y)$$ is true. Intuitively, this means that Z must either be a good reason to reject X, or a good reason to reject Y, or both. So in other words:

	(X ∧ Y)◃Z = X◃Z ∨ Y◃Z

So let's make this an axiom:

### Axiom 3: Demorgan's Law for Warrants

	(X ∧ Y)◃Z = X◃Z ∨ Y◃Z

It follows that

	(X ∧ Y)◃-Z 	= X◃-Z ∨ Y◃-Z

Because:

	(X ∧ Y)◃-Z 	= Z ∧ (X ∧ Y)◃Z 
				= Z ∧ ( (X◃Z ∨ Y◃Z) )
				= (Z ∧ X◃Z) ∨ Z ∧ Y◃Z)
				= X◃-Z ∨ Y◃-Z


The law holds for support as well:

	(X ∧ Y)◂Z = X◂Z ∨ Y◂Z 

because

   (X ∧ Y)◂Z  = (¬(X ∧ Y))◃Z      # axiom 1
			  = (¬X ∨ ¬Y)◃Z		# demorgan's law
			  = ¬X◃Z ∨ ¬Y◃Z		# demorgan's law for warrants
			  = ¬(¬X)◂Z ∨ ¬(¬Y)◂Z 		# axiom 1
			  = X◂Z ∨ Y◂Z 

Finally, it follows that:

	(X ∧ Y)◂-Z = X◂-Z ∨ Y◂-Z 

## Arguments for or against Arguments

The conclusion of an argument can be any claim. We distinguish premise and warrant arguments based on whether the conclusion is the premise or warrant of another argument. But if arguments themselves can be claims, then there is a type of argument where the conclusion is **another argument**. For example:

    A◂-B◂-C

Using Axiom 1, we replace $$A◂-B$$ with it's logical structure:

    (B ∧ A◂B)◂-C

And then using demorgan's law for warrants:

    	(B ∧ A◂B)◂-C
		= B◂-C ∨ (A◂B)◂-C
		= B◂-C ∨ A◂B◂-C

Which gives us this theorem

### Theorem: 

    A◂-B◂-C = B◂-C ∨ A◂B◂-C

Or in other words, an argument against an argument is either an argument against its premise, or against its warrant, or both.


In many argument models, there is no distinction between a premise or a warrant argument. For example, in the Dung model, arguments can oppose (attack) other arguments. 

