---
layout: tag
taxonomy: judgment-aggregation
title:  "Judgment Aggregation"
date:   2020-11-08 13:40:43 -0700
header:
    image: /assets/images/daniele-d-andreti-i2zKYlnFnCU-unsplash-wide-1920.jpg
    teaser: /assets/images/daniele-d-andreti-i2zKYlnFnCU-unsplash-wide-1920.jpg
    caption: "photo credit: [**Unsplash**](https://unsplash.com)"
---

Machines can't actually understand content or judge its accuracy or quality; **all they can do is aggregate the judgment of human beings.** 

In a sense, social media is just a way for our social connections to crowdsource our news feed based on other people's judgments about content.

But for many reasons, what we *engage* with, or even what we *like*, does not necessarily reflect what we **judge to be accurate or good**. Indeed often we engage with content **because** we judge it to be harmful, misleading, or unfair.

**Judgment Aggregation** is a method of analyzing the **conversation** underneath a post to compute the **coherent, reasoned consensus judgment of the community** -- not just knee-jerk popularity or engagement.


The post-deliberation judgment of a group of diverse and independent individuals is the core formula for collective intelligence: the "wisdom of crowds" of which we have heard so much promise. This remains an untapped potential of large social networks.

The essays below describe in more detail our ideas about Judgment Aggregation and social networks.
