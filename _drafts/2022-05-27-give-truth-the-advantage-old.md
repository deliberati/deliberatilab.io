

-----------



TODO: tie together extension of senses and marketplace of idea themes. Which is it?


[And the problem is not just about misinformation. It is also the intellectual dishonesty, the puerile, shallow debate, the peddling of fear and outrage, leading to increased polarization and loss of shared reality. ]


Our social networks are like extensions of our senses. I have eyes and ears and a brain, but I have never been to Cleveland. My direct experience is limited to a fraction of time and space, so most of what I know comes through other people. Today, we rely on social media for much of this information. 



Fortunately, we not only understand this feedback loop, we can design algorithms that tend to reward different types of content with attention, in order to give truth the advantage.



[
    Engagement-optimization algorithms use machine learning to predict what content is most likely to engage our attention, and gives that content an additional boost of attention by moving it to the top of our feeds and notifications. Unfortunately, outrageous lies (and other controversial content) is some of most engaging. As I argue in [The Law Of Attention](/the-law-of-attention), social networks will be dominated by the behaviors that are rewarded with the most attention. The result is that when platforms optimize for engagement, they tend to encourage and amplify lies.
]

[
    Engagement-optimization algorithms use machine learning to predict what content is most likely to engage our attention, and gives that content an additional boost of attention by moving it to the top of our feeds and notifications. As I argue in [The Law Of Attention](/the-law-of-attention), social networks will be dominated by the behaviors that are rewarded with the most attention. 

    So the people produce the content, and the algorithms, are continuously learning from each other

    So people will learn to produce more of the content boosted by the algorithms, while the algorithms refine


     The result is that when platforms optimize for engagement, they tend to encourage and amplify lies.

]
[ Problem: I have made a point that, while interesting, is not critical to my arugment. The fact that there is feedback is not critical? The fact thaa we can analyze the system is. But it is critical, because the feedback of attention drives behavior. If people didn't learn, then we couldn't apply game theory, could we? ]



## Amplifying Honesty

Some fascinating papers form the Game Theory literature show how to induce feedback loops that reward and amplify **honesty**.

These methods use the statistical correlation between people's beliefs and preferences to create an equilibrium where an attention-seeking user's best strategy is to simply be honest. Posting or upvoting content just because it is popular, engaging, or supports an agenda is counter-productive unless a majority of users coordinate their inauthentic behavior, and even this can be remedied using lessons learned from **blockchain** consensus protocols.

TODO: law of attention?

## Reason-Optimization Algorithms

But honesty is not enough, because people can be honest and wrong. 

Rumors often spread online simply because people see a post from somebody they trust and assume it is true. But if somebody else they trust comments on that post and explains why it is not true, they may be less likely to spread it -- that is, if they see the comment. Unfortunately, many people will upvote or share the rumor before seeing the comment. 

So even if people are honest, a social media algorithm that promotes content based on upvotes (popularity) will still tend to amplify misinformation. Whereas an engagement-optimization algorithm amplifies controversial content, a popularity-optimization algorithm amplifies uninformed content.

But a **reason-optimization** algorithm amplifies **informed** content: content that is best supported by **reasons**. It does this by inferring, from the patterns of upvotes on the post and its comments, how likely the average user is to upvote or share a post **if they had read all the comments**. 

## Argument Games

Social platforms such as Twitter have gamified argument. The influencers with high follower counts have mastered the game, but it is a game most people would be better of not playing.


. But the design of the game induce a disfunctional feedback loop, where divisive, misinformation, and shallow content.

Unfortunately, a group of people can be honest and informed, and yet still be biased. The final piece of the puzzle is **trust and reputation**.


An algorithm that optimizes for **honesty** and for **reason**, combined with a reputation system, turn discussions in online social platforms until a kind of 

Change the rules of the game, so that the way to win is not to post controversial or popular opinions, but to post and upvote opinions that **stand up to scrutiny**, and defend them with arguments that **your peers honestly find to be convincing**.




## Argument Games

If there is a comment that makes people statistically less likely to upvote or share some popular post (e.g. "this is just a rumor"), then that post has been "ratioed" and will be demoted. Being ratioed does not necessarily mean that the original post is false or misleading. In fact, the comment itself can be ratioed, turning the discussion into a kind of argument game. The game is won by the side that has statistically more support among people who have participated in a chain of arguments and counter-arguments with the highest ratios -- that is, the opinion of a subset of people who are **informed** of the strongest arguments that have been raised on both sides. 

- Doesn't look like a game
- Social platforms such as Twitter is already a kind of argument game. The people with huge follower counts, that even make a living as "influencers", have gotten good at the game.

...Change the rules of the game, so that the way to win is not to post controversial or popular opinions, but to post and upvote opinions that **stand up to scrutiny**, and defend them with arguments that **your peers honestly find to be convincing**.


But change the rules..



## Reputation

Winning an argument game provides **reputation**. Reputation is what gives users the ability to command attention.




## The Honest and Informed Opinion

An algorithm that optimizes both for honesty and for reason would tend to reward and amplify **honest and informed opinions**. 

Promoting the honest and informed opinion can solve not only for the spread of false and misleading information, but for the general toxicity of social media, because it provides a way for a community to self-moderate. If somebody flags a post as violating the written code of conduct of the community, the decision to remove the content can be determined, after discussion, by the honest and informed opinion of the community.

## Trust and Reputation

Unfortunately, a group of people can be honest and informed, and yet still be biased. The final piece of the puzzle is **trust and reputation**>

The online communities that have been most successful with dealing with toxicity at scale have reputation systems.

Trust and reputation systems are key to the 

New members of the community would gain


Many o


  All social platforms have reputation systems, even if they don't actually show you follower counts, karma, or other explicit reputation statistics; somewhere in a database there are coeffecients used to estimate how likely other people are to engage with content you produce. People that win the the attention game take actions that increase their reputations, even if that's not their intent.






## The Marketplace of Ideas

> If we do not have the capacity to distinguish what’s true from what’s false, then by definition the marketplace of ideas doesn’t work. And by definition our democracy doesn’t work."
> 
> -- Barack Obama
{: .notice--info}


The honest and informed opinion of a group of people on the Internet is not guaranteed to be the truth. It still depends on the group: they can be honest and informed yet biased.

But I think it brings us closer to the ideal of a free marketplace of ideas, where the ideas that flourish or those that stand up to the scrutiny of public opinion after everyone has made their case and responded to the arguments of others.

Today, the marketplace of ideas is held in public forums on the Internet, but it is broken and modern democracies are suffering for it. 

[And the problem is not just about misinformation. It is also the intellectual dishonesty, the puerile, shallow debate, the peddling of fear and outrage, leading to increased polarization and loss of shared reality. ]

But we don't have to throw up our hands and accept this as an inevitable consequence of technology, or of human nature. If the platforms were designed differently, they could bring out the best of human nature, and help society realize our collective intelligence. So we critically need to build a social network that functions as a healthy marketplace of ideas.


## Next in this Series

- In [The Law of Attention](/the-law-of-attention), I argue that social networks can be modeled as economic systems where **attention** effectively plays the role of money, which allows us to use Game Theory to design algorithms that induce people to behave in a certain way.

- In [Honesty Games](/subjective-consensus-protocols), I explain how Game Theory can be used to design, or at least explain, systems where people behave **honestly** even when there are rewards for cheating.

- In [Argument Games], I describe how social algorithms can turn argumentation into a game, and how to make the rules of the game favor the most informed content



FIN
-----


-----


 These are some ideas about how that could work.


---




where people learn to produce the kind of content that is promoted by the


that create an environment that certain types of content is amplified, and other is muted, and 



I will show that blockchains are able to function without centralized control based on similar mechanisms that induce people to behave honestly. I will discuss how the Bayesian survey mechanisms from Game Theory can create situations where people can gain the most currency by telling the truth, even when the answers are subjective and impossible to verify. I will propose algorithms that promote deep conversation and the spread of information with the most information content, and that promote content based on the hypothetical opinion if community-members all shared the same information.


