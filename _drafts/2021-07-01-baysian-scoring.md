---
layout: single
title:  "DRAFT: Scoring Online Debates using Distributed Bayesian Reasoning"
date:   2021-07-01 00:00:00 +0200
tags: judgment-aggregation
toc: true
toc_sticky: true
---

# Evaluating Group Arguments using Distributed Bayesian Reasoning

Deliberati is building technology to make social platforms more intelligent. Part of this technology is an algorithm that ranks and amplifies people's posts based on quality and reason instead of engagement and popularity. 

When an argument has taken place in an online platform, either informally in a social network or more formally in a argument mapping or structured debating system, is it possible to measure the outcome? Is it possible to quantify how well some claim has been supported by the arguments that have been made?

For example, would it be possible to estimate the *probability* that the claim is true? Can the *evidence* that arguers present in support of their standpoint be treated from a Bayesian point of view? If so, what are the priors? What about normative claims? Does it make any sense to estimate the probability that something should be done?

To use math answer a question such as "what is the probability that something is true" we need to more fully specify our epistemology: our criteria for acceptable beliefs. 

Once we ask the right question, the Bayesian approach can provide some very useful answers.

## Abstract Argumentation Frameworks and Acceptable Arguments

Dung's [Abstract argumentation frameworks](https://en.wikipedia.org/wiki/Argumentation_framework) describe a way of modeling arguments as directed graphs, and a method for analyzing these graphs to determine which claims are acceptable. 

To summarize Dung's criteria for acceptability at a high level: suppose we start out having with *some* reason to accept some claim A, and no reason not to. Then by default A is acceptable.

Suppose we then discover some reason to accept another claim B, *and* that  B is a good reason **not** to accept claim A. Maybe now we actually have more reason overall **not** to accept A. So A *is no longer acceptable*.

This is rather tenuous reasoning, but it's something to go on. It accords with the intuitions of some people about what is **reasonable**. Reasonable people should believe things they have reasons to believe, unless they have some other additional reason not to believe it.

Building on this basic idea: what if we discover some reason C not to accept B? Well then maybe now there is more reason overall *not* to accept B -- which means maybe there is now more reason overall to accept A again!

In abstract argumentation frameworks, A, B, and C are all called *arguments*, though this term encompasses what we will call *claims*, as well as arguments for and against those claims. Arguments against other arguments (or claims) are called "attacks". An **argumentation framework** as defined by Dung is a directed graph where nodes are arguments and edges are attacks.

An argument can A is "defended" from an attack B by a counter-attack C on B -- unless C is in turn attacked and not defended, and so on. An argument is acceptable if it is defended from all attacks. 

This essentially means that the "last word" wins in these frameworks. This means that when there is a single thread of argument, if there is an even number of arguments such as in the thread (A -> B), then the first claim is rejected because it is not defended from attacker B. But if there is an odd number of arguments such as in the thread (A -> B -> C), then the first claim is accepted because C defends it from attacker B.


## Arguments Between Humans Require Different Criteria for Acceptability

This definition of acceptability rests ultimately on that assumptions that 1) there is reason to accept the arguments in the framework 2) all known arguments are included. 

This is very often a good assumption in the AI applications in which these frameworks are generally used. Here, the set of arguments is derived from a curated knowledge-base representing everything the machine knows. The arguments represent *default* rules: things that are generally true, but can be overridden by other rules. Machine believes something if, as far as it knows, there exists reason to believe it and no reason not to.

But arguments on social platforms are different. People in online social platforms provide a potentially infinite source of new arguments, of varying quality. So the acceptance of a claim is determined entirely, and potentially arbitrarily, by whoever has the last word.

Use of a good reputation systems or closed forums does not solve this problem: the basic assumption of *equal default acceptability* of all arguments can't be made, since even the most reasonable person sometimes advances weak or misinformed arguments. Nor can the completeness assumption be made: even obvious counter-arguments will not always be submitted. Without these assumptions the logical basis of "last-word" acceptability criteria collapses. 

## Subjective vs Objective Scoring

Acceptability criteria for abstract argumentation frameworks is meant to be *objective*, but acceptability will always be in some sense subjective as long as the arguments input into these systems are produces by people.

Is it possible to *objectively* analyze the truth of claims that have been argued by a group of people that does not implicitly or explicitly depend partly on trusting in their subjective judgment?

I certainly can't think of any, and I suspect that most attempts at purely objective scoring of arguments between humans will ultimately found to be some sort of aggregation of human judgment.

I think the best we can do is a scoring method that is *as objective as possible*. We want to take the beliefs and assertions of people as input -- as evidence -- and then *evaluate that evidence objectively.* In a sense, opinions collected from people is data, just like data collected from a scientific instrument. And we can extract knowledge from the intelligent evaluation of that data: we aren't limited to outputting statistical summaries of people's opinions.

## The Informed Opinion

In the [Deliberative Poll](/the-deliberative-poll) process, the *deliberative consensus* is the opinion of *the people who have seen all the arguments in the critical argument thread.* It is the informed opinion instead of initial reaction.

So the deliberative poll is a single-thread argument graph where arguments also have votes. An abstract argumentation framework with a single thread can be seen as a *special case* of the deliberative poll, where implicitly each argument comes with a vote to accept that argument. The last word wins because the person who submitted the last argument in the thread is the only person counted in the deliberative consensus of people who have seen all the arguments.

The deliberative consensus encodes a similar intuition about what is *reasonable* to that of the acceptability criteria in abstract argumentation frameworks, but expands it to a social context. If the majority initially accepts A, but then a subset of the group decides that B is true and that B is a good reason not to accept A, then it is reasonable to expect that the rest of the group group should either accept B and reject A, or provide a good reason C not to.

The Deliberative poll process focuses the argument into a on a single "top argument" thread. This process has a lot of advantages, but in very complex topics there may be multiple useful threads of argument that develop, and even the top thread may become too long to be grasped by a single individual: we can no longer measure the deliberative consensus.

This is where *probability* comes in. 

## The **Hypothetical** Informed Opinion

We have identified the following considerations in the design of a measure of the acceptability of the claim:

1. It should be defensible with reasons
2. But it should not simply be determined by the last word
3. It should factor in data on the subjective beliefs of people
4. It should factor in multiple threads and branches of argument


This can all be boiled down to the answer to the following question:

***Would*** *the average participant in the argument accept this claim, if they they had seen all the arguments?*

Distributed Bayesian Reasoning method answers this question by modeling the average participant as a Bayesian reasoner, isolating the average participant's beliefs in claims from how strongly those beliefs support or oppose other claims.

