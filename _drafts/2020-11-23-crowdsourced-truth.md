---
layout: tag
classes: wide
entries_layout: grid
taxonomy: crowdsourced-truth
title:  "Crowdsourced Truth"
date:   2020-11-08 13:40:43 -0700
header:
    image: /assets/images/sebastian-pichler-bAQH53VquTc-unsplash-wide-1920.jpg
    teaser: /assets/images/sebastian-pichler-bAQH53VquTc-unsplash-wide-1920.jpg
    caption: "photo credit: [**Unsplash**](https://unsplash.com)"
---

We believe that a social network can be a **tool for openly collaborating with people on the Internet to discover truth about the world**.

The essays below describe in more detail our ideas about how this can work.
