# Toughts on Distributed Bayesian Reasoning with sparse data

In the case of a single argument thread, it is easy to use a Bayesian approach to infer priors. We can just use a hierarchical Bayesian averaging algorithm. Suppose we have priors for ω (average), κ (concentration) of a beta distribution representing our beliefs about how people will vote on a newly submitted claim, with no other information about the claim. We can then take the actual agree/disagree votes on that claim, which gives us a bernouli likelyhood function, and the expected value of a bernoulli/beta hierarchical model is just the weighed average of the prior average and the average of the data.

For example ω=.5, κ=15, then for our prior beta distribution
	a=ω(κ-2)+1 = 7.5
	b=(1-ω)(κ-2)+1 = 7.5

and votes 1 agree 20 disagree
	Z=1
	n=20

our best estimate of the actual agree/disagree ratio is:
	bayesian average = (Z + a) / (n + a+b) = (1+7.5)+(20+7.5+7.5) = .2429

we can now apply this hierarchically, making our new ω=.2429 but the same κ. This is because represents our prior belief of how *relevant* any random argument is -- how much people's beliefs given the argument will differ from their beliefs without the argument. κ can come from historical data.

Okay, so that allows us to compute the probability that the average person will agree with A given any relevance chain B, C, D, E.

But what if we instead have 2 or more independent arguments. This is the sort of data you get in a pro/con debate site?

Again, the fact of people submitting reasons and voting on them is evidence for what a fully-informed person might believe. Obviously if people submit a lot of pros and a lot of people agree with the pros (and A), then this is strong evidence. But how exactly do we quanity it?

Using a naive Bayes approach, and calculating P(A|B,C,D) in terms of P(B|A)*P(C|A)*P(D|A doesn't work if we can't assume these reasons are independent. And we can't. In fact we need to assume people submit a whole lot of redundant reasons (which are not dependent) or reasons that are semantically very similar and thus highly correlated.

So to to develop our intuition for this, let's compare paris of situations where we think the evidence has equal weight.

To start, let's compare a situation where there is one pro that gets 10 positive votes, vs two pros that get 5 positive votes each. Is the evidence provided in both situations equivalent? Should the estimate that a fully-informed juror would believe A given they saw all the arguments be the same in both cases?

If not, what about if there are 10 reasons, each that get one vote, by 10 different people? Is the fact that there are more individual pros evidence that the fully-informed person is more likely to agree with the conclusion?

If you think there is a difference, then in order to make inferences from the *number* of pros, you need to have some sort of prior beliefs on the set of pro arguments you are going to get. So you have a prior distribution on the belief of the fully informed juror (probably that same beta distribution we mentioned above, with ω being different from 0.5 if there is a generally tendancy for claims that are made to stand up to scrutiny, and κ being low if there is a general tendancy for peopel to be moved by argument). Then for any expected fully-informed belief, we have some likelyhood of different sets of arguments and the relevance of those arguments (measured somehow in terms of how much they move people from the uninformed to the fully-informed belief).

I think such a hierarchical model could be developed, but I suspect it is not necessary because I think you can go with the assumption that the **number of reasons doesn't provide evidence** about the hypothetical fully-informed belief. But *having reasons* does provide evidene, and therefore *the number of people who can back their beliefs with reasons* is what matters. That is, the number of justified votes. So I would say the examples above where there are 10 justified votes distributed in different ways all provide the same evidence, and our posterior beliefs in the beliefs of the hypothetical juror should be the same.

Thus by symmetry, if we have one pro that gets 10 positive votes, vs. 2 cons with 5 negative votes each, the evidence balances out.  The posterior estimate of the average jurors beliefs should be the same as the prior (though the distribution is possibly more concentrated.)


What if there is one pro B with 19 votes, and one con C with 1 votes? In that case, we have a situation that calls for the Bayesian average again. Our evidence if 19 justified pro votes, and 1 justified con vote. If our priors are as in the Bayesian averaging example above, then our best estiate of the probability that a fully-informed juror would agree with A if they had voted on both B and C is .2429.

What if we have one pro vote, but instead of getting supporting votes, it gets also opposing votes. For example, we have these votes:

	A&B   1
	!A&B  19

This represents a case where people agree with B but don't believe it is sufficient reason to believe A. Well, this is just our original Bayesian average example. The votes on both sides are equally justified, so our best estimate is that %24.29 of the jurors would agree with A given they had seen all the arguments.

What if we have two such arguments? And let's assume people don't vote twice on different arguments. So there are 40 voters total. Again, symmetry says these should balance out. All 40 votes are equally justified, and we have 20 on each side.

	A&B   1
	!A&B  19

	A&C   19
	!A&C  1

So the above seems like it should be equivalent to:

	A&B 20
	!A&C 20

So what about this?

	A&B   19
	A&B  1

	!A&D  16?

Here it would make sense to take the bayesian average of 1/19, and the bayesian average of 0/16. These averages should be fairly close (depending on priors).


---

So we come to the counter-intuitive conclusion that if B and C are independent threads with no overlap, then our best estimate of P(A|B,C)

	P(A|B,C) = P(A|B ∧ C) = P(A|B ∨ C)


Let's take an example. 5 people who supported A also voted on B as a reason. 5 different people also voted on C. Nobody voted against B and C.

	c(A&B) = 5
	c(A&C) = 5

Suppose Bayesian averaging moves the estimate of P(A|B) and P(A|C) each from 5/5 to 4/5. We can treat these as adjusted votes:

	c(A,B) = 4
	c(A,C) = 4
	c(!A,B) = 1
	c(!A,C) = 1

Now P(A|B ∨ C) = ( c(A,B) + c(A,C) ) / ( c(A,B) + c(A,C) + c(!A,B) + c(!A,C) ) = 8/10 = 4/5.










