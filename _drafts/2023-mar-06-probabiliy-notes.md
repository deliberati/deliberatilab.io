Probability that other people will upvote given you upvote

If it is less than the prior probability, it means your upvote predicts below-average

GAIN( P(V|ME,PRED), P(V|PRED) )

first upvote....
    user has no reputation
        bayesian average for anonymous user, standard prior weight
    user has some reputation
        what does reputation tell us?
            the weight of the evidence. But the weight of the evidence depends on what other people have voted
            so let's start with the weight of the evidence assuming nobody else has yet voted

            First, there are two different ways of looking at how to update P(V|ME) given I upvote 1/1 times
            The first is based on updating the beta distribution, adding 1 to both alpha and kappa

                P(V|ME) = (1 + alpha)/(1 + kappa)

            The other is the "σ̅" model where we just do a weighted average of the observed prior and
            σ̅ is the weighting factor. The data is 1/1, and the prior is alpha/kappa=omega.

                 P(V|ME) 
                     = (1 - σ̅) * omega + σ̅ * 1
                     = omega - σ̅*omega + σ̅
                     = omega + σ̅(1 - omega)

            Solve for σ̅ as a function of kappa we get

                σ̅ = 1/(kappa+1)
                (kappa+1) = 1/σ̅
                kappa = 1/σ̅ - 1

                NOTE: so σ̅ is the strength of **1** observation in a beta-distribution model


            So there is some true predictive power of the user's vote. we are trying to predict that based on past.

            okay maybe we calculate P(V|ME), and from that estimate sigma.

            So let's say I have upvoted 10 times, and number of times the next has upvoted given I have is 2 (20%).
            We need some prior on that. Let's say it's 1/11 (.0909...), So we have P(V|ME) = 2 + 1 / 10 + 11 = 3/21 = 1/7 


            ----

            So after observing users's record of Z:n, 
            we have a sigma of:

                (1 - σ)ω + σ = (Z + α)/(n + κ)
                solve for σ
                    σ(1 - ω) + ω = (Z + α)/(n + κ)

                    σ  = ( (Z + α)/(n + κ) - α/κ ) / (1 - α/κ)
                       = (Zκ - αn)/((κ-α)(n+κ))
                σ = (Zκ - αn)/((κ-α)(n+κ))

            example:                
                σ = (2*11 - 1*10)/10*21 = .05714 = 1/7

            OR alternatively
                σ = (P(V|ME) - ω) / (1 - ω)

            example:
                = (1/7 - 1/11) / (1 - 1/11)
                = .05714

            (1 - .05714) * 1/11 + .05714 = 1/7
                 

            let's say I have voted 5 times, and number of times the next upvoted given I have is 1 (also 20%)
            P(V|ME) = 1 + 1 / 5 + 11 = 2 / 16 = 1/8

            1 - 7/8 / 10/11 = sigma = 1 - 77/80 = .0375
            kappa =  1/sigma = 26.6667 

            let's say I have voted 1 times, and percentage of next who upvoted given I have is 20%
            P(V|ME) = .1 + 1 / 1 + 11 = 1.1 / 12 = 11/120
            sgima = 1 - 109/120 / 10/11 = .00083̅
            kappa = 1200 

            So the fact that I am one out of 1 only increases proability from .0909 to .09166, which means weight is still low

            What if I have negative predictive power? Say my record is 0/1

            P(V|ME) = 0+1 / 1+11 = 1/12
            sigma = 1 - 1/12 / 10/11 = -0.0008 


            P(V|ME) = (Z + priorAlpha) / (n + priorKappa)
            sigma = (P(V|ME) - priorOmega) / (1 - priorOmega)

            sigma = ( (Z + priorAlpha) / (n + priorKappa) - priorOmega ) / (1 - priorOmega)

        Now can we relate this to significance?
            P(V|ME) 
                = (1 - sigma) * alpha/kappa + sigma * 1
                = alpha/kappa * S

                S = ( (1 - sigma) * alpha/kappa + sigma ) / alpha/kappa
                  = (1 - sigma) + sigma/(alpha/kappa)
                  = 1 - 1/kappa + 1/kappa/(alpha/kappa)
                  = 1 - 1/kappa + 1/alpha

                So when sigma=.05917, alpha=1, kappa=19.26

                S = 1 - 1/17.5 + 1/alpha =  

                1/7 = 1/11 * S

            *HERE: S = P(V|ME)/P(V) = 1 - sigma(1 - kappa/alpha)

                So when .0375, alpha=1, kappa=11
                S = (1 - .0375(1 - 11/1)) = 1.375
                S = 1/8 / 1/11 = 1.375


        Okay, so we have sigma which is the weight of evidence of their vote. It can be negative. It is higher the more success the user has.

        Now suppose we have a second user. And let's say we know their votes are independent.


        What is P(V|U1,U2)? Given we have P(V|U1) and P(V|U2). Then it is the naive bayes logic?

        P(V|U1,U2) = prior * S(V|U1) * S(V|U2)

        = P(V) * P(V|U1)/P(V) * P(V|U2)/P(V)
        = P(V) * P(U1|V)/P(U1) * P(U2|V)/P(U2)

        or cynically

        = P(V) * S(U1|V) * S(U2|V,U1)
        = P(V) * P(U1|V)/P(U1) * P(U2|V,U1)/P(U2,U1)

        So what we want to calculate is.
        P_{U1}(V|U2) and P_{U1}(V), from which we can derive significance 

        But is it significance we want? or sigma?

        Significance is sort of forced. If we want to know how much we multiple the prior by to get the posterior, significance
        is the answer. But that's not how we get the posterior. We get it by a weighted average.

        The weighted average formula we derive based on the underlying Beta distribution formula. So what is the underlying formula
        in this case?

        Well what is the underlying hiearchical model?

        When model is prior, then P(V|U1), then P_U1(V|U2)


        SO we start with a prior weight reflecting belief that random user predicive power is same as average.
        Then there is a weight reflecting belief that second user votes is different from first

        So let's say prior is 1:11. U1 is 2:10. Now we want P(V|U1,U2). But say we don't have any intersection between U1 and U2.

        Then do we assume conditional independence? Not necessarily. Actually we have some prior belief that U2 will vote the same
        as U1, and we need evidence to indicate that they vote differently. If they both have above-average prediciton rate, that
        alone is evidence that they vote similary?

        So we need some underyling model that gives rise to that correlation (high upvote rate of U1 and U2 if they are iked somehow)

        A simple model is that there are duplicates.

        So there is a probability of duplicate. And then a distribution of prediction rates.

        So the prior on prediction rates is the overall average prediction rate. THen there is an overall average dupe rate.

        predicitonRates[1] ~ normal(averagePredictionRate, ...)
        isIndependent = ~ bern(dupeRate)
        nextPredictionRate ~ normal(averagePredictionRate, ...)        

        nextVote ~ bern(predictionRates[1])

        P(V|U1,U2) = predictionRates[1] * (if independent predictionRates[1] else 1)



        Then our estimate pf P(V|U1) is prior P(V|U1)

        Pr(U2|U1,C) = 
            naive:  Pr(U2|C)
            cynical: Pr(U2|U1)

        Sig(U2|U1,C) = 
            naive:  Sig(U2|C)
            cynical: Sig(U2|U1)




        So in a simple model, each person only has their own private signal.
        In a more complex model, we have observations of N other users private signals
        In an even more complex model, there are duplicates.

        Starting with no duplicates...
        say there are N users, and users 1 and 2 each observe 1 signal (their own).






So first we have the base rate of 1/11. Then let's say we have the observed rate for either u1 or u2 of 4/20, so Bayesian averaging
gives us a sigma for U1 or U2. Finally, the intersection. And we do another Bayesian average. If there is no intersection,
then we are with the prior, which is the union. IF there is a lot of intersection, then the posterior will be close to intersection.


So what is the difference between the simple beta-distribution upvote, and looking at the history of that user?kappa

    Formula 1: don't look at user history
       P(V|U1=1) = alpha+1 / kappa+1
       P(V|U1=0) = alpha / kappa+1

    Formula 2: look at user history

        P(V|U1=1) = alpha1+pastUpvotes / kappa1+pastVotes
        P(V|U1=0) = alpha1+(number of times not-upvoting was predictive) / kappa1+pastVotes

        This is the point estimate for the predictionRate for U1
        alpha1/kappa1 here is the prior on the predictionRate
 
        If we have no data for user, the estimated predictionRate is alpha1/kappa1
            P(V|U1=u1) = alpha+1 / kappa+1 = alpha1/kappa1
               alpha = alpha1-1

            If prior alpha:kappa = 1:11
                P(V|U1=1) = 1+1 / 11+1 = 2/12 = 1/6
                So our prior is that a user's vote has weight 1, and thus the prior on the predictionRate is 2:12

        Now let's say user has record of 1:1, then:
            P(V|U1=1) = alpha1+1 / kappa1+1 = 1+1+1 / 11+1+1 = 3/13 


    New add attention. So U1 upvoted, but out of how many? Let's say total attention was 10.

    IF we don't look at history, then
        P(V|U1=1,other=9) = alpha+1 / kappa+10 

    Now we need history of user. Let's say prediction rate was 1:1. Do we need to know how much attention that involved?
        P(V|U1=1,other=9) = p(U1=1|V)*P(V)/P(U1=1) * P(other=9|V)/P(other=9) 
            = P(v) * sig(U1=1,V) * sig(other=9,V)

    Okay we ahve U1, where we have history, and U2, where we have no history. Once we have U1, we have a new beta distribution.
    And then U1 of course has a smaller effect, because it adds to a distribution which already has a high alpha/beta.

    What's the difference between our estimate of theta in this case, and the estimate of the predictionRate of this user?

    The prediction rate is P(θ|U1) -- the probability that another user will vote given this user votes. 


    If the user is just honestly voting based on their private signal, their predictionRate will converge on the prior predictionRate
    of (alpha+1)/(kappa+1). If they have additional signals, what will it converge on? It can't be too high, because very vote would
    weigh too much and cause over-estimate of final upvote rate.



----




Scenario 1: independent, each user has own private signal
    P(V|U1,U2) should just be a based on upvoting a beta distribution.

    P(V|U1=u1,U2=u2) = alpha + u1 + u2 / kappa + 2
    so if they both upvote, it's alpha+2/kappa+2
    = 3/13

    So posterior for two independent reputationless votes is equal to posterior for one 1:1 vote.

    Formula 1: don't look at user history
       P(V|U1=1,U2=2) = alpha+2 / kappa+2

    Formula 2: look at user history
        P(V|U1=1,U2=2) = alpha1+pastUpvotesIntersection / kappa1+pastVotesIntersection

        This is the point estimate for the predictionRate for U1
        alpha1/kappa1 here is the prior on the predictionRate
 
 
        If we have no data for user, the estimated predictionRate is alpha1/kappa1
            P(V|U1=1,U2=1) = alpha+2 / kappa+2 = alpha1/kappa1
               alpha = alpha1-2

            If prior alpha:kappa = 1:11
                P(V|U1=1,U2=1) = 1+2 / 11+2 = 3/13
                So our prior is that a user's vote has weight 1, and thus the prior on the join predictionRate is 3:13

        Now let's say the joint prediction rate is 1:1, then:
            P(V|U1=1,U2=1) = alpha1+1 / kappa1+1 = 3+1 / 13+1 = 4/14 = 2/7

    Now let's add the denominator of total attention.



Scenario 2: duplicate
    Let's say the intersection is 1:1. But U1 and U2 always vote the same.  So the union is also 1:1
    So they voted 1 times. And in all 1 cases they both voted.
    So P(V|U1,U2) = P(V|either) is 3:13 

    So P(V|U1,U2) = P(V|U1)



    But the prior on the intersection should be 3/13





-----

# CALCULATION OF P(θ|U1 or U2)

We want to calculate P(θ|U1 or U2) from the same first principles we use to calculate the beta distribution. 


So here's how we start to derive the beta distribution
P(θ) = 1
P(U1|θ) = θ
p(U1) = sum_θ P(θ) * P(U1|θ) = sum_θ θ = 1/2 θ^2 | 1,0 = 1/2
p(θ|U1) = P(U1|θ) * P(θ) / P(U1) = θ * 1 / 1/2 = 2*θ
E p(θ|U1) = sum_θ θ * 2*θ = sum_θ 2*θ^2 = 2/3θ^3 | {1,0} = 2/3


After two observations
p1(θ) = 2*theta
p1(u2|theta) = theta
p1(u2) = sum_theta p1(θ) * p1(u2|θ) = sum_θ 2*theta * theta = sum_θ 2*theta^2 = 2/3*theta^3 | 1,0 = 2/3
p1(θ|u2) = P1(u2|θ) * p1(θ) / p1(u2) = θ * 2*theta / 2/3 = 3*theta^2
E p1(θ|u2) = sum_theta theta * 3*theta^2 = sum_theta 3*theta^3 = 3/4 theta^4 | 1,0 = 3/4



Let's start with a uniform distribution. P(θ)=1. We observe to samples, and one OR the other is true.
P(θ) = 1
P(either|θ) = 2θ - θ^2 
p(either) = sum_θ P(θ) * P(either|θ) = sum_θ 2θ - θ^2 = θ^2 - 1/3θ^3 | 0,1 = 1 - 1/3 = 2/3
P(θ|either) = P(either|θ) * P(θ) / P(either) = (2θ - θ^2 ) * 1 / 2/3 = 3θ - 3/2θ^2 = 3(θ - 1/2θ^2) 
E P(θ|either) = sum_theta P(θ|either) * θ = sum_theta 3(θ^2 - 1/2θ^3) = θ^3 - 3/8θ^4 | 1,0 = 5/8 



Okay where do we go from here? Well now we want to deal with many datapoints. Let's say U1 or U2 voted 2 times. And the second both
voted positive gain

p1(θ) = 3(θ - 1/2θ^2) 
p1(either|theta) = p(either|theta) = 2θ - θ^2 
p1(either) = sum_θ p1(θ) * p1(either|θ) = sum_θ 3(θ - 1/2θ^2) * (2θ - θ^2)
    = 2θ^3 - 3/2θ^4 + 3/10θ^5 | 1,0 = 2 - 3/2 + 3/10 = 4/5
p1(theta|either) = P1(either|θ) * P1(θ) / P1(either) = (2θ - θ^2) * 3(θ - 1/2θ^2) / (4/5)
    = 15/8 * (θ - 2)^2 * θ^2 
E p1(theta|either) = sum_theta P1(either|θ) * θ / P1(either) = sum_theta 15/8 * (θ - 2)^2 * θ^2  * θ
    = (theta^4*(5*theta^2-24*theta+30))/16 | 1,0
    = 11/16


Or negative 

p1(θ) = 3(θ - 1/2θ^2) 
p1(neither|theta) = p1(neither|theta) = (1-θ)^2 
p1(neither) = sum_θ p1(θ) * p1(neither|θ) = sum_θ 3(θ - 1/2θ^2) * (1 - 2θ + θ^2)
    = sum_θ 3θ - 6θ^2 + 3θ^3 - 3/2θ^2 + 3θ^3 - 3/2θ^4
    = sum_θ 3θ - 15/2θ^2 + 6θ^3 - 3/2θ^4
    = 3/2θ^2 - 15/6θ^3 + 3/2^4 - 3/10^5 | 1,0 = 3/2 - 15/6 + 3/2 - 3/10 = 0.2  
p1(theta|neither) = P1(neither|θ) * P1(θ) / P1(neither) = (2θ - θ^2) * 3(θ - 1/2θ^2) / 11/10
    =  6θ^2 - 4θ^3 + 1/2θ^4 / 11/10 = 60/11θ^2 - 40/11θ^3 + 10/220^4 



Okay now what? I have a formula for P(V|either). From there I can estimate P(V|both)

----


So what is the mutual information of U1 and U2

How much predictive power does U1 have for U2. And then we subtract that from the value U2 has for V

If they are dups
    P(V|U1,U2) = P(V|U1)
If they are not
    P(V|U1,U2) = ...beta formula

P(V|U1,U2) = P_1(V) * SIG_1(V,U2)
           = P(V|U1) * P(V,U2|U1)/P(V|U1)*P(U2|U1)












