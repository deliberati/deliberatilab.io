

## Information Theory Perspective

The formula for the expected score is identical to the negative of an important formula from information theory: [cross entropy]. And if the 11th juror votes honestly, the expected payout is simply the negative of entropy $$H(p)$$. 

$$

\begin{aligned} \displaystyle

    S(p;p)  &= -H(p) \\
            &= ∑_x p(x) \log(p(x)) \\
            &= p(0) \log p(0) - (1 - p(1)) \log(1-p(1))

\end{aligned}
$$


I think it will be useful to briefly review the idea behind entropy as the "expected value of surprise". The "surprise" of an event 𝑥 is defined as $$ -\log p(x) $$, where $$p(x)$$ was the estimated probability that 𝑥 would occur. The lower the expected probability, the higher the surprise. So our scoring formula basically penalizes surprise.

Something as simple as $$-p(x)$$ or $$1/p(x)$$ could also be a measure of surprise (the less probable you thought the event, the more surprise when it occurs). But the log probability it has some interesting properties. We already discussed how log probability gives us a [proper scoring rule](https://sites.stat.washington.edu/raftery/Research/PDF/Gneiting2007jasa.pdf). Another interesting result is that the expected value of surprise is highest -- 1 bit -- in the case of maximum uncertainty (50%), and approaches zero bits as belief approaches certainty (0% or 100%).

For example, if I think the probability of an event is 1/1024, then although I will be quite surprised whenever the event happens, it won't often happen. As my belief in the probability of X approaches 100%, my expected surprise approaches zero because the expected frequency of surprising events decreases faster than the magnitude of surprise of those events (linear vs log). If $$p(X=1)=1/1024$$, then $$p(X=0) = (1 - p(X)) = 1023/1024$$, and my expected surprise, or entropy, is:
 
 $$
 \begin{aligned} \displaystyle

    H(X)    &= - p(X=1) \log p(X=1) - (1 - p(X)) \log(1-p(X)) \\
            &= 1/1024*\log 1/1024 + (1023/1024)*\log(1023/1024) \\
            &= 1/1024*10 + (1023/1024)*0.14%  \\
            &= .0083 bits

\end{aligned}
$$

So the center rewards each juror proportionally to the negative of the surprise the center experiences when observing the next vote after taking into consideration the vote of each juror. Thus the expected score is equal to the expected surprise, or the negative of the entropy of posterior beliefs of the center.

> Information is the resolution of uncertainty.
>
> -- Claude Shannon

Information can be though of as delta surprise, or reduction of entropy. So the jurors are rewarded for reducing entropy, and thus providing information.

We can calculate how much information the 11th juror provides by voting honestly, by calculating the delta of the expected score from voting honestly vs dishonestly. If he report dishonestly, his posterior belief will be different from the center's belief, so q ≠ p. He will then expect the center's surprise to be

$$
\begin{aligned} \displaystyle

    S(p;q) = ∑_x p(x) \log q(x) 

\end{aligned}
$$



Whereas if he reports honestly he expects the center's surprise to be the entropy of p.

Equation ??  is called the cross entropy of 𝑝 and 𝑞.

The difference is 

$$
\begin{aligned} \displaystyle

    S(p;p) - S(p;q) &= \\
                    &= ∑_x p(x) \log(p(x)) - ∑_x p(x) \log(q(x)) \\
                    &= ∑_x p(x) \log(p(x)) - \log(q(x)) \\
                    &= ∑_x p(x) \log(p(x)/q(x))

\end{aligned}
$$


This is equal to the KL-divergence, or relative entropy. The KL divergence is usually notated as

$$
\begin{aligned} \displaystyle

    D_{KL}(p\|q)

\end{aligned}
$$

PDF on scoring rules: https://www.cis.upenn.edu/~aaroth/courses/slides/agt17/lect23.pdf

It can be proven to be always non-negative, and strictly positive if p ≠ q. 

So this scoring rule aligns the interests of the juror with the interests of the center in making an accurate prediction. The juror can best do this by providing actually useful information.

## Information Value of a Vote

The above tells the reduction of expected surprise resulting from a true answer vs. a false answer. But if we want to measure the information content of the vote of each juror, what we want to measure the expected value of surprise given an honest vote vs. no vote at all.

In other words, the information content of a piece of a vote (or a piece of information in general) as the KL divergence of the posterior and prior: that is, it is the expected reduction of surprise -- from the point of view of someone who already has the additional information -- that someone who doesn't yet have that information will experience if they are given that information.

$$
\begin{aligned} \displaystyle


    IV(X;y) &= D_{KL}(P(X|y)\|P(X))  \\
            &= ∑_x p(x|y) \log p(x|y)/p(x)

\end{aligned}
$$

The amount of information provided by each additional voter decreases rapidly as the number of votes increases. Obviously if the center has 1000 votes, a 1001th vote will have very little effect on the center's belief, and therefore make very little difference in the expected surprise of the 1002nd vote. The first votes on the other hand will be more informative.

To illustrate, let's represent the vote of the next voter as 𝑋, and the vote of the 𝑖th voter is 𝑋ᵢ. If the prior beliefs are $$beta(α=1, β=1)$$, the center's estimate of the probability of 𝑋 is $$P(𝑋=1) = α/(α+β)=1/2$$. If the first juror votes innocent (𝑋₁=0), then the posterior estimate is $$P(𝑋=1\|𝑋₁=0)$$ = $$(α)/(α+β+1$$) = 1/3. 

The information value is therefore:

$$
\begin{aligned} \displaystyle

\end{aligned}
$$
    IV(P;X₁=0) 
         = D_{KL}(P(X|X₁=0)\|P(X))  
         = sum_X P(X=x|X₁=0) \log P(X=x|X₁=0) / P(X=x)
         = P(X=1|X₁=0) \log [ P(X=1|X₁=0) / P(X=1) ] + P(X=0|X₁=0) \log [P(X=0|X₁=0) / P(X=0)]
         = 1/3 \log [ 1/3 / 1/2 ] + (1 - 1/3) \log [ (1 - 1/3) / (1 - 1/2) ] 
         = 0.082 bits
$$

Note that this value is positive. The information value of an honest vote will always be positive.

Now we move on to the second voter. The posterior becomes the new prior. So P₁(𝑋=1) = P(𝑋=1|𝑋₁=1) = 2/3. If the second voter votes guilty, the new posterior is P₁(X=1|X₂=1) = P(X=1|𝑋₁=1,X₂=1) = (α+2)/(α+β+2) = 3/4. 

The **additional** information provided by this second vote is:

$$
\begin{aligned} \displaystyle

\end{aligned}
$$
    IV(P₁;X₂=1) 
         = D_{KL}(P₁(X|X₂=1)\|P₁(X))  
         = sum_x P₁(X=x|X₂=1) \log P₁(X=x|X₂=1) / P₁(X=x)
         = P₁(X=1|X₂=1) \log [ P₁(X=1|X₂=1) / P₁(X=1) ] + P₁(X=0|X₂=1) \log [P₁(X=0|X₂=1) / P₁(X=0)]
         = 3/4 \log [3/4 / 2/3] + (1 - 3/4) \log [ (1 - 3/4) / (1 - 2/3)]
         = .024 bits
$$


This is much less information.

Now suppose there are 98 yes votes and 2 no votes, so the center's priors are beta(α=98,β=2). The center's estimate of the probability of 𝑋 is $$P(𝑋=1) = 500/(500+2)≈98.0%$$. If the next juror votes innocent ($$𝑋_101=0$$), then the posterior estimate is P(𝑋=1|𝑋_101=0) = (α)/(α+β+1) = (98)/(98+2+1) ≈ 97.0%.

The information value is therefore:

$$
\begin{aligned} \displaystyle

\end{aligned}
$$
    IV(P;X₁₀₁=0) 
         = D_{KL}(P(X|X₁₀₁=0)\|P(X))  
         = sum_X P(X=x|X₁₀₁=0) \log P(X=x|X₁₀₁=0) / P(X=x)
         = P(X=1|X₁₀₁=0) \log [ P(X=1|X₁₀₁=0) / P(X=1) ] + P(X=0|X₁₀₁=0) \log [P(X=0|X₁₀₁=0) / P(X=0)]
         = 0.97 \log [ 0.97 / 0.98 ] + (1 - .97) \log [ (1 - .97) / (1 - 0.98) ] 

         = 0.970297 * math.log( 0.970297 / 0.98,2 ) + (1 - 0.970297) * math.log ( (1 - 0.970297) / (1 - 0.98),2 )
         = .00302 bits

$$

Which is more than an order of magnitude less than the information value of the first vote (.082 bits)

TDOO: tab "Weighted Votes and Information as Distance" in peer-prediction-beta-distribution-scenarios.numbers


## An Alternative Scoring Formula

This suggest an alternative scoring formula such that each juror's expected reward is the information value of the information they provide. 

Specifically, the score of i+1st juror is:

$$
\begin{aligned} \displaystyle

    S_{ᵢ+1} = \log Pᵢ(X=x|X_{i+1}=1) / Pᵢ(X=x)

\end{aligned}
$$

So the first juror's score is

$$
\begin{aligned} \displaystyle

    S₁ = \log P(X=x|X₁=1) / P(X=x)

\end{aligned}
$$

The expected score from the point of view of the first juror, given a truthful answer, is the information value of that answer.
    
$$
\begin{aligned} \displaystyle

    E S₁    &= sum_X S₁(X=x) \\
            &= sum_X P(X=x|X₁=1) \log P(X=x|X₁=1) / P(X=x) \\
            &= IV(P;X₁=1) 
\end{aligned}
$$

This scoring formula has some interesting properties in the context of an online community:


- Users who vote earlier get more points. So if this method is used for upvoting content, credit goes to the voters who helped discover and promote new content. The user who actually posted the content can be credited with the first upvote, and thus by far the largest score.
- Users also get more points the more the content is upvoted by later users (the higher the probability of an upvote)
- The center's posterior estimate of the probability the next user will upvote content can be used to rank content. This implies the use of Bayesian Averaging for ranking, as suggested in this post by [Evan Miller](https://www.evanmiller.org/bayesian-average-ratings.html)
- Higher ranked content would be placed at the top of the page and get more upvotes. So the probability of an upvote would need to be rank-normalized, to represent an estimate of the probability that a user would upvotes an item were it in rank one.

## Redefining Information Value


We've defined information value as the *expected* score of each juror. The actual score depends on the vote of the next juror.

Instead, the actual score could be based on the **final** probability estimate. 

$$
\begin{aligned} \displaystyle

    S₁(X)   & = sum_x r(X=x) \log P(X=x|X₁=1) / P(X=x) \\
            & = IV(P;X₁=1) 

\end{aligned}
$$

What this does is measure how much the each vote reduced the center's **final** expected surprise. 



### Weighted Votes

- The expected score is always positive if the user is truthful (given the common prior assumption)
- consistently negative information value creates negative weight, so an upvote turns to a downvote
- The expected value of inauthentic votes is negative. That is, dishonest votes provide negative information. The center will assume each vote is informative and change its probability accordingly. If votes consistently make the center's prediction worse, your score will get more and more negative. 
- This means people can't game the system to gain reputation, unless they create enough fake accounts.
- Expontential decay



Also it is possible to provide **negative** information. This is what happens when you increase the center's surprise. 



## Information Value as Distance Metric

The analogy of information as a physical quantity is compelling to me. The total points awarded to all users is the total amount of Shannon information, in bits, conveyed by those votes. This should be the same amount of information no matter where it comes from: the sum of the information value of 3 votes should equal to the information value of a single piece of evidence that resulted in the same posterior as those three votes.

In fact, if we look at the expected score from a slightly different point of view, it has the mathematical properties of a [metric, or distance function](https://en.wikipedia.org/wiki/Metric_(mathematics).

The key here is that information value is always relative to a point of view. It is the expectation **of somebody who has information** of the reduction of surprise experienced **by somebody who does not**. 

Let's define the "information distance" between A and B as the value of information B to someone who already has information A -- or by how much will information B reduce the surprise of someone who has A -- from the point of view of someone who has information ω.



We can write the general formula for information distance as:

$$
    ID(A;B) = || \sum_x P(X=x|ω) \log [ P(X=x|B) / P(X=x|A) ] ||
$$


Let's call this relative information value. Let's say $$IV₃(P;X₁=1)$$ is the relative information value of the first vote from the point of view of the third voter. Similarly $$IV₃(P₁;X₂=1)$$ is the additional relative information value of the **second** vote, to someone who already knew the first vote, from the point of view of the third voter. And $$IV₃(P;X₁=1,X₂=1)$$ is relative the information value of the **first two** votes, to someone who didn't know about any of the votes, from the point of view of the third voter. 

We can write the general formula for relative information value as:

$$
    IVᵢ(Q;E) = \sum_x Pᵢ(X=x) \log [ Q(X=x|E) / Q(X=x) ] 
$$

We'd expect that, from the point of view of the third voter, that the information value of the first vote, plus the additional information value of the second, plus the additional information of the third, would equal the value of all three together.

$$
    IV(P₃;X₁=1,X₂=1,X₃=1)
    = IV₃(P;X₁=1) + IV₃(P₁;X₂=1) + IV₃(P₂;X₃=1)
$$

Which is the case.

$$
    IV(P₃;X₁=1,X₂=1,X₃=1)
    = \sum_x P₃(X=x) \log [ P(X=x|X₁=1,X₂=1,X₃=1) / P(X=x) ]
    = \sum_x P₃(X=x) \log [ P(X=x|X₁=1) / P(X=x) * P(X=x|X₁=1,X₂=1) / P(X=x|X₁=1) * P(X=x|X₃=1,X₂=1,X₁=1) / P(X=x|X₂=1,X₁=1)]
    = \sum_x P₃(X=x) \log [ P(X=x|X₁=1) / P(X=x) * P₁(X=x|X₂=1) / P₁(X=x) * P₂(X=x|X₃=1) / P₂(X=x)]

    = \sum_x P₃(X=x) \log [ P(X=x|X₁=1) / P(X=x) ]
    + \sum_x P₃(X=x) \log [ P₁(X=x|X₂=1) / P₁(X=x) ]
    + \sum_x P₃(X=x) \log [ P₂(X=x|X₃=1) / P₂(X=x) ]

    = IV₃(P;X₁=1) + IV₃(P₁;X₂=1) + IV₃(P₂;X₃=1)

$$

So IV


Sample calculations are worked out in peer-prediction-beta-distribution-scenarios.numbers, where we find.

    IV₃(P;X₁=1) = 0.215
    IV₃(P₁;X₂=1) = 0.053
    IV₃(P₂;X₃=1) = 0.010
    SUM = 0.278
    IV(P₃;X₁=1,X₂=1,X₃=1) = 0.278

## Mutual Information


We can write the definition of IV given the definition of conditional probability $$p(x|y)=p(x,y)/p(y)$$

$$
    
    IV(P;y) 
         = D_{KL}(P(X|y)\|P(X))  
         = ∑_x P(X=x|y) \log P(X=x|y)/P(X=x)
         = ∑_x P(x,y)/P(y) \log P(x,y)/(P(x)P(y))
$$

This value looks very similar to the definition of mutual information. In fact, it is mutual information of a random variable X and fixed value y. Or in other words, if you sum the information value across all values of Y, you get mutual information:

$$
    MI(X,Y) = ∑_y IV(P;y) 
        = ∑_y P(y) ∑_x P(x,y)/P(y) \log P(x,y)/(P(x)P(y))
        = ∑_y ∑_x P(x,y) \log P(x,y)/(P(x)P(y))
        
$$


## Another Scoring Formula

Okay I have come up with another scoring formula in the "Peer Prediction Formula Idea" tab in peer-prediction-beta-distribution-scenarios.numbers

I believe this might incentivize truth-telling even when there is no common prior. The trick is that user i does not know how informed the center's prior is, because the center's prior is based on all other voters except i and the reference voter j, and user i doesn't know how how many other voters there are -- or will be in the future.

If there are a lot of voters -- that is, if the strength of the center's prior is greater than some threshhold -- then it dominates any private information the user might have.

It is the basic peer prediction formula but the prior for scoring user i is is the posterior of a weak prior like beta(1,1) or a site-wide prior, revised with votes for all n-2 voters != i,j. And the score is the log of the posterior of the prior. In other words, the information value of the vote of user i for predicting user js vote (given the prior based on the voters of other users).

    score 
        = log(Pcenter(vj|vi)/P(vj))
        = log P( vj | all other votes != j) / P(vj|all other votes != i,j) )


But then we calculate this information value for all users j != i. This makes it so that your score is less dependent on who happens to be your reference voter. And if the sample is skewed in one way, then it will skew two components of the scoring formula in the ways that should kind of average out: the vote of the reference voter, and the prior.


But most importantly, if the user doesn’t know how many voters there will be, then the center's prior may contain more information than his private information.

    Ej score = 
        ∑v log(Pcenter(vj|vi)/P(vj))


To calculate the expected score of user i (from his point of view), user i needs to estimate the center's prior. The center's prior is a beta distribution, and the alpha and beta expected by user i are determined by the posterior beliefs of user i. It also is determined by the concentration -- the number of votes the center observes. Let's fix concentration at k. If X is user i's posterior, then the expected alpha and beta of the center's prior are:

    A = alpha + X(k - 2) + 1
    B = beta + (1 - X)(k - 2) + 1

The center's belief Q = is A/k. The ith user's expectated value of Q is

    Q = Ei A/k = sum_x p(X=x) A/k 
      = sum_x p(X=x) ( alpha + x(k - 2) + 1 ) / k




The posterior of user i is

    X ~ beta(X|alpha_i, beta_i)

So first, user i has plausible values of X: the ratio of yes/no votes of other jurors. This gives rise to plausible values of the priors of the center. For example if X=2/3, and K is fixed, then 

P(X=2/3, A=2, B=1) = beta(X=2/3|alpha_i, beta_i) * (2/3)^2*(1 - 2/3)^1


P(X=x | A=a, B=b) = beta(X=x|alpha_i+a, beta_i+b)


And the plausible value of A/B give us plausible value of the center's prior Q

Q = sum a,b P(A=a, B=b) A/K

Q = sum a,b ( sum_x P(A=a, B=b, X=x ) ) A/K

Q = sum a,b P(A=a, B=b) a/K A/K



So my expectation of the center's posterior is Pc(X=x|A=b,B=b) = beta(X=x|a+alpha_i, b+beta_i)??



In other words, it is what the common posterior would be if I shared the same information with the center.

So I expect sum_v Pc(V=v|my vote) * log Pc(V=v|my vote) / Pc(V=v)

So say I have a strong posterior alpha=4, beta=1. Mean is 80%. If center has one vote, then I expect 80% chance it is a 1, 20% chance it is a 0. The center has a weak original prior alpha=beta=1. So I think their beliefs, if I am truthful, are

80% chance
    prior 2/3 
    posterior 3/4
20% chance
    prior 1/3
    posterior 1/2

if reference vote is H my expected score is

    80% * log 3/4 / 2/3 + 20% log 1/2 / 1/3 = 80% log 9/8 + 20% log 3/2

which is positive

80% chance
    prior 2/3 
    posterior 1/2
20% chance
    prior 1/3
    posterior 1/4

if reference vote is H my expected score is

    80% * log 1/2 / 2/3 + 20% log 1/4 / 1/3 = 80% log 3/4 + 20% log 3/4

which is negative
    
































