
## Anatomy of an Argument

In argumentation theory, arguments are often modeled at a very high level as involving three claims. Terminology for these claims varies, but we will call them:

1. The **conclusion**: the claim in dispute.
2. The **premise**: the content of an argument: a reason to accept or reject the conclusion. This premise can itself be disputed. If multiple reasons are given, the combination is interpreted as a single premise. In some literature the premise is called the "grounds", "evidence", or "data". 
3. The **relevance**: the assertion that the premise, if accepted, is a good reason to accept or reject the conclusion. In some literature this is called the "implied" or "unexpressed" premise, or "warrant".

## Bayesian Support

The terminology of Bayesian probability theory overlaps not coincidentally with the terminology of argumentation theory. Bayesian inference also involves evidence (premises) and hypothesis (conclusions), and quantifies exactly how strongly the evidence supports or opposes the conclusion (relevance).

Bayes theorem is:

    P(A|B) = P(A) * P(B|A) / P(B)

However, the key intuition behind Bayesian inference can be more clearly represented if express the theorem simply as:

    P(A|B) = P(A) * S(A,B)   

This formula highlights the simple linear relationships between the probability of A (`P(A)`) and the probability of A given B (`P(A|B)`).

The coefficient `S(A,B)` tells us exactly how much more or less likely some hypothesis `A` is if some evidence `B` is "given". 

It is trivial to calculate from statistics about `A` and `B`. For example, suppose some medical study finds that 1 in 1,000 people have some rare disease, but 1 in 10 people have the rare disease if they have a certain rare gene. So people are exactly 100 times more likely to have the disease given they have the gene.

- estimates of `S(A,B)` are the basis of Bayesian inference: a process of updating one's estimate of the probability of a hypothesis as new evidence is observed. Starting with a prior probability estimate `P(A)`, representing one's degree of belief that the hypothesis is true before considering any evidence, a Bayesian reasoner updates their estimate when they learn B by multiplying by `S(A,B)`. They continue this process as more evidence becomes available

For example, suppose there is some test C where `S(A,C)=100`. So C also provides a high degree of support for A. Our estimate of the probability of A after observing both `B` and `C` is:

    P(A|B,C) 
        = P(A) * S(A,B) * S(A,C)
        = 1/1000 * 100 * 100
        = 1,000% !!

Clearly we have made a mistake here.

The problem is that `B` and `C` are themselves correlated. It may be that the test `C` does nothing more than identify the genetic marker for `B`. So we have observed `B`, observing `C` provides no new information.

So a very important assumption behind Bayesian inference is the *independence of the evidence*.



Okay alternative is to not even mention Bayes. First define anatomy of an argument. Then talk about probability. Hypothesis and evidence.

A Bayesian reasoner holds beliefs in terms of conditional probabilities. For example, a medical study. A doctor may form their beliefs in patient diagnosis entirely from medical studies. So a doctor who learns the patient has the genetic marker will believe the patient has a 1/10 chance of having the disease. 

- Beliefs are conditional probabilities
- Example medical study
- Doctor will believe exactly 1/10
- Evidence may not be independent
    - the gene only effects males
- So need to consider entirety of evidence.
- 
- relates to argumentation



- linear relationship
- says how much more or less likely
- trivial to calculate
- for example
- a kind of correlation
- can be said to support or oppose
- Evidence may not be independent
    - the gene does not effect people with AB blood type
- So need to consider entirety of evidence.
- # Bayesian Argument

Suppose two doctors have separately examined the same patient but arrived at different diagnoses. Further, suppose both doctors are Bayesian reasoners in that they form their diagnosis in a mathematically correct way based entirely 
on 

1. the probabilities derived from the study data (their prior probabilities), and 
2. the information they learn from examining the patient

This means that if the doctors had learned the same information from the patient, they would have come to the same conclusion. 

Further, assume the doctors are both aware of #1 and #2. This means that they will both believe that the disagreement is entirely due to different information gleaned from their separate examinations.

Suppose that both doctors allow for the possibility that they may have missed something in their examination. They will therefore both believe that either:

1. they know something the other doesn't know
2. the other knows something they don't know.

Finally, suppose the two doctors have pure motives: they wish only to form a correct diagnosis, and not to mislead the other, justify themselves, or play power games. And both believe this about the other.

They will both be motivated to learn which 

We can imagine the type of argument they might hold:

    Doctor 1: The patient probably has this disease.

    Doctor 2: I disagree.

    Doctor 1: She has the genetic marker.

    Doctor 2: But she has type AB blood.

Why did Doctor 1 choose "the have the genetic marker" as an argument? Intuitively it is the obvious argument because it strongly supports the conclusion, even if it is not sufficient. 

Doctor 1's intent in making the argument is to *gain* information. If Doctor 2 responds by changing their mind, they 


Aumann's agreement theorem says that, once they learn that they disagree on the diagnosis, they will not "agree to disagree" and obstinately hold each to their separate diagnosis. Rather they will believe that disagreement is entirely due to different information gleaned from their examination of the patient.

not "agree to disagree" -- meaning the will not hold to their individual opinions and believe the other is wrong, but rather they will assume that the other disagrees only because they have


cannot "agree to disagree". That is, they 


....will be motivated to discover either 1: that the





Each doctor is unaware of exactly what the other doctor knows. The first argues the premise that he believes, on its own, best supports their conclusion -- given what he thinks the other doctor is likely to believe. The second doctor responds with a premise that, taken together with the first premise, best supports the opposite conclusion.


don't need every thing


- example: learn they have the gene, but also they have some other gene...

Two Bayesian doctors

represents the degree of **support** (opposition) of B for A. If it is greater than 1, B supports A. If it is less than 1, B opposes A. 



A doctor trying to convince a colleague that a patient has the rare disease can therefore argue that the patient has the rare gene. This argument would be 






If two doctors disagree about the diagnosis of a patient, one doctor might assert that the patient has the disease, 



INSERT: `S(A,B)` and `S(B,A)`





For example we can look at US census data and calculate the probability that it is owner-occupied, and the probability that a household *in Georgia* is owner-occupied, and take the ratio of these to compute how much more or less likely a household is to be owner-occupied given it is in Georgia.


S(A,B) is just a measure of correlation. It is symmetrical.

It is used in Bayesian inference to update beliefs. 




TODO:
  - still don't make the link between Bayesian inference and argument
  - what's missing is insight into the role of the prior
  - - 



`S(A,B)` represents the degree of **support** (opposition) of B for A. If it is greater than 1, B supports A. If it is less than 1, B opposes A. 

`S(A,B)` can be seen as a measure of the correlation between A and B. 

And it can be used as a for making inferences about things we don't know based on what we do.

If she doesn't know if A is true, but starts with some prior estimate of the probability is true, and then learns that B is true, she can scale up or down her estimate by multiplying by `S(A,B)`.

If all we know about a house is that it is in Georgia, we can at least slightly improve our estimate of the probability that it is owner-occupied.

A Bayesian reasoner could also make subjective estimates based on past experience. I might start out thinking my opponent is probably not bluffing, but then decide that the sweat I observe on their brow supports the hypothesis that they are bluffing, and call.

We can now see a connection to Bayesian inference and argumentation. If an argument is a premise (evidence) given for accepting a conclusion, then a Bayesian reasoner could make a subjective estimate of how strongly that premise supports the conclusion (e.g. the relevance).

And we can estimate how strongly the premise supports the conclusion for the *average* participant in an argument by calculating how much more likely a participant is to accept the conclusion given they accept the premise.
