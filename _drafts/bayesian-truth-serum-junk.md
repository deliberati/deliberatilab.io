
### TODO

These mechanisms requires a **payment** for answers that actually motivate people. But people typically aren't getting paid to post on online platforms (at least, not in fungible currency). And of course the method only works if you expect other people to be truthful: if you expect other people to lie it simply incentivizes lying. So how do you get to the point where "everyone else" is telling the truth? And of course this method is vulnerable to manipulation for example by fake accounts.

But there is a solution to all these problems. The ingredients to creating a forum, social network, fact-checking tool, etc. that gives truth the advantage include:

    - The "Nash Equilibrium"
    - Bayesian Truth Serum and Other Methods: game theoretical approach for incentivizing honest behavior about **subjective** questions** without verifications
    - Using Attention as the Incentive: and why a mechanism that rewards users with **attention** is sufficient
    - Reputation: What would Larry Do
    - Social Consensus: Hard Forks




### The Keyensian Beauty Contest

So how do people decide which contestant to choose? In game theory there is the concept of a Schelling Point, or focal point, which the famous Thomas Schelling described in 19XX as 

> ...some clue for coordinating behavior, some focal point for each person's expectation of what the other expects him to expect to be expected to do.
> 
> -- Thomas Schelling, the Strategy of Conflict, 1960

For example, there might be one contestant that looks like a typical  supermodel. That is a 

One of the quotes that most sruck me in Vitalik Buterin's paper is


This is the key...




This is why is possible to create reliable data feeds on the blockchain with this sort of information (the goal of Buterin's original paper), and rely on this data for executing complex financial contracts (prediciton markets, stablecoins, insurance, etc.)


I don't think the blockchain world has yet seen a way out of this problem. 



Now, all this begs the question, why do you expect other people to tell the truth in the first place?

happen for other reasons (social consensus, hard forks).


---



The quality of each article Qi is drawn from a beta distribution with parameters α, β. These could be set at 1,1 to reflect "no assumptions", or to reflect our prior knowledge that the typical upvote rate is low, something like 1,9.

    Qi ~ beta(α, β)

The coefficient at each rank is drawn from some distribution -- not sure which.


This "virtuous cycle"
People hold each other accountable for telling the truth.


Buterin's basic idea has been proven to work amazingly well: it is now widely used as a source of consensus on basic facts about the world that underpin the new multi-trillion dollar industry of decentralized finance. And it works even though these basic facts come from **random anonymous people on the internet**.




Meanwhile, nobody has applied any of these techniques to social networks, where honesty certainly is one part of the problem with misinformation...

But why can't they be applied to social networks? Currently there is a big conceptual gap...there is a belief that lies are spread in social networks because people are dishonest, and that's juts the way it is. THe idea that *technology* can make people more honest is ludicrous.

But ....technology can change behavior, because technology creates environments that reward and amplify certain behavior with attention. The reason social media is so toxic, shallow, and misinformed is not simply that people are this way. It is because social media rewards and amplifies that behavior.

attention economy. attention is the motivator. ...the ability to command attention is the currency of the social internet.
 Followers, influence, status.

gain reputation not just for what you post...but what you endorse....like, upvote, etc...and otherwise help to promote

the way to gain followes, influence is to help amplify content that you honestly think is accurate, useful, relevant, etc.
...if you have a lot of followers, or karma, or the social media algorithms decide that people are likely to click on your posts, the platform will reward you with attention. All social platforms have in essence a reputation system that determines how much attention your posts will command, whether it is follower count, karma, or hidden coeffecients in a collaborative filtering matrix. 
all social platforms are in a sense a game where users compete for attention


not censorship....
....give truth the advantage.
protocols can allow a community to deal with abuse and self-moderate...


...social platforms, eDemocracy, DAO governance platforms, fact-checking tools -- any place where distributed groups of people collaborate, share information, and make decisions.

----

Assuming that the jurors and the center share a common prior is convenient, but it limits the situations where the method can be used. In a jury trial, for example, how on earth does the center know what the jurors are likely to believe about other jurors' opinions?

Later I'll talk about methods that don't require the common prior assumption, but I also want to create a better understanding of what exactly we mean by the prior.


The peer prediction paper describes a scenario where each participant receives a private "signal" which they use to update their own beliefs (#2). Since the participants and the center have common prior beliefs, then if they receive the same signal they will end up with the same posterior beliefs.

The signal is a noisy indicator of the ground truth. Observing a signal changes your opinion about the probability of the ground truth, and thus about the probability that others will observe the same signal. The belief in the noisiness of the signal is fixed in the peer prediction paper

But for the method to work, we don't need to actually modify participants opinions about ground truth. We only need their beliefs about what other people's opinions will be, given the signal. That is, we need a model that gives us a probability that the next user will receive a high/low signal given the current user receives a high/low signal. That's all. That's why we can swap in a beta distribution.

Also the system incentivizes honest reporting about your signal, not about what you think ground truth is.

So we don't need ideas about ground truth and noisiness of signals. The signal can just be "your opinion"


For this all to work, I need a model that tells how my opinion is correlated with other people's opinion.




It's important to stress that the priors/posteriors we are talking about about

Report your *signal*, not your opinion.


An example is a crowdsourcing context. You are shown an image and asked if it contains nudity or not. Everybody starts out with the same, weak prior about the probability that a random image will contain nudity, based possibly on published statistics from past images. So you, the other users, and the center have common priors. Then your posterior belief in the probability that *other* users

They use the example of eBay product ratings. You buy something from some seller on eBay and it is either high quality or low quality. Everybody starts out with the same, weak prior about quality of random products from eBay, based possibly on statistics published by eBay itself. So before receiving the product and experience its quality, you have the same beliefs as everyone else, and as the center. Then your posterior, after observing wither the product is high or low quality, is therefore the same that other users would have after receiving the same signal, and the same that the center would have after you report your signal.

It's important to stress that when we talk about priors/posteriors for peer prediction, we are talking about beliefs about what other people's opinions are. In the peer prediction model, your "signal" is not evidence that you use to update your opinion about the quality of the product, or the guilt of the defendant. Your signal is simply your opinion. It is your brain signalling to you what you think "this product is good", "this defendant is guilty", "I have cheated on my taxes", etc. The model assumes that these beliefs are binary, not probabilities.

At the beginning of this article we said each juror bases his forecast on:

1) the opinions of the other jurors 2) his own opinion and 3) his prior beliefs about other jurors opinions.

#2 is your "signal". #1 is the reported signal by other jurors (assuming they are honest). #3 is any prior beliefs you have about what other people will think about the product, before you observe its quality.

In the eBay example, or in many crowdsourcing scenarios, t is reasonable to believe that #3 is common. If you are given 



In the eBay example, since your priors are the same as other people's priors, then your own opinion is simply your belief after updating it based on your private "signal".

Since you and the other jurors have common priors, #2


So your experience is your signal. You have no prior expectations about what other people's experience will be with this particular seller. You only have a general, probably weak prior about the quality of products from eBay in general; based possibly on statistics published by eBay. The center may have these same statistics, and so it seems plausible that your priors, being based on roughly the same information, would be common.







If the juror considers the testimony from the jury trial, and find it convincing, does that effect #2 or #3?

The answer is both. Convincing testimony might be convincing to you, but you might not think it is convincing to others. Or vice versa. So these are separate things.

Now, even before the beginning of the trial, a juror will have prior beliefs. They may be weak or strong, they may be based on prejudice or statistics, but without priors a Bayesian reasoner cannot reason. If you told the juror to guess whether the defendant was guilty before seeing any evidence, they could still guess. They could also guess what the other jurors would guess.

In the context of a social platform, prior beliefs in the probability that other jurors will upvote some random post can be based on statistics about percentage of users that liked past posts. Priors based on just these statistics can actually be pretty good.

Jurors then udpate priors based on the case-specific information.




- subjective opinion. What part is a "prior" and what is my opinion.
- the peer prediction "private signal"
- signals are noisy. 
- receiving signal changes my belief about ground truth, and my expectation about what signals others will receive
- when it works. The etrade method
- separating "general" statistics from "specific" priors
- the center cannot evaluate specific cases.

We are trying to do two things. Elicidate the concept of a private signal to harden our mental model about what distinguishes priors from data. And then explain where common prior assumption doesn't hold.









This scenario obviously has limited applicability. An example introduced in the paper is eBay product ratings. You buy something from some seller on eBay and it is either high quality or low quality. So your experience is your signal. You have no prior expectations about what other people's experience will be with this particular seller. You only have a general, probably weak prior about the quality of products from eBay in general; based possibly on statistics published by eBay. The center may have these same statistics, and so it seems plausible that your priors, being based on roughly the same information, would be common.






In our jury trial example, the center may be some automated system incapable of evaluating the evidence from the trial itself. All they can do is update their beta distribution based on the votes of the jurors. The jurors on the other hand can look at the evidence presented in the trial and update their priors even before learning about the votes of other jurors. If the evidence is strong, they may have a strong expectation that the average juror will convict. Even if the first 10 votes are split 5:5, the 11th juror may, due to their prior belief, expect the 11th juror to vote guilty. So the 11th juror expects that the center underestimates the probability of a guilty vote.

If the 11th juror believes the defendant is guilty, this is not a problem. By voting guilty, the center's prediction of the next vote will move closer to the 11th jurors own prediction. But if the juror still believes that the defendant is innocent -- perhaps believing that he has a better understanding of the evidence presented at trial than other jurors -- then he is no longer incentivized to vote honestly.

This unfortunately is not very realistic in many situations. In a jury trial, for example, how on earth does the center know what the jurors are likely to believe about other jurors' beliefs?

TODO: conceptual separating of priors from opinions

