
## Hypothetical Opinion Polls

Distributed Bayesian reasoning can be thought of as a hypothetical opinion poll. An opinion poll is a method of making educated guess about people's beliefs: even if I don't specifically ask **you** what you believe, I can use data obtained from *other* people to make an educated guess.

Deliberated Bayesian Reasoning goes further by measuring not just what people believe, but why. If the poll determines that people who are told about the results of some scientific study are 10% more likely to believe in the effectiveness of some drug, then I can infer that if you learned about the study, you would also be more likely to believe in the drug as well.

This sort of poll is pretty routine: the novelty here is chaining these inferences together, estimating that if people believed this, they would probably believe that, and if they believed that, they would probably believe this other thing, and so on.

As with an opinion poll, what information influences people's opinions very much depends on the people who are polled. For example, we may often find certain information is convincing to Democrats but not Republicans, and vice versa. 
