---
layout: single
title:  "Deliberati Introduction"
date:   2022-02-03 00:00:00 +0200
toc: true
toc_sticky: true
sidebar:
  - nav: "deliberati-introduction-related"
    title: "Related Articles"

# header:
#     teaser: /assets/images/distributed-bayesian-reasoning/jeffrey-conditioning-formula.png

excerpt: "Introduction to Deliberati"


---



## Our Thesis

1. Our first thesis is that the social platforms themselves interfere with natural dynamics of group social interaction, often resulting in a toxic, divisive, irrational public discourse.
	- [invisible censor, simple misinformation, feedback loops]

2. Second, we understand why this happens.

3. Third, it is possible to intentionally design platforms(algorithms) to promote a healthy dynamic

The question is, but how?

## The Solution

We have designed an algorithm that promotes a dynamic that 1) gives truth the advantage, 2) encourages civil discourse, and 3) helps people with different viewpoints find common ground.

### Giving Truth the Advantage

Today's social networks tend to amplify misinformation because ranking and promotion algorithms inadvertently create a dynamic that rewards users (e.g. with followers, reputation, etc) for posting false and misleading information.

We think it's possible for a social platform to "give truth the advantage" with an algorithm we have designed that **1) rewards honesty 2) promotes productive conversation 3) amplifies the most informed content.**.

**Honesty** can be rewarded using a clever mechanism from the game theory literature called the Bayesian Truth Serum, where reputation points are awarded for upvoting and downvoting content, and for predicting the votes of others, in such a way that honesty is a Bayesian-Nash equilibrium.

**Productive conversation** can be promoted using a process called Deliberative Polling, which identifies and directs people's attention towards comments with the highest information content and drives deep, productive conversation.

**Informed content** can be amplified using a method of ranking base on Distributed Bayesian Reasoning, a method for calculating an informed consensus which, instead of revealing the most *popular* content, uses Bayesian Inference to estimate what content **would** be popular if users all shared the same information.


### Pluralistic Moderation

Second, we know that there are social platforms where the discourse is not so toxic. The common denominator of these successful online communities is moderation and competent moderators that uphold standards of civilized, healthy, acceptable discourse.

The problem here is that people have differing ideas of what is acceptable discourse. So moderation tends to results in echo chambers: eventually, people with dissenting views leave, often to other communities with their own biases. The result is multiple separate and increasingly polarized online communities.

Is it possible to avoid this trap, and uphold basic standards of civil, honest, rational discourse, while tolerating a diversity of viewpoints?

We think the solution is **pluralistic** moderation: a system that allows multiple sub-communities each with their norms and standards of acceptable speech, while promoting healthy conversation **between** communities by amplifying content that has broad support.

## How it looks

Our vision is a feedback and ranking algorithm that can be integrated into virtually any social platform.

First, users must be able to provide bi-directional feedback: upvote useful content but also downvote content that they think is misleading, low quality, or that violates community standards. And they must be able to give reasons for these downvotes, and leave comments explaining these **reasons**.


<aside class="custom-aside" markdown="1">

### Illustration

- Post (some political mud-slinging meme) with downvote button
- User click on button go get context menu
	- Agree, But...
	- Misleading or Misinformed
	- Low Quality Contribution
	- Violates Community Standards
	- Other
- User chooses one
- List of existing reasons, prompt to choose one or "add new reason"

</aside>


The platform then identifies critical comments with high information content. These are comments that actually cause changes in behavior: people who have seen these critical comments are less likely to upvote or share the original content. 

<aside class="custom-aside" markdown="1">

### Illustration


</aside>


It then amplifies these comments, in particular notifying people who previously upvoted or shared the content and giving them a chance to retract their support, or reply with their reasons for standing by the content. The most informative responses are then identified, and the process continues as long as conversation is still changing minds.

The Distributed Bayesian Ranking algorithm then calculates the deliberative consensus: the probability that a fully-informed user would still upvote the content after seeing all statistically informative comments. It then uses the deliberative consensus, instead of raw popularity, as the input to the Bayesian Truth Serum scoring formula. Users maximize their expected reputation gain from each vote by voting honestly, but also by accurately predicting how critical comments will change the deliberative consensus. 

So if a piece of content is initially popular, but your minority opinion is based on information that the majority vote does not yet reflect, and you think that at least at least reasonable people will honestly change their mind after you or others people in the minority share that information, then you can maximize your expected reputation gain by honestly sharing what you believe and why.

Each user has multiple reputation scores among different sub-communities. Sub-communities will have not only different ideas of what is acceptable speech, but also different epistemic standards: for example some being more scientific, other based more on faith or political ideology. Pluralistic moderation allows all these individual communities to thrive: if you are a liberal and develop a terrible reputation among some conservative sub-communities, that is probably okay. You will develop stronger reputation and thus more influence in the community of like-minded people.

But you will not necessarily be isolated from other sub-communities. This is because there are dimensions of reputation shared across sub-communities.

For example, you will have a separate "civility" reputation score: your reputation for promoting civil discussion and down-voting uncivil comments. You may possibly have a high reputation score within a variety of sub-communities, even if you generally disagree with the worldview of some of those communities. What this means is that you will still have a voice in these communities: you will be a **trusted** dissenting voice. What more, you can have a high reputation in these communities if you are often able to provide **informative** comments that are seen as civil and change at least some people's minds.


##WIP 

Okay a key part of the process is that a comment can be flagged as being false/misleading. If you agree with the argument, you agree with premise warrant. If you disagree and respond, your disagreement is a warrant argument. Otherwise, you flag it as false/misleading.

downvote
	I agree but...
	not true / I disagree
	not interesting, entertaining, or useful
	breaks a rule
		personal attack, etc.

Stake Reputation:
	do you think most others on this site will agree?

Staking reputation is a way of increasing your reputation score by **betting** on the upvote/downvote ratio of a comment or post. 

Research has shown that it is possible to obtain more accurate, truthful responses on opinion polls by asking people not just what they believe, but **what they think other people believe** (references). 

For example, if you personally like a post, but think it will be unpopular, then this system encourages you to honestly share your own (unpopular) opinion, while earning reputation by helping to identify content that will be accepted by the community. So you you can contribute to upholding community standards and (propogating) its unique culture while providing important feedback to help avoid echo chambers and groupthink.

The reputation formula promotes honesty using a clever technique called the Bayesian Truth Serum, or BTS. BTS is a mechanism grounded in Game Theory in which you are asked to share both your personal opinion, and your guess about other people's opinions. Then you are awarded reputation points using a formula designed such that, if other users are being honest with their opinions, then the best way for you to to maximize your reputation is to to be honest about what you think their opinions are. But the genius of BTS is that it goes both ways: if everyone is being honest about what they think others' opinions are, then the best way to maximize your reputation is to **be honest about your opinion**! The result is a Bayesian Nash equilibrium where everybody is honest because they expect everyone else to be honest, and -- like the tragedy of the commons in reverse -- it takes a tremendous, coordinated collective effort to break out of the equilibrium.

The mechanism takes advantage of the fact that your personal opinion is **information**, and that information has has value.

The two of these combined means there is a Bayesian Nash equilibrium about honestly sharing **what you think is reasonable**.

This is extremely powerful. This is so important. It means what? Echo chamber and conformity and all that. But it allows us to appeal to common values.

One way public discourse breaks down is that there is a situation of pluralistic ignorance around reason. What does that mean? It means that the majority can recognize something as unreasonable, but the fact that something is recognized as unreasonable is not common knowledge.


1) deny facts and 2) deny reason. 

reason leads to a conclusion that the majority **believe** is reasonable, but there is a conflicting conclusion that is a stronger Schelling point.

When you stake your reputation



