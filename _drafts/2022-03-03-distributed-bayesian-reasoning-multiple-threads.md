---
layout: single
title:  "Multiple Argument Threads"
date:   2022-03-03 00:00:00 +0200
toc: true
toc_sticky: true
header:
    # teaser: /assets/images/distributed-bayesian-reasoning/complex-argument-graph.svg
sidebar:
  - nav: "distributed-bayesian-reasoning"
    title: "In This Series"
  - nav: "distributed-bayesian-reasoning-related"
    title: "Related Articles"

---

This article is the last in our series on distributed Bayesian reasoning. It assumes you have read the previous article on the [Basic Math](/distributed-bayesian-reasoning-math).

In this article, we use Bayesian Hierarchical Models to estimate the priors of the meta-reasoner inthe case of multiple argument threads.

This document is a work in progress -- these models have not been fully developed. In fact, we are looking for collaborators. If you are an expert in Bayesian hierarchical models and causal inference, please contact jonathan@deliberati.io.


### Multiple Argument Threads

Recall that the [definition of an argument thread](/argument-model/#argument-threads) is a premise argument, followed possibly by one or more warrant arguments. These threads are each separate dialogs, and although we know that at least one or two users have participated in each argument thread (else it would not exist), it is certainly possible that different groups of users will initiate and hold separate arguments threads about the same claim, without participating in the other threads.

How can we estimate the priors of probability that the meta-reasoner will accept 𝐴 given they participated in all the threads, if no single user has actually done so?

It turns out, Bayesian Hierarchical Models provide a solution here too.

It's worth noting that we are now using Bayesian inference at two separate levels. In the first, we use a Bayesian Hierarchical Model to estimate the priors of the meta-reasoner. And then we use the rules for Bayesian Belief Revision to propagate information through the mind of the meta-reasoner and estimate the justified opinion.

## Further Development

This document is a work in progress -- these models have not been fully developed. In fact, we are looking for collaborators. If you are an expert in Bayesian hierarchical models and causal inference, please contact collaborations@deliberati.io.

