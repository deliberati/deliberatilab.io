---
layout: single
title:  "Significance"
toc: true
toc_sticky: true

---


## Warrants and Corelevance

We said that a warrant **exists** if the probability of the conclusion is greater given the premise than not. We also said warrant is a claim (or in Bayesian terminology, a proposition). But what exactly is the claim?

Is it the claim that $$P(A \vert B) > P(A \vert notB)$$? No: this claim is not something the **subject** believes. It is **our** description of the subject's beliefs. So it can't be the warrant because it is not a claim that justifies, in the subject's mind, the inferential leap from premise to conclusion.

Now, a warrant according to Toulmin is a **rule** of inference: if the subject accepts that rule, then they will also be more likely to accept the conclusion if they accept the premise.


Taking an example from Toulmin's *The Uses of Argument*, suppose (𝐵) *Petersen is a Swede* is argued in support of (𝐴) *Petersen is not a Roman Catholic*. The unexpressed warrant, according to Toulmin, is a rule that can roughly be expressed as (𝐶) *A Swede can be taken almost certainly not to be a Roman Catholic*. 

So if the subject accepts the claim 𝐶, then they should also be more likely to accepts 𝐴 if they accept 𝐵.

Let's represent this situation with some data. Suppose the beliefs of the subject are represented by the following probability distribution table. 

|  C |  A |  B |  Pr
| ---|----|----|-----
| 0  | 0  | 0  | 7.2%
| 0  | 0  | 1  | 64.8%
| 0  | 1  | 0  | 0.8%
| 0  | 1  | 1  | 7.2%
| 1  | 0  | 0  | 1.8%
| 1  | 0  | 1  | 3.6%
| 1  | 1  | 0  | 0.2%
| 1  | 1  | 1  | 14.4%

From this we can calculate the following conditional probabilities:

$$
\begin{aligned}
    &P(A|\bar{B})    = P(A|\bar{B},C) = P(A|B,\bar{C}) = P(A|\bar{B},\bar{C}) = 10\%\\
    &P(A|B,C)        = 80\%
\end{aligned}
$$

So acceptance of either 𝐶 or 𝐵 alone has no effect on the probability of accepting 𝐴. But acceptance of 𝐶 and 𝐵 together increases the probability from 10% to 80%.

This means that 𝐵 is relevant to A only if the subject accepts the rule of inference 𝐶.

### Definition of Conditional Relevance

Let's define the **conditional relevance delta** $$Rd(A,B \vert C)$$ as the relevance delta of 𝐵 to 𝐴 given the subject accepts 𝐶:

$$
    Rd(A,B|C) = P(A|B,C) - P(A|\bar{B},C)
$$

### Definition of Corelevant

We can say that 𝐶 and 𝐵 are **correlevant** to 𝐴 if **relevance** of 𝐵 to 𝐴 is higher if the subject accepts 𝐶 than if they do not. Mathematically:

$$
    Rd(A,B|C) > Rd(A,B|\bar{C})
$$

### Definition of Corelevance 

We can measure the magnitude of the correlevance as the difference:

$$
    CRd(A,B,C) = Rd(A,B|C) - Rd(A,B|\bar{C})
$$

It's easy show that co-relevance is symmetrical.

$$
    CRd(A,B,C) = CRd(A,C,B)
$$

This means that we can just as well consider 𝐵 to be the "rule" that justifies the inference from the 𝐶 to the conclusion. If someone where to argue that (𝐶) *A Swede can be taken almost certainly not to be a Roman Catholic* in support of (𝐴) *Petersen is not a Roman Catholic*, then this seems to imply that (𝐵) *Petersen is a Swede*, because Petersen being a Swede is the only thing that makes Swedes not being Roman Catholic relevant.

### Corelevance as Slope

This equation shows us how P(A) varies with P(B) and P(C). 

$$
    P(A) = P(A|notB,notC) + P(B)Rd(A,B|notC) + P(C)Rd(A,C|notB) + P(B,C)CRD(A,B,C)
$$

P(A|notB,notC) is the minimum value of P(A) if the subject rejects both B and C. If Rd(A,B|notC) is nonzero, it means there is the component of relevance of B to A that exists independently of the subjects acceptance of C. And vice versa if Rd(A,C|notB) is nonzero. CRD(A,B,C) can be thought of as the component of relevance of the conjunction of B and C to A. 



### Identifying the Warrant


A claim W is a warrant justifying the inference from B to A if:

$$
    Rd(A,B|¬C) = 0
    Rd(A,B|C) > 0
$$



Proof
    Rd(A,B|C) > Rd(A,B|\bar{C})
    ⟺ P(A|B,C) - P(A|notB,C) > P(A|B,notC) - P(A|notB,notC)    
    ⟺ P(A|B,C) - P(A|B,notC)  > P(A|notB,C) - P(A|notB,notC)
    ⟺ P(A|C,B) - P(A|notR,B)  > P(A|C,notB) - P(A|notB,notC)
    ⟺ Rd(A,C|B)  > P(A|C,notB) - P(A|notB,notC)




If a subject is a Bayesian reasoner, then if they believe C, then B will be relevant to A


Petersen is a Swede






### Definition of Significant

We will use the term **significance** to quantify **the degree to which an argument would change the subject's belief in the conclusion, if they were to accept the premise**.

**The premise 𝐵 is significant to the conclusion 𝐴 if**:

$$
    P(A|B) ≠ P(A)
$$

**The premise 𝐵 is insignificant to the conclusion 𝐴 if**:

$$
    P(A|B) = P(A)
$$

In other words, if learning 𝐵 has not effect on the subject's belief in 𝐴,   𝐵 is insignificant to 𝐴. Otherwise, 𝐵 is significant to 𝐴.


### Quantifying Significance

As with relevance, significance can be measured as a ratio or a delta. In this case, a ratio is most interesting.

**The significance of 𝐵 to 𝐴 is**:

$$
    S(A,B) = \frac{P(A|B)}{P(A)}
$$

If $$S(A,B) = 1$$, then $$P(A \vert B) = P(A)$, which means 𝐵 is insignificant to 𝐴.

If $$S(A,B) ≠ 1$$, then $$P(A \vert B) ≠ P(A)$, which means 𝐵 is significant to 𝐴.


### Significance and Bayes Theorem

Plugging the definition of $$P(A \vert B)$$ into the definition of $$S(A,B)$$, we see that:

$$
    S(A,B) = \frac{P(A,B)}{P(A)P(B)}
$$

So significance is symmetric:

$$
    \frac{P(A|B)}{P(A)} = C(𝐴,𝐵) = \frac{P(A,B)}{P(A)P(B)} = C(𝐵,𝐴) = \frac{P(B|A)}{P(B)}
$$


### Definition of Cogent

Besides changing the subject's belief in the premise, there is one other way that an argument can change an imperfect Bayesian's belief in the conclusion without providing new information: it can change their belief in the **warrant**.

For example, my wife may argue (𝐵) *it is President's Day* in opposition to (𝐴) *we should go to the bank*. I may have been perfectly aware that it is is president's day, but forgot that this is a good reason not to go to the bank. 

In this case, my wife has not reminded me of the premise 𝐵: she has reminded me of the **warrant** (the unexpressed premise) that 𝐵 is a good reason to oppose 𝐴. 

Since we are modeling me as a perfect Bayesian, we don't need to analyze the logic of the warrant justifying my inferential leap from 𝐵 to not 𝐴 (e.g. because banks are closed on President's Day). We can simply treat $$I_{ab}$$ **as if** it provided new information that caused $$P_i(A \vert B)$$ to be less than $$P(A \vert B)$$.

But how exactly does this work? How can new information change **conditional** probabilities?

This is best explained with a concrete numerical example.


Suppose my beliefs are as shown by the joint distribution in the below table. 

----

If P(A|B) = P(A|notB), B is irrelevant to A and no warrant exists. The degree of the subject's belief in the warrant can be measured as the **relevance** (the difference between P(A|B) and P(A|not B). For an argument to be persuasive, the


$$
    P_i(A) > P(A) iff P_i(B) > P(B) ∨ Rd_i(A|B) > Rd(A|B)
$$

proof

If we define Rd_i(A,B) = as P_i(A|B) - P_i(A|\bar{B}), then:

P_i(A) > P(A) ⟺ P_i(A|negB) + P_i(B)*Rd_i(A,B) > P(A|negB) + P(B)*Rd(A,B)
              ⟺ P_i(A|negB) > P(A|negB) 
              OR P_i(B) > P(B) -- -and rd > 0
              OR Rd_i(A,B) > Rd(A,B) -- and pb > 0




----


### Significance and Bayes Theorem

Plugging the definition of $$P(A \vert B)$$ into the definition of $$S(A,B)$$, we see that:

$$
    S(A,B) = \frac{P(A,B)}{P(A)P(B)}
$$

So significance is symmetric:

$$
    \frac{P(A|B)}{P(A)} = C(𝐴,𝐵) = \frac{P(A,B)}{P(A)P(B)} = C(𝐵,𝐴) = \frac{P(B|A)}{P(B)}
$$


And Bayes theorem can be rewritten in terms of significance:

$$
\begin{align}
    P(𝐴|𝐵)  &= P(𝐴) \frac{P(𝐵|𝐴)}{P(𝐵)} \\
            &= P(𝐴) C(𝐴,𝐵)
\end{align}
$$

Significance can be thought of as a factor of correlation between 𝐴 and 𝐵. And Bayes theorem tells us that, when a Bayesian reasoner learns evidence 𝐵, they increase or decrease their belief in hypothesis 𝐴 by this factor.

Perhaps counter-intuitively, this goes both ways: since $$S(A,B) = S(B,A)$$, for the purposes of inference, observing evidence 𝐴 provides the same support for hypothesis 𝐵, as observing evidence 𝐵 provides for hypothesis 𝐴.

Another interesting fact about this metric is that the expected value of $$log S(A,B)$$, taken over the joint probability distribution of 𝐴 and 𝐵, is the [mutual information] of 𝐴 and 𝐵.



-#### Prior Belief in the Premise
-
-In other words, **significance, unlike relevance is dependent of the subject's prior belief about the premise.**.
-
-To see this mathematically, here again are the definitions of relevance (as a ratio) and significance:
-
-$$
-\begin{align}
-    R(A,B) &= \frac{P(A|B)}{P(A|B̅)} &&\text{relevance}
-    S(A,B) &= \frac{P(A|B)}{P(A)} &&\text{signififance}
-\end{align}
-$$
-
-Both factors in the definition of relevance $$R(A,B)$$ are conditional probabilities: they tell us what the subject **would believe** if they accepted/rejected 𝐵. But the denominator in the definition of relevance, $$P(A)$$, is an unconditional probability: it tells us what the subject actually believes about 𝐴 -- which depends on what they believe about 𝐵 as illustrated in Figure X.
-
-### Examples
-
-For example, suppose the subject believes that (𝐵) *the sky is cloudy* is relevant to (𝐴) *it is going to rain today*. If the subject then looks out the window and sees a cloudy sky, will it change their belief about the probability of rain?
-
-That entirely depends on how surprising that cloudy sky is. Perhaps, if they are in Seattle in January, they already expected clouds and rain. Whereas if they are in Phoenix in June, they expected a blue sky.
-
-
-### Other Observations
-
-Here are some other observations about the relationship between significance $$S(A,B)$$ and the subject's prior beliefs in the premise $$P(B)$$
-
-- If the subject already accepts 𝐵, then $$P(A) = P(A \vert B)$$, and therefore significance will be equal to 1 (𝐵 is insignificant to 𝐴).
-
-- $$S(A,B)$$ approaches $$R(A,B)$$ as $$P(B)$$ approaches 0[^1].
-
-- In other words, significance becomes more extreme (further from 1) the more **surprising** 𝐵 would be (were the subject to learn that it was true).
-

-
-This is illustrated in the equation
-- In other words, significance becomes more extreme (further from 1) the more **surprising** 𝐵 would be (were the subject to learn that it was true).
-
-This is illustrated in the equation
-
-    Sd(A,B) = P(A|B) - P(A)
-        = P(A|B) - P(A|B)P(B) - P(A|not B)P(not B)
-        = P(A|B)(1 - P(B)) - P(A|not B)P(not B)
-        = P(A|B)P(not B) - P(A|not B)P(not B)
-        = P(not B)( P(A|B) - P(A|not B) )
-        = P(not B)( Rd(A,B) )
-
-    S(A,B) = P(A|B)/P(A)
-        = P(A|B)/(P(A|B)P(B) - P(A|not B)P(not B))
-        = P(A|B)(1 - P(B)) - P(A|not B)P(not B)
-        = P(A|B)P(not B) - P(A|not B)P(not B)
-        = P(not B)( P(A|B) - P(A|not B) )
-        = P(not B)( Rd(A,B) )
-
-
-
-
-        - Thus to be significant, a premise must also provide information.
-
-- **Significance implies relevance**. If 𝐵 is not relevant to 𝐴, then 𝐴 and 𝐵 are statistically independent, and thus changing belief in 𝐵 has no effect on belief in 𝐴.
-
-- **Significance is maximized when prior belief in B is P(B) = 0**. This is because if $$P(A) = P(A \vert B̅)$$, the definitions of relevance and significance become identical. TDO:show
-- **The more relevant the premise, the more significant it is **if* it is new information**
-
-TODO: show equations again
-
-
-
-So 𝐵 is only significant if:
-
-1. it is new information and
-2. it is relevant
-
-On the other hand, a premise can be relevant without being significant.
-
-
-
-In figure X, the closer $$P(B)$$ is to zero, the further to the left the subject's prior belief is along the line in figure X, and therefore the greater the change in $$P(A)$$ when $$P(B)$$ moves all the way to 1, thus the more significant 𝐵 is to 𝐴.
-
-However even if 𝐵 is new information, if it were not **relevant**, the line in Chart X would be flat, and therefore changing $$P(B)$$ will have no effect on $$P(A)$$.
-
-
-So 𝐵 is only significant if:
-
-1. it is new information and
-2. it is relevant
-
-On the other hand, a premise can be relevant without being significant.
-
-**If the subject does not accept premise 𝐵 a priori relevance and significance are the same**. This is because if $$P(A) = P(A \vert B̅)$$, the definitions of relevance and significance become identical.
-
-In other words, relevance is the maximum possible value of significance, obtained when $$P(B)$$ is minimized.
-
-
-[^1]: because no evidence can cause a Bayesian agent to believe something they previously believed was impossible.
-
-[^2]: we use the term **persuasive** but not **convincing** because the term **persuasiveness** is less awkward than **convincingness**.
-
-
-#### Relevance of Compliment
-
-**If a premise is relevant but not significant, then its compliment will be significant**. Example: *he has a pulse* is relevant but not very significant, but *he does not have a pulse* is very significant.
-
-This is because, the only way an argument can be relevant but not significant is if the premise is not new information: the subject already believes the premise to be true. But this implies that the compliment of the premise **would** be new information. Further, Rd(A,B)
-
-Examples:
-    - John is a Math Genius because he can do basic math
-    - The world is spherical because the universe exists
-
-
-
-Some other observations:
-
-- Rd(A,B) = -Rd(A̅,B) = -Rd(A,B̅) = Rd(A̅,B̅)
-**If the subject does not accept premise 𝐵 a priori relevance and significance are the same**. This is because if $$P(A) = P(A \vert B̅)$$, the definitions of relevance and significance become identical.
-
-In other words, relevance is the maximum possible value of significance, obtained when $$P(B)$$ is minimized.
-
-
-[^1]: because no evidence can cause a Bayesian agent to believe something they previously believed was impossible.
-
-[^2]: we use the term **persuasive** but not **convincing** because the term **persuasiveness** is less awkward than **convincingness**.
-
-
-#### Relevance of Compliment
-
-**If a premise is relevant but not significant, then its compliment will be significant**. Example: *he has a pulse* is relevant but not very significant, but *he does not have a pulse* is very significant.
-
-This is because, the only way an argument can be relevant but not significant is if the premise is not new information: the subject already believes the premise to be true. But this implies that the compliment of the premise **woul
d** be new information. Further, Rd(A,B)
-
-Examples:
-    - John is a Math Genius because he can do basic math
-    - The world is spherical because the universe exists
-
-
-
-Some other observations:
-
-- Rd(A,B) = -Rd(A̅,B) = -Rd(A,B̅) = Rd(A̅,B̅)
-
-
-
-




## Significance



<aside class="proof" markdown="1">

The terms "support" and "oppose" can also lead to confusion, because they mean different things in the context of the arguments in the argument graph and the beliefs of the meta-reasoner. In the argument graph, the premise 𝐵 supports the claim 𝐴, simply because this argument exists in the graph. But this does not necessarily mean that the claim 𝐵 **persuasively** supports the claim 𝐴, as defined by the beliefs of a Bayesian rational agent). So when the **argument** supports a conclusion, it doesn't mean that the **premise** supports the conclusion; it only means that it has been claimed to do so.

</aside>



### Conditional Relevance

Recall that we have defined the warrant of an argument as a claim that the premise supports or opposes the conclusion, and so the warrant of 𝐴<+𝐵 is the claim that:

$$
    P(A|B) > P(A|B̅)
$$

We can now extend this definition, and define the warrant of a warrant argument as the claim that the premise supports/opposes the root claim **given acceptance of all preceding premises in the thread**. 

So the warrant of 𝐴+𝐵◂-𝐶 is the claim that **𝐶 opposes 𝐴 given 𝐵**, or:

$$
    P(A|B,C) < P(A|B,¬C)
$$

We call the difference between $$ P(A \vert B,C) $$ and $$ P(A \vert B,¬C) $$ the **conditional relevance** of 𝐶 to 𝐴, given 𝐵.

It is impossible to calculate the conditional relevance based on the relevance of individual premises, because it is impossible to know how the particular combination of premises in an argument thread contribute to acceptance of the root claim claim. Arguments may be **linked** such that neither argument by itself is relevant, but the combination is (e.g. **the defendant's fingerprints were on the knife**, AND **the knife was the murder weapon**). Or they might individually add support (e.g. **the defendant signed a confession** AND **the DNA evidence is valid**). Or a counter-argument may completely nullify the support of some argument (e.g. **the defendant's fingerprints were on the knife**, BUT **the knife was NOT the murder weapon**).


This is important because the distributed Bayesian reasoning process involves estimating the probability that the meta-reasoner will accept the conclusion given they are accept/reject different possible combination of premises.



## Argument Significance, Relevance, and Criticality

There are various terms for measuring the persuasiveness of arguments and describing the degree to which different premises contribute to acceptance of a conclusion. Examples include "criticality", "validity, "soundness", relevance", "significance", etc. 

These concepts tend to be subjective and imprecisely defined. Yet we can give precise, objective definitions to some of these concepts in terms of the beliefs of a Bayesian reasoner. These beliefs may be subjective, but if we have quantified these beliefs in terms of probabilities, then we can *objectively* describe subjective beliefs.

The reasoners subjective beliefs claims $$A$$ and $$B$$ can be described by the prior probability function $$P$$. 


TODO: introduce same sample arguments


This difference can be measured in different ways. For example, the ratio of these two numbers is called the **likelyhood ratio**, and is a commonly used metrics in some fields. But for our purposes, we will find the absolutely mathematical difference in probabilities to be most useful. We call this difference the **relevance delta**, and denote it $$Rd(A,B)$$.

$$
    \begin{equation}
    Rd(A,B) = P(A|B) - P(A|B̅)
    \end{equation}
$$

It is natural to discuss the degree of relevance in plain-English terms such as "very relevant", "persuasively irrelevant", etc. We can say that, roughly speaking, that:

- $$B$$ is **very relevant to** $$A$$ if the difference between these two numbers if large
- $$B$$ is **persuasively irrelevant to** $$A$$ if the difference between these two numbers is very small

We won't try to specify exactly what constitutes a very small difference here.


Claims can support or oppose other claims

- $$B$$ **supports** $$A$$ if $$P(A \vert B) > P(A \vert B̅) $$
- $$B$$ **opposes** $$A$$ if $$P(A \vert B) < P(A \vert B̅) $$

The degree of support or opposition can also be loosely described in plain English

- If $$P(A \vert B) ≫ P(A \vert B̅)$$, then $$B$$ **critically supports** $$A$$
- If $$P(A \vert B̅) ≪ P(A \vert B̅)$$, then $$B$$ **critically opposes** $$A$$

Likewise we define **weakly relevant** premises as premises that weakly support or weakly oppose their conclusions.



From these definitions it follows that:

- 𝐵 is (critically/weakly) relevant to 𝐴 IFF 𝐵 (critically/weakly) supports or opposes 𝐴
- If 𝐵 is (critically/weakly) relevant to 𝐴, then ¬𝐵 is (critically/weakly) relevant to 𝐴
- If 𝐵 (critically/weakly) supports 𝐴 IFF:
    - 𝐵 (critically/weakly) opposes ¬𝐴
    - ¬𝐵 (critically/weakly) opposes 𝐴

We have discussed the relevance of claims to other claims. We can also discuss the relevance of an **argument**, which is the relevance of its premise to its conclusion. We can also define supporting or opposing a arguments as arguments whose premises support or oppose their conclusions, respectively.

TODO: but critically


## Significance

Relevance will be the most useful way of quantifying the persuasiveness of arguments for our purposes. But we also define the term **significance** primarily to contrast it with from relevance. These two concepts can be easily confused because there is a certain category of arguments that one is tempted to call "irrelevant" but that are, according to the definition above, quite relevant.

Assuming the rational agent whose beliefs we are describing has basic common sense, here are examples of arguments that fall under this category.

- Jack is in extraordinary athlete because he has a pulse
- The water is boiling because it is not frozen
- We should hire Bob because he wore pants to the interview

What these examples all have in common is that they are not very **persuasive**. If we learn that Bob worte pants

or that Jack has a pulse, or that Bob wore pants, we are unlikely to change our opinois


Using some common sense examples


There are certain arguments that 




$$
    P(A)    = P(A|B̅)P(A|B̅) + P(B)P(A|B)
            = (1 - P(B))P(A|B̅) + P(B)P(A|B)
            = P(A|B̅) - P(B)P(A|B̅) + P(B)P(A|B) 
            = P(A|B̅) + P(B)( P(A|B) - P(B)P(A|B̅) )
            = P(A|B̅) + P(B)Rd(A,B) 
$$





We will define the following terms:

- **Significance**: We say the premise is **significant** to the conclusion if the prior probability that the meta-reasoner accepts the conclusion given they accept the premise ($$ P(A \vert B) $$) is higher or lower than the prior probability that they accept the conclusion ($$ P(A) $$).
- **Critical**: We say an argument is **critical** to the extent that it is significant, and the posterior probability that the meta-reasoner accepts the premise is also high.
- **Relevant**: We say a premise is **relevant** to the extent that it is significant to the conclusion, or its compliment is significant to the conclusion. 


For example, if the argument is that we are all going to die tomorrow because the earth will be hit by a giant asteroid tomorrow, then the premise is probably very significant to the conclusion, but probably not true, so the argument is probably not very critical.

If the argument is that Alice is a math genius because she can do basic arithmetic, then the premise is probably not very significant, but it is probably very relevant, because the claim that Alice **cannot** do basic arithmetic **is** probably very significant. (aside: the difference between relevance and significance is related to the difference between necessary and sufficient conditions.)

These are of course all relative terms. An argument can be barely significant or very significant. And of course this all depends on the beliefs of the meta-reasoner, which depend on the average beliefs of the participants.



# Argument Metrics


briefly touched on significance in arg model
give concrete definition in terms of beliefs of meta-reasoner

terminology
signiifance is correlation
but oddly, can be significant but compliment not significant

related to prior belief. cannot be significant unless there is an element of suprise


this is dissatisfying...can't define a objectively good argument without resorting to 

what about syllogism? Is the argument that Aristotle is mortal critical? Only if my prior is that he is immortal....

relevane does not depend on prior belief in the premise

in a sense, relevance peels back a layer of subjectivity

todo: work out implications in terms of statistical correlation metrics

binary vs scalar and log metric


conditional significance
    significance of A◂-B◂-C is Pb(A|C) / Pb(A) = abc/bc*ab/b = c|ab / c|b = Pb(C|A) / Pb(C)

joint significance
    significance of  A◂-(B∧C). is that the same thing?
        P(A|B,C) / P(A)

x

significantly supporst or opposes

criticality of claimm, strenght of warrant, strenght of argment

related to valididty and soundness
related to necessity and sufficiency
related to PN and S


TODO:
    - syllogism, validity and soundness
    - Necessary and sufficient conditions examples
    - Significance is symmetrical
    - Uncertain Evidence
    - Independent Arguments (S(A, B&C) = S(A,b)*S(A,C))
        Does it follow that relevance is additive?
        Does R(A,B&C) =? R(A,B) + R(A,C)
    - Joint Significance







