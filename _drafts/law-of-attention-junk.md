



[[[

Whereas in a regular room, what you say might be heard by a small group of people, in this room, everyone stops talking except the few who have learned to win the attention game.


## Bending The Attention Curve

How to make your voice heard depends on the algorithm, but the algorithm depends on feedback from people. 

For example, an engagement-deamon algorithm will amplify the voices that other people engage with the most. A popularity-deamon will amplify the voices of people saying things that are popular. A social platform with a simple chronological feed will give voice to those who post frequently (e.g. shout the loudest). A follower-count-driven platform will amplify the voices of those who gain a following by being popular within some niche.



## The Null Algorithm

Suppose an online forum has a simple chronological feed, and anyone can post. Then the way to gain attention is to post frequently. The community will be taken over by spam bots; everyone else will stop posting, and the community will die.

Suppose Twitter only had a chronological feed. The result would be different, because the only way to gain attention on chronological Twitter is to gain followers within some niche. The retweet, retweet, and quote tweet features instantly make the dynamics of doing so quite complicated, and unfortunately this dynamic leads to amplification of controversy and toxicity in some of these niches.

]]]




How to make your voice heard depends on a combination of user feedback and the algorithm. In a real physical room, you can make a comment that is interesting, funny, or kind, but not particularly engaging. But an engagement-daemon will silence those comments: only a minority of the most engaging voices will be heard. In a real room you can express an unpopular opinion, in it will be heard. But an engagement-deamon will only allow the very-most popular opinions to be heard. Even a social platform with no algorithm -- that just shows posts chronologically -- ensures that only the voices that post most frequently (e.g. shout the loudest) will be heard.


All this is further amplified by the depersonalizing effect of electronic communication. In the real world people actual **avoid** attention for bad behavior that can result in embarrassment, shame, or damage to their reputation. But people behave surprisingly badly when they can hide behind a keyboard, shielded from the uncomfortable negative feedback of body language and facial expression. And with anonymous accounts they are shielded from any harm to their real-world reputation.






The only way the algorithms can effect our behavior is by effecting what we pay attention to.




We pay attention to content that engages our attention. This is true online and off. So it is easy to miss the fact that the algorithms actually **alter** the amount of attention content receives.

Only the most engaging...the most popular...the most frequently.

Easy to leave online communities and form new ones.

So it's not always so much that the echo chamber caused people to adapt extreme views, but that people that don't sharre those extreme views left, possibly to join a different echo chabmer. Those that remain are those that successfully play the popularity game in that community.



First...alter
second...can leae




## Explanatory Value

What sort of behaviors are rewarded with attention in social platforms?

Algorithms that promote content that people **like** will....
Fortunately, useful, entertaining content. 

childishness, etc. In a normal human context, there are social feedback mechamisms (a scowl)...in a social platform, these mechanisms are gone. 

From the early days of social platforms, the phenomenon of people behaving extremely badly.

hire moderators to deal with iguana torture
don't feed the trolls


## Theory

Many readers may doubt the usefulness of economic theories for modeling human behavior.

First: because people are not "rational agents" seeking to maximize utility, or in our case, attention. But the Law of Attention says that they will act **as if** they are, regardless of the actual reasons for their behavior. 

Second: human behavior is complex. We need practical tools to help improve social media, not ivory tower "theories".

But years ago there was a "theory" that you could fill a metal tube with explosives, light it, and ride it up to the moon. The Apollo required more than just theory, but it started by asking the same question that I ask here: if we are going to do this difficult but important thing, how, in theory, can it be done? 


Using the tools of economic theory to model social platforms gives us access to a powerful suit of tools not just for modeling social networks, but for **designing** algorithms to produce specific desired outcomes. 
