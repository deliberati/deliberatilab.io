
## APPENDIX A: Derivation of Formula 2

Formula 3 says

    (3) 𝑃ᵢ(𝐴|𝐵,𝐶 ∨ !𝐶) = 𝑃ᵢ(𝐴|𝐵)


Recall that 𝑃ᵢ counts users that voted on B, and 𝑃ₕ counts users that voted on both B and C. So:

    𝑃ₕ(𝐴|𝐵) = 𝑃ᵢ(𝐴|𝐵, 𝐶 ∨ !𝐶)

So per formula 3

    (4) 𝑃ₕ(𝐴|𝐵) = 𝑃ᵢ(𝐴|𝐵)

Now, the law of total probability tells us that:

    (5) 𝑃ₕ(𝐴) = 𝑃ₕ(𝐴|𝐵) * 𝑃ₕ(𝐵) + 𝑃ₕ(𝐴|¬𝐵) * 𝑃ₕ(¬𝐵)

Substituting 4 into 5

    (6) 𝑃ₕ(𝐴) = 𝑃ᵢ(𝐴|𝐵)*𝑃ₕ(𝐵) + 𝑃ᵢ(𝐴|¬𝐵)*𝑃ₕ(¬𝐵)

Now since

    (7) 𝑃ₕ(𝐵) = 𝑃ⱼ(𝐵|𝐵 ∨ !𝐵) = 𝑃ⱼ(𝐵)

We can substitute 7 into 6 to get Formula 2 again.

    𝑃ₕ(𝐴) = 𝑃ᵢ(𝐴|𝐵)*𝑃ⱼ(𝐵) + 𝑃ᵢ(𝐴|¬𝐵)*𝑃ⱼ(¬𝐵)





## multiple independent premises


## Recursive Formula

We can define also this recursive formula for 𝑃ₕ

    𝑃ₕ(𝑋) =
        if 𝑋 = A
            𝑃ᵢ(𝐴|𝐵)*𝑃ₕ(𝐵) + 𝑃ᵢ(𝐴|¬𝐵)*𝑃ₕ(¬𝐵)
        else if 𝑋 = 𝐵    
            𝑃ⱼ(𝐵)

How imagine a chain...

    𝑃ₕ(𝑋) =
        if 𝑋 has arguments Y
            𝑃ᵢ(𝑋|Y)*𝑃ₕ(Y) + 𝑃ᵢ(X|¬Y)*(1 - 𝑃ₕ(¬Y))
        else if 𝑋 = 𝐵    
            𝑃ⱼ(𝐵)