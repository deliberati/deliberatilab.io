---
layout: single
title:  "What Is a Social Protocol?"
toc: false 
# toc_sticky: true
sidebar:
  - title: "In This Series"
    nav: "social-protocols"
  - nav: "social-protocols-related"
    title: "Related Articles"

---

The rules that determine [what gets attention] in social platforms are often called *algorithms*. But they are more than algorithms. They are *protocols* defining complex interactions between multiple humans. Like the protocols of a legislative body or the procedures of a court, they govern discourse in online spaces, by determining what gets to be said and, more importantly, what gets attention.

When we choose to use any online communication tool, from email to online forums to Twitter, we are choosing to participate in the protocol. We feed the protocol a certain amount of our attention when we scroll through our feeds or scan over the emails in our inbox. And while we don't have to pay attention to the content presented to us in in the order it is presented to us, people tend to behave in certain ways, and as a result the protocol has a great deal of influence on what content receives attention. 

We can also choose to submit content, which may consume the attention of others, or to like, upvote, etc. which further influences what content comes to other people's attention. Because people won't waste their time on posting or liking things if they don't think anyone else will notice, they adjust their behavior based on what they believe will get the right content to the attention of the right people.

So people are behaving in response to the protocol, which is in turn behaving in response to people. The result is [dynamic system]. One of the properties dynamic systems can have is [feedback loops]. The feedback loops induced by a social protocol can create some complex and surprising emergent social dynamics. 

And yet we understand some of these loops and dynamics. We understand for example how certain protocols amplify lies and abuse. Human beings respond to misinformation and abuse by leaving comments, and some algorithms perversely respond to these comments by giving the controversial posts even more attention. Humans then learn that they need to produce more controversial content or their posts won't get attention, and the result is a toxic feedback loop.

This understanding hints at how we can design better protocols. For example if the algorithm can differentiate positive engagement from negative comments that signal disagreement or disapproval, it can give posts with a lot of negative engagement less attention, not more. 

[Reason-based ranking] takes this a step further, by considering how people respond to negative comments. Does the comment make people less likely to share or upvote the post? Or do people respond with counter arguments? Reason-based ranking can induce feedback loops that encourage people to post content that stands up to the scrutiny of an online community, instead of content people reflexively like and share, in order direct attention to constructive conversation and informed content.

When people voluntarily come together in an online community, they have the opportunity to decide collectively what protocols govern their online interactions. These rules should be fair and transparent. They should be as decentralized as possible, so no single entity or group can censor what can or cannot be said. And yet they must allow the community to moderate itself: to protect itself from spam, harassment, and illegal content, and content that otherwise violates the values agreed on by the community.


----




or irrelevant content

. But moderations processes

Moderation processes should be balanced against the risk of formation of echo chambers. Just as we understand how the dynamics of a social protocol can amplify lies and abuse, we understand how they can promote conformity, group-think, extremism, and out-group animosity. On the other hand, techniques such as [bridging-based ranking] can be used to break-down the walls of echo chambers, and [reason-based ranking] can allow people to safely and constructively challenge dominant beliefs through argument. 

Other considerations in the design of a social protocol include the extent that they act more like recommender systems, feeding individualized but isolated experience, or contribute to formation of like-minded communities directing their 

We also understand how 


The rules should be designed to bring conversations out of the gutter
epimstemic standards...

They have the opportunity to agree on a fair, open, transparent, community-driven processes for choosing what can and cannot be said, and what content gets attention.


Techniques such as reason-based ranking, [bridging-based ranking], [subjective truth-elicitation mechanisms], and others can be used to design social protocols that encourage civil, intelligent conversation and the sharing of accurate, relevant, and useful content. 

TODO: more on moderation







identifying

In this essay we identify some of the key principles in the design of social protocols.

- civility
- intelligent conversation
- information content
- transparency
- non-censorship
- bridging
- subjective honesty
- relevance
- community






 galvanize a response, and if the machines are further responding

and yet some of the emergent dynamics are fairly easy to understand. in fact, social scientists and social media professionals
have come to understand  both the pathological dynamics that can emerge, and the beneficial ones.

For example, we understand virality. We understand what goes into a viral coefficient and the conditions that make a piece of content go viral. Further, we know what kind of content is likely to go viral.

Given the probability that a person reposts a piece of content given they see, any the number of other people that will see if it is reposted, we can predict if it will *go viral*. 

If the protocol makes it sufficiently easy to re-post a piece of content, 

such that the number of people


The machines use human input in the form of posts, clicks, upvotes, etc. to determine 


## The Attention Marketplace

The rules of the protocol influences not only what content we consume, but also what content we contribute. For example, we won't waste our time on a post if we don't think anyone will see it. But we don't want to post things that other people aren't interested in either. 

Posting to any social protocol can be seen as redeeming a claim on other people's attention. Before posting, we may ask ourselves if we want to make that claim: if the people that will see our posts really want to see pictures of our kids, or our political rants or philosophical musings. Some people conscientiously avoid making these claims when in doubt. Less scrupulous people abuse their claim on other people's attention. SPAM, as well as hesitation to post, are both indications of a flaw in the protocol. Well-designed social protocols should produce healthy, collaborative dynamics, that incentives people to participate in ways that others will find useful while discouraging abuse.



We don't *have* to follow the protocol. We don't have to pay attention to the post on the top of page. We can ignore or filter DMs or emails. We can turn off notifications. We don't have to check Facebook, or answer our phone. But because people do *tend* to behave in a certain ways, the rules for what content is presented to have significant impact on the resulting social dynamics. 


But we also won't post if we think it gets the wrong attention.


### Classifying Social Protocols


### Individualized vs Community Dynamics


For example, can protocol can create *personalized* dynamics, or *community* dynamics. Personalized recommendation algorithms shunt us into individualized experience that are ironically, impersonal. We experience the content alone, instead of as part of a group. We know other people may be consuming the same content, but we don't know exactly who. There is no common-knowledge generating event. There is no community to discuss the content with later -- only random strangers in the comments section. 

Many online forums, on the other hand, create **community** dynamics, by directing the collective attention of some group to the same content. We know other like-minded from this particular community are experiencing this content at the same time. We know many people are commenting and reading the comments, and we can participate in often vibrant conversations that result. We may even start to recognize and form relationships with members of the community.

Follower- and friend-based social networks create a hybrid between community and individual dynamics, tending towards one or the other dynamic depending on how much the platform relies on recommendation algorithms to boost engagement at the expense of community,




Attention of whom, and who/what decides how it is allocated
Who decides
Who decides, 

News Aggregator:
	- News Aggregators: Whole Community, Whole Community Decides
	- Mass Media (TV, Newspaper, etc): Masses, Platform Decides
	- Traditional Advertising: Masses, Advertiser Decides
	- Twitter, Follower systems, Newsletters, Feeds: Individual/Group, The people I follow
	- Group Chat: Specific Groups, Individuals decide
	- Online Advertising: Individual, Advertiser Decides
	- Recommender Systems: Individual, platform decides
	- Email, WhatsApp, etc.: Individual, other people decide
	- Search Engine: Individual, individual cooperatively decide with search engine


From: Felix
Btw Jonathan, these are my notes about attention that we talked about:

## News Aggregator
- whole community decides together what the whole community should draw attention to

## Advertising
- Amplify attention to something specific

## Search engine
- I give a rough idea what I want to draw attention to, give me content that fits my request

## Twitter, Follower systems, Newsletter, feeds
- I blindly trust someone to draw attention to things they decide

## Tik Tok, recommender systems
- Draw may attention to something that optimizes for a metric
  e.g. Learn drawing attention to something that rewards me or that I will like

## Whatsapp, Email
- Drawing Attention of specific individuals

## Group chats
- Drawing Attention of specific groups of individuals

## TV, radio, newspaper
- Drawing mass attention






We may have a sense that we are viewing popular content but no sense of 

shunt us into individualized

impersonal experience...



...create dynamics






The goal of Social Protocols is to design these rules, based on this understanding that 

 attention is everything. 
realization that all that social platforms can do is direct attention, but by defining

All a form of collaborative filtering.
But much mroe intelligent

--




We believe that by allocating 

Our thesis is that by carefully designing these social protocols to allocate attention to the right content, we can produce more constructive public discourse.



Social Protocols is working



A social protocol can be designed to achieve desired goal through the way that it **allocates attention**. 



For example, 


The rules of social protocols are ultimately enforced by **allocating of attention**. 






the pricing mechanism for online marketplace of ideas.

Like the rules of a courtroom, they define the rules of the game





but we prefer to call them *social protocols*. 


When designing social protocols, the desired outcome should be creation of common knowledge, narratives, values, and culture. Towards a collective, cooperative pursuit of truth. Towards uniting and coordinating through constructive, civilized public discourse. Towards the realization of our collective will, and manifestation of our collective intelligence.

So how can a social protocol promote these things? 


 But as these systems involve a complex interaction between huamns and machines we prefer to cal them protocols.





This is why we are working 

 the rules that govern discourse in online public spaces.




The (social protocols) project is designing **protocols** for public discourse. 


In real-world conversations, there are complex social rules about who gets to speak, and what can be said. If people violate these rules, others in the group will respond with scowls, cleared throats, interruptions, by leaving the room or breaking the relationship. These rules help make conversations more relevant, civil, and constructive. But the nature of discourse in online public disc

But the rules that should govern discourse in online public spaces should be different from the rules that govern real-world conversation, because the communication medium is different, and poses unique challenges.






 The nature of spoken communication means that one person in a group speaks while the rest listen. Each person is expected to consider whether what they are saying is relevant and interesting to the group, and not to take too much time. If people violate these rules

interrrupt, clear throat. 
if what peopple say is offensive we scowl, we (call),  we may leave the room.



Who gets to speak. Informal rules.



But some of the basic principles are the same.





- the algorithm
- stress rules
- like designing a government.
- the procedure. 


But designing rules that promote truth, civility, and so on, does not require 

True. Civil. Useful

The problem is not designing what content deserves attention.


- Civilized Discourse.



How the rules of social networks and online forums -- the public squares in which public discourse takes place -- should be designed. 


## Example

This is easiest to explain with an example. 

Suppose some easily-debunkable rumor has been circulating. It spreads because people tend to be careless about repeating things they've heard; one rarely takes the time to verify that a rumor is true before upvoting or sharing it, especially if it comes from a credible source. In other words, the rumor spreads because people lack information.

But suppose you happen to possess information others don't. You know that it's just a rumor. You can flag this post as being false or misleading, and leave a comment explaining **why**.

Now in many social platforms, your comment will be interpreted as an engagement signal and only lead to more exposure for the rumor, and not necessarily for your comment. Such engagement-based algorithms create perverse incentives and toxic outcomes. A platform willing to optimize for anything other than short-term engagement should instead drive attention to your comment: a subset of people who have already seen the rumor, especially those who have reposted or upvoted it, should be shown your comment and asked if they would like to **retract** their endorsement. Since many of those people acted based on carelessness, not malice, if your comment truly is informative and convincing, some of them may retract.

## Falsehood Flies

Thomas Swift said "falsehood flies, and the truth comes limping along after it". But with this algorithm, falsehood may still fly, but now the truth can fly just as fast, spreading along the very same social patheways by which the lie spread in the first place.

What's more, the system can identify which comments, if any, lead to the greatest change in behavior: either a retraction, or a reduction in the probability of sharing or upvoting in the first place. It can then channel attention to these comments. 

## Driving Informed Conversations

A comment will not change behavior unless it tells them something people didn't already know, or hadn't already considered. So this algorithm can be seen as simply concentrating attention on the comments with the highest information content.

Now of course, your informative comment may itself be a lie. Perhaps the rumor was true after all. That is why it is so important to drive **conversations**. People get to have their say, you get to say why you disagree, and *then they get to respond to you*. If somebody responds with a counter-argument rebutting your argument and explaining why the rumor was true after all, then that new information will spread along the same pathways by which your comment spread. Everyone who retracted their endorsement of the rumor based on your comment will be asked if they want to un-retract based on this new information. 

And so the conversation thread will continue as long as comments are providing information that influences belief as expressed by voting behavior. At some point, all the arguments will have been made, and all the information there is to share will have been shared, and further comments will only rehash previous points without changing minds. At that point there is no need for any more attention to be spent.

## Summary

This is how our attention should be directed. 


# Discard



At the end of this conversation thread there will remain a small subset of **highly informed users**: those who have seed and responded to all the critical information that has been shared and arguments that have been made. And it is the deliberative judgment of these informed users -- the proportion who still endorse the rumor -- and not raw superficial popularity that determines whether or not the rumor deserves the attention of the community.

## Manipulation

Such a system is of course highly subject to manipulation. 

## The Details






. Promoting deep conversations requires nothing more than concentrating attention on depth. Promoting quality content requires concentrating the community's attention on judging quality.

This requires more than a simple button that allows users to say "this is deep! This is quality!" This would ultimately be just another upvote button, which is just a crude way of measuring popularity. Popularity metrics are superficial. Upvotes and downvotes do not demand reflection, discussion, credibility, or trust. There is a big difference between a post that gets a lot of knee-jerk updates today, and one that "ages well": that people will still stand by after the truth has come out and the arguments have played out. 

What we want to know is not how likely people are to upvote something after seeing it for a brief moment on their screen, but rather how willing they are to endorse it after reading the content, seeing the comments, and participating in the discussion. This requires concentrating the community's attention on deep conversation threads of informative comments, and using these conversations to determine which posts are most informed, in the judgment of the community.
