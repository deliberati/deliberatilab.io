### Proof of Conditional Independence Assumption


## Relevance and Strength

The relevance of arguments is an important concept. There is no widely agreed-on definition of relevance, but generally an argument is said to be relevant if it effectively supports or opposes another argument.

For our purposes we will *define* a relevance argument as one where acceptance of the premise is correlated with acceptance of the conclusion. Specifically.

**Relevance of Premise Arguments**

    if 𝑃ᵢ(α|β) ≠ 𝑃ᵢ(α), then β is relevant to α 

**Relevance of Warrant Arguments**

    if 𝑃ᵢ(α|β,γ) ≠ 𝑃ᵢ(α|β), then γ is relevant to α->β

If β is relevant to α, it either supports or opposes α:

    if 𝑃ᵢ(α|β) > 𝑃ᵢ(α), β supports α
    if 𝑃ᵢ(α|β) < 𝑃ᵢ(α), β opposes α

TODO strength must be post-argument: For an argument to be strong, it must be relevant and its premise must be accepted. The strength of argument `α->β` can be measured as the degree to which *voting on β* supports or opposes for α. That is, strength is:

    Strength(α,β) = 𝑃ᵢ(α|β ∨ !β)/𝑃ᵢ(α)

Note that it can be the case that β is not relevant to α, but !β is relevant (e.g. the defendant wore pants to the interview). 

It is also possible for an argument that is not relevant to **backfire**, because !β is relevant and Strength(α,β ∨ !β) is less than 1.

This creates an interesting possibility of defining warrant in terms of relevance:

    the warrant of the argument α->β is the claim that β is relevant to α

## Fair Deliberation Justifies Conditional Independence

Now we can express the fair deliberation assumption more precisely. 

**Definition of Fair Deliberation**

> Fair deliberation says all relevant arguments have been made

TODO: all independently relevant premise arguments have been made

Focusing our attention to warrant arguments, we can express this more formally:

> let 𝐸(α->β) be the list of premises in the argument graph that support or oppose the warrant of α->β. There exists no premise γ relevant to α->β that is not in 𝐸(α->β).

Or to be precise:

    (4) 𝑃ᵢ(α|β,γ) = 𝑃ᵢ(α|β) for all γ ∉ 𝐸(α->β)


In order to justify our use of Jeffrey conditioning in our example argument, we needed to make the assumption that *acceptance of 𝐴 and votes on 𝐺 are conditionally independent, given acceptance of B*, which we formally expressed using formula 3, which we repeat here.

    (3) 𝑃ᵢ(𝐴|𝐵, 𝐺) = 𝑃ᵢ(𝐴|𝐵,¬𝐺) = 𝑃ᵢ(𝐴|𝐵)

Formula 3 clearly follows form formula (4).



