

# Coordinating Honesty Essay

### Nash Equilibrium and Coordination Problems

The situation...is called a Nash Equilibrium (famous John Nash from the movie). It is an equilibrium because there is a sort of balance or stability in the situation, because no one person will deviate...

In a Keynesian Beauty Contest, there are actually **multiple** equilibria: for example, always the first in alphabetical order, or always blonds on Mondays..

So it's a coordination problem....

Coordination problems require a schelling point...clue.
Sufficiently objective, uncontroversial facts, the truth is an obvious shelling point.





But in a Keynesian Beauty Contest, answering X can only be an equilibrium if everybody knows what X is, because X is objective. So honesty is an equilibrium only if everybody knows what everybody else's honestly believes: or more precisely, if the "honest answer" is [common knowledge]. 

But unlike the Keynesian Beauty Contest, in Peer Prediction

 there exists an equilibrium at answering honestly, even if the "honest answer" is not common knowledge! 




- people vote based on their expectation of others
- but vote on truth
- the reason is that truthtelling is a Schelling Point
- in reality, this outcome is not guaranteed.

this is a type of equilibrium

coordination problems and  multiple equilibrium
such as always lying, answering the same way

### The Problrem of


### Equilibrium of Universal Honesty

[TODO: links. I am assuming the reader understands basics of game theory, including Nash Equilibria, Schelling points (focal points), Keynes Beauty Contests, Common Knowledge, coordination problems, and convergence of expectations (conventions).]

[ NOTE: No, coordination problem, multiple equilibrium, and convergence of expectations are advanced concepts. Not introduced in the Consensus Protocols essay] 



The Peer Prediction paper proved that, given a proper scoring rule and common priors, it's possible to design a mechanism where universal honesty is a Nash Equilibrium. 

Peer Prediction is similar to a Keynesian Beauty Contest, where X is the best response if everyone else answers X, and thus universally answering X, whatever X is, is a Nash equilibrium. 

But in a Keynesian Beauty Contest, answering X can only be an equilibrium if everybody knows what X is. So honesty is an equilibrium only if everybody knows what everybody else's honestly believes: or more precisely, if the "honest answer" is [common knowledge]. 

But unlike the Keynesian Beauty Contest, in Peer Prediction there exists an equilibrium around answering honestly, even if the "honest answer" is not common knowledge! Coordination no longer requires common knowledge of other people's beliefs!!! 

{TODO: your own belief is the strongest SChelling point}


What's more, even if you *do* have knowledge of other people's beliefs, and you disagree with the majority, there is *still* an equilibrium around honesty! These mechanisms actually, and counter-intuitively, reward people for answering with the minority -- but, on average, only if that answer is **informative** (honest). 

Obviously, in the case of beliefs with high reputational consequence, these mechanisms only work if answers are anonymous, and participants believe this (for this reason, I want to research use of zero-knowledge proofs so that, even if this mechanism is not run on the blockchain, people's beliefs remain anonymous even if there is a data breach). And clearly there are potentially other exogenous incentives, including political propaganda, that need to be addressed. I will discuss these more in the section on Multiple Equilibria.

TODO: discuss example of 

In [another essay] I will discuss how these mechanisms are key to allowing people to **coordinate on reason and common sense**. This is one of the most important discoveries I have made during my years of researching the problem of designing better social algorithms. This is tremendous, it explains so much toxicity in social media and points to a solution.


### Subjective vs Objective Belief

TODO: consistent use of term honest-feedback induction and truth-elicitation mechanism

Truth-elicitation mechanisms also provide the key to dealing with **subjective** data. The successful subjective consensus protocols on the blockchain all deal with **minimally** subjective data. Objective facts such as price data feeds or the score of a football game are still subjective in the sense that you cannot algorithmically verify they are true. At some point, some human beings took the step to connect real-world data to the blockchain and there is no way to mathematically verify what they did. But these simple, publicly verifiable facts are *almost* totally objective in that there is little room for difference of opinion. There is almost always 100% agreement, otherwise there would be forks all the time. And this can only happen if people almost always know exactly how everyone else is going to answer. And this only works for *almost* totally objective data: unambiguous, uncontroversial, commonly known or easily verifiable  facts.

Peer prediction does not require 100% agreement, or even majority agreement. It actually, and counter-intuitively, rewards people for answering with the minority -- but, on average, only if that answer is **informative** (honest). 

This is hard to get your head around. It helps to look at this mechanism through the lense of information theory, which I'll do in more depth [in another post]. You can show for example that creating a bunch of sockpuppet accounts that all vote the same way as you can be profitable so long as those extra votes are informative. Once you over-do it, and cause the center to over-estimates the probability that other users will vote the same way as you, the information value of the sockpuppet votes becomes **negative**. Accounts that vote randomly or that always upvote also have no information value. This limits the ability to gain reputation through inauthentic activity. 

TODO: users need to be honest to get into the trusted core, and then they lose reputation faster than they gain it.


### Multiple Equilibria

As mentioned above, these mechanisms can have multiple equilibria, including always telling the opposite of the truth (honesty when every day is opposite day), or always answering "yes", etc. It helps me to remember that a Nash equilibria is a **possible** state of equilibrium. That is, if the participants happen to be in that particular state, they are in equilibrium: no single agent can profit from deviating. But they may not ever coordinate successfully and arrive in this desirable equilibrium state.


But multiple equilibria don't worry me. There are now many examples of consensus protocols based on the Keynesian beauty contest in which people have successfully arrived at an equilibrium of honesty. For example, any time data from the "real world" makes it onto a decentralized blockchain -- whether it is real-world price data for DeFi, events for prediction markets, or recommendations in token-curated registries -- it's the result of a "subjective" consensus protocol working on the principle of a Keynesian Beauty Contest, using some variation of the mechanism Vitalik Buterin first described in his article on [SchellingCoin](https://blog.ethereum.org/2014/03/28/schellingcoin-a-minimal-trust-universal-data-feed/) from 2014. In all such successful projects, people end up coordinating on a honest equilibrium. But this is no surprise: projects that fail to do so simply won't be successful and will cease to exist. 


And projects don't have to simply fail in these cases: they can [hard fork](https://en.wikipedia.org/wiki/Fork_(blockchain)#Hard_fork). The fork that is useless because the data feed is full of bad data will be abandoned. The mere possibility of a fork makes it so that coordinated attacks are not worth attempting. The more tokens you earn by injecting inaccurate data into the blockchain, the less value those tokens will have when the blockchain inevitably forks. This can be described as a sort of "social consensus" mechanism that takes when the underlying blockchain consensus mechanism fails.

> On the one hand, you have a fork where the larger share of the internal currency is controlled by truth-tellers. On the other hand, you have a fork where the larger share is controlled by liars. Well, guess which of the two currencies has a higher price on the market…

> 
> -- Vitalik Buterin, [The Subjectivity / Exploitability Tradeoff](https://blog.ethereum.org/2015/02/14/subjectivity-exploitability-tradeoff/), 2015

<img src="truth-lies-chart.png" alt="truth and lies" height="200px" align="right"/>


## Trust and Reputation

### The Trusted Core

It's also possible for the creators of a system to nudge the system into the desired equilibrium. In an online forum or social network you can  have a trusted core of users, and initially people's payments are determined by how well their answers predict the votes of the trusted core. If the trusted core is voting honestly, then other users best response is to vote honestly. At this point, an equilibrium will have been established. There is at this point, as Thomas Schelling put it, an "convergence of expectations". Moving to a different equilibrium will be quite difficult.

The forking mechanism is so valuable that it might be a good idea to have a forking option for online forums. Theoretically, over time, control of the accounts in the trusted core could concentrate into the hands of a cabal, who can start upvoting spam content. But when this happens, the legitimate users of the forum can create a fork where the users who up-voted crappy content are kicked out of the trusted core. People can decide to which fork of the forum to use, but as long as it is clear that an attack was taken place, most legitimate users will choose the legitimate forum.


### Reputation as Payments

For these mechanisms to work, the payments provided to users must actually motivate people even in the presence of ulterior motives to lie. I have often heard doubt expressed about "gamification" of social platforms. It is reasonable to question whether virtual rewards actually motivate people. 

But I think concern about incentives becomes moot when payments are **reputation points**, and reputation points are what actually give you the ability to generate attention. 

All social platforms can be thought of as games where the payment is ultimately **attention**. Those who learn how to play the game stay on the platform, those who don't find themselves shouting into the void, and so they stop shouting. People who don't derive value from the attention they generate through the platform don't play; those who remain derive pleasure, social status, influence, or money from the attention they generate. 

Attention is, almost by definition, a sufficient incentive on social platforms. If the platform rewards a certain type of behavior with attention, then that's how people will behave.



For example, users may need some minimum reputation points to be able to post. And upvotes votes may be weighted by reputation, so greater reputation gives the power to amplify posts and influence what other people pay attention to.

We don't need to actually try to *motivate* users with reputation -- or the ability to command attention -- because users who act as if they are motivated by reputation will get the attention -- whether that actually motivates the them or not.

This actually happens on virtually all social platforms. Users whose posts get lots of likes, upvotes, an followers dominate the platform. 

Whatever ultimately motivates people, it happens through the medium of attention. Attention can be converted into status and influence, and often large amounts of money. Users who do not get attention, finding themselves shouting into the void, stop posting.


