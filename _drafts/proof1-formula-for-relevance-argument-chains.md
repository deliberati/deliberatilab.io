# Another working proof for argument threads

Equation \eqref{eq:1}, repeated below, expresses 𝑃ᵢ(𝐴=1) using the law of total probability.

$$
    \begin{aligned}

        P_i(𝐴=1)     &=  P_i(𝐴=1|𝐵>=0) \newline\\    

                  &= \sum_{b=0}^{1} P_i(𝐵=b)P_i(𝐴=1|𝐵=b)

    \end{aligned}
$$

Now that we have redefined 𝑃ᵢ to include condition that C>=0, we can further break down the term 𝑃ᵢ(𝐴=1\|𝐵=b) in this equation, again using the law of total probability, and a little probability calculus:


$$
    \begin{aligned}
        P_i(𝐴=1|B=b)     &= P_i(𝐴=1|B=b,C>=0)                            && \text{definition of P_i}\newline\\
                         &= \sum_{c=0}^{1} P_i(𝐴=1,C=c|B=b)              && \text{law of total probability}\newline\\
                         &= \sum_{c=0}^{1} P_i(C=c|B=b)P_i(𝐴=1|𝐵=b,C=c)  && \text{probability calculus}
    \end{aligned}
$$

Plugging this in

$$
    \begin{aligned}

        P_i(𝐴=1)     &= \sum_{b} P_i(B=b) P_i(A=1|B=b)\newline\\

                     &= \sum_{b} P_i(B=b)  \sum_{c=0}^{1} P_i(C=c|B=b)P_i(𝐴=1|𝐵=b,C=c)
    \end{aligned}
$$

$$
    \begin{aligned}

        P_h(𝐴=1)     &= sum_{b} Pi(B=b) Pi(A=1|B=b)\newline\\

                     &= sum_{b} Ph(B=b)  \sum_{c=0}^{1} P_h(C=c)P_i(𝐴=1|𝐵=b,C=c)
    \end{aligned}
$$




# Working: proof of formula for relevant argument chains

   Pi(A) 
    = P(A)*W(A,B&C) 
 
    = Pi(A) *
        Pi(B) * S(A,B) * (
            Pi(C)*X(A+>B,C)
            Pi(!C)*X(A+>B,!C)
        )
        * P(!B) * (
            ...
    = P(A) *
        P(B) * S(A,B) * (
            P(C)*S(A+>B,C)/S(A,B)
            P(!C)*S(A+>B,!C)/S(A,B)
        )
        * P(!B) * (
            ...


S(A,B&C)*S(B,C) = S(A,B)*X

X = S(A,B&C)*S(B,C)/S(A,B)


To simplify notation, let's defined the **informed probability** distribution `Pi` as the probability that a users accepts something *given* they have voted on all the supporting and opposing arguments in the argument graph.

There are 3 arguments (`A`, `B`, and `C`) in the above scenario, so for example:

    Pi(A) = P(A|vA,vB,vC)


If the *relevance* of C can be defined as

    P(A|B&C) = P(A|B) * X

  
  P(A&B|C) = P(C)*S(A&B,C)


This can be rewritten as:

    Pi(A,B,C) 

        = Pi(A&B) * Pi(C|A,B)

        = Pi(A|B)*Pi(B) * Pi(C|A,B)

        = Pi(A)*S(A,B)*Pi(B) * Pi(C)*S(C, A&B)


Now since prove:



    Pi(A,B,C) 
        = P(A) *
            Pi(B) * S(A,B) *
                Pi(C) * S(A&B,C)


Now also

        P(A|B&C) = P(A,B,C)/Pi(B&C) = P(A,B,C)/Pi(B)*S(B,C)/Pi(C)


equating

    P(A|B&C) 
        = Pi(A)*S(A,B)*Pi(B) * Pi(C)*S(C, A&B) / (Pi(B)*S(B,C)*Pi(C))
        = Pi(A) * S(A,B) * S(C,A&B) / S(B,C)



prove

    S(A&B,C) = S(A,B&C)*S(B,C)/S(A,B)


because

    S(A,B&C)*S(B,C)/S(A,B)
        = P(A,B,C)/P(A)/P(B,C)*P(B,C)/P(B)/P(C)/P(A,B)*P(A)*P(B)
        = P(A,B,C)/P(C)/P(A,B)
        = S(A&B,C)



Law of total prob


    Pi(A) = 
        P(A|B,C)*Pi(B,C)
        + P(A|B,!C)*Pi(B,!C)
        + P(A|!B,C)*Pi(!B,C)
        + P(A|!B,!C)*Pi(!B,!C)

    =
        P(A)*S(A,B&C)*Pi(B)*S(B,C)*Pi(C)
        + P(A)*S(A,B&!C)*Pi(B)*S(B,!C)*Pi(!C)
        + P(A)*S(A,!B&C)*Pi(!B)*S(!B,C)*Pi(!C)
        + P(A)*S(A,!B&!C)*Pi(!B)*S(!B,!C)*Pi(!C)
    = P(A) *
        P(B) *
            S(A,B&C) * S(B,C) * Pi(C) 
            + S(A,B&!C) * S(B,!C) * Pi(!C)
        P(!B) *
            S(A,!B&C) * S(!B,C) * Pi(C)
            + S(A,!B&!C) * S(!B,!C) * Pi(!C) 

Now since prove:

    S(A&B,C) = S(A,B&C)*S(B,C)/S(A,B)

because

    S(A,B&C)*S(B,C)/S(A,B)
        = P(A,B,C)/P(A)/P(B,C)*P(B,C)/P(B)/P(C)/P(A,B)*P(A)*P(B)
        = P(A,B,C)/P(C)/P(A,B)
        = S(A&B,C)

Then continuing
    = P(A) *
        P(B) * S(A,B) * 
            Pi(C)*S(A&B,C)
            + Pi(!C)*S(A&B,!C)
        ...


    thats it.

Full Formula! 

    Pi(A)
        = P(A) *
            P(B) * S(A,B) * 
                Pi(C)*S(A&B,C)
                + Pi(!C)*S(A&B,!C)
            P(!B) * S(A,!B) * 
                Pi(C)*S(A&!B,C)
                + Pi(!C)*S(A&!B,!C)



P(A|B) = P(A) * P(B|A) / P(B)

P(A|B&C) = P(A) * P(B|A) / P(B) * P(B) * P(A&B|C) / P(A&B) * P(C)

P(A&B|C)/P(A&B)

P(A&B&C) = P(A&B|C)*P(C) = P(A&B)*P(C|A&B)/P(C)*P(C) 
    = P(A)*P(B|A)/P(B)*P(B)*P(C|A&B)/P(C)*P(C) 
    = P(A)*P(B|A)*P(C|A&B) 

---

S(A&B,C) 
    = Pi(A,B,C) / Pi(A,B) / Pi(C)
    = t(A,B,C) / v(A,B,C) / t(A,B) * v(A,B) / t(c) * v(c)




---

P(A,B,C) = P(A|B,C)*P(B|C)*P(C) = P(A,B,C)/P(B,C)*P(B,C)/P(C)*P(C)





